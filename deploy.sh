#!/bin/bash

cp docker-compose.yml.prod docker-compose.yml && \
docker-compose build && \
docker-compose run --rm web yarn && \
docker-compose run --rm web yarn build && \
docker-compose up -d --force-recreate
