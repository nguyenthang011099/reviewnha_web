module.exports = {
  apps: [
    {
      name: 'home',
      script: 'server.js',
      instances: 2, // number process of application
      autorestart: true,
      cwd: __dirname,
      exec_mode: 'cluster',
      env: {
        NODE_ENV: 'production',
      },
      env_production: {
        NODE_ENV: 'production',
      },
    },
  ],
};
