const express = require('express');
const next = require('next');
const dotenv = require('dotenv');
dotenv.config();
const dev = process.env.NODE_ENV !== 'production';
var port = dev ? parseInt(process.env.PORT, 10) || process.env.PORT : 3000;
const app = next({ dev });
const handle = app.getRequestHandler();
// lấy cookie ở phía server(parser = > {})
const cookieParser = require('cookie-parser');
const axios = require('axios');

const setCookieUserId = function (req, res, next) {
  if (req.query.user_id) {
    res.cookie('userId', req.query.user_id, { maxAge: 900000 });
  }
  next();
};

app.prepare().then(() => {
  const server = express();
  server.use(cookieParser());
  server.use(setCookieUserId);

  server.get('/', async (req, res) => {
    const response = await axios.get(`${process.env.NEXT_PUBLIC_API_DOMAIN}sell-listings/create/part-one`);
    if (response?.data?.success && response.data?.data?.type_transactions?.length) {
      res.redirect(`/sell-listings?transactionType=${response.data?.data?.type_transactions[0].key}`);
    }
    res.redirect('/blogs');
  });

  server.get('/sell-listings', (req, res) => {
    return app.render(req, res, '/sell-listings-page', {
      token: req.query.token,
      status: req.query.status,
      transactionType: req.query.transactionType,
      provinceCode: req.query.provinceCode,
      districtCode: req.query.districtCode,
      wardCode: req.query.wardCode,
      projectId: req.query.projectId,
    });
  });

  server.get('/sell-listings/:postId', (req, res) => {
    return app.render(req, res, '/sell-detail-page', {
      token: req.query.token,
      status: req.query.status,
      postId: req.params.postId,
    });
  });

  server.get('/login', (req, res) => {
    return app.render(req, res, '/login-page');
  });

  server.get('/register', (req, res) => {
    return app.render(req, res, '/register-page');
  });

  server.get('/blogs', (req, res) => {
    return app.render(req, res, '/blogs-page', {
      token: req.query.token,
      status: req.query.status,
    });
  });

  server.get('/blogs/feature', (req, res) => {
    return app.render(req, res, '/custom-blogs-page', {
      token: req.query.token,
      status: req.query.status,
    });
  });

  server.get('/blogs/tags/:tagSlug', (req, res) => {
    return app.render(req, res, '/custom-blogs-page', {
      token: req.query.token,
      status: req.query.status,
      tagSlug: req.params.tagSlug,
    });
  });

  server.get('/blogs/:categorySlug', (req, res) => {
    return app.render(req, res, '/blogs-page', {
      token: req.query.token,
      status: req.query.status,
      categorySlug: req.params.categorySlug,
    });
  });

  server.get('/blogs/:categorySlug/:slug', (req, res) => {
    return app.render(req, res, '/blog-detail-page', {
      token: req.query.token,
      status: req.query.status,
      slug: req.params.slug,
      categorySlug: req.params.categorySlug,
    });
  });

  server.get('/seller/:sellerId/sell-listings', (req, res) => {
    return app.render(req, res, '/seller-sell-listings-page', {
      token: req.query.token,
      status: req.query.status,
      sellerId: req.params.sellerId,
    });
  });

  server.get('/profile/sell-listings', (req, res) => {
    return app.render(req, res, '/profile-sell-listings-page', {
      token: req.query.token,
      status: req.query.status,
    });
  });

  server.get('/profile/appointments', (req, res) => {
    return app.render(req, res, '/profile-schedules-page', {
      token: req.query.token,
      status: req.query.status,
    });
  });

  server.get('/profile/customers', (req, res) => {
    return app.render(req, res, '/profile-customers-page', {
      token: req.query.token,
      status: req.query.status,
    });
  });

  server.get('/account', (req, res) => {
    return app.render(req, res, '/profile-account-page', {
      token: req.query.token,
      status: req.query.status,
    });
  });

  server.get('/robots.txt', (req, res) => {
    return app.render(req, res, '/robots.txt');
  });

  server.get('/sitemap.xml', (req, res) => {
    return app.render(req, res, '/sitemap.xml');
  });

  server.get('*', (req, res) => {
    return handle(req, res);
  });

  server.listen(port, (err) => {
    if (err) throw err;
    console.log(`> Ready on http://localhost:${port}`);
  });
});
