FROM node:14.16.0-alpine3.10
WORKDIR /app
RUN apk add --no-cache bash vim
RUN npm install http-server -g
RUN npm install serve -g
COPY package.json yarn.lock ./

