export type PostBodySellListing = {
  type_transaction: string;
  type_product: string;
  province_code: string;
  district_code: string;
  ward_code: string;
  type_project?: string;
  address: string;
  type: string;
  area: number;
  direction: string;
  bedroom?: number;
  bathroom?: number;
  furniture?: string;
  juridical: string;
  title: string;
  description: string;
  images: number[];
  video_url: string;
  contact_name: string;
  contact_phone: string;
  contact_email: string;
  service_code: string;
  price: number | string;
  map_payload: { longitude: number; latitude: number };
  project_experiences?: any[];
  project_id?: string;
  project_name?: string;
};

export type SellListing = {
  id?: number;
  title?: string;
  thumbnail?: string;
  address?: string;
  ward?: {
    code: string;
    name: string;
    nameWithType: string;
    path: string;
    pathWithType: string;
  };
  district?: {
    code: string;
    name: string;
    nameWithType: string;
  };
  province?: {
    code: string;
    name: string;
  };
  price?: number;
  typeTransaction?: {
    content: string;
    id: string;
  };
  typeProduct?: {
    content: string;
    id: string;
  };
  juridical?: {
    id: string;
    content: string;
  };
  furniture?: {
    content: string;
    id: string;
  };
  area?: number;
  serviceCode?: string;
  seller?: {
    id?: number;
    name: string;
    phone: string;
    email: string;
    avatar: string;
    type: string;
    description: string | null;
  };
  direction?: {
    content: string;
    id: string;
  };
  images?: {
    url: string;
    id: number;
  }[];
  description?: string;
  sellStatus?: {
    id: string;
    content: string;
  };
  startTimeActive?: string;
  endTimeActive?: string;
  publishedDate?: string;
  bedroom?: number;
  bathroom?: number;
  projectId?: number | string;
  status?: 'draft' | 'published' | 'cancel';
  type?: {
    id: string;
    content: string;
  };
  mapPayload?: { longitude: number; latitude: number };
  videoUrl?: string;
  projectExperiences?: Experiences[];
  contactName?: string;
  contactPhone?: string;
  contactEmail?: string;
  projectName?: string;
};

export type SellListingFilter = {
  type?: 'all' | 'district' | 'highlight' | 'filter';
  limit?: number;
  page?: number;
  userId?: number;
  projectId?: number;
  typeTransaction?: string;
  query?: string;
  provinceCode?: string;
  districtCode?: string;
  wardCode?: string;
  typeProduct?: string;
  price?: {
    min: string;
    max: string;
  };
  area?: {
    min: number;
    max: number;
  };
  direction?: number | string;
  bedroom?: number;
  bathroom?: number;
};

export type Project = {
  id?: number | string;
  name?: string;
  url?: string;
  image?: string;
  address?: string;
  pointRate?: number;
  totalRate?: number;
  countExperience?: number;
};

export type Feedback = {
  id?: number | string;
  content?: string;
  createdAt?: string;
  point?: number;
  cusName?: string;
  cusAvatar?: string;
  urlCus?: string;
  media?: string[];
};

export type Experiences = {
  id?: string;
  name?: string;
  url?: string;
  image?: string;
  video?: string;
  publicDate?: string;
  totalView?: string;
  cusName?: string;
  urlCus?: string;
  cusAvatar?: string;
};

export type AreaAround = {
  address: string;
  distance: string;
  id: string;
  lat: string;
  lon: string;
  name: string;
  type: string;
  typeid: string;
};
