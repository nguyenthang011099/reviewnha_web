export type Province = {
  code: string;
  name: string;
  nameWithType: string;
};

export type District = {
  code: string;
  name: string;
  nameWithType: string;
  path: string;
  pathWithType: string;
};

export type Ward = {
  code: string;
  name: string;
  nameWithType: string;
  path: string;
  pathWithType: string;
};
