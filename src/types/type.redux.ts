export type Action = {
  type: string;
  payload?: {
    query: string;
    method:
      | 'POST'
      | 'PUT'
      | 'GET'
      | 'get'
      | 'delete'
      | 'DELETE'
      | 'head'
      | 'HEAD'
      | 'options'
      | 'OPTIONS'
      | 'post'
      | 'put'
      | 'patch'
      | 'PATCH'
      | 'purge'
      | 'PURGE'
      | 'link'
      | 'LINK'
      | 'unlink'
      | 'UNLINK'
      | undefined;
    body?: any;
  };
  data?: any;
  old_action?: any;
};

// eslint-disable-next-line no-unused-vars
export type DispatchType = (args: any) => any;
