import { IncomingMessage, ServerResponse } from 'http';
import { ParsedUrlQuery } from 'querystring';
import { NextPageContext } from 'next';
import { Request } from 'express';

export interface ServerSidePageContext {
  req?: IncomingMessage | any;
  res: ServerResponse;
  params?: ParsedUrlQuery;
  query: ParsedUrlQuery;
}

export interface PageContext extends NextPageContext {
  req: Request;
}
