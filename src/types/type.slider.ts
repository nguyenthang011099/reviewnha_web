export type SliderMethod = {
  prevSlideIndex?: () => void;
  nextSlideIndex?: () => void;
};
