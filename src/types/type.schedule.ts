export type CreateScheduleBody = {
  name?: string;
  phone?: string;
  email?: string;
  time?: Date | string;
  type?: string | number;
  sellListingId?: number;
  content?: string;
};

export type Schedule = {
  id?: number;
  name?: string;
  phone?: string;
  email?: string;
  time?: Date;
  type?: string | number;
  status?: string;
  sellListing?: {
    id?: number;
    title?: string;
  };
  isCustomer?: boolean;
  content?: boolean;
};
