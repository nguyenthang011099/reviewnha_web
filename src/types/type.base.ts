export type OptionInputRadio = {
  value: string;
  title: string;
  hidden?: boolean;
  data?: any;
};

export type OptionRangeDouble = {
  min: number;
  max: number;
};
