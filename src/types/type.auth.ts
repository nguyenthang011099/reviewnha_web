export type RegisterParams = {
  name: string;
  email: string;
  phone: string;
  type?: 'seller' | 'normal';
  password: string;
  password_confirmation: string;
};

export type LoginParams = {
  username: string;
  password: string;
};

export type CallbackURLType = {
  url: string;
  pathname: string;
  asPath: string;
  query: any;
};

export type UserInfo = {
  id?: number;
  name?: string;
  avatar?: string | number;
  type?: string;
  email?: string;
  phone?: string;
  sellListingsCount?: number;
  description?: string;
  sex?: string;
  dateOfBirth?: string;
  address?: string;
  company?: string;
  position?: string;
  background?: string;
};

export type NotificationUser = {
  content: string;
  createdAt: string;
  read: boolean;
  readAt: string;
};
