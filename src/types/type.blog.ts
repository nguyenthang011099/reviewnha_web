export type BlogCategory = {
  id: number;
  slug: string;
  name: string;
  total_doc: number;
};

export type Blog = {
  id: number;
  authorName: string;
  featured: boolean;
  title: string;
  slug: string;
  category: BlogCategory;
  tags: BlogTag[];
  description: string;
  publishedAt: string;
  thumbnail: string;
  viewCount: number;
  content: string;
};

export type BlogTag = {
  name: string;
  slug: string;
};

export type BlogsCategoryData = {
  [key: string]: {
    blogs: Blog[];
    total: number;
    currentOffset: number;
    loading: boolean;
  };
};

export type BlogsAllData = {
  featureBlogs: {
    blogs: Blog[];
    total: number;
    currentOffset?: number;
    loading: boolean;
  };
  videos: {
    blogs: Blog[];
    total: number;
    currentOffset?: number;
  };
  normalBlogs: {
    blogs: Blog[];
    total: number;
    currentOffset?: number;
  };
  highlightBlogs: {
    blogs: Blog[];
    total: number;
    currentOffset?: number;
    loading: boolean;
  };
};

export type VideoBlog = {
  id: number;
  authorName: string;
  title: string;
  slug: string;
  category: BlogCategory;
  description: string;
  url: string;
  publishedAt: string;
};

export type VideoList = {
  videos: VideoBlog[];
  loading: boolean;
  currentOffset: number;
  total: number;
};

export type VideoCount = {
  id: string;
  count: number;
};

export type BlogsByTag = {
  blogs: Blog[];
  currentOffset: number;
  loading: boolean;
  total: number;
  currentTag: BlogTag;
};
