export type NotificationType = {
  title: string; // title hiển thị
  type: 'success' | 'error'; // màu success hay error
  isOpen: boolean; // mở hay tắt
  reset?: () => any | null; // hàm reset biến isOpen thành fail: () => { setIsOpen(false) }
};
