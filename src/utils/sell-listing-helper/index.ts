import { SellListing } from '../../types/type.sell-listing';

export const renderAddress = (post: SellListing) => {
  return [post.address, post?.ward?.path ?? ''].join(', ');
};

export const renderPrice = (price: string | number) => {
  if (price > Math.pow(10, 9)) {
    return Number(Number(price) / 1000000000).toFixed(1) + ' tỷ';
  } else if (price < Math.pow(10, 9)) {
    return Number(Number(price) / 1000000).toFixed(0) + ' triệu';
  }
  return 'Thỏa thuận';
};
