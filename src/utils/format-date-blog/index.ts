import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';
import 'dayjs/locale/vi';
dayjs.locale('vi');
dayjs.extend(relativeTime);

export const formatDateBlog = (dateInput: string) => {
  const today = dayjs(new Date());
  const date = dayjs(dateInput);
  if (today.get('date') - date.get('date') === 0) {
    return date.fromNow();
  }
  return date.format('DD/MM/YYYY');
};
