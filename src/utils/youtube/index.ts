export const getYoutubeId = (url: string) => {
  if (url) {
    const regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|&v=)([^#&?]*).*/;
    const match = url.match(regExp);

    return match && match[2].length === 11 ? match[2] : null;
  }
  return '';
};

export const renderYoutubeThumnail = (url: string, type: 'high' | 'low') => {
  const ytbId: string = getYoutubeId(url);
  return ytbId ? `https://img.youtube.com/vi/${ytbId}/${type === 'high' ? 'hq' : 'mq'}default.jpg` : '';
};
