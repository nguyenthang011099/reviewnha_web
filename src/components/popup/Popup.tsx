import classnames from 'classnames';
import { useEffect, useRef } from 'react';
import useOutsideClick from '../../utils/customHooks/useOutsideClick';
import './Popup.scss';

interface Props {
  isOpen: boolean;
  setIsOpen: (...args: any) => void;
  isHiddenExitButton?: boolean;
  className?: string;
  style?: any;
}

const Popup: React.FC<Props> = (props) => {
  const contentRef = useRef(null);

  useOutsideClick(contentRef, () => {
    props.setIsOpen(false);
  });

  useEffect(() => {
    if (props.isOpen) {
      const bodyWidth = document.body.offsetWidth;
      window.document.body.style.overflow = 'hidden';
      window.document.body.style.width = bodyWidth + 'px';
    } else {
      window.document.body.style.overflow = '';
      window.document.body.style.width = '';
    }
  }, [props.isOpen]);

  return (
    <div
      className={classnames(
        { display_none: !props.isOpen },
        'popup position_fixed overflow_auto z_index_1000 width_100_vw height_100_percent top_0 left_0 display_flex padding_top_70 padding_bottom_xs_10 padding_top_xs_10 justify_content_center align_items_fs',
      )}
      style={props.style}
    >
      <div ref={contentRef} className={`${props?.className ?? ''} width_sm_95_percent bg_white position_relative`}>
        {props.isHiddenExitButton ? null : (
          <div
            className="close_icon position_absolute right_5 top_5 z_index_1000 cursor_pointer padding_10"
            onClick={() => {
              props.setIsOpen(false);
            }}
          >
            <svg width="28" height="28" viewBox="0 0 28 28" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path
                fillRule="evenodd"
                clipRule="evenodd"
                d="M13.9993 0.666656C21.3513 0.666656 27.3327 6.64799 27.3327 14C27.3327 21.352 21.3513 27.3333 13.9993 27.3333C6.64735 27.3333 0.666016 21.352 0.666016 14C0.666016 6.64799 6.64735 0.666656 13.9993 0.666656ZM13.9993 2.66666C7.75002 2.66666 2.66602 7.75066 2.66602 14C2.66602 20.2493 7.75002 25.3333 13.9993 25.3333C20.2487 25.3333 25.3327 20.2493 25.3327 14C25.3327 7.75066 20.2487 2.66666 13.9993 2.66666Z"
                fill="black"
              />
              <path
                fillRule="evenodd"
                clipRule="evenodd"
                d="M10.7737 9.28594C10.3626 8.87487 9.69609 8.87487 9.28501 9.28594C8.87393 9.69702 8.87393 10.3635 9.28501 10.7746L12.5104 14L9.28501 17.2254C8.87393 17.6365 8.87393 18.303 9.28501 18.714C9.69609 19.1251 10.3626 19.1251 10.7737 18.714L13.9991 15.4886L17.2245 18.714C17.6355 19.1251 18.302 19.1251 18.7131 18.714C19.1242 18.303 19.1242 17.6365 18.7131 17.2254L15.4877 14L18.7131 10.7746C19.1242 10.3635 19.1242 9.69702 18.7131 9.28594C18.302 8.87487 17.6355 8.87487 17.2245 9.28594L13.9991 12.5113L10.7737 9.28594Z"
                fill="black"
              />
            </svg>
          </div>
        )}
        {props.children}
      </div>
    </div>
  );
};

export default Popup;
