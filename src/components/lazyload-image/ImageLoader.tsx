import React, { Component, useEffect, useRef } from 'react';
import './ImageLoader.scss';
import lazyImg from '../../images/lazy.png';

interface Props {
  src: string;
  className?: string;
  style?: any;
  alt: string;
  width?: string | number;
  height?: string | number;
  onClick?: () => any;
}

const ImageLoader: React.FC<Props> = (props) => {
  const _img = useRef();

  useEffect(() => {
    const image = _img.current;
    if (image) {
      //lazy load
      let lazyImages = _img.current as any;
      if ('IntersectionObserver' in window) {
        let lazyImageObserver = new IntersectionObserver((entries) => {
          entries.forEach(function (entry) {
            if (entry.isIntersecting) {
              let lazyImage = entry.target as any;
              lazyImage.src = lazyImage.dataset.src;
              lazyImage.classList.remove('lazy');
              lazyImage.classList.add('load_img_lazy');
              lazyImageObserver.unobserve(lazyImage);
            }
          });
        });
        lazyImageObserver.observe(lazyImages);
      }
    }
  }, []);

  return (
    <img
      ref={_img}
      src={lazyImg}
      className={`${props.className ? props.className : ''} lazy object_fit_cover`}
      data-src={props.src}
      style={props.style}
      alt={props.alt}
      width={props.width}
      height={props.height}
      onClick={props.onClick}
    />
  );
};

export default ImageLoader;
