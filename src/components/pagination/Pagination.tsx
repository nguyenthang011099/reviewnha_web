import React, { useEffect, useRef, useState } from 'react';
import classnames from 'classnames';
import './Pagination.scss';

interface Props {
  fetchDataEachPage: (any) => any; //action
  pageAmount: number; // so luong items tren 1 page
  totalItem: number; // tong tat ca so luong item
  data: any; // gia tri param cua ham fetchDataEachPage(data)
  scrollTop?: () => any; // sau khi click thi scroll len dau
  checkRecalculateTotalPage?: any; // dieu kien de tinh toan lai cac state neu co
  notResetActivePage?: boolean; // khi xay ra dieu kien tinh toan lai thi co reset lai activePage hay khong
}

const Pagination: React.FC<Props> = (props) => {
  const [arrayPage, setArrayPage] = useState<number[]>([]);
  const [totalPage, setTotalPage] = useState<number>(0);
  const [activePage, setActivePage] = useState<number>(1);
  const [pageAmount] = useState<number>(props.pageAmount);
  const isMount = useRef(false);

  /* khi component mount thi tinh toan array page*/
  useEffect(() => {
    // khi mount thi tinh toan totalPage
    let totalPage: number = 0;
    if (props.totalItem % pageAmount === 0) {
      totalPage = props.totalItem / pageAmount;
    } else {
      totalPage = parseInt(`${props.totalItem / pageAmount}`) + 1;
    }
    setTotalPage(totalPage);
    let newArrayPage = [];
    if (totalPage < 5) {
      for (let i = 0; i < totalPage; i++) {
        newArrayPage.push(i + 1);
      }
    } else {
      for (let i = 0; i < 5; i++) {
        newArrayPage.push(i + 1);
      }
    }
    setArrayPage(newArrayPage);
  }, []);

  /* tinh toan lai array page khi co su kien tinh toan lai */
  useEffect(() => {
    let totalPage: number = 0;
    if (props.totalItem % pageAmount === 0) {
      totalPage = props.totalItem / pageAmount;
    } else {
      totalPage = parseInt(`${props.totalItem / pageAmount}`) + 1;
    }
    setTotalPage(totalPage);
    if (!props.notResetActivePage) {
      setActivePage(1);
    }
    let newArrayPage: number[] = [];
    if (totalPage < 5) {
      for (let i = 0; i < totalPage; i++) {
        newArrayPage.push(i + 1);
      }
    } else {
      for (let i = 0; i < 5; i++) {
        newArrayPage.push(i + 1);
      }
    }
    setArrayPage(newArrayPage);
  }, [props.checkRecalculateTotalPage]);

  /* scroll len top khi page active thay doi*/
  useEffect(() => {
    if (isMount.current) {
      const { fetchDataEachPage, data } = props;
      fetchDataEachPage && fetchDataEachPage({ ...data, page: activePage });
    } else {
      isMount.current = true;
    }
  }, [activePage]);

  /**
   * handlePageClick: ham thuc thi khi click vao 1 page
   * @param index: index cua page trong arrayPage
   * @param value: gia tri cua page
   */
  const handlePageClick = (index: number, value: number) => {
    if (activePage !== value) {
      setActivePage(value);
      let newArrayPage: number[] = arrayPage;
      if (index < 2 && value !== 1 && value !== 2) {
        if (index === 1) {
          newArrayPage = arrayPage.map((page) => {
            return page - 1;
          });
        }
        if (index === 0) {
          newArrayPage = arrayPage.map((page) => {
            return page - 2;
          });
        }
      }
      if (index > 2 && value !== totalPage - 1 && value !== totalPage) {
        if (index === 3) {
          newArrayPage = arrayPage.map((page) => {
            return page + 1;
          });
        }
        if (index === 4) {
          newArrayPage = arrayPage.map((page) => {
            return page + 2;
          });
        }
      }
      if (index === 0 && value === 2) {
        newArrayPage = arrayPage.map((page) => {
          return page - 1;
        });
      }
      if (index === 4 && value === totalPage - 1) {
        newArrayPage = arrayPage.map((page) => {
          return page + 1;
        });
      }
      setArrayPage(newArrayPage);
      if (props.scrollTop) {
        props.scrollTop();
      } else {
        window.scrollTo({ top: 0, left: 0, behavior: 'smooth' });
      }
    }
  };

  /**
   * handleClickPrev: ham thuc thi khi click nut prev
   */
  const handleClickPrev = () => {
    if (activePage > 3 && activePage < totalPage - 1) {
      let newArrayPage: number[];
      newArrayPage = arrayPage.map((page) => {
        return page - 1;
      });
      setArrayPage(newArrayPage);
      setActivePage(activePage - 1);
    } else {
      setActivePage(activePage - 1);
    }
    if (props.scrollTop) {
      props.scrollTop();
    } else {
      window.scrollTo({ top: 0, left: 0, behavior: 'smooth' });
    }
  };

  /**
   * handleClickNext: ham thuc thi khi click nut next
   */
  const handleClickNext = () => {
    if (activePage < totalPage - 2 && activePage > 2) {
      let newArrayPage: number[];
      newArrayPage = arrayPage.map((page) => {
        return page + 1;
      });
      setArrayPage(newArrayPage);
      setActivePage(activePage + 1);
    } else {
      setActivePage(activePage + 1);
    }
    if (props.scrollTop) {
      props.scrollTop();
    } else {
      window.scrollTo({ top: 0, left: 0, behavior: 'smooth' });
    }
  };

  return (
    <div className="pagination display_flex justify_content_center align_items_center">
      {activePage > 1 ? (
        <div
          className="prev border_width_1 border_style_solid border-color-black padding_x_24 padding_y_8
          display_flex justify_content_center align_items_center border_radius_25 no_select cursor_pointer bg_black-hover color_white-hover"
          onClick={handleClickPrev}
        >
          <span className="font_size_14 font_weight_600 line_height_17">Quay lại</span>
        </div>
      ) : (
        <div
          className="prev opacity_0_5 border_width_1 border_style_solid border-color-black padding_x_24 padding_y_8
          display_flex justify_content_center align_items_center border_radius_25 no_select cursor_pointer"
        >
          <span className="font_size_14 font_weight_600 line_height_17">Quay lại</span>
        </div>
      )}
      <div className="array_pages display_flex margin_x_10">
        {arrayPage.map((pageNumber, i) => {
          return (
            <div
              key={pageNumber}
              className={classnames(
                {
                  color_red: pageNumber === activePage,
                },
                `margin_x_5 cursor_pointer width_19 height_19 display_flex justify_content_center align_items_center no_select 
                font_weight_600 font_size_14 line_height_17 color_red-hover`,
              )}
              onClick={() => {
                handlePageClick(i, pageNumber);
              }}
            >
              {pageNumber}
            </div>
          );
        })}
      </div>
      {activePage < totalPage ? (
        <div
          className="next border_width_1 border_style_solid border-color-black padding_x_24 padding_y_8
          display_flex justify_content_center align_items_center border_radius_25 no_select cursor_pointer bg_black-hover color_white-hover"
          onClick={handleClickNext}
        >
          <span className="font_size_14 font_weight_600 line_height_17">Tiếp theo</span>
        </div>
      ) : (
        <div
          className="next opacity_0_5 border_width_1 border_style_solid border-color-black padding_x_24 padding_y_8
          display_flex justify_content_center align_items_center border_radius_25 no_select"
        >
          <span className="font_size_14 font_weight_600 line_height_17">Tiếp theo</span>
        </div>
      )}
    </div>
  );
};

export default Pagination;
