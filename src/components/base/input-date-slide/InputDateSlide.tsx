import dayjs, { Dayjs } from 'dayjs';
import React, { useEffect, useState } from 'react';
import { SliderMethod } from '../../../types/type.slider';
import Slider from '../../slider/Slider';
import classnames from 'classnames';

interface Props {
  value: string;
  setValue: (..._args: any) => void;
}

interface Date {
  value: Dayjs;
  date: number;
  day: number;
  month: number;
}

const InputDateSlide: React.FC<Props> = (props) => {
  const [allDates, setAllDates] = useState<Date[]>([]);
  let slider: SliderMethod = {};
  const [disableNext, setDisableNext] = useState<boolean>(false);
  const [disablePrev, setDisablePrev] = useState<boolean>(false);

  useEffect(() => {
    const today = new Date();
    const allDatesArray: Date[] = [];
    for (let i = 0; i < 7; i++) {
      const value = dayjs(today).add(i, 'day');
      const date = value.date();
      const day = value.day() + 1;
      const month = value.month() + 1;
      const newDate = {
        value,
        date,
        day,
        month,
      };
      allDatesArray.push(newDate);
    }
    setAllDates(allDatesArray);
  }, []);

  return (
    <div className="base_input_date_slider position_relative width_360 width_xs_270">
      <div
        className={classnames(
          { cursor_pointer: !disablePrev },
          `position_absolute width_24 height_24 border_radius_50_percent bg_white display_flex justify_content_center align_items_center
            left_-12 top_32 z_index_1 no_select`,
        )}
        style={{ boxShadow: '1px 1px 2px 1px rgba(23, 23, 23, 0.16)' }}
        onClick={() => {
          slider.prevSlideIndex();
        }}
      >
        <svg
          width="20"
          height="20"
          viewBox="0 0 20 20"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
          className={classnames({
            opacity_0_3: disablePrev,
          })}
        >
          <path
            fillRule="evenodd"
            clipRule="evenodd"
            d="M10 20C4.486 20 0 15.514 0 10C0 4.486 4.486 0 10 0C15.514 0 20 4.486 20 10C20 15.514 15.514 20 10 20ZM10 18.5C14.687 18.5 18.5 14.687 18.5 10C18.5 5.313 14.687 1.5 10 1.5C5.313 1.5 1.5 5.313 1.5 10C1.5 14.687 5.313 18.5 10 18.5ZM10.9124 14.0023L7.4264 10.5313C7.2854 10.3903 7.2064 10.1993 7.2064 10.0003C7.2064 9.8003 7.2854 9.6093 7.4264 9.4683L10.9124 5.9983C11.0584 5.8523 11.2504 5.7793 11.4414 5.7793C11.6334 5.7793 11.8264 5.8523 11.9724 6.0003C12.2644 6.2943 12.2634 6.7683 11.9704 7.0603L9.0184 10.0003L11.9704 12.9393C12.2634 13.2313 12.2644 13.7063 11.9724 14.0003C11.6804 14.2953 11.2064 14.2933 10.9124 14.0023Z"
            fill="black"
          />
        </svg>
      </div>
      <div
        className={classnames(
          { cursor_pointer: !disableNext },
          `position_absolute width_24 height_24 border_radius_50_percent bg_white display_flex justify_content_center align_items_center
            right_-12 top_32 z_index_1 no_select`,
        )}
        style={{ boxShadow: '1px 1px 2px 1px rgba(23, 23, 23, 0.16)' }}
        onClick={() => {
          slider.nextSlideIndex();
        }}
      >
        <svg
          width="20"
          height="20"
          viewBox="0 0 20 20"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
          className={classnames({
            opacity_0_3: disableNext,
          })}
        >
          <path
            fillRule="evenodd"
            clipRule="evenodd"
            d="M10 0C15.514 0 20 4.486 20 10C20 15.514 15.514 20 10 20C4.486 20 0 15.514 0 10C0 4.486 4.486 0 10 0ZM10 1.5C5.313 1.5 1.5 5.313 1.5 10C1.5 14.687 5.313 18.5 10 18.5C14.687 18.5 18.5 14.687 18.5 10C18.5 5.313 14.687 1.5 10 1.5ZM9.0876 5.9977L12.5736 9.4687C12.7146 9.6097 12.7936 9.8007 12.7936 9.9997C12.7936 10.1997 12.7146 10.3907 12.5736 10.5317L9.0876 14.0017C8.9416 14.1477 8.7496 14.2207 8.5586 14.2207C8.3666 14.2207 8.1736 14.1477 8.0276 13.9997C7.7356 13.7057 7.7366 13.2317 8.0296 12.9397L10.9816 9.9997L8.0296 7.0607C7.7366 6.7687 7.7356 6.2937 8.0276 5.9997C8.3196 5.7047 8.7936 5.7067 9.0876 5.9977Z"
            fill="black"
          />
        </svg>
      </div>
      {allDates.length ? (
        <Slider
          className="width_100_percent"
          listImages={allDates}
          slidesPerView={4}
          setListImages={setAllDates}
          slidesNm={25}
          slidesMd={25}
          slidesSm={25}
          slidesXs={33}
          isStopAutoPlay={true}
          margin={16}
          innerRef={slider}
          onDisableNext={setDisableNext}
          onDisablePrev={setDisablePrev}
        >
          {allDates.map((date: Date, index: number) => {
            return (
              <div
                key={index}
                className={classnames(
                  {
                    'color_white bg_green border_color_green': props.value === date.value.toString(),
                    border_color_black: props.value !== date.value.toString(),
                  },
                  `width_78 height_88 border_radius_8 padding_y_8 display_flex align_items_center flex_direction_col 
        border_width_1 border_style_solid flex_none margin_x_8 cursor_pointer no_select width_xs_79`,
                )}
                data-value={date.value.toString()}
                onClick={() => {
                  props.setValue(date.value.toString());
                }}
              >
                <div>
                  <span className="font_size_12 line_height_18">Tháng {date.month}</span>
                </div>
                <div>
                  <span className="font_size_24 line_height_36 font_weight_600">{date.date}</span>
                </div>
                <div className="font_size_12 line_height_18">
                  <span>{date.day === 1 ? 'CN ' : `Th.${date.day}`}</span>
                </div>
              </div>
            );
          })}
        </Slider>
      ) : null}
    </div>
  );
};

export default InputDateSlide;
