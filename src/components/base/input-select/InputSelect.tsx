import React, { useEffect, useRef, useState } from 'react';
import classnames from 'classnames';
import useOutsideClick from '../../../utils/customHooks/useOutsideClick';
import './InputSelect.scss';
import { OptionInputRadio } from '../../../types/type.base';

interface Props {
  value: OptionInputRadio;
  setValue: (..._args: any[]) => void;
  label: string;
  className?: string;
  options: OptionInputRadio[];
  onOutsideClick?: () => void;
}

const InputSelect: React.FC<Props> = (props) => {
  const [isOpen, setIsOpen] = useState<boolean>(false);
  const $selectRef = useRef(null);
  const isOpenCached: any = useRef(null);

  useEffect(() => {
    isOpenCached.current = isOpen;
  }, [isOpen]);

  useOutsideClick($selectRef, () => {
    if (isOpenCached.current) {
      setIsOpen(false);
      props.onOutsideClick && props.onOutsideClick();
    }
  });

  return (
    <div className="input_select_base width_100_percent position_relative" ref={$selectRef}>
      <div
        className={`${
          props.className ? props.className : ''
        } display_flex height_45 align_items_center justify_content_center bg_white border_radius_16 cursor_pointer`}
        onClick={() => {
          setIsOpen(!isOpen);
        }}
      >
        <div className="icon display_flex flex_none">{props.children}</div>
        <span className="margin_x_18 font_size_14 line_height_17 font_weight_600 no_select text_over_flow_1">
          {props.value?.value ? props.value.title : props.label}
        </span>
        <svg
          width="14"
          height="14"
          viewBox="0 0 14 14"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
          className={classnames(
            {
              rotate_180: isOpen,
            },
            'flex_none',
          )}
          style={{ transition: 'all 0.3s' }}
        >
          <path
            fillRule="evenodd"
            clipRule="evenodd"
            d="M6.99967 0.333333C10.6757 0.333333 13.6663 3.324 13.6663 7C13.6663 10.676 10.6757 13.6667 6.99967 13.6667C3.32367 13.6667 0.333008 10.676 0.333008 7C0.333008 3.324 3.32367 0.333333 6.99967 0.333333ZM6.99967 1.33333C3.87501 1.33333 1.33301 3.87533 1.33301 7C1.33301 10.1247 3.87501 12.6667 6.99967 12.6667C10.1243 12.6667 12.6663 10.1247 12.6663 7C12.6663 3.87533 10.1243 1.33333 6.99967 1.33333ZM9.66701 5.68393C9.86234 5.8786 9.86301 6.19527 9.66834 6.39127L7.35367 8.71527C7.27901 8.791 7.18215 8.83985 7.07847 8.85637L6.99967 8.8626C6.86701 8.8626 6.73901 8.80993 6.64567 8.71527L4.33167 6.39127C4.13634 6.19527 4.13767 5.8786 4.33301 5.68393C4.52901 5.48927 4.84567 5.48927 5.04034 5.68527L6.99967 7.6546L8.95967 5.68527C9.15434 5.48927 9.47101 5.48927 9.66701 5.68393Z"
            fill="black"
          />
        </svg>
      </div>
      <div
        className={classnames(
          {
            active: isOpen,
          },
          `drop_down position_absolute bg_white border_radius_24 min_width_210 left_-1 max_height_160 overflow_auto padding_x_18 padding_y_25 
          border_width_1 border_color_grey border_style_solid width_auto`,
        )}
        style={{
          boxShadow: '1px 1px 2px 1px rgba(23, 23, 23, 0.16)',
        }}
      >
        {props.options.map((option: OptionInputRadio) => {
          return (
            <div
              className="option_item display_flex align_items_center cursor_pointer bg_whitesmoke-hover margin_bottom_20 no_select"
              key={option.value}
              onClick={() => {
                props.setValue(option);
                setIsOpen(false);
              }}
            >
              {props.value?.value !== option.value ? (
                <svg width={24} height={24} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path
                    fillRule="evenodd"
                    clipRule="evenodd"
                    d="M16.335 2C19.724 2 22 4.378 22 7.916V16.084C22 19.622 19.724 22 16.335 22H7.666C4.277 22 2 19.622 2 16.084V7.916C2 4.378 4.277 2 7.666 2H16.335ZM16.335 3.5H7.666C5.136 3.5 3.5 5.233 3.5 7.916V16.084C3.5 18.767 5.136 20.5 7.666 20.5H16.335C18.865 20.5 20.5 18.767 20.5 16.084V7.916C20.5 5.233 18.865 3.5 16.335 3.5Z"
                    fill="#3B4144"
                  />
                </svg>
              ) : (
                <svg width={24} height={24} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path
                    fillRule="evenodd"
                    clipRule="evenodd"
                    d="M16.335 2C19.724 2 22 4.378 22 7.916V16.084C22 19.622 19.724 22 16.335 22H7.666C4.277 22 2 19.622 2 16.084V7.916C2 4.378 4.277 2 7.666 2H16.335Z"
                    fill="white"
                  />
                  <path
                    fillRule="evenodd"
                    clipRule="evenodd"
                    d="M19.3549 4.6686C18.675 3.95846 17.6655 3.5 16.335 3.5H7.666C6.33548 3.5 5.32566 3.95848 4.64546 4.66874C3.95984 5.38465 3.5 6.47186 3.5 7.916V16.084C3.5 17.5281 3.95984 18.6153 4.64546 19.3313C5.32566 20.0415 6.33548 20.5 7.666 20.5H16.335C17.6655 20.5 18.675 20.0415 19.3549 19.3314C20.0403 18.6155 20.5 17.5283 20.5 16.084V7.916C20.5 6.47167 20.0403 5.38445 19.3549 4.6686ZM22 7.916C22 4.378 19.724 2 16.335 2H7.666C4.277 2 2 4.378 2 7.916V16.084C2 19.622 4.277 22 7.666 22H16.335C19.724 22 22 19.622 22 16.084V7.916Z"
                    fill="#3B4144"
                  />
                  <path
                    fillRule="evenodd"
                    clipRule="evenodd"
                    d="M18.6131 7.88586C18.9072 8.17756 18.9091 8.65243 18.6174 8.94651L11.674 15.9465C11.5332 16.0885 11.3415 16.1683 11.1416 16.1683C10.9416 16.1683 10.7499 16.0885 10.6091 15.9465L6.05248 11.3528C5.76078 11.0587 5.76271 10.5838 6.05679 10.2921C6.35087 10.0004 6.82574 10.0023 7.11744 10.2964L11.1416 14.3533L17.5525 7.89016C17.8442 7.59608 18.3191 7.59415 18.6131 7.88586Z"
                    fill="#3B4144"
                  />
                </svg>
              )}
              <span className="margin_left_8 font_weight_600 font_size_14 line_height_17">{option.title}</span>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default InputSelect;
