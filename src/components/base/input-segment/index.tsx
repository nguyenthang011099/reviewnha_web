import React, { useEffect, useRef, useState } from 'react';
import classnames from 'classnames';
import useOutsideClick from '../../../utils/customHooks/useOutsideClick';
import { OptionInputRadio } from '../../../types/type.base';

interface Props {
  value: OptionInputRadio;
  setValue: (..._args: any[]) => void;
  className?: string;
  label?: string;
  style?: any;
  isWarning?: boolean;
  setIsWarning?: (..._args: any[]) => void;
  options: OptionInputRadio[];
}

const InputSegment: React.FC<Props> = (props) => {
  const $inputRef = useRef(null);
  const [isFocus, setIsFocus] = useState<boolean>(false);
  const isFocusState: any = useRef(false);

  useEffect(() => {
    isFocusState.current = isFocus;
  }, [isFocus]);

  useOutsideClick($inputRef, () => {
    if (isFocusState.current) {
      setIsFocus(false);
    }
  });

  return (
    <div
      ref={$inputRef}
      className={classnames(
        {
          warning: props.isWarning,
        },
        `${
          props.className ? props.className : ''
        } input_segment_base margin_bottom_16 border_width_1 border_color_black border_style_solid border_radius_8 padding_x_24 display_flex 
          height_48 align_items_center position_relative`,
      )}
      style={props.style}
    >
      <div className="display_flex align_items_center width_100_percent height_100_percent">
        {props.label ? (
          <label className="display_flex align_items_center font_size_14 line_height_24 opacity_0_5 color_black width_110 flex_none height_100_percent margin_right_40">
            {props.label}
          </label>
        ) : null}
        <div className="display_flex font_size_16 font_weight_500 line_height_28 width_100_percent height_100_percent align_items_center">
          {props.options.map((option: OptionInputRadio, index) => {
            return (
              <div
                key={option.value}
                onClick={() => {
                  props.setValue(option);
                }}
                className={classnames(
                  {
                    margin_right_16: index !== props.options.length - 1,
                    'color_white bg_green cursor_default': props.value.value === option.value,
                    cursor_pointer: props.value.value !== option.value,
                  },
                  'width_40 height_40 border_radius_50_percent display_flex justify_content_center align_items_center no_select',
                )}
              >
                <span>{option.title}</span>
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
};

export default InputSegment;
