import React, { useRef, useState } from 'react';
import classnames from 'classnames';

interface Props {
  value: string;
  setValue: (_value: string) => void;
  warning?: boolean;
  placeholder?: string;
  label?: string;
  setWarning?: (_value: boolean) => void;
  maxValue?: number;
  showCount?: boolean;
  className?: string;
}

const TextArea: React.FC<Props> = (props) => {
  const $textareaRef = useRef(null);
  const [isFocus, setIsFocus] = useState<boolean>(false);

  return (
    <div
      className={classnames(
        { focus: isFocus, warning: props.warning, [props.className]: props.className },
        `description_input input_select display_flex justify_content_sb border_color_black border_style_solid border_radius_8
            border_width_1 padding_x_24`,
      )}
      ref={$textareaRef}
    >
      <div className="display_flex flex_1 width_xs_1">
        <span className="label width_97 font_size_14 line_height_18 display_block padding_top_15 opacity_0_5 width_xs_80">
          {props.label}
        </span>
        <textarea
          className="border_none font_size_16 font_weight_500 line_height_28 padding_y_10 flex_1 height_100_percent"
          placeholder={props.placeholder}
          value={props.value}
          required
          onChange={(e: React.ChangeEvent<HTMLTextAreaElement>) => {
            if (e.target.value.length <= 1000) {
              props.setValue(e.target.value);
            }
            props.warning && props.setWarning(false);
          }}
          onFocus={() => {
            setIsFocus(true);
          }}
        ></textarea>
      </div>
      {props.showCount ? (
        <div className="display_flex align_items_fe margin_bottom_16 margin_left_10">
          <span className="font_size_12 line_height_18">
            {props.value.length}/{props.maxValue}
          </span>
        </div>
      ) : null}
    </div>
  );
};
export default TextArea;
