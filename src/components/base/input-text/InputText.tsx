import React, { useEffect, useRef, useState } from 'react';
import './InputText.scss';
import classnames from 'classnames';
import useOutsideClick from '../../../utils/customHooks/useOutsideClick';

interface Props {
  value: string | number;
  setValue: (..._args: any[]) => void;
  name: string;
  placeholder?: string;
  required?: boolean;
  type: string;
  className?: string;
  label?: string;
  style?: any;
  isWarning?: boolean;
  setIsWarning?: (..._args: any[]) => void;
  setIcon?: () => any;
  min?: number;
  max?: number;
  isHiddenClear?: boolean;
  disabled?: boolean;
}

const InputText: React.FC<Props> = (props) => {
  const $inputRef = useRef(null);
  const [isFocus, setIsFocus] = useState<boolean>(false);
  const isFocusState: any = useRef(false);
  const [showPw, setShowPw] = useState<boolean>(false);

  useEffect(() => {
    isFocusState.current = isFocus;
  }, [isFocus]);

  useOutsideClick($inputRef, () => {
    if (isFocusState.current) {
      setIsFocus(false);
    }
  });

  /**
   * togglePw
   */
  const togglePw = () => {
    setShowPw(!showPw);
  };

  /**
   * renderType: render type input
   */
  const renderType = () => {
    if (props.type === 'password') {
      return !showPw ? 'password' : 'text';
    }
    return props.type;
  };

  /**
   * changeValue: input change value
   * @param e event
   */
  const changeValue = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (props.isWarning) {
      props.setIsWarning && props.setIsWarning('');
    }
    props.setValue(e.target.value);
  };

  return (
    <div
      ref={$inputRef}
      className={classnames(
        {
          focus: isFocus,
          warning: props.isWarning,
        },
        `${
          props.className ? props.className : ''
        } input_text_base margin_bottom_16 border_width_1 border_color_black border_style_solid border_radius_8 padding_x_24 display_flex 
          height_48 align_items_center position_relative`,
      )}
      style={props.style}
    >
      <div className="display_flex align_items_center width_100_percent height_100_percent">
        {props.label ? (
          <label
            className="display_flex align_items_center font_size_14 line_height_24 opacity_0_5 color_black width_95 flex_none height_100_percent 
          width_xs_80 flex_none"
          >
            {props.label}
          </label>
        ) : null}
        <input
          type={renderType()}
          name={props.name}
          placeholder={props.placeholder}
          required={props.required}
          className="border_none font_size_16 font_weight_500 line_height_28 width_100_percent height_100_percent padding_right_30 "
          value={props.value}
          onChange={changeValue}
          onFocus={() => {
            setIsFocus(true);
          }}
          min={props.min ? props.min : ''}
          max={props.max ? props.max : ''}
          disabled={props.disabled}
        />
      </div>
      {props.type !== 'password' && props.setIcon ? (
        props.isWarning ? null : (
          <div
            className={classnames(
              { custom_icon: !props.isHiddenClear, custom_fixed_icon: props.isHiddenClear },
              'display_flex cursor_pointer',
            )}
          >
            {props.setIcon()}
          </div>
        )
      ) : null}
      {props.type !== 'password' ? (
        props.isWarning ? (
          <svg
            width="20"
            height="18"
            viewBox="0 0 20 18"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            className="custom_icon"
          >
            <path
              d="M8.6279 0.353094C9.98767 -0.40098 11.7174 0.0944694 12.4773 1.44209L19.746 14.0572C19.906 14.4338 19.976 14.7399 19.996 15.058C20.036 15.8012 19.776 16.5236 19.2661 17.0795C18.7562 17.6334 18.0663 17.9604 17.3164 18H2.6789C2.36896 17.9812 2.05901 17.9108 1.76906 17.8018C0.319301 17.2172 -0.380581 15.5723 0.20932 14.1464L7.52809 1.43317C7.77804 0.986278 8.15798 0.600818 8.6279 0.353094ZM9.99767 12.2726C9.51775 12.2726 9.11782 12.669 9.11782 13.1456C9.11782 13.6202 9.51775 14.0176 9.99767 14.0176C10.4776 14.0176 10.8675 13.6202 10.8675 13.1347C10.8675 12.66 10.4776 12.2726 9.99767 12.2726ZM9.99767 6.09039C9.51775 6.09039 9.11782 6.47585 9.11782 6.95248V9.75573C9.11782 10.2314 9.51775 10.6287 9.99767 10.6287C10.4776 10.6287 10.8675 10.2314 10.8675 9.75573V6.95248C10.8675 6.47585 10.4776 6.09039 9.99767 6.09039Z"
              fill="#D93C23"
            />
          </svg>
        ) : null
      ) : null}
      {props.type !== 'password' ? (
        props.isHiddenClear ? null : (
          <div
            className="cancel_button cursor_pointer display_none position_absolute right_24 z_index_1 height_100_percent align_items_center "
            onClick={() => {
              props.setValue('');
            }}
          >
            <svg width={20} height={20} viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path
                fillRule="evenodd"
                clipRule="evenodd"
                d="M10 0C15.514 0 20 4.486 20 10C20 15.514 15.514 20 10 20C4.486 20 0 15.514 0 10C0 4.486 4.486 0 10 0ZM10 1.5C5.313 1.5 1.5 5.313 1.5 10C1.5 14.687 5.313 18.5 10 18.5C14.687 18.5 18.5 14.687 18.5 10C18.5 5.313 14.687 1.5 10 1.5Z"
                fill="#3B4144"
              />
              <path
                fillRule="evenodd"
                clipRule="evenodd"
                d="M7.58073 6.46447C7.27242 6.15616 6.77255 6.15616 6.46424 6.46447C6.15594 6.77277 6.15594 7.27264 6.46424 7.58095L8.88329 10L6.46424 12.419C6.15594 12.7274 6.15594 13.2272 6.46424 13.5355C6.77255 13.8438 7.27242 13.8438 7.58073 13.5355L9.99978 11.1165L12.4188 13.5355C12.7271 13.8438 13.227 13.8438 13.5353 13.5355C13.8436 13.2272 13.8436 12.7274 13.5353 12.419L11.1163 10L13.5353 7.58095C13.8436 7.27264 13.8436 6.77277 13.5353 6.46447C13.227 6.15616 12.7271 6.15616 12.4188 6.46447L9.99978 8.88352L7.58073 6.46447Z"
                fill="#3B4144"
              />
            </svg>
          </div>
        )
      ) : (
        <div className="display_flex align_items_center no_select cursor_pointer" onClick={togglePw}>
          {props.isWarning ? (
            <svg width="20" height="18" viewBox="0 0 20 18" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path
                d="M8.6279 0.353094C9.98767 -0.40098 11.7174 0.0944694 12.4773 1.44209L19.746 14.0572C19.906 14.4338 19.976 14.7399 19.996 15.058C20.036 15.8012 19.776 16.5236 19.2661 17.0795C18.7562 17.6334 18.0663 17.9604 17.3164 18H2.6789C2.36896 17.9812 2.05901 17.9108 1.76906 17.8018C0.319301 17.2172 -0.380581 15.5723 0.20932 14.1464L7.52809 1.43317C7.77804 0.986278 8.15798 0.600818 8.6279 0.353094ZM9.99767 12.2726C9.51775 12.2726 9.11782 12.669 9.11782 13.1456C9.11782 13.6202 9.51775 14.0176 9.99767 14.0176C10.4776 14.0176 10.8675 13.6202 10.8675 13.1347C10.8675 12.66 10.4776 12.2726 9.99767 12.2726ZM9.99767 6.09039C9.51775 6.09039 9.11782 6.47585 9.11782 6.95248V9.75573C9.11782 10.2314 9.51775 10.6287 9.99767 10.6287C10.4776 10.6287 10.8675 10.2314 10.8675 9.75573V6.95248C10.8675 6.47585 10.4776 6.09039 9.99767 6.09039Z"
                fill="#D93C23"
              />
            </svg>
          ) : showPw ? (
            <svg width="20" height="16" viewBox="0 0 20 16" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path
                d="M10 0C12.0683 0 14.0293 0.717576 15.7366 2.04606C17.4439 3.36485 18.8976 5.29455 19.9415 7.70909C20.0195 7.89333 20.0195 8.10667 19.9415 8.28121C17.8537 13.1103 14.1366 16 10 16H9.99024C5.86341 16 2.14634 13.1103 0.0585366 8.28121C-0.0195122 8.10667 -0.0195122 7.89333 0.0585366 7.70909C2.14634 2.88 5.86341 0 9.99024 0H10ZM10 4.12121C7.8439 4.12121 6.09756 5.85697 6.09756 8C6.09756 10.1333 7.8439 11.8691 10 11.8691C12.1463 11.8691 13.8927 10.1333 13.8927 8C13.8927 5.85697 12.1463 4.12121 10 4.12121ZM10.0012 5.57362C11.3378 5.57362 12.4304 6.65968 12.4304 7.99787C12.4304 9.32635 11.3378 10.4124 10.0012 10.4124C8.65483 10.4124 7.56215 9.32635 7.56215 7.99787C7.56215 7.83302 7.58166 7.67787 7.61093 7.52272H7.65971C8.74263 7.52272 9.62068 6.66938 9.65971 5.60272C9.76702 5.58332 9.8841 5.57362 10.0012 5.57362Z"
                fill="black"
              />
            </svg>
          ) : (
            <svg width="20" height="18" viewBox="0 0 20 18" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path
                d="M17.1704 0.714131C17.4631 0.428623 17.9217 0.428623 18.2046 0.714131C18.4974 0.999638 18.4974 1.4722 18.2046 1.75771L16.4288 3.54952C17.8436 4.84907 19.0438 6.60149 19.9415 8.70834C20.0195 8.8954 20.0195 9.11199 19.9415 9.2892C17.8534 14.1921 14.1358 17.1259 9.99868 17.1259H9.98892C8.10575 17.1259 6.30063 16.5056 4.71018 15.3735L2.81725 17.2834C2.67089 17.4311 2.4855 17.5 2.30011 17.5C2.11472 17.5 1.91957 17.4311 1.78297 17.2834C1.53903 17.0373 1.5 16.6435 1.69515 16.358L1.72442 16.3186L16.1556 1.75771C16.1751 1.73802 16.1946 1.71833 16.2044 1.69864C16.2239 1.67895 16.2434 1.65926 16.2532 1.63957L17.1704 0.714131ZM10.0013 0.885337C11.3966 0.885337 12.7529 1.22007 14.0018 1.85015L10.7429 5.13841C10.5087 5.09903 10.255 5.0695 10.0013 5.0695C7.84494 5.0695 6.09836 6.83177 6.09836 9.00753C6.09836 9.2635 6.12764 9.51948 6.16667 9.75576L2.55643 13.3984C1.5807 12.2564 0.731804 10.8781 0.0585443 9.29304C-0.0195148 9.11583 -0.0195148 8.89924 0.0585443 8.71218C2.14662 3.80933 5.86419 0.885337 9.99156 0.885337H10.0013ZM13.2186 6.78855L12.1551 7.86166C12.3307 8.19639 12.4283 8.5902 12.4283 9.00369C12.4283 10.3525 11.3354 11.4551 9.99868 11.4551C9.58887 11.4551 9.19858 11.3567 8.86683 11.1795L7.80327 12.2526C8.42774 12.6759 9.18882 12.9319 9.99868 12.9319C12.1453 12.9319 13.8919 11.1696 13.8919 9.00369C13.8919 8.18655 13.6382 7.41863 13.2186 6.78855Z"
                fill="black"
              />
            </svg>
          )}
        </div>
      )}
    </div>
  );
};

export default InputText;
