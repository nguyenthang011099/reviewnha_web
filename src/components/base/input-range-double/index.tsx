import React, { useEffect, useRef, useState } from 'react';
import { OptionRangeDouble } from '../../../types/type.base';
import './index.scss';

interface Props {
  value: OptionRangeDouble;
  setValue: (_value: OptionRangeDouble) => void;
  min: number;
  max: number;
  step: number;
}

const InputRangeDouble: React.FC<Props> = (props) => {
  const { value, setValue, min, max, step } = props;
  const $thumbMin = useRef(null);
  const $line = useRef(null);
  const $thumbMax = useRef(null);
  const $minVal = useRef(null);
  const $maxVal = useRef(null);
  const mount = useRef(null);
  const $ranges = useRef(null);
  const $numberRanges = useRef(null);
  const [listRange, setListRange] = useState<number[]>([]);

  useEffect(() => {
    const numberRangeCol = (max - min) / step + 1;
    setListRange(Array.from(Array(numberRangeCol).keys()));
  }, []);

  useEffect(() => {
    if (mount.current) {
      if ($minVal.current) {
        $minVal.current.style.left = `calc(${calcLeftPosition(value.min)}% - ${$minVal.current.offsetWidth / 2}px)`;
      }
      if ($maxVal.current) {
        $maxVal.current.style.left = `calc(${calcLeftPosition(value.max)}% - ${$maxVal.current.offsetWidth / 2}px)`;
      }
    } else {
      mount.current = true;
    }
  }, [value]);

  const onChangeMin = (e: React.ChangeEvent<HTMLInputElement>) => {
    const newValue = parseInt(e.target.value);
    if (newValue >= value.max) return;
    setValue({
      ...value,
      min: newValue,
    });
    if ($thumbMin.current) {
      $thumbMin.current.style.left = calcLeftPosition(newValue) + '%';
    }
    if ($line.current) {
      $line.current.style.left = calcLeftPosition(newValue) + '%';
      $line.current.style.right = 100 - calcLeftPosition(value.max) + '%';
    }
  };

  const onChangeMax = (e: React.ChangeEvent<HTMLInputElement>) => {
    const newValue = parseInt(e.target.value);
    if (newValue <= value.min) return;
    setValue({
      ...value,
      max: newValue,
    });
    if ($thumbMax.current) {
      $thumbMax.current.style.left = calcLeftPosition(newValue) + '%';
    }
    if ($line.current) {
      $line.current.style.left = calcLeftPosition(value.min) + '%';
      $line.current.style.right = 100 - calcLeftPosition(newValue) + '%';
    }
  };

  const calcLeftPosition = (value: number) => {
    return (100 / (max - min)) * (value - min);
  };

  return (
    <div className="input_range_double_base">
      <div className="margin_bottom_5 height_20 position_relative margin_x_11">
        <span
          className="value_min position_absolute font_size_14 line_height_20"
          style={{ left: 'calc(0% - 5px)' }}
          ref={$minVal}
        >
          {value.min}
        </span>
        <span
          className="value_min position_absolute font_size_14 line_height_20"
          style={{ left: 'calc(100% - 13.5px)' }}
          ref={$maxVal}
        >
          {value.max}
        </span>
      </div>
      <div className="range-slide">
        <div className="slide">
          <div className="line" id="line" style={{ left: '0%', right: '0%' }} ref={$line} />
          <span className="thumb" id="thumbMin" style={{ left: '0%' }} ref={$thumbMin} />
          <span className="thumb" id="thumbMax" style={{ left: '100%' }} ref={$thumbMax} />
        </div>
        <input id="rangeMin" type="range" max={max} min={min} step={step} value={value.min} onChange={onChangeMin} />
        <input id="rangeMax" type="range" max={max} min={min} step={step} value={value.max} onChange={onChangeMax} />
      </div>
      <div className="margin_top_20 width_100_percent display_flex justify_content_sb padding_x_11" ref={$ranges}>
        {listRange.map((range) => {
          if (range % 2 === 0) {
            return <div className="height_10 width_1 bg_grey" key={range} data-id={range}></div>;
          }
          return <div className="height_6 width_1 bg_grey" key={range}></div>;
        })}
      </div>
      <div className="height_18 margin_x_11 position_relative" ref={$numberRanges}>
        {listRange.map((range) => {
          if (range % 2 === 0) {
            return (
              <div
                className="font_size_12 line_height_18 position_absolute translate_-50_0_percent"
                key={range}
                data-id={range}
                style={{ left: `${calcLeftPosition(range * step)}%` }}
              >
                {range * step}
              </div>
            );
          }
        })}
      </div>
    </div>
  );
};

export default InputRangeDouble;
