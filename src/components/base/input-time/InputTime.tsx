import React, { useEffect, useRef, useState } from 'react';
import './InputTime.scss';
import classnames from 'classnames';
import useOutsideClick from '../../../utils/customHooks/useOutsideClick';

interface Props {
  value: string; // format: HH:mma
  date?: string; // todo: check disable những thời gian đã qua hiện tại
  setValue: (..._args: any[]) => void;
  label?: string;
}

const InputTime: React.FC<Props> = (props) => {
  const HOURS = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
  const MINUTES = ['00', '15', '30', '45'];
  const AMPM = ['AM', 'PM'];
  const $hourRef = useRef(null);
  const $inputRef = useRef(null);
  const [hour, setHour] = useState<string>('12');
  const [minute, setMinute] = useState<string>('00');
  const [ampm, setAmpm] = useState<string>('AM');
  const [openDropdown, setOpenDropdown] = useState<boolean>(false);
  const openDropdownRef: any = useRef(null);

  useEffect(() => {
    if (props.value) {
      splitTime();
    }
  }, []);

  useEffect(() => {
    setTime();
  }, [hour, minute, ampm]);

  useEffect(() => {
    openDropdownRef.current = openDropdown;
  }, [openDropdown]);

  useOutsideClick($inputRef, () => {
    if (openDropdownRef.current) {
      setOpenDropdown(false);
    }
  });

  useEffect(() => {
    if (openDropdown) {
      scrollToHourItem(String(hour));
    }
  }, [openDropdown]);

  /**
   * splitTime tách value thành hour, minute, ampm
   */
  const splitTime = () => {
    const splitHour = props.value.split(':');
    if (splitHour.length === 2) {
      const hourNumber = splitHour[0];
      setHour(hourNumber === '00' ? '12' : hourNumber);
      scrollToHourItem(hourNumber);
      let minuteNumber = splitHour[1].match(/(\d+)(.+)/)[1];
      if (Number(minuteNumber) > 0 && Number(minuteNumber) < 15) {
        minuteNumber = '15';
      } else if (Number(minuteNumber) > 15 && Number(minuteNumber) < 30) {
        minuteNumber = '30';
      } else if (Number(minuteNumber) > 30 && Number(minuteNumber) < 45) {
        minuteNumber = '45';
      } else if (Number(minuteNumber) > 45 && Number(minuteNumber) <= 59) {
        minuteNumber = '00';
        let newHour: string | number;
        if (Number(hourNumber) === 12) {
          newHour = 1;
        } else if (Number(hourNumber) === 11) {
          newHour = 12;
        } else {
          newHour = String(Number(hourNumber) + 1);
        }
        if (Number(newHour) < 10) {
          newHour = '0' + newHour;
        }
        scrollToHourItem(String(newHour));
        setHour(String(newHour));
      }
      const ampmVal = splitHour[1].match(/(\d+)(.+)/)[2];
      setMinute(minuteNumber);
      setAmpm(ampmVal);
    }
  };

  /**
   * scrollToHourItem: scroll to giờ được active
   * @param hour giờ muốn scroll tới
   */
  const scrollToHourItem = (hour: string) => {
    const scrollTop = (Number(hour) - 1) * (40 + 8);
    $hourRef.current.scrollTop = scrollTop;
  };

  /**
   * onChangeHour khi thay đổi giờ
   */
  const onChangeHour = (value: string) => {
    setHour(value);
  };

  /**
   * onChangeMinute khi thay đổi phút
   */
  const onChangeMinute = (value: string) => {
    setMinute(value);
  };

  /**
   * onChangeAmpm khi thay đổi am pm
   */
  const onChangeAmpm = (value: string) => {
    setAmpm(value);
  };

  /**
   * setTime set giá trị time
   */
  const setTime = () => {
    const time = hour + ':' + minute + ampm;
    props.setValue(time);
  };

  /**
   * toggleDropdown: đóng mở dropdown
   */
  const toggleDropdown = () => {
    setOpenDropdown(!openDropdown);
  };

  return (
    <div
      className="base_input_time margin_bottom_16 border_width_1 border_color_black border_style_solid border_radius_8
      padding_x_24 height_48 display_flex justify_content_sb position_relative"
      ref={$inputRef}
    >
      <div
        className={classnames(
          { active: openDropdown },
          'select_time position_absolute padding_24 width_312 display_flex border_width_1 border_color_black border_style_solid border_radius_18 bg_white',
        )}
      >
        <div className="width_77 margin_right_17 height_184 overflow_auto hours" ref={$hourRef}>
          {HOURS.map((hourItem: string) => {
            return (
              <div
                className={classnames(
                  {
                    'bg_green color_white border_radius_24': hourItem === hour,
                  },
                  'hours_item height_40 margin_bottom_8 width_100_percent display_flex align_items_center justify_content_center cursor_pointer',
                )}
                key={hourItem}
                onClick={() => {
                  onChangeHour(hourItem);
                }}
              >
                <span>{hourItem}</span>
              </div>
            );
          })}
        </div>
        <div className="width_77 margin_right_17">
          {MINUTES.map((minuteItem: string) => {
            return (
              <div
                key={minuteItem}
                className={classnames(
                  {
                    'bg_green color_white border_radius_24': minuteItem === minute,
                  },
                  'hours_item height_40 margin_bottom_8 width_100_percent display_flex align_items_center justify_content_center cursor_pointer',
                )}
                onClick={() => {
                  onChangeMinute(minuteItem);
                }}
              >
                <span>{minuteItem}</span>
              </div>
            );
          })}
        </div>
        <div className="width_77">
          {AMPM.map((item: string) => {
            return (
              <div
                key={item}
                className={classnames(
                  {
                    'bg_green color_white border_radius_24': item === ampm,
                  },
                  'hours_item height_40 margin_bottom_8 width_100_percent display_flex align_items_center justify_content_center cursor_pointer',
                )}
                onClick={() => {
                  onChangeAmpm(item);
                }}
              >
                <span>{item}</span>
              </div>
            );
          })}
        </div>
      </div>
      <div className="display_flex align_items_center">
        {props.label ? (
          <label className="display_flex align_items_center font_size_12 line_height_18 color_black width_95 flex_none width_xs_80 opacity_0_5">
            Thời gian:
          </label>
        ) : null}
        <div className="width_100_percent display_flex" onClick={toggleDropdown}>
          <input
            value={props.value}
            className="border_none font_size_16 font_weight_500 line_height_28 cursor_pointer width_100_percent"
            disabled
          />
        </div>
      </div>
      <div className="display_flex align_items_center cursor_pointer no_select" onClick={toggleDropdown}>
        <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path
            fillRule="evenodd"
            clipRule="evenodd"
            d="M14.334 0C17.723 0 20 2.378 20 5.916V14.084C20 17.622 17.723 20 14.333 20H5.665C2.276 20 0 17.622 0 14.084V5.916C0 2.378 2.276 0 5.665 0H14.334ZM14.334 1.5H5.665C3.135 1.5 1.5 3.233 1.5 5.916V14.084C1.5 16.767 3.135 18.5 5.665 18.5H14.333C16.864 18.5 18.5 16.767 18.5 14.084V5.916C18.5 3.233 16.865 1.5 14.334 1.5ZM9.9991 4.8836C10.4131 4.8836 10.7491 5.2196 10.7491 5.6336V9.5696L13.7751 11.3726C14.1301 11.5856 14.2471 12.0456 14.0351 12.4016C13.8941 12.6366 13.6451 12.7676 13.3901 12.7676C13.2591 12.7676 13.1271 12.7336 13.0061 12.6626L9.6151 10.6396C9.3891 10.5036 9.2491 10.2586 9.2491 9.9956V5.6336C9.2491 5.2196 9.5851 4.8836 9.9991 4.8836Z"
            fill="black"
          />
        </svg>
      </div>
    </div>
  );
};

export default InputTime;
