import React, { useRef } from 'react';
import './RoundGraph.scss';

interface Props {
  prePayPer: number; // orange
  historicalCostPer: number; // blue
  profitPayPer: number; // green
  totalPrice: string;
}

const RoundGraph: React.FC<Props> = (props) => {
  const $graph = useRef(null);
  const perimeter = 87 * 3.14 * 2;
  const prePayLength = (props.prePayPer + props.historicalCostPer) * perimeter;
  const historicalCostLength = props.historicalCostPer * perimeter;

  return (
    <div className="round_graph_base position_absolute" ref={$graph}>
      <figure className="figure">
        <svg role="img" xmlns="http://www.w3.org/2000/svg">
          <circle className="circle-green" />
          <circle
            className="circle-orange"
            style={{
              strokeDasharray: `${prePayLength}px calc(${perimeter}px - ${prePayLength}px)`,
              strokeDashoffset: `${prePayLength}px`,
            }}
          />
          <circle
            className="circle-blue"
            style={{
              strokeDasharray: `${historicalCostLength}px calc(${perimeter}px - ${historicalCostLength}px)`,
              strokeDashoffset: `${historicalCostLength}px`,
            }}
          />
          <circle className="circle-green-overlay" />
        </svg>
      </figure>
      <div className="position_absolute left_50_percent top_50_percent translate_-50_-50_percent">
        <span className="font_size_24 font_weight_600 line_height_36">{props.totalPrice}</span>
      </div>
    </div>
  );
};

export default RoundGraph;
