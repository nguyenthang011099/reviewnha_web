import React, { useEffect, useRef, useState } from 'react';
import classnames from 'classnames';
import useOutsideClick from '../../../utils/customHooks/useOutsideClick';
import './InputRadio.scss';
import { OptionInputRadio } from '../../../types/type.base';

interface Props {
  value: OptionInputRadio;
  setValue: (..._args: any[]) => void;
  label?: string;
  className?: string;
  options: OptionInputRadio[];
  inputEditable?: boolean;
  placeholder?: string;
  isWarning?: boolean;
  setIsWarning?: (..._args: any[]) => void;
  disabledClick?: boolean;
  valueFontSize?: number;
  isSearch?: boolean;
}

const InputRadio: React.FC<Props> = (props) => {
  const [isOpen, setIsOpen] = useState<boolean>(false);
  const $selectRef = useRef(null);
  const isOpenCached: any = useRef(null);

  useEffect(() => {
    isOpenCached.current = isOpen;
  }, [isOpen]);

  useOutsideClick($selectRef, () => {
    if (isOpenCached.current) {
      setIsOpen(false);
    }
  });

  return (
    <div className="input_radio_base width_100_percent position_relative" ref={$selectRef}>
      <div
        className={classnames(
          {
            [props.className]: props.className,
            focus: isOpen,
            warning: props.isWarning,
          },
          `input_select display_flex align_items_center justify_content_sb height_48 border_color_black border_style_solid border_radius_8 border_width_1 padding_x_24 cursor_pointer`,
        )}
        onClick={() => {
          props.isWarning && props.setIsWarning && props.setIsWarning(false);
          if (!props.disabledClick) {
            if (props.inputEditable) {
              setIsOpen(true);
            } else {
              setIsOpen(!isOpen);
            }
          }
        }}
      >
        <div className="display_flex align_items_center width_100_percent">
          {props.label ? (
            <span className="width_97 font_size_14 line_height_24 opacity_0_5 display_block flex_none width_xs_80">
              {props.label}
            </span>
          ) : null}
          <input
            className={`font_size_${
              props.valueFontSize ? props.valueFontSize : 16
            } line_height_18 border_none width_100_percent ${props.inputEditable ? '' : 'cursor_pointer'}`}
            value={props.value.title}
            disabled={props.inputEditable ? false : true}
            placeholder={props.placeholder}
            onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
              if (props.inputEditable) {
                props.setValue({
                  value: '',
                  title: e.target.value,
                });
              }
            }}
          ></input>
        </div>
        <div className="display_flex cursor_pointer">
          {props.isWarning ? (
            <svg width="20" height="18" viewBox="0 0 20 18" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path
                d="M8.6279 0.353094C9.98767 -0.40098 11.7174 0.0944694 12.4773 1.44209L19.746 14.0572C19.906 14.4338 19.976 14.7399 19.996 15.058C20.036 15.8012 19.776 16.5236 19.2661 17.0795C18.7562 17.6334 18.0663 17.9604 17.3164 18H2.6789C2.36896 17.9812 2.05901 17.9108 1.76906 17.8018C0.319301 17.2172 -0.380581 15.5723 0.20932 14.1464L7.52809 1.43317C7.77804 0.986278 8.15798 0.600818 8.6279 0.353094ZM9.99767 12.2726C9.51775 12.2726 9.11782 12.669 9.11782 13.1456C9.11782 13.6202 9.51775 14.0176 9.99767 14.0176C10.4776 14.0176 10.8675 13.6202 10.8675 13.1347C10.8675 12.66 10.4776 12.2726 9.99767 12.2726ZM9.99767 6.09039C9.51775 6.09039 9.11782 6.47585 9.11782 6.95248V9.75573C9.11782 10.2314 9.51775 10.6287 9.99767 10.6287C10.4776 10.6287 10.8675 10.2314 10.8675 9.75573V6.95248C10.8675 6.47585 10.4776 6.09039 9.99767 6.09039Z"
                fill="#D93C23"
              />
            </svg>
          ) : (
            <svg
              width="20"
              height="20"
              viewBox="0 0 20 20"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
              className={classnames({
                rotate_180: isOpen,
              })}
              style={{ transition: 'all 0.3s' }}
            >
              <path
                fillRule="evenodd"
                clipRule="evenodd"
                d="M10 0C15.514 0 20 4.486 20 10C20 15.514 15.514 20 10 20C4.486 20 0 15.514 0 10C0 4.486 4.486 0 10 0ZM10 1.5C5.313 1.5 1.5 5.313 1.5 10C1.5 14.687 5.313 18.5 10 18.5C14.687 18.5 18.5 14.687 18.5 10C18.5 5.313 14.687 1.5 10 1.5ZM14.001 8.0259C14.294 8.3179 14.295 8.7929 14.003 9.0869L10.531 12.5729C10.419 12.6865 10.2737 12.7598 10.1182 12.7845L10 12.7939C9.801 12.7939 9.609 12.7149 9.469 12.5729L5.998 9.0869C5.705 8.7929 5.707 8.3179 6 8.0259C6.294 7.7339 6.769 7.7339 7.061 8.0279L10 10.9819L12.94 8.0279C13.232 7.7339 13.707 7.7339 14.001 8.0259Z"
                fill="black"
              />
            </svg>
          )}
        </div>
      </div>
      <div
        className={classnames(
          {
            active: isOpen,
          },
          `drop_down position_absolute bg_white border_radius_8 min_width_210 left_-1 padding_y_25 
          border_width_0 border_top_width_1 border_left_width_1 border_color_black border_style_solid`,
        )}
        style={{
          boxShadow: '1px 1px 2px 1px rgba(23, 23, 23, 0.16)',
        }}
      >
        <div className="drop_down_wrapper padding_x_16 overflow_auto width_100_percent">
          {!props.options?.find((item) => !item.hidden) && props.isSearch ? (
            <div className="option_item display_flex align_items_center cursor_pointermargin_bottom_20 no_select justify_content_sb">
              <span className="font_weight_600 font_size_14 line_height_17">Không có kết quả</span>
            </div>
          ) : (
            props.options.map((option: OptionInputRadio) => {
              if (!option.hidden) {
                return (
                  <div
                    className="option_item display_flex align_items_center cursor_pointer bg_whitesmoke-hover margin_bottom_20 no_select justify_content_sb"
                    key={option.value}
                    onClick={() => {
                      props.setValue(option);
                      setIsOpen(false);
                    }}
                  >
                    <span className="font_weight_600 font_size_14 line_height_17">{option.title}</span>
                    {props.value.value !== option.value ? (
                      <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path
                          fillRule="evenodd"
                          clipRule="evenodd"
                          d="M10 0C15.514 0 20 4.486 20 10C20 15.514 15.514 20 10 20C4.486 20 0 15.514 0 10C0 4.486 4.486 0 10 0ZM10 1.5C5.313 1.5 1.5 5.313 1.5 10C1.5 14.687 5.313 18.5 10 18.5C14.687 18.5 18.5 14.687 18.5 10C18.5 5.313 14.687 1.5 10 1.5Z"
                          fill="#3B4144"
                        />
                      </svg>
                    ) : (
                      <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path
                          fillRule="evenodd"
                          clipRule="evenodd"
                          d="M10 0C15.514 0 20 4.486 20 10C20 15.514 15.514 20 10 20C4.486 20 0 15.514 0 10C0 4.486 4.486 0 10 0ZM10 1.5C5.313 1.5 1.5 5.313 1.5 10C1.5 14.687 5.313 18.5 10 18.5C14.687 18.5 18.5 14.687 18.5 10C18.5 5.313 14.687 1.5 10 1.5Z"
                          fill="#3B4144"
                        />
                        <path
                          d="M16.5 10C16.5 13.5899 13.5899 16.5 10 16.5C6.41015 16.5 3.5 13.5899 3.5 10C3.5 6.41015 6.41015 3.5 10 3.5C13.5899 3.5 16.5 6.41015 16.5 10Z"
                          fill="#3B4144"
                        />
                      </svg>
                    )}
                  </div>
                );
              }
              return null;
            })
          )}
        </div>
      </div>
    </div>
  );
};

export default InputRadio;
