import React, { useEffect, useRef } from 'react';
import './InputRange.scss';

interface Props {
  min: number;
  max: number;
  step: number;
  value: number;
  setValue: (..._args: any) => void;
  label?: string;
}

const InputRange: React.FC<Props> = (props) => {
  const $inputRef = useRef(null);

  useEffect(() => {
    const $inputEl = $inputRef.current;
    if ($inputEl) {
      // set các giá trị để tính toán process range bằng css
      $inputEl.style.setProperty('--val', +$inputEl.value);
      $inputEl.style.setProperty('--max', +$inputEl.max);
      $inputEl.style.setProperty('--min', +$inputEl.min);
    }
  }, []);

  useEffect(() => {
    const styleInput = $inputRef.current.style;
    if (styleInput.getPropertyValue('--val') !== props.value) {
      styleInput.setProperty('--val', props.value);
    }
  }, [props.value]);

  /**
   * changeValue: sự kiện input
   */
  const changeValue = (e: React.ChangeEvent<HTMLInputElement>) => {
    $inputRef.current.style.setProperty('--val', e.target.value);
    props.setValue(e.target.value);
  };

  return (
    <div className="base_input_range width_100_percent">
      {props.label ? (
        <div className="flex_none display_flex margin_bottom_10">
          <span className="font_size_14 line_height_17 font_weight_600">{props.label}</span>
        </div>
      ) : null}
      <div className="height_32 width_100_percent display_flex align_items_center ">
        <input
          type="range"
          min={props.min}
          max={props.max}
          step={props.step}
          value={props.value}
          className="display_block width_401 margin_right_14 width_xs_100_percent"
          ref={$inputRef}
          onChange={changeValue}
        />
        <div className="flex_1 display_flex justify_content_center border_radius_16 border_color_black border_style_solid border_width_1 align_items_center height_100_percent flex_xs_none width_xs_60">
          <span className="font_size_14 font_weight_600 line_height_17">{props.value}</span>
        </div>
      </div>
    </div>
  );
};

export default InputRange;
