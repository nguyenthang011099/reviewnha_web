import { useEffect, useRef, useState } from 'react';
import './ModalNotification.scss';

interface Props {
  title: string; // title hiển thị
  type: 'success' | 'error'; // màu success hay error
  isOpen: boolean; // mở hay tắt
  reset: () => any; // hàm reset biến isOpen thành fail: () => { setIsOpen(false) }
}

const ModalNotification: React.FC<Props> = (props) => {
  const [isOpen, setIsOpen] = useState(false);
  const isAnimated = useRef(true);
  const timeout = useRef(null);

  useEffect(() => {
    if (props.isOpen) {
      if (isAnimated.current) {
        isAnimated.current = false;
        setIsOpen(true);
      }
      clearTimeout(timeout.current);
      timeout.current = setTimeout(() => {
        props.reset();
        setIsOpen(false);
        isAnimated.current = true;
      }, 3000);
    }
    return () => {
      clearTimeout(timeout.current);
    };
  }, [props.isOpen]);

  return (
    <div
      className="modal_notification position_fixed top_20 right_50_percent padding_x_20 padding_y_10 border_radius_4 z_index_1001 translate_50_0_percent"
      style={{
        background: `${props.type === 'success' ? '#077064' : '#cc212c'}`,
        boxShadow: `${props.type === 'success' ? '0px 0px 5px #01645a' : '0px 0px 5px #cc212c'}`,
        animation: `${isOpen ? 'fadeOut 1s 2s both' : 'none'}`,
      }}
    >
      <div>
        <span className="color_white white_space_xs_nowrap">{props.title}</span>
      </div>
    </div>
  );
};

export default ModalNotification;
