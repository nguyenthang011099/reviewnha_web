import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../reducers';
import { closeNotification } from '../../reducers/notification/actions';
import ModalNotification from './ModalNotification';

const Notification: React.FC = () => {
  const type: 'success' | 'error' = useSelector((state: RootState) => state.notification.type);
  const isOpen: boolean = useSelector((state: RootState) => state.notification.isOpen);
  const reset: (...args: any) => void = useSelector((state: RootState) => state.notification.reset);
  const title: string = useSelector((state: RootState) => state.notification.title);
  const dispatch = useDispatch();

  return (
    <div>
      <ModalNotification
        title={title}
        isOpen={isOpen}
        reset={() => {
          reset && reset();
          dispatch(closeNotification());
        }}
        type={type}
      ></ModalNotification>
    </div>
  );
};

export default Notification;
