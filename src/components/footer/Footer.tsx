import React from 'react';
import logo from '../../images/logo.png';

const Footer: React.FC = () => {
  return (
    <footer className="border_width_0 border_top_width_1 border_color_gray border_style_solid">
      <div className="container  padding_y_56 padding_x_xs_22 padding_y_xs_24">
        <div className="display_flex margin_bottom_54 flex_wrap margin_bottom_xs_30">
          <div className="col_3 col_xs_12 margin_bottom_xs_24">
            <h2 className="margin_0 margin_bottom_20 font_size_16 font_weight_600">VỀ CHÚNG TÔI</h2>
            <div className="">
              <p className="margin_0 margin_bottom_12 font_size_14">
                <a href="http://localhost:3003/tin-tuc/gioi-thieu-n1.html" target="_blank">
                  Giới thiệu
                </a>
              </p>
              {/* <p className="margin_0 margin_bottom_12 font_size_14">Đối Tác</p>
              <p className="margin_0 margin_bottom_12 font_size_14">Báo chí</p> */}
              <p className="margin_0 margin_bottom_12 font_size_14">
                <a href="http://localhost:3003/tin-tuc/chinh-sach-dieu-khoan-n4.html" target="_blank">
                  Chính sách & điều khoản
                </a>
              </p>
              {/* <p className="margin_0 margin_bottom_12 font_size_14">Phản hồi</p> */}
            </div>
          </div>
          <div className="col_3 col_xs_12 margin_bottom_xs_24">
            <h2 className="margin_0 margin_bottom_20 font_size_16 font_weight_600">REVIEWNHA PREMIUM</h2>
            {/* <p className="margin_0 margin_bottom_12 font_size_14">Cộng đồng chuyên gia</p>
            <p className="margin_0 margin_bottom_12 font_size_14">Học viện Reviewnha</p> */}
            <p className="margin_0 margin_bottom_12 font_size_14">
              <a href="http://localhost:3003/tin-tuc/reviewnha-studio-n8.html" target="_blank">
                Reviewnha Studio
              </a>
            </p>
            <p className="margin_0 margin_bottom_12 font_size_14">
              <a href="http://localhost:3003/tin-tuc/reviewnha-media-n9.html" target="_blank">
                Reviewnha Media
              </a>
            </p>
          </div>
          <div className="col_3 col_xs_12 margin_bottom_xs_24">
            <h2 className="margin_0 margin_bottom_20 font_size_16 font_weight_600">LIÊN HỆ DỊCH VỤ</h2>
            <p className="margin_0 margin_bottom_12 font_size_14">
              <a href="http://localhost:3003/tin-tuc.html" target="_blank">
                Tin tức
              </a>
            </p>
            <p className="margin_0 margin_bottom_12 font_size_14">Hotline 24/7: 0869.319.688</p>
            <p className="margin_0 margin_bottom_12 font_size_14">Email: id.reviewnha@gmail.com</p>
          </div>
          <div className="col_3 col_xs_12">
            <h2 className="margin_0 margin_bottom_20 font_size_16 font_weight_600">LIÊN KẾT</h2>
            <div className="margin_0 margin_bottom_12 font_size_14 display_flex align_items_center">
              <span className="margin_right_24">Mạng xã hội:</span>
              <a href="https://www.facebook.com/Reviewnhvn/" target="_blank">
                <svg
                  width={10}
                  height={19}
                  viewBox="0 0 10 19"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                  className="margin_right_18"
                >
                  <path
                    d="M9.152 10.6532L9.66 7.34419H6.485V5.19619C6.46293 4.94911 6.49669 4.70023 6.58377 4.46796C6.67085 4.23569 6.80904 4.02596 6.9881 3.85429C7.16716 3.68262 7.38252 3.5534 7.61826 3.47618C7.85399 3.39897 8.10407 3.37573 8.35 3.40819H9.791V0.591188C8.94426 0.45424 8.08865 0.379373 7.231 0.367188C4.616 0.367188 2.907 1.95219 2.907 4.82119V7.34319H0V10.6522H2.907V18.6522H6.485V10.6522L9.152 10.6532Z"
                    fill="#888888"
                  />
                </svg>
              </a>
              <a href="https://www.youtube.com/channel/UC_eq-FYxlaEQz1kqjvL8LCA?view_as=subscriber" target="_blank">
                <svg
                  width={20}
                  height={15}
                  viewBox="0 0 20 15"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                  className="margin_right_18"
                >
                  <path
                    d="M19.097 2.79932C18.9868 2.38418 18.7695 2.00529 18.4667 1.70062C18.164 1.39594 17.7865 1.17617 17.372 1.06332C15.85 0.65332 9.75004 0.65332 9.75004 0.65332C9.75004 0.65332 3.65004 0.65332 2.13004 1.06332C1.71616 1.17662 1.33928 1.3966 1.03709 1.70125C0.734899 2.0059 0.517984 2.38454 0.408035 2.79932C0.126744 4.35839 -0.00983697 5.94011 3.52675e-05 7.52432C-0.0100837 9.10986 0.126498 10.6929 0.408035 12.2533C0.520243 12.6634 0.738765 13.0366 1.04149 13.3351C1.34422 13.6336 1.72042 13.8469 2.13204 13.9533C3.65304 14.3633 9.75204 14.3633 9.75204 14.3633C9.75204 14.3633 15.852 14.3633 17.372 13.9533C17.7838 13.847 18.1602 13.6338 18.4632 13.3353C18.7661 13.0368 18.9847 12.6635 19.097 12.2533C19.3783 10.6943 19.5149 9.11253 19.505 7.52832C19.5149 5.94411 19.3783 4.36239 19.097 2.80332V2.79932ZM7.75704 10.4243V4.62432L12.857 7.52432L7.75704 10.4243Z"
                    fill="#888888"
                  />
                </svg>
              </a>
              <a href="#" target="_blank">
                <svg width={17} height={17} viewBox="0 0 17 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path
                    d="M8.99622 4.55233C8.18531 4.55233 7.39262 4.79279 6.71838 5.2433C6.04414 5.69381 5.51863 6.33415 5.20831 7.08332C4.89799 7.8325 4.8168 8.65687 4.975 9.4522C5.1332 10.2475 5.52368 10.9781 6.09708 11.5515C6.67047 12.1249 7.40102 12.5153 8.19635 12.6735C8.99167 12.8317 9.81604 12.7506 10.5652 12.4402C11.3144 12.1299 11.9547 11.6044 12.4052 10.9302C12.8558 10.2559 13.0962 9.46323 13.0962 8.65233C13.0962 8.11391 12.9902 7.58076 12.7841 7.08332C12.5781 6.58589 12.2761 6.13391 11.8954 5.75319C11.5146 5.37247 11.0627 5.07046 10.5652 4.86442C10.0678 4.65838 9.53464 4.55233 8.99622 4.55233ZM8.99622 11.3233C8.46854 11.3233 7.95271 11.1669 7.51396 10.8737C7.07521 10.5805 6.73324 10.1638 6.53131 9.67633C6.32937 9.18881 6.27654 8.65237 6.37948 8.13482C6.48243 7.61728 6.73653 7.14189 7.10966 6.76877C7.48278 6.39564 7.95817 6.14154 8.47572 6.03859C8.99326 5.93565 9.5297 5.98848 10.0172 6.19042C10.5047 6.39235 10.9214 6.73431 11.2146 7.17307C11.5077 7.61182 11.6642 8.12765 11.6642 8.65533C11.6629 9.36252 11.3814 10.0404 10.8813 10.5404C10.3813 11.0405 9.70341 11.322 8.99622 11.3233ZM14.2242 4.38433C14.2242 4.5736 14.1681 4.75863 14.0629 4.91601C13.9578 5.07338 13.8083 5.19605 13.6334 5.26848C13.4586 5.34091 13.2662 5.35986 13.0805 5.32294C12.8949 5.28601 12.7244 5.19487 12.5905 5.06103C12.4567 4.92719 12.3655 4.75667 12.3286 4.57103C12.2917 4.38539 12.3106 4.19297 12.3831 4.0181C12.4555 3.84323 12.5782 3.69377 12.7355 3.58861C12.8929 3.48345 13.0779 3.42733 13.2672 3.42733C13.3931 3.42693 13.5177 3.45141 13.6341 3.49935C13.7504 3.54729 13.8561 3.61775 13.9452 3.70668C14.0342 3.79561 14.1047 3.90126 14.1528 4.01755C14.2009 4.13385 14.2255 4.25849 14.2252 4.38433H14.2242ZM16.9422 5.35533C16.9686 4.11087 16.5041 2.90602 15.6492 2.00133C14.7428 1.14987 13.5396 0.686243 12.2962 0.709326C10.9752 0.634326 7.01422 0.634326 5.69622 0.709326C4.45208 0.683899 3.24744 1.14659 2.34022 1.99833C1.48668 2.9039 1.02243 4.10815 1.04722 5.35233C0.972217 6.67333 0.972217 10.6343 1.04722 11.9523C1.02081 13.1968 1.48529 14.4016 2.34022 15.3063C3.24708 16.1594 4.45135 16.6245 5.69622 16.6023C7.01722 16.6773 10.9782 16.6773 12.2962 16.6023C13.5407 16.6287 14.7455 16.1642 15.6502 15.3093C16.5023 14.4028 16.9663 13.1992 16.9432 11.9553C17.0182 10.6343 17.0182 6.67733 16.9432 5.35533H16.9422ZM15.2362 13.3733C15.1004 13.7175 14.8954 14.0302 14.6337 14.2918C14.3721 14.5535 14.0594 14.7586 13.7152 14.8943C12.6612 15.3153 10.1612 15.2153 8.99622 15.2153C7.83122 15.2153 5.32822 15.3083 4.27822 14.8943C3.934 14.7586 3.62136 14.5535 3.35971 14.2918C3.09806 14.0302 2.89299 13.7175 2.75722 13.3733C2.34022 12.3153 2.43622 9.81533 2.43622 8.65533C2.43622 7.49533 2.34322 4.98733 2.75722 3.93733C2.89298 3.59283 3.09816 3.27994 3.35999 3.0181C3.62183 2.75627 3.93472 2.55109 4.27922 2.41533C5.33322 1.99833 7.83322 2.09533 8.99622 2.09533C10.1592 2.09533 12.6642 2.00233 13.7142 2.41633C14.0586 2.55198 14.3715 2.757 14.6333 3.01866C14.8951 3.28031 15.1003 3.59301 15.2362 3.93733C15.6542 4.99133 15.5572 7.49133 15.5572 8.65533C15.5572 9.81933 15.6542 12.3233 15.2362 13.3733Z"
                    fill="#888888"
                  />
                </svg>
              </a>
            </div>
            {/* <p className="margin_0 margin_bottom_12 font_size_14">Đăng ký nhận thông tin từ Reviewnha</p> */}
          </div>
        </div>
        <div className="display_flex align_items_center margin_bottom_22">
          <a href="https://reviewnha.vn" target="_blank">
            <img src={logo} alt="reviewnha" className="height_28 margin_right_100 margin_right_xs_0"></img>
          </a>
          <div className="height_1 flex_1 bg_grey display_xs_none"></div>
        </div>
        <div className="display_flex flex_wrap">
          <div className="col_6 col_xs_12 margin_bottom_xs_24">
            <h2 className="margin_0 margin_bottom_9 font_size_14 font_weight_600">CÔNG TY CỔ PHẦN COCOCO VIỆT NAM</h2>
            <p className="margin_0 margin_bottom_5 font_size_14">
              Địa chỉ: Ô S25B Toà CT3 The Pride, KĐT An Hưng, La Khê, Hà Đông, Hà Nội
            </p>
            <p className="margin_0 margin_bottom_5 font_size_14">Chịu trách nhiệm nội dung: Ông Đặng Vũ Hiệp</p>
            <p className="margin_0 margin_bottom_5 font_size_14">
              Quy chế, quy định giao dịch có hiệu lực từ 01/03/2020
            </p>
          </div>
          <div className="col_6 col_xs_12 ">
            <p className="margin_0 font_size_14 margin_bottom_5">Copyright ©2020 All rights reserved by COCOCO</p>
            <p className="margin_0 font_size_14 margin_bottom_5">
              Ghi rõ nguồn "Reviewnha.vn" khi phát hành lại thông tin từ website này.
            </p>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
