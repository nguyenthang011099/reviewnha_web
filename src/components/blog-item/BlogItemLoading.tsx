import React from 'react';

const BlogItemLoading: React.FC = () => {
  return (
    <a className="blog_item display_flex flex_direction_xs_col_reverse">
      <div className="image display_xs_flex">
        <div className="width_336 height_220 object_fit_cover border_radius_24 width_xs_161 height_xs_105 loading_placeholder"></div>
        <div className="flex_1 height_xs_105 loading_placeholder border_radius_5 display_none display_xs_block  margin_left_xs_16"></div>
      </div>
      <div className="margin_left_24 padding_top_20 margin_left_xs_0 margin_bottom_xs_8 padding_top_xs_0 flex_1">
        <div className="height_27 width_100_percent border_radius_5 loading_placeholder"></div>
        <div className="margin_top_4 display_flex align_items_center height_22 width_150 border_radius_5 loading_placeholder width_xs_100_percent"></div>
        <div className="margin_top_8 height_50 width_100_percent border_radius_5 loading_placeholder display_xs_none"></div>
      </div>
    </a>
  );
};

export default BlogItemLoading;
