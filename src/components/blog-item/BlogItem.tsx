import Link from 'next/link';
import React from 'react';
import { Blog } from '../../types/type.blog';
import { formatDateBlog } from '../../utils/format-date-blog';

interface Props {
  blog: Blog;
}

const BlogItem: React.FC<Props> = (props) => {
  return (
    <Link
      href={{ pathname: '/blog-detail-page', query: { slug: props.blog.slug } }}
      as={`/blogs/${props.blog?.category?.slug || ''}/${props.blog.slug}`}
    >
      <a className="blog_item display_flex flex_direction_xs_col_reverse">
        <div className="image display_xs_flex position_relative">
          <svg
            width={24}
            height={24}
            viewBox="0 0 24 24"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            className="position_absolute right_26 top_26 left_xs_18 bottom_xs_18 top_xs_unset right_xs_unset"
          >
            <path
              d="M8.27977 2.50015C8.90977 2.51931 9.51977 2.62931 10.1108 2.83031H10.1698C10.2098 2.84931 10.2398 2.87031 10.2598 2.88931C10.4808 2.96031 10.6898 3.04031 10.8898 3.15031L11.2698 3.32031C11.4198 3.40031 11.5998 3.54931 11.6998 3.61031C11.7998 3.66931 11.9098 3.73031 11.9998 3.79931C13.1108 2.95031 14.4598 2.49031 15.8498 2.50015C16.4808 2.50015 17.1108 2.58931 17.7098 2.79031C21.4008 3.99031 22.7308 8.04031 21.6198 11.5803C20.9898 13.3893 19.9598 15.0403 18.6108 16.3893C16.6798 18.2593 14.5608 19.9193 12.2798 21.3493L12.0298 21.5003L11.7698 21.3393C9.48077 19.9193 7.34977 18.2593 5.40077 16.3793C4.06077 15.0303 3.02977 13.3893 2.38977 11.5803C1.25977 8.04031 2.58977 3.99031 6.32077 2.76931C6.61077 2.66931 6.90977 2.59931 7.20977 2.56031H7.32977C7.61077 2.51931 7.88977 2.50015 8.16977 2.50015H8.27977ZM17.1898 5.66031C16.7798 5.51931 16.3298 5.74031 16.1798 6.16031C16.0398 6.58031 16.2598 7.04031 16.6798 7.18931C17.3208 7.42931 17.7498 8.06031 17.7498 8.75931V8.79031C17.7308 9.01931 17.7998 9.24031 17.9398 9.41031C18.0798 9.58031 18.2898 9.67931 18.5098 9.70031C18.9198 9.68931 19.2698 9.36031 19.2998 8.93931V8.82031C19.3298 7.41931 18.4808 6.15031 17.1898 5.66031Z"
              fill="white"
            />
          </svg>
          <img
            src={props.blog.thumbnail}
            alt={props.blog.title}
            className="width_336 height_220 object_fit_cover border_radius_24 width_xs_161 height_xs_105"
          />
          <div className="margin_left_8 display_xs_flex flex_1 align_items_center display_none">
            <span className="font_size_14 line_height_21 opacity_0_5 text_over_flow_4 text_align_left">
              {props.blog.description}
            </span>
          </div>
        </div>
        <div className="margin_left_24 padding_top_20 margin_left_xs_0 margin_bottom_xs_8 padding_top_xs_0">
          <h2 className="margin_y_0 font_size_18 line_height_27 font_weight_600 text_over_flow_2 font_size_xs_14 line_height_xs_21">
            {props.blog.title}
          </h2>
          <div className="margin_top_4 display_flex align_items_center">
            {props.blog.category ? (
              <>
                <span className="font_weight_600 font_size_14 line_height_22 color_green white_space_nowrap font_size_xs_13 line_height_xs_15">
                  {props?.blog?.category?.name}
                </span>
                <svg
                  width="3"
                  height="3"
                  viewBox="0 0 3 3"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                  className="margin_x_8 "
                >
                  <circle cx="1.5" cy="1.5" r="1.5" fill="#3B4144" />
                </svg>
              </>
            ) : null}
            <span className="font_size_15 line_height_22 white_space_nowrap font_size_xs_13 line_height_xs_15">
              {props?.blog?.authorName?.length > 15
                ? props?.blog?.authorName?.substr(0, 15) + '...'
                : props?.blog?.authorName}
            </span>
            <svg
              width="3"
              height="3"
              viewBox="0 0 3 3"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
              className="margin_x_8 "
            >
              <circle cx="1.5" cy="1.5" r="1.5" fill="#3B4144" />
            </svg>
            <span className="font_size_15 line_height_22 opacity_0_5 white_space_nowrap font_size_xs_13 line_height_xs_15">
              {formatDateBlog(props.blog.publishedAt)}
            </span>
          </div>
          <div className="margin_top_8 display_xs_none">
            <span className="font_size_16 line_height_28 opacity_0_5 text_over_flow_3">{props.blog.description}</span>
          </div>
        </div>
      </a>
    </Link>
  );
};

export default BlogItem;
