import React, { useEffect, useRef } from 'react';
import Popup from '../popup/Popup';
import './ImageViewPopup.scss';

interface Props {
  images: {
    url: string;
  }[];
  isOpen: boolean;
  setIsOpen: (_value: boolean) => void;
}

const ImageViewPopup: React.FC<Props> = (props) => {
  const { isOpen, setIsOpen } = props;
  const $scrollImages = useRef(null);

  useEffect(() => {
    if ($scrollImages.current) {
      $scrollImages.current.scrollTop = 0;
    }
  }, [props.images]);

  return (
    <Popup
      className="border_radius_18 height_100_percent margin_top_-50 margin_top_xs_0 popup_view_image"
      isOpen={isOpen}
      setIsOpen={setIsOpen}
      isHiddenExitButton={true}
    >
      <div className="container height_100_percent padding_top_38 display_flex flex_direction_col padding_top_xs_24">
        {/* header */}
        <div className="header text_align_center position_relative margin_bottom_36 margin_x_40 margin_x_xs_18">
          <div className="subtitle position_absolute left_0 display_flex align_items_center">
            <svg width={28} height={28} viewBox="0 0 28 28" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path
                d="M19.7785 0.666748C24.2976 0.666748 27.3334 3.83757 27.3334 8.55571V19.4445C27.3334 24.1626 24.2976 27.3334 19.7772 27.3334H8.22166C3.70261 27.3334 0.666748 24.1626 0.666748 19.4445V8.55571C0.666748 3.83757 3.70261 0.666748 8.22166 0.666748H19.7785ZM21.2489 14.7335C19.8196 13.8418 18.7162 15.094 18.4185 15.4944C18.1316 15.8811 17.8849 16.3076 17.6248 16.7342C16.9892 17.7868 16.2612 19.0004 15.0009 19.7063C13.1694 20.7204 11.779 19.7861 10.7788 19.1064C10.4034 18.8532 10.0387 18.6138 9.67535 18.4542C8.77972 18.0675 7.97392 18.5078 6.77795 20.0269C6.15047 20.8209 5.52835 21.6079 4.89819 22.3922C4.52144 22.8615 4.61127 23.5852 5.11942 23.8989C5.93059 24.3984 6.92007 24.6667 8.03827 24.6667H19.2753C19.9094 24.6667 20.545 24.5801 21.151 24.3819C22.5159 23.9361 23.5992 22.9151 24.165 21.5666C24.6424 20.4328 24.8743 19.0568 24.4278 17.912C24.279 17.5322 24.0564 17.1786 23.744 16.8676C22.9248 16.0544 22.1593 15.2949 21.2489 14.7335ZM9.33189 6.00008C7.4937 6.00008 6.00008 7.49572 6.00008 9.33342C6.00008 11.1711 7.4937 12.6667 9.33189 12.6667C11.1687 12.6667 12.6637 11.1711 12.6637 9.33342C12.6637 7.49572 11.1687 6.00008 9.33189 6.00008Z"
                fill="#3B4144"
              />
            </svg>
            <span className="font_size_18 line_height_27 font_weight_600 margin_left_18">Hình ảnh</span>
          </div>
          <div
            className="close display_flex position_absolute right_0 cursor_pointer"
            onClick={() => {
              setIsOpen(false);
            }}
          >
            <svg width="28" height="28" viewBox="0 0 28 28" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path
                fillRule="evenodd"
                clipRule="evenodd"
                d="M13.9993 0.666656C21.3513 0.666656 27.3327 6.64799 27.3327 14C27.3327 21.352 21.3513 27.3333 13.9993 27.3333C6.64735 27.3333 0.666016 21.352 0.666016 14C0.666016 6.64799 6.64735 0.666656 13.9993 0.666656ZM13.9993 2.66666C7.75002 2.66666 2.66602 7.75066 2.66602 14C2.66602 20.2493 7.75002 25.3333 13.9993 25.3333C20.2487 25.3333 25.3327 20.2493 25.3327 14C25.3327 7.75066 20.2487 2.66666 13.9993 2.66666Z"
                fill="black"
              />
              <path
                fillRule="evenodd"
                clipRule="evenodd"
                d="M10.7737 9.28594C10.3626 8.87487 9.69609 8.87487 9.28501 9.28594C8.87393 9.69702 8.87393 10.3635 9.28501 10.7746L12.5104 14L9.28501 17.2254C8.87393 17.6365 8.87393 18.303 9.28501 18.714C9.69609 19.1251 10.3626 19.1251 10.7737 18.714L13.9991 15.4886L17.2245 18.714C17.6355 19.1251 18.302 19.1251 18.7131 18.714C19.1242 18.303 19.1242 17.6365 18.7131 17.2254L15.4877 14L18.7131 10.7746C19.1242 10.3635 19.1242 9.69702 18.7131 9.28594C18.302 8.87487 17.6355 8.87487 17.2245 9.28594L13.9991 12.5113L10.7737 9.28594Z"
                fill="black"
              />
            </svg>
          </div>
        </div>
        {/* body */}
        <div
          className="margin_x_100 margin_top_26 overflow_auto scroll_bar_width_4 margin_x_xs_18 scroll_bar_width_xs_0 margin_bottom_xs_8"
          ref={$scrollImages}
        >
          {props.images.map((image, idx) => {
            return (
              <div key={idx} className="width_1176 width_xs_100_percent">
                <img
                  src={image.url}
                  alt="image"
                  className="width_100_percent border_radius_18 margin_bottom_56 margin_bottom_xs_12"
                />
              </div>
            );
          })}
        </div>
      </div>
    </Popup>
  );
};

export default ImageViewPopup;
