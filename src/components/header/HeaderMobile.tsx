import Link from 'next/link';
import React, { useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import logo from '../../images/logo_black.png';
import logoRed from '../../images/logo.png';
import { RootState } from '../../reducers';
import { OptionInputRadio } from '../../types/type.base';
import { SellListing } from '../../types/type.sell-listing';
import { Status } from '../../types/type.status';
import classnames from 'classnames';
import { logout, resetStatusAuth, setCurrentUrl } from '../../reducers/auth/actions';
import { useRouter } from 'next/router';
import SellCreatePopup from '../../containers/sell-create-popup/SellCreatePopup';
import { getSellListingPartOne } from '../../reducers/sell-listing/actions';
import './Header.scss';
import notificationApi from '../../api/notification';
import { NotificationUser } from '../../types/type.auth';
import HTMLReactParser from 'html-react-parser';
import dayjs from 'dayjs';
import useOutsideClick from '../../utils/customHooks/useOutsideClick';

const HeaderMobile: React.FC = () => {
  const logoutStatus: Status = useSelector((state: RootState) => state.auth.logoutStatus as Status);
  const dataType = useSelector((state: RootState) => state.sellListing.stepOneData);
  const sellListingActive: SellListing = useSelector((state: RootState) => state.sellListing.sellListingDetail.data);
  const auth = useSelector((state: RootState) => state.auth);
  const dispatch = useDispatch();
  const router = useRouter();
  const [openSidebar, setOpenSidebar] = useState<boolean>(false);
  const $notification = useRef(null);
  const isOpenNotificationVal: any = useRef(null);
  const [isOpenNotification, setIsOpenNotification] = useState<boolean>(false);
  const [notificationList, setNotificationList] = useState<NotificationUser[]>([]);

  useEffect(() => {
    if (!dataType.productTypes?.length || !dataType.transactionTypes?.length) {
      dispatch(getSellListingPartOne());
    }
    getNotifications();
  }, []);

  useEffect(() => {
    if (logoutStatus === 'success') {
      dispatch(resetStatusAuth('logout'));
      router.push({ pathname: '/login-page' }, '/login');
    }
  }, [logoutStatus]);

  useOutsideClick($notification, () => {
    if (isOpenNotificationVal) {
      setIsOpenNotification(false);
    }
  });

  useEffect(() => {
    isOpenNotificationVal.current = isOpenNotification;
  }, [isOpenNotification]);

  const getNotifications = () => {
    auth.authenticated &&
      notificationApi.getList(0, 100).then((res) => {
        if (res.success && res.data?.length) {
          setNotificationList(res.data);
        }
      });
  };

  /**
   * onLogout: logout
   */
  const onLogout = () => {
    dispatch(logout());
    dispatch(
      setCurrentUrl({
        url: window.location.origin + router.asPath,
        pathname: router.pathname,
        query: router.query,
        asPath: router.asPath,
      }),
    );
  };

  const checkActiveSellListingsPage = (type: string) => {
    if (
      (router.pathname === '/sell-listings-page' && router.query.transactionType === type) ||
      (router.pathname === '/sell-detail-page' && type === sellListingActive?.typeTransaction?.id)
    ) {
      return true;
    }
    return false;
  };

  return (
    <div
      className="container height_75 display_flex align_items_center justify_content_sb padding_x_xs_22 header_mobile
    position_fixed top_0 left_0 width_100_vw box_shadow_primary bg_white z_index_999"
    >
      <div className="display_flex align_items_center">
        <svg
          width="24"
          height="24"
          viewBox="0 0 24 24"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
          onClick={() => {
            setOpenSidebar(true);
          }}
        >
          <rect x="2" y="2" width="16" height="4" rx="2" fill="#3B4144" />
          <rect x="9" y="10" width="13" height="4" rx="2" fill="#3B4144" />
          <rect x="2" y="18" width="16" height="4" rx="2" fill="#3B4144" />
        </svg>
        <div className="display_flex margin_left_26">
          <img src={logoRed} alt="logo" className="height_22  "></img>
        </div>
      </div>
      <div className="display_flex align_items_center">
        {auth.authenticated ? (
          <div className="position_relative" ref={$notification}>
            <svg
              width={24}
              height={24}
              viewBox="0 0 24 24"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
              className="display_flex"
              onClick={() => {
                setIsOpenNotification(!isOpenNotification);
              }}
            >
              <g clipPath="url(#clip0)">
                <path
                  d="M9.96318 19.2279C10.4631 19.1222 13.5093 19.1222 14.0092 19.2279C14.4366 19.3266 14.8987 19.5573 14.8987 20.0608C14.8738 20.5402 14.5926 20.9653 14.204 21.2352C13.7001 21.628 13.1088 21.8767 12.4906 21.9664C12.1487 22.0107 11.8128 22.0117 11.4828 21.9664C10.8636 21.8767 10.2723 21.628 9.76938 21.2342C9.37978 20.9653 9.09852 20.5402 9.07367 20.0608C9.07367 19.5573 9.53582 19.3266 9.96318 19.2279ZM12.0452 2C14.1254 2 16.2502 2.98702 17.5125 4.62466C18.3314 5.67916 18.7071 6.73265 18.7071 8.3703V8.79633C18.7071 10.0523 19.039 10.7925 19.7695 11.6456C20.3231 12.2741 20.5 13.0808 20.5 13.956C20.5 14.8302 20.2128 15.6601 19.6373 16.3339C18.884 17.1417 17.8215 17.6573 16.7372 17.747C15.1659 17.8809 13.5937 17.9937 12.0005 17.9937C10.4063 17.9937 8.83505 17.9263 7.26375 17.747C6.17846 17.6573 5.11602 17.1417 4.36367 16.3339C3.78822 15.6601 3.5 14.8302 3.5 13.956C3.5 13.0808 3.6779 12.2741 4.23049 11.6456C4.98384 10.7925 5.29392 10.0523 5.29392 8.79633V8.3703C5.29392 6.68834 5.71333 5.58852 6.577 4.51186C7.86106 2.9417 9.91935 2 11.9558 2H12.0452Z"
                  fill="#3B4144"
                />
                {notificationList.find((item) => !item.read) ? (
                  <circle cx="18.4537" cy="18.4534" r="5.04647" fill="#D93C23" stroke="white" />
                ) : null}
              </g>
              <defs>
                <clipPath id="clip0">
                  <rect width={24} height={24} fill="white" />
                </clipPath>
              </defs>
            </svg>
            {/* noti dropdown  */}
            <div
              className={classnames(
                {
                  display_none: !isOpenNotification,
                },
                `popup_notification_mobile position_absolute padding_y_12 border_radius_18 border_width_1 border_color_grey border_style_solid right_0 z_index_1 bg_white width_80_vw`,
              )}
              style={{ top: 'calc(100% + 15px)' }}
            >
              <div className="border_tooltip"></div>
              <div className="margin_bottom_16 padding_x_18">
                <span className="font_weight_600">Thông báo</span>
              </div>
              <div className="max_height_300 overflow_auto scroll_bar_width_4">
                {notificationList?.length ? (
                  notificationList.map((noti: NotificationUser, index: number) => {
                    return (
                      <div
                        className={classnames(
                          { bg_whitesmoke: !noti.read, border_top_width_1: index === 0 },
                          'padding_y_10 border_width_0 border_bottom_width_1 border_color_grey border_style_solid padding_x_18',
                        )}
                        key={noti.createdAt}
                      >
                        <div className="text_over_flow_2 margin_bottom_8 font_size_14">
                          {HTMLReactParser(noti.content)}
                        </div>
                        <div className="text_over_flow_2 font_size_12 opacity_0_5">
                          {dayjs(noti.createdAt).format('HH:mm DD/MM/YYYY')}
                        </div>
                      </div>
                    );
                  })
                ) : (
                  <div className="padding_x_18 font_size_14">
                    <span>Không có thông báo nào</span>
                  </div>
                )}
              </div>
            </div>
          </div>
        ) : null}
        {/* <span className="margin_x_20">
          <svg
            width={20}
            height={20}
            viewBox="0 0 20 20"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            className="display_flex"
          >
            <path
              d="M6.28002 0.500155C6.91002 0.519314 7.52002 0.629314 8.11102 0.830314H8.17002C8.21002 0.849314 8.24002 0.870314 8.26002 0.889314C8.48102 0.960314 8.69002 1.04031 8.89002 1.15031L9.27002 1.32031C9.42002 1.40031 9.60002 1.54931 9.70002 1.61031C9.80002 1.66931 9.91002 1.73031 10 1.79931C11.111 0.950314 12.46 0.490314 13.85 0.500155C14.481 0.500155 15.111 0.589314 15.71 0.790314C19.401 1.99031 20.731 6.04031 19.62 9.58031C18.99 11.3893 17.96 13.0403 16.611 14.3893C14.68 16.2593 12.561 17.9193 10.28 19.3493L10.03 19.5003L9.77002 19.3393C7.48102 17.9193 5.35002 16.2593 3.40102 14.3793C2.06102 13.0303 1.03002 11.3893 0.390017 9.58031C-0.739983 6.04031 0.590017 1.99031 4.32102 0.769314C4.61102 0.669314 4.91002 0.599314 5.21002 0.560314H5.33002C5.61102 0.519314 5.89002 0.500155 6.17002 0.500155H6.28002ZM15.19 3.66031C14.78 3.51931 14.33 3.74031 14.18 4.16031C14.04 4.58031 14.26 5.04031 14.68 5.18931C15.321 5.42931 15.75 6.06031 15.75 6.75931V6.79031C15.731 7.01931 15.8 7.24031 15.94 7.41031C16.08 7.58031 16.29 7.67931 16.51 7.70031C16.92 7.68931 17.27 7.36031 17.3 6.93931V6.82031C17.33 5.41931 16.481 4.15031 15.19 3.66031Z"
              fill="#3B4144"
            />
          </svg>
        </span> */}
      </div>
      <div
        className={classnames(
          { active: openSidebar },
          `sidebar position_fixed top_0 width_100_vw height_100_vh bg_white z_index_1000 padding_x_22 padding_y_36 display_flex flex_direction_col`,
        )}
      >
        <div
          className="display_flex align_items_center"
          onClick={() => {
            setOpenSidebar(false);
          }}
        >
          <svg width={24} height={24} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
            <rect
              x="1.74707"
              y="19.5459"
              width={25}
              height={4}
              rx={2}
              transform="rotate(-45 1.74707 19.5459)"
              fill="#3B4144"
            />
            <rect
              x="4.5752"
              y="1.62598"
              width={25}
              height={4}
              rx={2}
              transform="rotate(45 4.5752 1.62598)"
              fill="#3B4144"
            />
          </svg>
          <img src={logo} alt="logo" className="height_15 margin_left_18"></img>
        </div>
        <div className="height_100_percent display_flex flex_direction_col justify_content_sb">
          <div className="padding_top_60">
            <a href="http://localhost:3003/">
              <div className="cursor_pointer margin_bottom_24">
                <span className="font_size_24 opacity_0_5 font_weight_600">Trang chủ</span>
              </div>
            </a>
            <a href="http://localhost:3003/du-an.html">
              <div className="cursor_pointer margin_bottom_24">
                <span className="font_size_24 opacity_0_5 font_weight_600">Dự án</span>
              </div>
            </a>
            {dataType.transactionTypes?.map((type: OptionInputRadio) => {
              return (
                <Link
                  href={{ pathname: '/sell-listings-page', query: { transactionType: type.value } }}
                  as={`/sell-listings/?transactionType=${type.value}`}
                  key={type.value}
                >
                  <a
                    onClick={() => {
                      setOpenSidebar(false);
                    }}
                  >
                    <div
                      className={classnames(
                        {
                          'color_red opacity_1': checkActiveSellListingsPage(type.value),
                        },
                        'cursor_pointer margin_bottom_24 opacity_0_5 display_flex justify_content_sb align_items_center',
                      )}
                    >
                      <span className="font_size_24 font_weight_600">{type.title}</span>
                      <div
                        className={classnames(
                          { display_none: !checkActiveSellListingsPage(type.value) },
                          'width_80 height_4 bg_red',
                        )}
                      ></div>
                    </div>
                  </a>
                </Link>
              );
            })}
            <Link href={{ pathname: '/blogs-page' }} as="/blogs">
              <a
                onClick={() => {
                  setOpenSidebar(false);
                }}
              >
                <div
                  className={classnames(
                    {
                      'color_red opacity_1': router.pathname.includes('/blog'),
                    },
                    'cursor_pointer margin_bottom_24 opacity_0_5 display_flex justify_content_sb align_items_center ',
                  )}
                >
                  <span className="font_size_24 font_weight_600">Blogs</span>
                  <div
                    className={classnames(
                      { display_none: !router.pathname.includes('/blog') },
                      'width_80 height_4 bg_red',
                    )}
                  ></div>
                </div>
              </a>
            </Link>
          </div>
          <div className="padding_bottom_50">
            {auth.authenticated ? (
              <div className="">
                <div className="margin_right_26 width_100_percent margin_bottom_8">
                  <SellCreatePopup></SellCreatePopup>
                </div>
                <Link href={{ pathname: '/profile-sell-listings-page' }} as="/profile/sell-listings">
                  <a
                    className="display_flex align_items_center padding_y_8 justify_content_center border_width_1 border_color_grey border_style_solid
                    border_radius_25 margin_bottom_8"
                    onClick={() => {
                      setOpenSidebar(false);
                    }}
                  >
                    <span className="margin_left_18 font_size_14 line_height_17 font_weight_600 white_space_nowrap">
                      Hồ sơ cá nhân
                    </span>
                  </a>
                </Link>
                <div
                  className="display_flex align_items_center padding_y_8 justify_content_center border_width_1 border_color_grey border_style_solid border_radius_25"
                  onClick={onLogout}
                >
                  <span className="margin_left_18 font_size_14 line_height_17 font_weight_600 white_space_nowrap">
                    Đăng xuất
                  </span>
                </div>
              </div>
            ) : (
              <div className="auth display_flex align_items_center">
                <div className="margin_right_30 font_size_14 line_height_17 font_weight_600 cursor_pointer width_50_percent text_align_center">
                  <Link href={{ pathname: '/login-page' }} as="/login">
                    <a>
                      <span>Đăng nhập</span>
                    </a>
                  </Link>
                </div>
                <div className="width_50_percent">
                  <Link href={{ pathname: '/register-page' }} as="/register">
                    <a>
                      <button
                        className="height_32 font_size_14 font_weight_600 line_height_17 border_none border_radius_25 width_100_percent
            display_flex align_items_center justify_content_center bg_red color_white cursor_pointer"
                      >
                        Đăng tin
                      </button>
                    </a>
                  </Link>
                </div>
              </div>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default HeaderMobile;
