import React, { useEffect, useRef, useState } from 'react';
import Link from 'next/link';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../reducers';
import userAvatar from '../../images/user_avatar.png';
import './Header.scss';
import classnames from 'classnames';
import useOutsideClick from '../../utils/customHooks/useOutsideClick';
import { logout, resetStatusAuth, setCurrentUrl } from '../../reducers/auth/actions';
import { useRouter } from 'next/router';
import { Status } from '../../types/type.status';
import SellCreatePopup from '../../containers/sell-create-popup/SellCreatePopup';
import { OptionInputRadio } from '../../types/type.base';
import { getSellListingPartOne } from '../../reducers/sell-listing/actions';
import { SellListing } from '../../types/type.sell-listing';
import logo from '../../images/logo.png';
import { NotificationUser } from '../../types/type.auth';
import notificationApi from '../../api/notification';
import HTMLReactParser from 'html-react-parser';
import dayjs from 'dayjs';

const Header: React.FC = () => {
  const auth = useSelector((state: RootState) => state.auth);
  const [isOpenUserPopup, setIsOpenUserPopup] = useState<boolean>(false);
  const [isOpenNotification, setIsOpenNotification] = useState<boolean>(false);
  const $userInfo = useRef(null);
  const $notification = useRef(null);
  const isOpenUserPopupVal: any = useRef(null);
  const isOpenNotificationVal: any = useRef(null);
  const dispatch = useDispatch();
  const router = useRouter();
  const logoutStatus: Status = useSelector((state: RootState) => state.auth.logoutStatus as Status);
  const dataType = useSelector((state: RootState) => state.sellListing.stepOneData);
  const sellListingActive: SellListing = useSelector((state: RootState) => state.sellListing.sellListingDetail.data);
  const [notificationList, setNotificationList] = useState<NotificationUser[]>([]);

  useEffect(() => {
    if (!dataType.productTypes?.length || !dataType.transactionTypes?.length) {
      dispatch(getSellListingPartOne());
    }
    getNotifications();
  }, []);

  useEffect(() => {
    isOpenUserPopupVal.current = isOpenUserPopup;
  }, [isOpenUserPopup]);

  useEffect(() => {
    isOpenNotificationVal.current = isOpenNotification;
  }, [isOpenNotification]);

  useEffect(() => {
    if (logoutStatus === 'success') {
      dispatch(resetStatusAuth('logout'));
      router.push({ pathname: '/login-page' }, '/login');
    }
  }, [logoutStatus]);

  useOutsideClick($userInfo, () => {
    if (isOpenUserPopupVal) {
      setIsOpenUserPopup(false);
    }
  });

  useOutsideClick($notification, () => {
    if (isOpenNotificationVal) {
      setIsOpenNotification(false);
    }
  });

  const getNotifications = () => {
    auth.authenticated &&
      notificationApi.getList(0, 100).then((res) => {
        if (res.success && res.data?.length) {
          setNotificationList(res.data);
        }
      });
  };

  /**
   * toggleUserPopup: tắt bật popup user
   */
  const toggleUserPopup = () => {
    setIsOpenUserPopup(!isOpenUserPopup);
  };

  /**
   * onLogout: logout
   */
  const onLogout = () => {
    dispatch(logout());
    dispatch(
      setCurrentUrl({
        url: window.location.origin + router.asPath,
        pathname: router.pathname,
        query: router.query,
        asPath: router.asPath,
      }),
    );
  };

  const checkActiveSellListingsPage = (type: string) => {
    if (
      (router.pathname === '/sell-listings-page' && router.query.transactionType === type) ||
      (router.pathname === '/sell-detail-page' && type === sellListingActive?.typeTransaction?.id)
    ) {
      return true;
    }
    return false;
  };

  return (
    <div className="header position_fixed left_0 top_0 width_100_vw z_index_999 bg_white box_shadow_primary">
      <Link href={{ pathname: '/home-page' }} as="/">
        <a className="logo position_absolute width_132 top_50_percent left_50_percent translate_-50_-50_percent cursor_pointer display_block">
          <img src={logo} alt="logo" className="height_20"></img>
        </a>
      </Link>
      <div className="padding_x_48 height_75 display_flex align_items_center justify_content_sb">
        <div className="navigation display_flex height_100_percent align_items_center">
          <a href="http://localhost:3003/">
            <div className="margin_right_45 cursor_pointer padding_bottom_8">
              <span className="line_height_17 font_size_14 opacity_0_5 font_weight_600">Trang chủ</span>
            </div>
          </a>
          <a href="http://localhost:3003/du-an.html">
            <div className="margin_right_45 cursor_pointer padding_bottom_8">
              <span className="line_height_17 font_size_14 opacity_0_5 font_weight_600">Dự án</span>
            </div>
          </a>
          {dataType.transactionTypes?.map((type: OptionInputRadio) => {
            return (
              <Link
                href={{ pathname: '/sell-listings-page', query: { transactionType: type.value } }}
                as={`/sell-listings/?transactionType=${type.value}`}
                key={type.value}
              >
                <a>
                  <div
                    className={classnames(
                      {
                        'border_width_0 border_bottom_width_2 border_color_red border_style_solid font_weight_600 opacity_1': checkActiveSellListingsPage(
                          type.value,
                        ),
                      },
                      'margin_right_45 cursor_pointer padding_bottom_8 opacity_0_5 ',
                    )}
                  >
                    <span className="line_height_17 font_size_14 font_weight_600">{type.title}</span>
                  </div>
                </a>
              </Link>
            );
          })}
          <Link href={{ pathname: '/blogs-page' }} as="/blogs">
            <a>
              <div
                className={classnames(
                  {
                    'border_width_0 border_bottom_width_2 border_color_red border_style_solid font_weight_600 opacity_1': router.pathname.includes(
                      '/blog',
                    ),
                  },
                  'margin_right_45 cursor_pointer padding_bottom_8 opacity_0_5 ',
                )}
              >
                <span className="line_height_17 font_size_14 font_weight_600">Blogs</span>
              </div>
            </a>
          </Link>
        </div>
        {auth.authenticated ? (
          <div className="display_flex align_items_center">
            {/* notification */}
            <div className="margin_right_26 position_relative" ref={$notification}>
              <svg
                width={24}
                height={24}
                viewBox="0 0 24 24"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
                onClick={() => {
                  setIsOpenNotification(!isOpenNotification);
                }}
                className="no_select cursor_pointer"
              >
                <g clipPath="url(#clip0)">
                  <path
                    d="M9.96318 19.2279C10.4631 19.1222 13.5093 19.1222 14.0092 19.2279C14.4366 19.3266 14.8987 19.5573 14.8987 20.0608C14.8738 20.5402 14.5926 20.9653 14.204 21.2352C13.7001 21.628 13.1088 21.8767 12.4906 21.9664C12.1487 22.0107 11.8128 22.0117 11.4828 21.9664C10.8636 21.8767 10.2723 21.628 9.76938 21.2342C9.37978 20.9653 9.09852 20.5402 9.07367 20.0608C9.07367 19.5573 9.53582 19.3266 9.96318 19.2279ZM12.0452 2C14.1254 2 16.2502 2.98702 17.5125 4.62466C18.3314 5.67916 18.7071 6.73265 18.7071 8.3703V8.79633C18.7071 10.0523 19.039 10.7925 19.7695 11.6456C20.3231 12.2741 20.5 13.0808 20.5 13.956C20.5 14.8302 20.2128 15.6601 19.6373 16.3339C18.884 17.1417 17.8215 17.6573 16.7372 17.747C15.1659 17.8809 13.5937 17.9937 12.0005 17.9937C10.4063 17.9937 8.83505 17.9263 7.26375 17.747C6.17846 17.6573 5.11602 17.1417 4.36367 16.3339C3.78822 15.6601 3.5 14.8302 3.5 13.956C3.5 13.0808 3.6779 12.2741 4.23049 11.6456C4.98384 10.7925 5.29392 10.0523 5.29392 8.79633V8.3703C5.29392 6.68834 5.71333 5.58852 6.577 4.51186C7.86106 2.9417 9.91935 2 11.9558 2H12.0452Z"
                    fill="#3B4144"
                  />
                  {notificationList.find((item) => !item.read) ? (
                    <circle cx="18.4537" cy="18.4534" r="5.04647" fill="#D93C23" stroke="white" />
                  ) : null}
                </g>
                <defs>
                  <clipPath id="clip0">
                    <rect width={24} height={24} fill="white" />
                  </clipPath>
                </defs>
              </svg>
              {/* noti dropdown  */}
              <div
                className={classnames(
                  {
                    display_none: !isOpenNotification,
                  },
                  `popup_notification position_absolute padding_y_24 border_radius_18 border_width_1 border_color_grey border_style_solid right_0 z_index_1 bg_white width_400`,
                )}
                style={{ top: 'calc(100% + 15px)' }}
              >
                <div className="border_tooltip"></div>
                <div className="margin_bottom_16 padding_x_18">
                  <span className="font_weight_600">Thông báo</span>
                </div>
                <div className="max_height_400 overflow_auto scroll_bar_width_4">
                  {notificationList?.length ? (
                    notificationList.map((noti: NotificationUser, index: number) => {
                      return (
                        <div
                          className={classnames(
                            { bg_whitesmoke: !noti.read, border_top_width_1: index === 0 },
                            'padding_y_10 border_width_0 border_bottom_width_1 border_color_grey border_style_solid padding_x_18',
                          )}
                          key={noti.createdAt}
                        >
                          <div className="text_over_flow_2 margin_bottom_8">{HTMLReactParser(noti.content)}</div>
                          <div className="text_over_flow_2 font_size_12 opacity_0_5">
                            {dayjs(noti.createdAt).format('HH:mm DD/MM/YYYY')}
                          </div>
                        </div>
                      );
                    })
                  ) : (
                    <div className="padding_x_18 font_size_14">
                      <span>Không có thông báo nào</span>
                    </div>
                  )}
                </div>
              </div>
            </div>
            {/* dang tin */}
            <div className="margin_right_26">
              <SellCreatePopup></SellCreatePopup>
            </div>
            <div className="user_info display_flex align_items_center cursor_pointer position_relative" ref={$userInfo}>
              <div className="padding_right_10 text_align_right cursor_pointer" onClick={toggleUserPopup}>
                <p className="margin_y_0 font_size_14 line_height_17 font_weight_600 no_select">{auth.user.name}</p>
                <p className="margin_y_0 font_size_12 line_height_18 no_select">
                  {auth.user.type === 'seller' ? 'Tư vấn' : 'Người dùng'}
                </p>
              </div>
              <div className="display_flex no_select" onClick={toggleUserPopup}>
                <img
                  src={auth.user?.avatar ?? userAvatar}
                  alt={auth.user.name}
                  className="width_36 height_36 border_radius_50_percent object_fit_cover"
                />
              </div>
              <div
                className={classnames(
                  {
                    display_none: !isOpenUserPopup,
                  },
                  `popup_user_setting position_absolute padding_x_18 padding_y_24 border_radius_18 border_width_1 border_color_grey border_style_solid right_0 z_index_1 bg_white width_210`,
                )}
                style={{ top: 'calc(100% + 15px)' }}
              >
                <div className="border_tooltip"></div>
                <Link href={{ pathname: '/profile-sell-listings-page' }} as="/profile/sell-listings">
                  <a
                    className="display_flex align_items_center padding_bottom_16"
                    onClick={() => {
                      setIsOpenUserPopup(false);
                    }}
                  >
                    <svg width={24} height={24} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path
                        d="M22 12C22 6.49 17.51 2 12 2C6.49 2 2 6.49 2 12C2 14.9 3.25 17.51 5.23 19.34C5.23 19.35 5.23 19.35 5.22 19.36C5.32 19.46 5.44 19.54 5.54 19.63C5.6 19.68 5.65 19.73 5.71 19.77C5.89 19.92 6.09 20.06 6.28 20.2C6.35 20.25 6.41 20.29 6.48 20.34C6.67 20.47 6.87 20.59 7.08 20.7C7.15 20.74 7.23 20.79 7.3 20.83C7.5 20.94 7.71 21.04 7.93 21.13C8.01 21.17 8.09 21.21 8.17 21.24C8.39 21.33 8.61 21.41 8.83 21.48C8.91 21.51 8.99 21.54 9.07 21.56C9.31 21.63 9.55 21.69 9.79 21.75C9.86 21.77 9.93 21.79 10.01 21.8C10.29 21.86 10.57 21.9 10.86 21.93C10.9 21.93 10.94 21.94 10.98 21.95C11.32 21.98 11.66 22 12 22C12.34 22 12.68 21.98 13.01 21.95C13.05 21.95 13.09 21.94 13.13 21.93C13.42 21.9 13.7 21.86 13.98 21.8C14.05 21.79 14.12 21.76 14.2 21.75C14.44 21.69 14.69 21.64 14.92 21.56C15 21.53 15.08 21.5 15.16 21.48C15.38 21.4 15.61 21.33 15.82 21.24C15.9 21.21 15.98 21.17 16.06 21.13C16.27 21.04 16.48 20.94 16.69 20.83C16.77 20.79 16.84 20.74 16.91 20.7C17.11 20.58 17.31 20.47 17.51 20.34C17.58 20.3 17.64 20.25 17.71 20.2C17.91 20.06 18.1 19.92 18.28 19.77C18.34 19.72 18.39 19.67 18.45 19.63C18.56 19.54 18.67 19.45 18.77 19.36C18.77 19.35 18.77 19.35 18.76 19.34C20.75 17.51 22 14.9 22 12ZM16.94 16.97C14.23 15.15 9.79 15.15 7.06 16.97C6.62 17.26 6.26 17.6 5.96 17.97C4.44 16.43 3.5 14.32 3.5 12C3.5 7.31 7.31 3.5 12 3.5C16.69 3.5 20.5 7.31 20.5 12C20.5 14.32 19.56 16.43 18.04 17.97C17.75 17.6 17.38 17.26 16.94 16.97Z"
                        fill="#3B4144"
                      />
                      <path
                        d="M12 6.92993C9.93 6.92993 8.25 8.60993 8.25 10.6799C8.25 12.7099 9.84 14.3599 11.95 14.4199C11.98 14.4199 12.02 14.4199 12.04 14.4199C12.06 14.4199 12.09 14.4199 12.11 14.4199C12.12 14.4199 12.13 14.4199 12.13 14.4199C14.15 14.3499 15.74 12.7099 15.75 10.6799C15.75 8.60993 14.07 6.92993 12 6.92993Z"
                        fill="#3B4144"
                      />
                    </svg>
                    <span className="margin_left_18 font_size_14 line_height_17 font_weight_600 white_space_nowrap">
                      Hồ sơ cá nhân
                    </span>
                  </a>
                </Link>
                <div
                  className="display_flex align_items_center padding_top_16 border_top_width_1 border_width_0 border_color_grey border_style_solid"
                  onClick={onLogout}
                >
                  <svg width={24} height={24} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path
                      d="M16.8 2H14.2C11 2 9 4 9 7.2V11.25H15.25C15.66 11.25 16 11.59 16 12C16 12.41 15.66 12.75 15.25 12.75H9V16.8C9 20 11 22 14.2 22H16.79C19.99 22 21.99 20 21.99 16.8V7.2C22 4 20 2 16.8 2Z"
                      fill="#D93C23"
                    />
                    <path
                      d="M4.55994 11.2501L6.62994 9.18009C6.77994 9.03009 6.84994 8.84009 6.84994 8.65009C6.84994 8.46009 6.77994 8.26009 6.62994 8.12009C6.33994 7.83009 5.85994 7.83009 5.56994 8.12009L2.21994 11.4701C1.92994 11.7601 1.92994 12.2401 2.21994 12.5301L5.56994 15.8801C5.85994 16.1701 6.33994 16.1701 6.62994 15.8801C6.91994 15.5901 6.91994 15.1101 6.62994 14.8201L4.55994 12.7501H8.99994V11.2501H4.55994Z"
                      fill="#D93C23"
                    />
                  </svg>
                  <span className="margin_left_18 font_size_14 line_height_17 font_weight_600 white_space_nowrap color_red">
                    Đăng xuất
                  </span>
                </div>
              </div>
            </div>
          </div>
        ) : (
          <div className="auth display_flex align_items_center">
            <div className="margin_right_30 font_size_14 line_height_17 font_weight_600 cursor_pointer">
              <Link href={{ pathname: '/login-page' }} as="/login">
                <a>
                  <span>Đăng nhập</span>
                </a>
              </Link>
            </div>
            <div>
              <Link href={{ pathname: '/register-page' }} as="/register">
                <a>
                  <button
                    className="height_32 font_size_14 font_weight_600 line_height_17 padding_x_24 border_none border_radius_25
            display_flex align_items_center justify_content_center bg_red color_white cursor_pointer"
                  >
                    Đăng tin
                  </button>
                </a>
              </Link>
            </div>
          </div>
        )}
      </div>
    </div>
  );
};

export default Header;
