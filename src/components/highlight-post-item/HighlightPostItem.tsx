import Link from 'next/link';
import React from 'react';
import { useSelector } from 'react-redux';
import { RootState } from '../../reducers';
import { Project, SellListing } from '../../types/type.sell-listing';
import { renderAddress, renderPrice } from '../../utils/sell-listing-helper';

interface Props {
  post: SellListing;
  setViewImages: (_value: any) => void;
}

const HighlightPostItem: React.FC<Props> = (props) => {
  const projectList: Project[] = useSelector((state: RootState) => state.sellListing.projectList);

  const renderProject = (id: string) => {
    const currentProject = projectList.find((item) => String(item.id) === String(id));
    if (currentProject?.name) {
      return <span className="text_transform_capitalize">{currentProject.name}</span>;
    }
    return null;
  };

  const renderIconRoom = (code: string) => {
    switch (code) {
      case 'BATHROOM': {
        return (
          <svg width={12} height={14} viewBox="0 0 12 14" fill="white" xmlns="http://www.w3.org/2000/svg">
            <path
              opacity="0.5"
              fillRule="evenodd"
              clipRule="evenodd"
              d="M3.28643 1.99494C3.47426 1.99494 3.64096 2.1141 3.70152 2.29107L3.80229 2.58551C3.41863 2.93953 3.24467 3.49219 3.40101 4.02773L3.41281 4.06809C3.49123 4.33668 3.77272 4.4905 4.04118 4.41241L5.80203 3.90034C6.07066 3.82223 6.22543 3.54128 6.14696 3.27256L6.13519 3.2322C5.95147 2.6029 5.37054 2.19808 4.74477 2.20868L4.6609 1.9636C4.45996 1.37659 3.90752 0.982422 3.28643 0.982422C2.4857 0.982422 1.83398 1.63241 1.83398 2.43238V6.80883C1.83398 7.0888 2.06095 7.31575 2.34092 7.31574C2.62087 7.31573 2.84782 7.08878 2.84782 6.80883V2.43238C2.84782 2.19162 3.04441 1.99494 3.28643 1.99494Z"
              fill="#3B4144"
            />
            <path
              opacity="0.5"
              d="M4.96871 8.06374H11.649C11.8431 8.06374 12.0005 8.22113 12.0005 8.41528V9.04857C12.0005 9.16501 11.9421 9.27282 11.8465 9.33917C11.7754 9.38848 11.7287 9.47089 11.7287 9.5638V10.7218C11.7287 11.4826 11.2114 12.1223 10.5094 12.309L10.704 12.604C10.8109 12.766 10.7662 12.9841 10.6042 13.091C10.5446 13.1303 10.4773 13.1491 10.4109 13.1491C10.2967 13.1491 10.1847 13.0936 10.1171 12.9911L9.73451 12.4112C9.72452 12.396 9.71627 12.3803 9.70891 12.3643H2.2921C2.28474 12.3803 2.27649 12.396 2.2665 12.4112L1.88391 12.9911C1.81631 13.0936 1.70431 13.1491 1.59012 13.1491C1.52367 13.1491 1.45645 13.1303 1.39685 13.091C1.23478 12.9841 1.19006 12.766 1.29698 12.604L1.49159 12.309C0.789608 12.1223 0.27227 11.4826 0.27227 10.7218V9.5638C0.27227 9.47089 0.225653 9.38848 0.154543 9.33917C0.0588478 9.27282 0.000488281 9.16501 0.000488281 9.04857V8.41528C0.000488281 8.22113 0.157895 8.06372 0.352052 8.06372H1.55011L4.96883 8.06831L4.96871 8.06374Z"
              fill="#3B4144"
            />
          </svg>
        );
      }
      default: {
        return (
          <svg width="14" height="10" viewBox="0 0 14 10" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path
              opacity="0.5"
              d="M1.7221 3.61109H3.11097C3.02542 3.61109 2.94485 3.57164 2.89209 3.50442C2.8393 3.43721 2.82097 3.34942 2.84154 3.26609L3.01433 2.57555C3.10712 2.20388 3.43987 1.94443 3.82266 1.94443H5.61099C6.07044 1.94443 6.44431 2.31831 6.44431 2.77776V3.3333C6.44431 3.48664 6.31986 3.61109 6.16653 3.61109H7.83319C7.67986 3.61109 7.5554 3.48664 7.5554 3.3333V2.77776C7.5554 2.31831 7.92928 1.94443 8.38873 1.94443H10.1771C10.5598 1.94443 10.8926 2.20388 10.9854 2.57555L11.1582 3.26609C11.1793 3.34888 11.1604 3.43721 11.1076 3.50442C11.0548 3.57164 10.9743 3.61109 10.8888 3.61109H12.2776C12.431 3.61109 12.5554 3.48664 12.5554 3.3333V1.66665C12.5554 0.901102 11.9321 0.277771 11.1665 0.277771H2.83321C2.06767 0.277771 1.44434 0.901102 1.44434 1.66665V3.3333C1.44434 3.48664 1.56876 3.61109 1.7221 3.61109Z"
              fill="#3B4144"
            />
            <path
              opacity="0.5"
              d="M12.2774 4.16663H1.72188C0.956339 4.16663 0.333008 4.78996 0.333008 5.5555V9.44436C0.333008 9.5977 0.45746 9.72215 0.610793 9.72215C0.764126 9.72215 0.888578 9.5977 0.888578 9.44436V8.61103H13.1107V9.44436C13.1107 9.5977 13.2352 9.72215 13.3885 9.72215C13.5419 9.72215 13.6663 9.5977 13.6663 9.44436V5.5555C13.6663 4.78996 13.043 4.16663 12.2774 4.16663Z"
              fill="#3B4144"
            />
          </svg>
        );
      }
    }
  };

  return (
    <div className="width_100_percent border_radius_18 position_relative">
      <div className="tag_sale position_absolute top_0 left_36 z_index_1 display_none">
        <svg width="68" height="95" viewBox="0 0 68 95" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path
            d="M0 0H68V88.5346C68 93.056 63.1887 95.9528 59.1927 93.8373L36.8073 81.9862C35.0512 81.0565 32.9488 81.0565 31.1927 81.9862L8.80732 93.8373C4.81132 95.9528 0 93.056 0 88.5346V0Z"
            fill="#D93C23"
          />
        </svg>
        <div className="width_40 display_flex align_items_center flex_direction_col  position_absolute top_50_percent left_50_percent translate_-50_-60_percent">
          <svg width="14" height="12" viewBox="0 0 14 12" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path
              d="M10.9037 0C12.4263 0 13.6657 1.22801 13.6657 2.73676L13.6663 4.52163C13.6663 4.65243 13.6137 4.77926 13.5203 4.87174C13.4263 4.96488 13.2997 5.01707 13.1663 5.01707C12.619 5.01707 12.1737 5.45833 12.1737 6.00066C12.1737 6.54299 12.619 6.98426 13.1663 6.98426C13.4423 6.98426 13.6663 7.20621 13.6663 7.47969V9.2639C13.6663 10.7727 12.4277 12 10.905 12H3.09501C1.57234 12 0.333008 10.7727 0.333008 9.2639V7.47969C0.333008 7.20621 0.557008 6.98426 0.833008 6.98426C1.38101 6.98426 1.82634 6.54299 1.82634 6.00066C1.82634 5.4722 1.39901 5.07453 0.833008 5.07453C0.700341 5.07453 0.573674 5.02235 0.479674 4.92921C0.385674 4.83607 0.333008 4.7099 0.333008 4.5791L0.334341 2.73676C0.334341 1.22801 1.57301 0 3.09567 0H10.9037ZM7.00101 3.43565C6.81167 3.43565 6.64234 3.54002 6.55767 3.70781L6.07167 4.68347L4.98767 4.84003C4.80034 4.86645 4.64701 4.99328 4.58767 5.17164C4.52901 5.34999 4.57634 5.54288 4.71234 5.67368L5.49834 6.43202L5.31301 7.50413C5.28101 7.68909 5.35634 7.87273 5.50967 7.98305C5.59634 8.04448 5.69701 8.07619 5.79901 8.07619C5.87701 8.07619 5.95567 8.05703 6.02767 8.01938L6.99967 7.51338L7.96967 8.01806C8.13767 8.10723 8.33701 8.09336 8.48967 7.98238C8.64367 7.87273 8.71901 7.68909 8.68701 7.50413L8.50101 6.43202L9.28701 5.67368C9.42367 5.54288 9.47101 5.34999 9.41167 5.17164C9.35301 4.99328 9.19967 4.86645 9.01434 4.84069L7.92834 4.68347L7.44234 3.70847C7.35901 3.54068 7.18967 3.43631 7.00101 3.43565Z"
              fill="white"
            />
          </svg>
          <div className="text_align_center margin_top_6">
            <span className="font_size_14 line_height_17 font_weight_600 color_white">Chính chủ</span>
          </div>
        </div>
      </div>
      <div
        style={{
          background: `url(${props.post?.images?.length ? props.post.images[0]?.url : props.post.thumbnail})`,
          backgroundSize: 'cover',
          backgroundRepeat: 'no-repeat',
        }}
        className="position_relative padding_top_280 min_height_275 display_flex flex_direction_col border_radius_18 cursor_pointer"
        onClick={() => {
          props.setViewImages(props.post?.images ?? []);
        }}
      >
        {/* image list  */}
        <div
          className="display_flex flex_direction_col align_items_fe position_absolute right_0 top_0 padding_16 width_140 z_index_1
          border_top_right_radius_18 border_bottom_right_radius_18"
          style={{ background: 'linear-gradient(to right,rgba(39, 39, 39, 0) 0%, #272727 100%)' }}
        >
          {/* <div
            className="width_24 height_24 align_items_center justify_content_center display_flex border_radius_50_percent margin_bottom_12"
            style={{
              background:
                'linear-gradient(90deg, rgba(39, 39, 39, 0) 0%, rgba(39, 39, 39, 0.760417) 52.6%, #272727 100%)',
              backdropFilter: 'blur(60px)',
            }}
          >
            <svg width={14} height={14} viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path
                d="M4.52042 0.667095C4.94042 0.679868 5.34708 0.753202 5.74108 0.887202H5.78042C5.80708 0.899868 5.82708 0.913868 5.84042 0.926535C5.98775 0.973868 6.12708 1.0272 6.26042 1.10054L6.51375 1.21387C6.61375 1.2672 6.73375 1.36654 6.80042 1.4072C6.86708 1.44654 6.94042 1.4872 7.00042 1.5332C7.74108 0.967202 8.64042 0.660535 9.56709 0.667095C9.98775 0.667095 10.4078 0.726535 10.8071 0.860535C13.2678 1.66054 14.1544 4.36054 13.4138 6.72054C12.9938 7.92654 12.3071 9.0272 11.4078 9.92654C10.1204 11.1732 8.70775 12.2799 7.18709 13.2332L7.02042 13.3339L6.84708 13.2265C5.32108 12.2799 3.90042 11.1732 2.60108 9.91987C1.70775 9.02054 1.02042 7.92654 0.593751 6.72054C-0.159582 4.36054 0.727085 1.66054 3.21442 0.846535C3.40775 0.779868 3.60708 0.733202 3.80708 0.707202H3.88708C4.07442 0.679868 4.26042 0.667095 4.44708 0.667095H4.52042ZM10.4604 2.77387C10.1871 2.67987 9.88708 2.8272 9.78708 3.1072C9.69375 3.3872 9.84042 3.69387 10.1204 3.7932C10.5478 3.9532 10.8338 4.37387 10.8338 4.83987V4.86054C10.8211 5.0132 10.8671 5.16053 10.9604 5.27387C11.0538 5.3872 11.1938 5.4532 11.3404 5.4672C11.6138 5.45987 11.8471 5.24054 11.8671 4.95987V4.88054C11.8871 3.94653 11.3211 3.10054 10.4604 2.77387Z"
                fill="white"
              />
            </svg>
          </div> */}
          {props.post.images.map((image, index) => {
            if (index && index < 3) {
              return (
                <img
                  key={image.id}
                  src={image.url}
                  alt="post-images"
                  className="width_64 height_64 object_fit_cover margin_bottom_10 border_radius_16 cursor_pointer"
                />
              );
            } else if (index === 3) {
              return (
                <div className="position_relative" key={image.id}>
                  <div
                    className="position_absolute width_100_percent height_100_percent border_width_1 border_color_white border_style_solid
              display_flex align_items_center justify_content_center flex_direction_col border_radius_15 cursor_pointer"
                    style={{ background: 'rgba(39,39,39,0.7)' }}
                  >
                    <div className="display_flex">
                      <svg width={18} height={18} viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path
                          d="M12.8058 0.083252C15.7808 0.083252 17.7794 2.17071 17.7794 5.27682V12.4452C17.7794 15.5514 15.7808 17.6388 12.8049 17.6388H5.19753C2.22249 17.6388 0.223877 15.5514 0.223877 12.4452V5.27682C0.223877 2.17071 2.22249 0.083252 5.19753 0.083252H12.8058ZM13.7738 9.34385C12.8328 8.75686 12.1064 9.58119 11.9105 9.84479C11.7216 10.0993 11.5591 10.3802 11.3879 10.661C10.9695 11.354 10.4902 12.1529 9.66052 12.6176C8.45479 13.2852 7.53946 12.6702 6.88098 12.2227C6.63383 12.056 6.39375 11.8984 6.15454 11.7933C5.56492 11.5388 5.03443 11.8286 4.24709 12.8287C3.834 13.3514 3.42443 13.8695 3.00958 14.3859C2.76155 14.6948 2.82069 15.1712 3.15522 15.3778C3.68924 15.7066 4.34065 15.8833 5.0768 15.8833H12.4745C12.892 15.8833 13.3104 15.8262 13.7093 15.6957C14.6079 15.4022 15.3211 14.7301 15.6936 13.8424C16.0078 13.0959 16.1605 12.1901 15.8666 11.4364C15.7686 11.1864 15.6221 10.9536 15.4164 10.7488C14.8771 10.2135 14.3731 9.71344 13.7738 9.34385ZM5.92843 3.59436C4.71829 3.59436 3.73499 4.57899 3.73499 5.78881C3.73499 6.99862 4.71829 7.98325 5.92843 7.98325C7.13769 7.98325 8.12187 6.99862 8.12187 5.78881C8.12187 4.57899 7.13769 3.59436 5.92843 3.59436Z"
                          fill="white"
                        />
                      </svg>
                    </div>
                    <div className="margin_top_5">
                      {props.post.images?.length > 4 ? (
                        <span className="color_white font_size_12 line_height_18">
                          +{props.post.images.length - 4} ảnh
                        </span>
                      ) : null}
                    </div>
                  </div>
                  <img
                    src={image.url}
                    alt="post-images"
                    className="display_block width_64 height_64 object_fit_cover border_radius_16"
                  />
                </div>
              );
            }
            return null;
          })}
        </div>
        {/* project */}
        {renderProject(props.post.projectId as string) ? (
          <div
            className="display_flex align_items_fe padding_16 flex_1 color_white position_absolute left_0 top_164 height_116 width_100_percent border_bottom_left_radius_18
          border_bottom_right_radius_18"
            style={{
              background:
                'linear-gradient(180deg, rgba(39, 39, 39, 0) 0%, rgba(39, 39, 39, 0.760417) 45.31%, #272727 100%)',
            }}
          >
            <div className="border_width_1 border_style_solid border_color_orange padding_x_24 padding_y_2 border_radius_8 max_width_160">
              <div className="font_size_14 font_weight_600 color_orange text_over_flow_1">
                #{renderProject(props.post.projectId as string)}
              </div>
            </div>
          </div>
        ) : null}
      </div>
      {/* information  */}
      <Link
        href={{ pathname: '/sell-detail-page', query: { postId: props.post.id } }}
        as={`/sell-listings/${props.post.id}`}
      >
        <a className="display_flex">
          <div className="padding_16 border_bottom_left_radius_18 border_bottom_right_radius_18">
            {/* title  */}

            <div className="position_relative text_over_flow_1" title={props.post.title}>
              <span className="line_height_20 font_weight_600">{props.post.title}</span>
            </div>

            {/* address  */}
            <div className="margin_top_8 display_flex align_items_center position_relative">
              <div className="width_22 flex_none display_flex align_item_center">
                <svg width={10} height={14} viewBox="0 0 10 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path
                    opacity="0.5"
                    d="M2.68938 1.05738C4.14595 0.211044 5.93631 0.225837 7.37922 1.09612C8.80793 1.98414 9.67627 3.56898 9.66824 5.27382C9.63493 6.96749 8.70382 8.55953 7.53994 9.79026C6.86818 10.5038 6.1167 11.1347 5.30086 11.6702C5.21683 11.7188 5.12479 11.7513 5.02928 11.7662C4.93736 11.7623 4.84784 11.7351 4.7688 11.6872C3.52324 10.8826 2.43052 9.85555 1.54318 8.6555C0.800686 7.65377 0.378851 6.44353 0.334961 5.18914L0.338285 5.00666C0.398909 3.36929 1.28482 1.87348 2.68938 1.05738ZM5.60647 3.78938C5.01434 3.53768 4.33166 3.67443 3.87717 4.13576C3.42269 4.59709 3.28606 5.29201 3.53107 5.89605C3.77608 6.50009 4.35441 6.89409 4.99603 6.89409C5.41636 6.89711 5.82042 6.72875 6.11817 6.42651C6.41592 6.12428 6.58262 5.71329 6.58114 5.28512C6.58337 4.63155 6.1986 4.04107 5.60647 3.78938Z"
                    fill="#3B4144"
                  />
                  <path
                    opacity="0.5"
                    d="M5.00179 13.7662C6.84274 13.7662 8.33512 13.4677 8.33512 13.0995C8.33512 12.7313 6.84274 12.4329 5.00179 12.4329C3.16084 12.4329 1.66846 12.7313 1.66846 13.0995C1.66846 13.4677 3.16084 13.7662 5.00179 13.7662Z"
                    fill="#3B4144"
                  />
                </svg>
              </div>
              <span
                className="font_size_14 opacity_0_5 line_height_18 text_over_flow_1 text_transform_capitalize"
                title={renderAddress(props.post)}
              >
                {renderAddress(props.post)}
              </span>
            </div>
            {/* phone  */}
            <div className="margin_top_8 display_flex align_items_center border_width_0 border_bottom_width_1 border_color_grey border_style_solid padding_bottom_8">
              <div className="width_22 display_flex align_item_center">
                <svg width={14} height={14} viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path
                    opacity="0.5"
                    fillRule="evenodd"
                    clipRule="evenodd"
                    d="M6.68815 7.41462C9.34754 10.0733 9.95084 6.9975 11.6441 8.68956C13.2765 10.3215 14.2147 10.6485 12.1465 12.7162C11.8874 12.9244 10.2414 15.4292 4.45673 9.64613C-1.32866 3.86236 1.17472 2.21465 1.38297 1.95566C3.45624 -0.117745 3.77757 0.825946 5.40999 2.45791C7.10323 4.15069 4.02876 4.75597 6.68815 7.41462Z"
                    fill="#3B4144"
                  />
                </svg>
              </div>
              <span className="font_size_14 opacity_0_5 line_height_18">
                {props.post?.contactPhone ?? props.post?.seller?.phone}
              </span>
            </div>
            <div className="display_flex flex_wrap">
              <div className="width_50_percent">
                <div className="display_flex align_items_center margin_top_8 width_100_percent">
                  <div className="width_22 display_flex align_item_center">{renderIconRoom('BEDROOM')}</div>
                  <span className="font_size_14 opacity_0_5 line_height_18">
                    {props.post.bedroom ? props.post.bedroom + ' phòng ngủ' : 'Liên hệ'}
                  </span>
                </div>
                <div className="display_flex align_items_center margin_top_8 width_100_percent">
                  <div className="width_22 display_flex align_item_center">{renderIconRoom('BATHROOM')}</div>
                  <span className="font_size_14 opacity_0_5 line_height_18">
                    {props.post.bathroom ? props.post.bathroom + ' vệ sinh' : 'Liên hệ'}
                  </span>
                </div>
              </div>
              <div className="width_50_percent">
                <div className="display_flex align_items_center margin_top_8 width_100_percent">
                  <div className="width_22 display_flex align_item_center">
                    <svg width={16} height={17} viewBox="0 0 16 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path
                        opacity="0.5"
                        d="M14.5042 13.8178L13.0932 12.4067C13.3843 11.9673 13.5559 11.4423 13.5559 10.8773C13.5559 9.34569 12.3098 8.09961 10.7782 8.09961C9.24657 8.09961 8.00049 9.34569 8.00049 10.8773C8.00049 12.409 9.24657 13.655 10.7782 13.655C11.3432 13.655 11.8682 13.4834 12.3076 13.1923L13.7187 14.6034C13.9359 14.8206 14.287 14.8206 14.5042 14.6034C14.7215 14.3861 14.7215 14.035 14.5042 13.8178ZM10.7782 12.544C9.85934 12.544 9.11158 11.7962 9.11158 10.8773C9.11158 9.95846 9.85934 9.2107 10.7782 9.2107C11.6971 9.2107 12.4448 9.95846 12.4448 10.8773C12.4448 11.7962 11.6971 12.544 10.7782 12.544Z"
                        fill="#3B4144"
                      />
                      <path
                        opacity="0.5"
                        d="M1.33374 2.5309V9.66816C1.33374 10.2737 1.83207 10.7662 2.44485 10.7662H4.66707C5.27985 10.7662 5.77818 10.2737 5.77818 9.66816V5.82502H13.556C14.1687 5.82502 14.6671 5.33255 14.6671 4.72698V2.5309C14.6671 1.92533 14.1687 1.43286 13.556 1.43286H2.44485C1.83207 1.43286 1.33374 1.92533 1.33374 2.5309Z"
                        fill="#3B4144"
                      />
                    </svg>
                  </div>
                  <span className="font_size_14 opacity_0_5 line_height_18">{props.post.area}M2</span>
                </div>
                <div className="display_flex align_items_center margin_top_8 width_100_percent">
                  <div className="width_22 display_flex align_item_center">
                    <svg width={14} height={14} viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path
                        opacity="0.5"
                        d="M7.00041 0.48291C10.6804 0.48291 13.6671 3.46958 13.6671 7.14958C13.6671 10.8362 10.6804 13.8162 7.00041 13.8162C3.31374 13.8162 0.33374 10.8362 0.33374 7.14958C0.33374 3.46958 3.31374 0.48291 7.00041 0.48291ZM9.56707 4.95624C9.64041 4.72291 9.42707 4.50291 9.19374 4.57624L5.78041 5.64291C5.64041 5.68958 5.52707 5.79624 5.48707 5.93624L4.42041 9.35624C4.34707 9.58291 4.56707 9.80291 4.79374 9.72958L8.19374 8.66291C8.33374 8.62291 8.44707 8.50958 8.48707 8.36958L9.56707 4.95624Z"
                        fill="#3B4144"
                      />
                    </svg>
                  </div>
                  <span className="font_size_14 opacity_0_5 line_height_18">{props.post?.direction?.content}</span>
                </div>
              </div>
            </div>
            <div className="margin_top_14 display_flex justify_content_sb align_items_center ">
              <button
                className="cursor_pointer padding_y_8 padding_x_24 font_size_14 font_weight_600 line_height_17 border_width_1 border_style_solid border_color_green
            color_green border_radius_8 bg_white"
              >
                {props.post.typeTransaction?.content || 'Bán nhà'}
              </button>
              <div className="price">
                <span className="font_size_21 line_height_31 font_weight_600">{renderPrice(props?.post?.price)}</span>
              </div>
            </div>
          </div>
        </a>
      </Link>
    </div>
  );
};

export default HighlightPostItem;
