import React, { useEffect, useRef, useState } from 'react';
import { SliderMethod } from '../../types/type.slider';
import classnames from 'classnames';

interface Props {
  listImages: any[]; // list image slide
  // eslint-disable-next-line no-unused-vars
  setListImages: (listImages: any[]) => void; // hàm thay đổi list images
  slidesNm: number; // phần trăm width 1 slide
  slidesMd: number;
  slidesSm: number;
  slidesXs: number;
  slidesPerView: number; // slide trên 1 view
  margin: number; // khoảng cách giữa các item
  prevClass?: string;
  nextClass?: string;
  innerRef?: SliderMethod; // ref chứa hàm next slide và prev slide
  showButton?: boolean; // có show button next và prev không
  isStopAutoPlay?: boolean; // có tắt auto play không
  className?: string; // class của slide
  loop?: boolean; // không or co loop
  onDisableNext?: (..._args: any[]) => void;
  onDisablePrev?: (..._args: any[]) => void;
  timePlay?: number; // thời gian để chạy qua slide giữa các lần
  reCalculate?: any;
}

/**
 * Slider
 * @param props props
 * innerRef: cung cấp 2 hàm next slide and prev slide. cách dùng: innerRef.prevSlideIndex() hoặc innerRef.nextSlideIndex()
 * @returns element
 */

const Slider: React.FC<Props> = (props) => {
  const transitionDuration = 0.5;
  const spaceTimeInfinite = props.timePlay ? props.timePlay : 3;
  const timeWaitContinueInfinite = 10;
  const [slideIdx, setSlideIdx] = useState(1);
  const [translateXSlider, setTranslateXSlider] = useState(0);
  const [slicePercent, setSlicePercent] = useState(20);
  const [isDoneNextTransition, setIsDoneNextTransition] = useState(false);
  const [isDonePrevTransition, setIsDonePrevTransition] = useState(false);
  const [isAbleClickNextButton, setIsAbleClickNextButton] = useState(true);
  const [isAbleClickPrevButton, setIsAbleClickPrevButton] = useState(true);
  const intervalInfinite: any = useRef(null);
  const timeoutContinueInfinite: any = useRef(null);
  const isAbleClickNextButtonRef: any = useRef(null);
  const translateXSliderRef = useRef(-20);
  const slicePercentRef = useRef(-20);
  const [marginContainer, setMarginContainer] = useState<number>(0);
  const [disableNext, setDisableNext] = useState<boolean>(false);
  const [disablePrev, setDisablePrev] = useState<boolean>(true);

  /* gắn func của slider vào ref */
  if (props.innerRef) {
    props.innerRef.prevSlideIndex = () => {
      prevSlideIndex(false);
    };
    props.innerRef.nextSlideIndex = () => {
      nextSlideIndex(false);
    };
  }

  useEffect(() => {
    setSlideIdx(1);
    setTranslateXSlider(0);
    onResize();
    loopInfinite();
    initSliderItems();
    window.removeEventListener('resize', onResize);
    window.addEventListener('resize', onResize);
    return () => {
      clearInterval(intervalInfinite.current);
      window.removeEventListener('resize', onResize);
    };
  }, [props.reCalculate]);

  useEffect(() => {
    if (props.onDisablePrev) {
      if (slideIdx === 1) {
        props.onDisablePrev(true);
      } else {
        props.onDisablePrev(false);
      }
    }
    if (!props.loop) {
      if (slideIdx === 0 || props.listImages.length <= props.slidesPerView) {
        setDisablePrev(true);
      } else {
        setDisablePrev(false);
      }
    }
    if (props.onDisableNext) {
      if (
        props.listImages.length <= props.slidesPerView ||
        slideIdx === props.listImages.length - props.slidesPerView
      ) {
        props.onDisableNext(true);
      } else {
        props.onDisableNext(false);
      }
    }
    if (!props.loop) {
      if (
        props.listImages.length <= props.slidesPerView ||
        slideIdx === props.listImages.length - props.slidesPerView
      ) {
        setDisableNext(true);
      } else {
        setDisableNext(false);
      }
    }
  }, [props.listImages]);

  /*cached lai state*/
  useEffect(() => {
    slicePercentRef.current = slicePercent;
  }, [slicePercent]);

  useEffect(() => {
    translateXSliderRef.current = translateXSlider;
  }, [translateXSlider]);

  /**
   * useEffect: check khi nào xong transition NEXT thì loop lại images
   */
  useEffect(() => {
    if (isDoneNextTransition) {
      changeImagesListClickNext();
    }
  }, [isDoneNextTransition]);

  /**
   * useEffect: check khi nào xong transition PREV thì loop lại images
   */
  useEffect(() => {
    if (isDonePrevTransition) {
      changeImagesListClickPrev();
    }
  }, [isDonePrevTransition]);

  useEffect(() => {
    isAbleClickNextButtonRef.current = isAbleClickNextButton;
  }, [isAbleClickNextButton]);

  /**
   * initSliderItems tính toán lại array items được truyền vào
   */
  const initSliderItems = () => {
    if (props.loop) {
      let newListImages = [];
      while (newListImages.length <= props.slidesPerView + 2 && props.listImages.length) {
        newListImages.push(...props.listImages);
      }
      const lastImage = newListImages[newListImages.length - 1];
      newListImages.splice(newListImages.length - 1, 1);
      newListImages = [lastImage, ...newListImages];
      props.setListImages(newListImages);
    }
  };

  /**
   * onResize func thực thi khi resize màn hình
   */
  const onResize = () => {
    if (window.innerWidth > 1024) {
      setDataSlide(props.slidesNm);
    } else if (window.innerWidth <= 576) {
      setDataSlide(props.slidesXs);
    } else if (window.innerWidth <= 768) {
      setDataSlide(props.slidesSm);
    } else if (window.innerWidth <= 1024) {
      setDataSlide(props.slidesMd);
    }
  };

  /**
   * setDataSlide set các biến slide percent và translateXSlider
   * @param slidePercent phần trăm width percent
   */
  const setDataSlide = (slidePercent: number) => {
    if (props.loop) {
      setSlicePercent(slidePercent);
      setMarginContainer(props.margin / 2 + (props.margin / 100) * slidePercent);
      setTranslateXSlider(-slidePercent);
    } else {
      setSlideIdx(0);
      setSlicePercent(slidePercent);
      setMarginContainer(props.margin / 2 + (props.margin / 100) * 0);
      setTranslateXSlider(0);
      if (props.onDisableNext && props.onDisablePrev) {
        props.onDisablePrev(true);
        if (props.listImages.length <= props.slidesPerView) {
          props.onDisableNext(true);
        } else {
          props.onDisableNext(false);
        }
      }
      if (!props.loop) {
        setDisablePrev(true);
        if (props.listImages.length <= props.slidesPerView) {
          setDisableNext(true);
        } else {
          setDisableNext(false);
        }
      }
    }
  };

  /**
   * loopInfinite: auto chạy slider
   */
  const loopInfinite = () => {
    if (!props.isStopAutoPlay) {
      intervalInfinite.current = setInterval(() => {
        nextSlideIndex(true);
      }, spaceTimeInfinite * 1000);
    }
  };

  /**
   * waitToContinueInfinite
   */
  const waitToContinueInfinite = () => {
    if (!props.isStopAutoPlay) {
      clearInterval(intervalInfinite.current);
      clearTimeout(timeoutContinueInfinite.current);
      timeoutContinueInfinite.current = setTimeout(() => {
        setIsAbleClickNextButton(true);
        loopInfinite();
      }, timeWaitContinueInfinite * 1000);
    }
  };

  /**
   * changeImagesListClickNext: thay đổi array ảnh khi click vào nút next
   */
  const changeImagesListClickNext = () => {
    if (props.loop) {
      const firstImage: any[] = props.listImages.slice(0, 1);

      const restImages: any[] = props.listImages.slice(1);

      const newListImages: any[] = [...restImages, ...firstImage];
      props.setListImages(newListImages);
      setTranslateXSlider(-slideIdx * slicePercent);
      setMarginContainer(props.margin / 2 + (props.margin / 100) * slideIdx * slicePercent);
    }
    setTimeout(() => {
      setIsDoneNextTransition(false);
      setIsAbleClickNextButton(true);
    }, 100);
  };

  /**
   * changeImagesListClickNext: thay đổi array ảnh khi click vào nút prev
   */
  const changeImagesListClickPrev = () => {
    if (props.loop) {
      const lastImage: any[] = props.listImages.slice(-1);
      const restImages: any[] = props.listImages.slice(0, -1);
      const newListImages: any[] = [...lastImage, ...restImages];
      props.setListImages(newListImages);
      setTranslateXSlider(-slideIdx * slicePercent);
      setMarginContainer(props.margin / 2 + (props.margin / 100) * slideIdx * slicePercent);
    }
    setTimeout(() => {
      setIsDonePrevTransition(false);
      setIsAbleClickPrevButton(true);
    }, 100);
  };

  /**
   * nextSlideIndex: chuyen next slide
   * @param isAuto biến để check xem là user click hay auto
   */
  const nextSlideIndex = (isAuto: boolean) => {
    !isAuto && waitToContinueInfinite();
    if (isAbleClickNextButtonRef.current) {
      if (props.loop) {
        setIsDoneNextTransition(false);
        setIsAbleClickNextButton(false);
        setTranslateXSlider(-(slideIdx + 1) * slicePercentRef.current);
        setMarginContainer(props.margin / 2 + (props.margin / 100) * (slideIdx + 1) * slicePercentRef.current);
      } else {
        if (slideIdx < props.listImages.length - props.slidesPerView) {
          resetDisableButton();
          if (slideIdx === props.listImages.length - props.slidesPerView - 1) {
            props.onDisableNext && props.onDisableNext(true);
            setDisableNext(true);
          }
          setTranslateXSlider(-(slideIdx + 1) * slicePercentRef.current);
          setMarginContainer(props.margin / 2 + (props.margin / 100) * (slideIdx + 1) * slicePercentRef.current);
          setSlideIdx(slideIdx + 1);
        }
      }
      setTimeout(() => {
        setIsDoneNextTransition(true);
      }, transitionDuration * 1000);
    }
  };

  /**
   * prevSlideIndex: chuyen prev slide
   * @param isAuto biến để check xem là user click hay auto
   */
  const prevSlideIndex = (isAuto: boolean) => {
    !isAuto && waitToContinueInfinite();
    if (isAbleClickPrevButton) {
      if (props.loop) {
        setIsDonePrevTransition(false);
        setIsAbleClickPrevButton(false);
        setTranslateXSlider(-(slideIdx - 1) * slicePercentRef.current);
        setMarginContainer(props.margin / 2 + (props.margin / 100) * (slideIdx - 1) * slicePercentRef.current);
      } else {
        if (slideIdx > 0) {
          resetDisableButton();
          if (slideIdx === 1) {
            props.onDisablePrev && props.onDisablePrev(true);
            setDisablePrev(true);
          }
          if (slideIdx === props.listImages.length - props.slidesPerView + 1) {
            props.onDisableNext && props.onDisableNext(true);
            setDisableNext(true);
          }
          setTranslateXSlider(-(slideIdx - 1) * slicePercentRef.current);
          setMarginContainer(props.margin / 2 + (props.margin / 100) * (slideIdx - 1) * slicePercentRef.current);
          setSlideIdx(slideIdx - 1);
        }
      }
      setTimeout(() => {
        setIsDonePrevTransition(true);
      }, transitionDuration * 1000);
    }
  };

  const resetDisableButton = () => {
    props.onDisablePrev && props.onDisablePrev(false);
    props.onDisableNext && props.onDisableNext(false);
    setDisableNext(false);
    setDisablePrev(false);
  };

  return (
    <div className="position_relative width_100_percent">
      <div className={`align_items_center overflow_hidden slider_container ${props.className}`}>
        <div
          className="slider width_100_percent display_flex"
          style={{
            willChange: 'transform',
            transform: `translate3d(calc(${translateXSlider}%), 0px, 0px)`,
            transition: `${
              (!isDonePrevTransition && !isDoneNextTransition) || !props.loop ? `${transitionDuration}s` : '0s'
            }`,
            margin: `0px -${marginContainer}px`,
          }}
        >
          {props.children}
        </div>
      </div>
      {/* next and prev btn */}
      {props.showButton ? (
        <>
          <div
            className={`position_absolute width_36 height_36 border_radius_50_percent bg_white display_flex justify_content_center align_items_center
            left_-15 top_50_percent z_index_1 cursor_pointer translate_0_-18 ${props.prevClass}`}
            style={{ boxShadow: '1px 1px 2px 1px rgba(23, 23, 23, 0.16)' }}
            onClick={() => prevSlideIndex(false)}
          >
            <svg
              width="20"
              height="20"
              viewBox="0 0 20 20"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
              className={classnames({ 'opacity_0_3 cursor_default': disablePrev })}
            >
              <path
                fillRule="evenodd"
                clipRule="evenodd"
                d="M10 20C4.486 20 0 15.514 0 10C0 4.486 4.486 0 10 0C15.514 0 20 4.486 20 10C20 15.514 15.514 20 10 20ZM10 18.5C14.687 18.5 18.5 14.687 18.5 10C18.5 5.313 14.687 1.5 10 1.5C5.313 1.5 1.5 5.313 1.5 10C1.5 14.687 5.313 18.5 10 18.5ZM10.9124 14.0023L7.4264 10.5313C7.2854 10.3903 7.2064 10.1993 7.2064 10.0003C7.2064 9.8003 7.2854 9.6093 7.4264 9.4683L10.9124 5.9983C11.0584 5.8523 11.2504 5.7793 11.4414 5.7793C11.6334 5.7793 11.8264 5.8523 11.9724 6.0003C12.2644 6.2943 12.2634 6.7683 11.9704 7.0603L9.0184 10.0003L11.9704 12.9393C12.2634 13.2313 12.2644 13.7063 11.9724 14.0003C11.6804 14.2953 11.2064 14.2933 10.9124 14.0023Z"
                fill="black"
              />
            </svg>
          </div>
          <div
            className={`position_absolute width_36 height_36 border_radius_50_percent bg_white display_flex justify_content_center align_items_center
            right_-15 top_50_percent z_index_1 cursor_pointer translate_0_-18 ${props.nextClass}`}
            style={{ boxShadow: '1px 1px 2px 1px rgba(23, 23, 23, 0.16)' }}
            onClick={() => nextSlideIndex(false)}
          >
            <svg
              width="20"
              height="20"
              viewBox="0 0 20 20"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
              className={classnames({ 'opacity_0_3 cursor_default': disableNext })}
            >
              <path
                fillRule="evenodd"
                clipRule="evenodd"
                d="M10 0C15.514 0 20 4.486 20 10C20 15.514 15.514 20 10 20C4.486 20 0 15.514 0 10C0 4.486 4.486 0 10 0ZM10 1.5C5.313 1.5 1.5 5.313 1.5 10C1.5 14.687 5.313 18.5 10 18.5C14.687 18.5 18.5 14.687 18.5 10C18.5 5.313 14.687 1.5 10 1.5ZM9.0876 5.9977L12.5736 9.4687C12.7146 9.6097 12.7936 9.8007 12.7936 9.9997C12.7936 10.1997 12.7146 10.3907 12.5736 10.5317L9.0876 14.0017C8.9416 14.1477 8.7496 14.2207 8.5586 14.2207C8.3666 14.2207 8.1736 14.1477 8.0276 13.9997C7.7356 13.7057 7.7366 13.2317 8.0296 12.9397L10.9816 9.9997L8.0296 7.0607C7.7366 6.7687 7.7356 6.2937 8.0276 5.9997C8.3196 5.7047 8.7936 5.7067 9.0876 5.9977Z"
                fill="black"
              />
            </svg>
          </div>
        </>
      ) : null}
    </div>
  );
};

export default Slider;
