import Link from 'next/link';
import React, { useState } from 'react';
import { SellListing } from '../../types/type.sell-listing';
import { SliderMethod } from '../../types/type.slider';
import { renderAddress, renderPrice } from '../../utils/sell-listing-helper';
import Slider from '../slider/Slider';

interface Props {
  post: SellListing;
}

const PostItem: React.FC<Props> = (props) => {
  const [listImages, setListImages] = useState<{ url: string; id: number }[]>(props.post.images);
  let slider: SliderMethod = {};

  const renderIconRoom = (code: string) => {
    switch (code) {
      case 'BATHROOM': {
        return (
          <svg width={12} height={14} viewBox="0 0 12 14" fill="white" xmlns="http://www.w3.org/2000/svg">
            <path
              opacity="0.5"
              fillRule="evenodd"
              clipRule="evenodd"
              d="M3.28643 1.99494C3.47426 1.99494 3.64096 2.1141 3.70152 2.29107L3.80229 2.58551C3.41863 2.93953 3.24467 3.49219 3.40101 4.02773L3.41281 4.06809C3.49123 4.33668 3.77272 4.4905 4.04118 4.41241L5.80203 3.90034C6.07066 3.82223 6.22543 3.54128 6.14696 3.27256L6.13519 3.2322C5.95147 2.6029 5.37054 2.19808 4.74477 2.20868L4.6609 1.9636C4.45996 1.37659 3.90752 0.982422 3.28643 0.982422C2.4857 0.982422 1.83398 1.63241 1.83398 2.43238V6.80883C1.83398 7.0888 2.06095 7.31575 2.34092 7.31574C2.62087 7.31573 2.84782 7.08878 2.84782 6.80883V2.43238C2.84782 2.19162 3.04441 1.99494 3.28643 1.99494Z"
              fill="#3B4144"
            />
            <path
              opacity="0.5"
              d="M4.96871 8.06374H11.649C11.8431 8.06374 12.0005 8.22113 12.0005 8.41528V9.04857C12.0005 9.16501 11.9421 9.27282 11.8465 9.33917C11.7754 9.38848 11.7287 9.47089 11.7287 9.5638V10.7218C11.7287 11.4826 11.2114 12.1223 10.5094 12.309L10.704 12.604C10.8109 12.766 10.7662 12.9841 10.6042 13.091C10.5446 13.1303 10.4773 13.1491 10.4109 13.1491C10.2967 13.1491 10.1847 13.0936 10.1171 12.9911L9.73451 12.4112C9.72452 12.396 9.71627 12.3803 9.70891 12.3643H2.2921C2.28474 12.3803 2.27649 12.396 2.2665 12.4112L1.88391 12.9911C1.81631 13.0936 1.70431 13.1491 1.59012 13.1491C1.52367 13.1491 1.45645 13.1303 1.39685 13.091C1.23478 12.9841 1.19006 12.766 1.29698 12.604L1.49159 12.309C0.789608 12.1223 0.27227 11.4826 0.27227 10.7218V9.5638C0.27227 9.47089 0.225653 9.38848 0.154543 9.33917C0.0588478 9.27282 0.000488281 9.16501 0.000488281 9.04857V8.41528C0.000488281 8.22113 0.157895 8.06372 0.352052 8.06372H1.55011L4.96883 8.06831L4.96871 8.06374Z"
              fill="#3B4144"
            />
          </svg>
        );
      }
      default: {
        return (
          <svg width="14" height="10" viewBox="0 0 14 10" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path
              opacity="0.5"
              d="M1.7221 3.61109H3.11097C3.02542 3.61109 2.94485 3.57164 2.89209 3.50442C2.8393 3.43721 2.82097 3.34942 2.84154 3.26609L3.01433 2.57555C3.10712 2.20388 3.43987 1.94443 3.82266 1.94443H5.61099C6.07044 1.94443 6.44431 2.31831 6.44431 2.77776V3.3333C6.44431 3.48664 6.31986 3.61109 6.16653 3.61109H7.83319C7.67986 3.61109 7.5554 3.48664 7.5554 3.3333V2.77776C7.5554 2.31831 7.92928 1.94443 8.38873 1.94443H10.1771C10.5598 1.94443 10.8926 2.20388 10.9854 2.57555L11.1582 3.26609C11.1793 3.34888 11.1604 3.43721 11.1076 3.50442C11.0548 3.57164 10.9743 3.61109 10.8888 3.61109H12.2776C12.431 3.61109 12.5554 3.48664 12.5554 3.3333V1.66665C12.5554 0.901102 11.9321 0.277771 11.1665 0.277771H2.83321C2.06767 0.277771 1.44434 0.901102 1.44434 1.66665V3.3333C1.44434 3.48664 1.56876 3.61109 1.7221 3.61109Z"
              fill="#3B4144"
            />
            <path
              opacity="0.5"
              d="M12.2774 4.16663H1.72188C0.956339 4.16663 0.333008 4.78996 0.333008 5.5555V9.44436C0.333008 9.5977 0.45746 9.72215 0.610793 9.72215C0.764126 9.72215 0.888578 9.5977 0.888578 9.44436V8.61103H13.1107V9.44436C13.1107 9.5977 13.2352 9.72215 13.3885 9.72215C13.5419 9.72215 13.6663 9.5977 13.6663 9.44436V5.5555C13.6663 4.78996 13.043 4.16663 12.2774 4.16663Z"
              fill="#3B4144"
            />
          </svg>
        );
      }
    }
  };

  return (
    <div className="position_relative">
      {/* <div className="tag_sale position_absolute top_8 left_36 z_index_1">
        <svg width="68" height="95" viewBox="0 0 68 95" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path
            d="M0 0H68V88.5346C68 93.056 63.1887 95.9528 59.1927 93.8373L36.8073 81.9862C35.0512 81.0565 32.9488 81.0565 31.1927 81.9862L8.80732 93.8373C4.81132 95.9528 0 93.056 0 88.5346V0Z"
            fill="#D93C23"
          />
        </svg>
        <div className="width_40 display_flex align_items_center flex_direction_col  position_absolute top_50_percent left_50_percent translate_-50_-60_percent">
          <svg width="14" height="12" viewBox="0 0 14 12" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path
              d="M10.9037 0C12.4263 0 13.6657 1.22801 13.6657 2.73676L13.6663 4.52163C13.6663 4.65243 13.6137 4.77926 13.5203 4.87174C13.4263 4.96488 13.2997 5.01707 13.1663 5.01707C12.619 5.01707 12.1737 5.45833 12.1737 6.00066C12.1737 6.54299 12.619 6.98426 13.1663 6.98426C13.4423 6.98426 13.6663 7.20621 13.6663 7.47969V9.2639C13.6663 10.7727 12.4277 12 10.905 12H3.09501C1.57234 12 0.333008 10.7727 0.333008 9.2639V7.47969C0.333008 7.20621 0.557008 6.98426 0.833008 6.98426C1.38101 6.98426 1.82634 6.54299 1.82634 6.00066C1.82634 5.4722 1.39901 5.07453 0.833008 5.07453C0.700341 5.07453 0.573674 5.02235 0.479674 4.92921C0.385674 4.83607 0.333008 4.7099 0.333008 4.5791L0.334341 2.73676C0.334341 1.22801 1.57301 0 3.09567 0H10.9037ZM7.00101 3.43565C6.81167 3.43565 6.64234 3.54002 6.55767 3.70781L6.07167 4.68347L4.98767 4.84003C4.80034 4.86645 4.64701 4.99328 4.58767 5.17164C4.52901 5.34999 4.57634 5.54288 4.71234 5.67368L5.49834 6.43202L5.31301 7.50413C5.28101 7.68909 5.35634 7.87273 5.50967 7.98305C5.59634 8.04448 5.69701 8.07619 5.79901 8.07619C5.87701 8.07619 5.95567 8.05703 6.02767 8.01938L6.99967 7.51338L7.96967 8.01806C8.13767 8.10723 8.33701 8.09336 8.48967 7.98238C8.64367 7.87273 8.71901 7.68909 8.68701 7.50413L8.50101 6.43202L9.28701 5.67368C9.42367 5.54288 9.47101 5.34999 9.41167 5.17164C9.35301 4.99328 9.19967 4.86645 9.01434 4.84069L7.92834 4.68347L7.44234 3.70847C7.35901 3.54068 7.18967 3.43631 7.00101 3.43565Z"
              fill="white"
            />
          </svg>
          <div className="text_align_center margin_top_6">
            <span className="font_size_14 line_height_17 font_weight_600 color_white">Chính chủ</span>
          </div>
        </div>
      </div> */}
      <div className="book_mark position_absolute right_24 top_22">
        <svg width="16" height="20" viewBox="0 0 16 20" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path
            d="M11.07 0C13.78 0 15.97 1.07 16 3.79V18.97C16 19.14 15.96 19.31 15.88 19.46C15.75 19.7 15.53 19.88 15.26 19.96C15 20.04 14.71 20 14.47 19.86L7.99 16.62L1.5 19.86C1.351 19.939 1.18 19.99 1.01 19.99C0.45 19.99 0 19.53 0 18.97V3.79C0 1.07 2.2 0 4.9 0H11.07ZM11.75 6.04H4.22C3.79 6.04 3.44 6.39 3.44 6.83C3.44 7.269 3.79 7.62 4.22 7.62H11.75C12.18 7.62 12.53 7.269 12.53 6.83C12.53 6.39 12.18 6.04 11.75 6.04Z"
            fill="white"
            fillOpacity="0.6"
          />
        </svg>
      </div>
      {/* image list  */}
      <div className="images position_relative ">
        <div
          className="width_30 height_30 cursor_pointer position_absolute display_flex justify_content_center align_items_center 
          left_13 top_50_percent translate_0_-50_percent z_index_1"
          onClick={() => {
            slider.prevSlideIndex && slider.prevSlideIndex();
          }}
        >
          <svg width="6" height="12" viewBox="0 0 6 12" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path
              d="M5.68656 0.979699C5.86407 1.15721 5.88021 1.43499 5.73497 1.63073L5.68656 1.68681L1.37367 5.99992L5.68656 10.313C5.86407 10.4905 5.88021 10.7683 5.73497 10.9641L5.68656 11.0201C5.50905 11.1976 5.23127 11.2138 5.03553 11.0686L4.97945 11.0201L0.312788 6.35347C0.135277 6.17596 0.11914 5.89819 0.264376 5.70244L0.312788 5.64637L4.97945 0.979699C5.17472 0.784436 5.4913 0.784436 5.68656 0.979699Z"
              fill="white"
            />
          </svg>
        </div>
        <div
          className="width_30 height_30 cursor_pointer position_absolute display_flex justify_content_center align_items_center 
          right_13 top_50_percent translate_0_-50_percent z_index_1"
          onClick={() => {
            slider.nextSlideIndex && slider.nextSlideIndex();
          }}
        >
          <svg width="6" height="12" viewBox="0 0 6 12" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path
              d="M0.313439 11.0202C0.135928 10.8427 0.11979 10.5649 0.265027 10.3692L0.313439 10.3131L4.62633 5.99996L0.313439 1.68685C0.135928 1.50933 0.11979 1.23156 0.265027 1.03582L0.313439 0.979739C0.49095 0.802228 0.768726 0.786091 0.964467 0.931327L1.02055 0.979739L5.68721 5.64641C5.86472 5.82392 5.88086 6.10169 5.73562 6.29743L5.68721 6.35351L1.02055 11.0202C0.825283 11.2154 0.508701 11.2154 0.313439 11.0202Z"
              fill="white"
            />
          </svg>
        </div>
        <Slider
          className="border_radius_24"
          listImages={listImages as any}
          setListImages={setListImages}
          slidesNm={100}
          slidesMd={100}
          slidesSm={100}
          slidesXs={100}
          innerRef={slider}
          showButton={false}
          slidesPerView={1}
          isStopAutoPlay={true}
          margin={0}
          loop={true}
        >
          {listImages.map((item) => {
            return (
              <Link
                href={{ pathname: '/sell-detail-page', query: { postId: props.post.id } }}
                as={`/sell-listings/${props.post.id}`}
                key={item?.id}
              >
                <a
                  className="width_100_percent height_185 flex_none border_radius_18 width_xs_100_percent height_xs_220"
                  data-id={item?.id}
                >
                  <img
                    src={item?.url}
                    alt="post-images"
                    className="width_100_percent height_100_percent object_fit_cover"
                  />
                </a>
              </Link>
            );
          })}
        </Slider>
      </div>
      {/* information  */}
      <Link
        href={{ pathname: '/sell-detail-page', query: { postId: props.post.id } }}
        as={`/sell-listings/${props.post.id}`}
      >
        <a className="display_block">
          <div className="information padding_x_8 margin_top_12">
            <h2
              className="text_over_flow_1 font_size_16 line_height_20 font_weight_600 margin_y_0"
              title={props.post.title}
            >
              {props.post.title}
            </h2>
            <div className="display_flex align_items_center margin_top_8 padding_bottom_8 border_width_0 border_bottom_width_1 border_style_solid border_color_grey">
              <div className="width_22 display_flex flex_none">
                <svg width={10} height={14} viewBox="0 0 10 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path
                    d="M2.68791 0.655398C4.14448 -0.190933 5.93485 -0.176141 7.37775 0.694147C8.80647 1.58216 9.6748 3.167 9.66677 4.87185C9.63346 6.56551 8.70236 8.15755 7.53848 9.38828C6.86672 10.1018 6.11524 10.7328 5.29939 11.2682C5.21537 11.3168 5.12333 11.3494 5.02782 11.3642C4.93589 11.3603 4.84637 11.3331 4.76733 11.2852C3.52178 10.4806 2.42905 9.45357 1.54172 8.25352C0.799221 7.25179 0.377386 6.04155 0.333496 4.78716L0.33682 4.60468C0.397444 2.96731 1.28336 1.4715 2.68791 0.655398ZM5.605 3.3874C5.01288 3.13571 4.33019 3.27245 3.87571 3.73378C3.42122 4.19511 3.28459 4.89003 3.5296 5.49407C3.77462 6.09811 4.35295 6.49211 4.99456 6.49212C5.4149 6.49513 5.81895 6.32677 6.1167 6.02453C6.41445 5.7223 6.58115 5.31131 6.57967 4.88314C6.5819 4.22957 6.19713 3.63909 5.605 3.3874Z"
                    fill="#3B4144"
                    opacity="0.5"
                  />
                  <path
                    d="M5.00033 13.3642C6.84127 13.3642 8.33366 13.0657 8.33366 12.6976C8.33366 12.3294 6.84127 12.0309 5.00033 12.0309C3.15938 12.0309 1.66699 12.3294 1.66699 12.6976C1.66699 13.0657 3.15938 13.3642 5.00033 13.3642Z"
                    fill="#3B4144"
                    opacity="0.5"
                  />
                </svg>
              </div>
              <span
                className="text_over_flow_1 font_size_12 line_height_18 opacity_0_5 text_transform_capitalize"
                title={renderAddress(props.post)}
              >
                {renderAddress(props.post)}
              </span>
            </div>
            <div className="margin_top_8 display_flex padding_x_2">
              <div className="width_50_percent">
                <div className="display_flex align_items_center margin_bottom_6">
                  <div className="width_22 display_flex">{renderIconRoom('BEDROOM')}</div>
                  <span className="text_over_flow_1 font_size_12 line_height_18 opacity_0_5">
                    {props.post.bedroom ? props.post.bedroom + ' phòng ngủ' : 'Liên hệ'}
                  </span>
                </div>
                <div className="display_flex align_items_center margin_bottom_6">
                  <div className="width_22 display_flex">{renderIconRoom('BATHROOM')}</div>
                  <span className="text_over_flow_1 font_size_12 line_height_18 opacity_0_5">
                    {props.post.bathroom ? props.post.bathroom + ' vệ sinh' : 'Liên hệ'}
                  </span>
                </div>
              </div>
              <div className="width_50_percent">
                <div className="display_flex align_items_center margin_bottom_6">
                  <div className="width_22 display_flex">
                    <svg width={16} height={17} viewBox="0 0 16 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path
                        opacity="0.5"
                        d="M14.5042 13.8178L13.0932 12.4067C13.3843 11.9673 13.5559 11.4423 13.5559 10.8773C13.5559 9.34569 12.3098 8.09961 10.7782 8.09961C9.24657 8.09961 8.00049 9.34569 8.00049 10.8773C8.00049 12.409 9.24657 13.655 10.7782 13.655C11.3432 13.655 11.8682 13.4834 12.3076 13.1923L13.7187 14.6034C13.9359 14.8206 14.287 14.8206 14.5042 14.6034C14.7215 14.3861 14.7215 14.035 14.5042 13.8178ZM10.7782 12.544C9.85934 12.544 9.11158 11.7962 9.11158 10.8773C9.11158 9.95846 9.85934 9.2107 10.7782 9.2107C11.6971 9.2107 12.4448 9.95846 12.4448 10.8773C12.4448 11.7962 11.6971 12.544 10.7782 12.544Z"
                        fill="#3B4144"
                      />
                      <path
                        opacity="0.5"
                        d="M1.33374 2.5309V9.66816C1.33374 10.2737 1.83207 10.7662 2.44485 10.7662H4.66707C5.27985 10.7662 5.77818 10.2737 5.77818 9.66816V5.82502H13.556C14.1687 5.82502 14.6671 5.33255 14.6671 4.72698V2.5309C14.6671 1.92533 14.1687 1.43286 13.556 1.43286H2.44485C1.83207 1.43286 1.33374 1.92533 1.33374 2.5309Z"
                        fill="#3B4144"
                      />
                    </svg>
                  </div>
                  <span className="text_over_flow_1 font_size_12 line_height_18 opacity_0_5">{props.post.area}M2</span>
                </div>
                <div className="display_flex align_items_center margin_bottom_6">
                  <div className="width_22 display_flex">
                    <svg width={14} height={14} viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path
                        opacity="0.5"
                        d="M7.00041 0.48291C10.6804 0.48291 13.6671 3.46958 13.6671 7.14958C13.6671 10.8362 10.6804 13.8162 7.00041 13.8162C3.31374 13.8162 0.33374 10.8362 0.33374 7.14958C0.33374 3.46958 3.31374 0.48291 7.00041 0.48291ZM9.56707 4.95624C9.64041 4.72291 9.42707 4.50291 9.19374 4.57624L5.78041 5.64291C5.64041 5.68958 5.52707 5.79624 5.48707 5.93624L4.42041 9.35624C4.34707 9.58291 4.56707 9.80291 4.79374 9.72958L8.19374 8.66291C8.33374 8.62291 8.44707 8.50958 8.48707 8.36958L9.56707 4.95624Z"
                        fill="#3B4144"
                      />
                    </svg>
                  </div>
                  <span className="text_over_flow_1 font_size_12 line_height_18 opacity_0_5">
                    {props.post.direction.content}
                  </span>
                </div>
              </div>
            </div>
            <div className="display_flex justify_content_sb align_items_center margin_top_6">
              <button
                className="cursor_pointer padding_y_8 padding_x_24 font_size_14 font_weight_600 line_height_20 border_width_1 border_style_solid border_color_green bg_white 
            color_green border_radius_8"
              >
                {props.post.typeTransaction?.content || 'Bán nhà'}
              </button>
              <div className="price">
                <span className="font_size_16 line_height_20 font_weight_600">{renderPrice(props?.post?.price)}</span>
              </div>
            </div>
          </div>
        </a>
      </Link>
    </div>
  );
};

export default PostItem;
