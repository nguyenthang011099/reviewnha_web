import React, { useEffect, useState } from 'react';
import logo from '../../images/logo.png';
import Link from 'next/link';
import { RegisterParams } from '../../types/type.auth';
import { useDispatch, useSelector } from 'react-redux';
import { register } from '../../reducers/auth/actions';
import { Status } from '../../types/type.status';
import { RootState } from '../../reducers';
import { useRouter } from 'next/router';
import InputText from '../../components/base/input-text/InputText';
import './Register.scss';

const Register: React.FC = () => {
  const [phone, setPhone] = useState<string>('');
  const [name, setName] = useState<string>('');
  const [email, setEmail] = useState<string>('');
  const [password, setPassword] = useState<string>('');
  const [cfPassword, setCfPassword] = useState<string>('');
  const [warningType, setWarningType] = useState<string>('');
  const [warningMessage, setWarningMessage] = useState<string>('');
  const dispatch = useDispatch();
  const registerStatus: Status = useSelector((state: RootState) => state.auth.registerStatus as Status);
  const router = useRouter();
  const authenticated: boolean = useSelector((state: RootState) => state.auth.authenticated);

  /**
   * lắng nghe khi submit đăng ký
   */
  useEffect(() => {
    if (registerStatus === 'success') {
      router.push({ pathname: '/login-page' }, '/login');
    }
  }, [registerStatus]);

  useEffect(() => {
    if (authenticated) {
      router.push({ pathname: '/home-page' }, '/');
    }
  }, []);

  /**
   * changePhone change và validate phone
   * @param value value change
   */
  const changePhone = (value: string) => {
    const validatedValue = value.replace(/[^0-9+()]/g, '');
    setPhone(validatedValue);
  };

  /**
   * changeName
   * @param value value change
   */
  const changeName = (value: string) => {
    setName(value);
  };

  /**
   * changeEmail
   * @param value value change
   */
  const changeEmail = (value: string) => {
    setEmail(value);
  };

  /**
   * changePassword
   * @param value value change
   */
  const changePassword = (value: string) => {
    setPassword(value);
  };

  /**
   * changeConfirmPassword
   * @param value value change
   */
  const changeCfPassword = (value: string) => {
    setCfPassword(value);
  };

  /**
   * onRegister
   */
  const onRegister = (e: React.FormEvent<HTMLButtonElement>) => {
    if (name && email && phone && password && cfPassword) {
      e.preventDefault();
      if (password.includes(' ')) {
        setWarningType('password');
        setWarningMessage('Mật khẩu không được chứa dấu cách');
        return;
      }
      if (password.length < 6) {
        setWarningType('password');
        setWarningMessage('Mật khẩu phải có ít nhất 6 kí tự');
        return;
      }
      if (password === cfPassword) {
        const data: RegisterParams = {
          name,
          email,
          phone,
          password,
          type: 'seller',
          password_confirmation: cfPassword,
        };
        dispatch(register(data));
      } else {
        setWarningType('cfPassword');
        setWarningMessage('Mật khẩu không khớp');
      }
    }
  };

  if (authenticated) {
    return null;
  }

  return (
    <div className="register_page display_flex height_100_percent auth  align_items_xs_fs flex_direction_xs_col">
      <div className="logo col_6 display_flex align_items_center justify_content_fe col_xs_12 display_xs_block padding_y_xs_24">
        <Link href={{ pathname: '/home-page' }} as="/">
          <a>
            <div className="padding_right_150 padding_x_xs_22 logo_image_auth">
              <div className="margin_bottom_15 margin_bottom_xs_4">
                <img src={logo} alt="review nha" className="height_xs_30" />
              </div>
              <div>
                <span className="font_size_36 line_height_57 font_logo font_size_xs_16 line_height_xs_16">
                  Finding your best home
                </span>
              </div>
            </div>
          </a>
        </Link>
      </div>
      <div className="form_login col_6 display_flex align_items_center margin_left_12 col_xs_12 margin_left_xs_0 padding_x_xs_22">
        <div className="width_xs_100_percent">
          <form className="width_575 width_xs_100_percent">
            <h2 className="margin_y_0 margin_bottom_36 font_size_32 line_height_39 font_weight_600">
              Đăng ký tài khoản
            </h2>
            <div>
              <InputText
                name="name"
                placeholder="Họ và tên của bạn"
                required={true}
                type="text"
                label="Họ tên:"
                value={name}
                setValue={changeName}
                isWarning={warningType === 'name'}
                setIsWarning={setWarningType}
              />
              <InputText
                label="SĐT:"
                type="tel"
                name="phone"
                placeholder="Số điện thoại"
                value={phone}
                setValue={changePhone}
                required={true}
                isWarning={warningType === 'phone'}
                setIsWarning={setWarningType}
              />
              <InputText
                label="Email:"
                type="text"
                name="email"
                placeholder="Email"
                required={true}
                value={email}
                setValue={changeEmail}
                isWarning={warningType === 'email'}
                setIsWarning={setWarningType}
              />
              <InputText
                label="Mật khẩu:"
                name="password"
                placeholder="Mật khẩu của bạn"
                type="password"
                required={true}
                value={password}
                setValue={changePassword}
                isWarning={warningType === 'password'}
                setIsWarning={setWarningType}
              />
              <InputText
                label="Xác nhận lại:"
                name="password"
                placeholder="Xác nhận lại mật khẩu"
                type="password"
                required={true}
                value={cfPassword}
                setValue={changeCfPassword}
                isWarning={warningType === 'cfPassword'}
                setIsWarning={setWarningType}
              />
              {warningType ? (
                <div className="margin_bottom_16 margin_top_-8 padding_left_24">
                  <span className="font_size_12 line_height_18 color_red">{warningMessage}</span>
                </div>
              ) : null}
              <div className="margin_top_36 display_flex justify_content_fe align_items_center width_xs_100_percent">
                <div className="width_xs_100_percent">
                  <button
                    className="font_size_14 line_height_17 font_weight_600 height_32 width_215 bg_red border_none color_white
                  border_radius_25 width_xs_100_percent"
                    type="submit"
                    onClick={onRegister}
                  >
                    Đăng ký
                  </button>
                </div>
              </div>
            </div>
          </form>
          <div className="margin_top_52 display_flex align_items_center">
            <span className="font_size_15 line_height_22 margin_right_16">Bạn đã có tài khoản?</span>
            <Link href={{ pathname: '/login-page' }} as="/login">
              <a>
                <span className="font_size_14 line_height_17 font_weight_600 margin_right_16 color_red">Đăng nhập</span>
              </a>
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Register;
