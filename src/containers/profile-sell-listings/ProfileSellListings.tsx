import dayjs from 'dayjs';
import React, { useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Confirm from '../../components/confirm/Confirm';
import { RootState } from '../../reducers';
import { deleteSellListing } from '../../reducers/sell-listing/actions';
import { SellListing } from '../../types/type.sell-listing';
import SellCreatePopup from '../sell-create-popup/SellCreatePopup';
import './ProfileSellListing.scss';
import api from '../../api';
import { openNotification } from '../../reducers/notification/actions';
import Link from 'next/link';

const ProfileSellListings: React.FC = () => {
  const allSellListings = useSelector((state: RootState) => state.sellListing.auth.allSellListings);
  const [isOpen, setIsOpen] = useState<boolean>(false);
  const [activeId, setActiveId] = useState<number>(0);
  const [editSell, setEditSell] = useState<SellListing>({});
  const dispatch = useDispatch();
  const editSellPopup: any = useRef({});

  const getRemainingDate = (endTime: string) => {
    const remain = dayjs(endTime).diff(new Date(), 'day');
    if (remain) {
      return 'Còn ' + remain + ' ngày';
    }
    return 'Hết hạn';
  };

  const onDeleteSellListing = () => {
    if (activeId) {
      dispatch(deleteSellListing(activeId));
      setIsOpen(false);
    }
  };

  const renderStatus = (status: 'draft' | 'published' | 'cancel') => {
    switch (status) {
      case 'draft': {
        return <span className="padding_x_16 padding_y_2 border_radius_25 bg_grey">Chờ phê duyệt</span>;
      }
      case 'published': {
        return (
          <span
            className="padding_x_16 padding_y_2 border_radius_25"
            style={{ background: 'rgba(80, 198, 78, 0.3)', color: 'rgb(80, 198, 78)' }}
          >
            Đã phê duyệt
          </span>
        );
      }
      default: {
        return (
          <span
            className="padding_x_16 padding_y_2 border_radius_25 color_red"
            style={{ background: 'rgba(217,60,35,0.3)' }}
          >
            Đã hủy
          </span>
        );
      }
    }
  };

  const upgrade = (id: number) => {
    api.post(`/sell-listings/${id}/service-upgrade`).then((res: any) => {
      dispatch(
        openNotification({
          type: res.success ? 'success' : 'error',
          title: res.message,
          isOpen: true,
        }),
      );
    });
  };

  const extend = (id: number) => {
    api.post(`/sell-listings/${id}/service-extension`).then((res: any) => {
      dispatch(
        openNotification({
          type: res.success ? 'success' : 'error',
          title: res.message,
          isOpen: true,
        }),
      );
    });
  };

  return (
    <div className="margin_top_30 overflow_auto scroll_bar_width_5 margin_top_xs_18 profile_sell_listings">
      <Confirm
        isOpen={isOpen}
        setIsOpen={setIsOpen}
        title="Bạn có chắc chắn muốn xóa không?"
        onReject={() => {
          setIsOpen(false);
        }}
        onAccept={onDeleteSellListing}
      ></Confirm>
      <div className="width_1168 display_xs_none">
        {/* header */}
        <div className="display_flex border_width_0 border_bottom_width_1 border_style_solid border_color_grey padding_left_24 padding_bottom_10 margin_x_2">
          <div className="text_align_left font_size_14 line_height_24 font_weight_400 width_42">#</div>
          <div className="text_align_left font_size_14 line_height_24 font_weight_400 border_width_0 width_450">
            Tiêu đề
          </div>
          <div className="text_align_left font_size_14 line_height_24 font_weight_400 border_width_0 width_162">
            Trạng thái
          </div>
          <div className="text_align_left font_size_14 line_height_24 font_weight_400 border_width_0 width_83">
            Loại tin
          </div>
          <div className="text_align_left font_size_14 line_height_24 font_weight_400 border_width_0 width_128">
            Thời hạn
          </div>
          <div className="text_align_left font_size_14 line_height_24 font_weight_400 border_width_0 width_342 flex_1">
            Tác vụ
          </div>
        </div>
        {/* sell listings */}
        {allSellListings.lists?.map((sell: SellListing, index: number) => {
          return (
            <div
              className="display_flex padding_left_26 padding_y_12 margin_y_16 border_radius_18 bg_grey_3-hover box_shadow_primary-hover align_items_center margin_x_2"
              key={sell.id}
            >
              <div className="text_align_left font_size_14 line_height_24 font_weight_400 width_42">{index + 1}</div>
              <Link
                href={{ pathname: '/sell-detail-page', query: { postId: sell.id } }}
                as={`/sell-listings/${sell.id}`}
              >
                <a className="text_align_left font_size_14 line_height_24 font_weight_400 border_width_0 width_450 text_over_flow_1 cursor_pointer padding_y_5">
                  {sell.title}
                </a>
              </Link>
              <div className="text_align_left font_size_14 line_height_24 font_weight_400 border_width_0 width_162">
                {renderStatus(sell.status)}
              </div>
              <div className="text_align_left font_size_14 line_height_24 font_weight_400 border_width_0 width_83 text_transform_uppercase">
                {sell.serviceCode}
              </div>
              <div className="text_align_left font_size_14 line_height_24 font_weight_400 border_width_0 width_128">
                {sell.endTimeActive ? getRemainingDate(sell.endTimeActive) : 'Không có thông tin'}
              </div>
              <div className="text_align_left font_size_14 line_height_24 font_weight_400 border_width_0 width_342 flex_1 display_flex align_items_center">
                <button
                  className="bg_black color_white border_radius_25 border_width_1 border_style_solid border_color_black display_flex padding_x_20 padding_y_6 
              font_size_14 line_height_24 margin_right_8"
                  onClick={() => {
                    extend(sell.id);
                  }}
                >
                  Gia hạn
                </button>
                <button
                  className="bg_white border_radius_25 border_width_1 border_style_solid border_color_grey display_flex padding_x_20 padding_y_6 
              font_size_14 line_height_24 margin_right_8"
                  onClick={() => {
                    upgrade(sell.id);
                  }}
                >
                  Nâng cấp
                </button>
                <div
                  className="padding_y_8 cursor_pointer no_select display_flex margin_right_8"
                  onClick={() => {
                    setEditSell(sell);
                    if (editSellPopup.current && editSellPopup.current.togglePopup) {
                      console.log(editSellPopup);
                      editSellPopup.current.togglePopup(true);
                    }
                  }}
                >
                  <svg width={18} height={18} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path
                      d="M3 17.4601V20.5001C3 20.7801 3.22 21.0001 3.5 21.0001H6.54C6.67 21.0001 6.8 20.9501 6.89 20.8501L17.81 9.94006L14.06 6.19006L3.15 17.1001C3.05 17.2001 3 17.3201 3 17.4601ZM20.71 7.04006C21.1 6.65006 21.1 6.02006 20.71 5.63006L18.37 3.29006C17.98 2.90006 17.35 2.90006 16.96 3.29006L15.13 5.12006L18.88 8.87006L20.71 7.04006Z"
                      fill="#323232"
                    />
                  </svg>
                </div>
                <div
                  className="padding_y_8 cursor_pointer no_select display_flex"
                  onClick={() => {
                    setIsOpen(true);
                    setActiveId(sell.id);
                  }}
                >
                  <svg width={15} height={16} viewBox="0 0 15 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path
                      fillRule="evenodd"
                      clipRule="evenodd"
                      d="M12.5385 5.53978C12.8482 5.56528 13.0792 5.83603 13.0544 6.14578C13.0499 6.19678 12.6435 11.23 12.4095 13.3413C12.264 14.6515 11.3984 15.4488 10.0919 15.4728C9.0922 15.49 8.1277 15.4998 7.18495 15.4998C6.1687 15.4998 5.17795 15.4885 4.1977 15.4683C2.9437 15.4435 2.07595 14.6305 1.9342 13.3465C1.69795 11.2165 1.2937 6.19603 1.28995 6.14578C1.26445 5.83603 1.49545 5.56453 1.8052 5.53978C2.11045 5.53153 2.38645 5.74603 2.4112 6.05503C2.41359 6.08758 2.57886 8.1378 2.75895 10.1663L2.79512 10.5711C2.88582 11.5794 2.97777 12.5483 3.05245 13.2228C3.1327 13.9525 3.52645 14.329 4.22095 14.3433C6.09595 14.383 8.0092 14.3853 10.0717 14.3478C10.8097 14.3335 11.2087 13.9645 11.2912 13.2175C11.5237 11.122 11.9287 6.10603 11.9332 6.05503C11.9579 5.74603 12.2317 5.53003 12.5385 5.53978ZM8.75905 0.5C9.44755 0.5 10.0528 0.96425 10.2305 1.6295L10.4211 2.57525C10.4826 2.88528 10.7547 3.11169 11.0697 3.11667L13.531 3.11675C13.8415 3.11675 14.0935 3.36875 14.0935 3.67925C14.0935 3.98975 13.8415 4.24175 13.531 4.24175L11.0917 4.24164C11.0879 4.24171 11.0841 4.24175 11.0803 4.24175L11.062 4.241L3.28121 4.24166C3.27517 4.24172 3.26911 4.24175 3.26305 4.24175L3.2515 4.241L0.8125 4.24175C0.502 4.24175 0.25 3.98975 0.25 3.67925C0.25 3.36875 0.502 3.11675 0.8125 3.11675L3.27325 3.116L3.34902 3.1112C3.63123 3.07459 3.86578 2.86025 3.92305 2.57525L4.1053 1.66325C4.29055 0.96425 4.8958 0.5 5.5843 0.5H8.75905ZM8.75905 1.625H5.5843C5.4043 1.625 5.24605 1.74575 5.2003 1.919L5.02555 2.7965C5.00334 2.90765 4.971 3.01476 4.92961 3.11695H9.41396C9.37252 3.01476 9.34011 2.90765 9.3178 2.7965L9.13555 1.8845C9.0973 1.74575 8.93905 1.625 8.75905 1.625Z"
                      fill="black"
                    />
                  </svg>
                </div>
              </div>
            </div>
          );
        })}
      </div>
      {/* mobile  */}
      <div className="display_xs_block display_none">
        {allSellListings.lists?.map((sell: SellListing, index: number) => {
          return (
            <div
              className="display_flex padding_y_12 margin_y_12 border_radius_18 align_items_center margin_x_2"
              key={sell.id}
            >
              <div className="text_align_left font_size_14 line_height_24 font_weight_600 border_width_0 width_450 text_over_flow_1 cursor_pointer padding_y_5">
                {sell.title}
              </div>
              <div className="display_flex more_settings">
                <svg width={24} height={24} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path
                    d="M12 22C17.5 22 22 17.5 22 12C22 6.5 17.5 2 12 2C6.5 2 2 6.5 2 12C2 17.5 6.5 22 12 22Z"
                    stroke="#3B4144"
                    strokeWidth="1.5"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                  />
                  <path
                    d="M15.9965 12H16.0054"
                    stroke="#3B4144"
                    strokeWidth={2}
                    strokeLinecap="round"
                    strokeLinejoin="round"
                  />
                  <path
                    d="M11.9955 12H12.0045"
                    stroke="#3B4144"
                    strokeWidth={2}
                    strokeLinecap="round"
                    strokeLinejoin="round"
                  />
                  <path
                    d="M7.99451 12H8.00349"
                    stroke="#3B4144"
                    strokeWidth={2}
                    strokeLinecap="round"
                    strokeLinejoin="round"
                  />
                </svg>
                <div className="popup_setting position_absolute padding_x_18 padding_y_24 border_radius_18 border_width_1 border_color_grey border_style_solid right_0 z_index_1 bg_white width_210">
                  <div className="border_tooltip"></div>
                  <div
                    className="display_flex align_items_center padding_bottom_16"
                    onClick={() => {
                      extend(sell.id);
                    }}
                  >
                    <svg width={18} height={20} viewBox="0 0 18 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path
                        d="M10.2525 0C10.5 0 10.7079 0.21 10.7079 0.46V3.68C10.7079 5.51 12.1931 7.01 14.0149 7.02C14.7574 7.02 15.3515 7.03 15.8069 7.03C16.1139 7.03 16.6188 7.02 17.0446 7.02C17.2921 7.02 17.5 7.22 17.5 7.47V15.51C17.5 17.99 15.5 20 13.0446 20H5.17327C2.58911 20 0.5 17.89 0.5 15.29V4.51C0.5 2.03 2.4901 0 4.96535 0H10.2525ZM8.63861 6.74C8.5396 6.74 8.44059 6.76 8.35149 6.8C8.26238 6.84 8.18317 6.89 8.11386 6.96L5.28218 9.84C4.99505 10.13 4.99505 10.6 5.28218 10.89C5.56931 11.18 6.03465 11.18 6.32178 10.89L7.89604 9.29V14.12C7.89604 14.53 8.22277 14.86 8.63861 14.86C9.04455 14.86 9.37129 14.53 9.37129 14.12V9.29L10.9455 10.89C11.2327 11.18 11.698 11.18 11.9851 10.89C12.2822 10.6 12.2822 10.13 11.995 9.84L9.15347 6.96C9.08416 6.89 9.00495 6.84 8.91584 6.8C8.82673 6.76 8.73762 6.74 8.63861 6.74ZM12.1452 0.906C12.1452 0.475 12.6621 0.261 12.9581 0.572C14.0294 1.696 15.8997 3.661 16.9452 4.759C17.2334 5.062 17.0215 5.565 16.6047 5.566C15.7908 5.569 14.8324 5.566 14.1423 5.559C13.0472 5.559 12.1452 4.648 12.1452 3.542V0.906Z"
                        fill="#3B4144"
                      />
                    </svg>
                    <span className="margin_left_18 font_size_14 line_height_17 font_weight_600 white_space_nowrap">
                      Gia hạn
                    </span>
                  </div>
                  <div
                    className="display_flex align_items_center padding_bottom_16"
                    onClick={() => {
                      upgrade(sell.id);
                    }}
                  >
                    <svg width={18} height={20} viewBox="0 0 18 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path
                        d="M10.2525 0C10.5 0 10.7079 0.21 10.7079 0.46V3.68C10.7079 5.51 12.1931 7.01 14.0149 7.02C14.7574 7.02 15.3515 7.03 15.8069 7.03C16.1139 7.03 16.6188 7.02 17.0446 7.02C17.2921 7.02 17.5 7.22 17.5 7.47V15.51C17.5 17.99 15.5 20 13.0446 20H5.17327C2.58911 20 0.5 17.89 0.5 15.29V4.51C0.5 2.03 2.4901 0 4.96535 0H10.2525ZM8.63861 6.74C8.5396 6.74 8.44059 6.76 8.35149 6.8C8.26238 6.84 8.18317 6.89 8.11386 6.96L5.28218 9.84C4.99505 10.13 4.99505 10.6 5.28218 10.89C5.56931 11.18 6.03465 11.18 6.32178 10.89L7.89604 9.29V14.12C7.89604 14.53 8.22277 14.86 8.63861 14.86C9.04455 14.86 9.37129 14.53 9.37129 14.12V9.29L10.9455 10.89C11.2327 11.18 11.698 11.18 11.9851 10.89C12.2822 10.6 12.2822 10.13 11.995 9.84L9.15347 6.96C9.08416 6.89 9.00495 6.84 8.91584 6.8C8.82673 6.76 8.73762 6.74 8.63861 6.74ZM12.1452 0.906C12.1452 0.475 12.6621 0.261 12.9581 0.572C14.0294 1.696 15.8997 3.661 16.9452 4.759C17.2334 5.062 17.0215 5.565 16.6047 5.566C15.7908 5.569 14.8324 5.566 14.1423 5.559C13.0472 5.559 12.1452 4.648 12.1452 3.542V0.906Z"
                        fill="#3B4144"
                      />
                    </svg>
                    <span className="margin_left_18 font_size_14 line_height_17 font_weight_600 white_space_nowrap">
                      Nâng cấp tin
                    </span>
                  </div>
                  <div
                    className="display_flex align_items_center padding_bottom_16"
                    onClick={() => {
                      setEditSell(sell);
                      if (editSellPopup.current && editSellPopup.current.togglePopup) {
                        editSellPopup.current.togglePopup(true);
                      }
                    }}
                  >
                    <svg width={18} height={18} viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path
                        d="M16.9898 15.9533C17.5469 15.9533 18 16.4123 18 16.9766C18 17.5421 17.5469 18 16.9898 18H11.2797C10.7226 18 10.2695 17.5421 10.2695 16.9766C10.2695 16.4123 10.7226 15.9533 11.2797 15.9533H16.9898ZM13.0299 0.699064L14.5049 1.87078C15.1097 2.34377 15.513 2.96726 15.6509 3.62299C15.8101 4.3443 15.6403 5.0527 15.1628 5.66544L6.3764 17.0279C5.97316 17.5439 5.37891 17.8341 4.74222 17.8449L1.24039 17.8879C1.04939 17.8879 0.890213 17.7589 0.847767 17.5761L0.0518984 14.1255C-0.086052 13.4912 0.0518984 12.8355 0.455138 12.3303L6.68413 4.26797C6.79025 4.13898 6.98126 4.11855 7.1086 4.21422L9.72966 6.29967C9.89944 6.43942 10.1329 6.51467 10.377 6.48242C10.8969 6.41792 11.2471 5.94493 11.1941 5.43969C11.1622 5.1817 11.0349 4.96671 10.8651 4.80546C10.812 4.76246 8.31832 2.76301 8.31832 2.76301C8.15914 2.63401 8.12731 2.39752 8.25465 2.23735L9.24152 0.957057C10.1541 -0.214663 11.7459 -0.322161 13.0299 0.699064Z"
                        fill="#3B4144"
                      />
                    </svg>
                    <span className="margin_left_18 font_size_14 line_height_17 font_weight_600 white_space_nowrap">
                      Sửa thông tin
                    </span>
                  </div>
                  <div
                    className="display_flex align_items_center"
                    onClick={() => {
                      setIsOpen(true);
                      setActiveId(sell.id);
                    }}
                  >
                    <svg width={18} height={20} viewBox="0 0 18 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path
                        d="M15.9391 6.69713C16.1384 6.69713 16.3193 6.78413 16.4623 6.93113C16.5955 7.08813 16.6626 7.28313 16.6432 7.48913C16.6432 7.55712 16.1102 14.2971 15.8058 17.134C15.6152 18.875 14.4929 19.932 12.8094 19.961C11.5149 19.99 10.2496 20 9.00379 20C7.68112 20 6.38763 19.99 5.13206 19.961C3.50498 19.922 2.38168 18.846 2.20079 17.134C1.88763 14.2871 1.36439 7.55712 1.35467 7.48913C1.34494 7.28313 1.41108 7.08813 1.54529 6.93113C1.67756 6.78413 1.86818 6.69713 2.06852 6.69713H15.9391ZM11.0647 0C11.9488 0 12.7385 0.616994 12.967 1.49699L13.1304 2.22698C13.2627 2.82197 13.7781 3.24297 14.3714 3.24297H17.2871C17.6761 3.24297 18 3.56596 18 3.97696V4.35696C18 4.75795 17.6761 5.09095 17.2871 5.09095H0.713853C0.32386 5.09095 0 4.75795 0 4.35696V3.97696C0 3.56596 0.32386 3.24297 0.713853 3.24297H3.62957C4.22185 3.24297 4.7373 2.82197 4.87054 2.22798L5.02323 1.54598C5.26054 0.616994 6.0415 0 6.93527 0H11.0647Z"
                        fill="#3B4144"
                      />
                    </svg>
                    <span className="margin_left_18 font_size_14 line_height_17 font_weight_600 white_space_nowrap">
                      Xóa tin
                    </span>
                  </div>
                </div>
              </div>
            </div>
          );
        })}
      </div>
      <div className="position_absolute" style={{ left: '-1000px' }}>
        <SellCreatePopup innerRef={editSellPopup} editMode={true} sellListing={editSell}></SellCreatePopup>
      </div>
    </div>
  );
};

export default ProfileSellListings;
