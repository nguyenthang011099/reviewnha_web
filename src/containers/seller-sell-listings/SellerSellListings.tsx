import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import HighlightPostItem from '../../components/highlight-post-item/HighlightPostItem';
import PostItem from '../../components/post-item/PostItem';
import { RootState } from '../../reducers';
import { SellListing } from '../../types/type.sell-listing';
import ImageViewPopup from '../../components/image-view/ImageViewPopup';

const SellerSellListings: React.FC = () => {
  const [images, setImages] = useState<{ url: string }[]>([]);
  const [isOpenViewImages, setIsOpenViewImages] = useState<boolean>(false);
  const allSellListings = useSelector((state: RootState) => state.sellListing.seller.allSellListings);
  const highlightSellListings = useSelector((state: RootState) => state.sellListing.seller.highlightSellListings);

  const setViewImages = (images: { url: string }[]) => {
    setImages(images);
    setIsOpenViewImages(true);
  };

  return (
    <div className="sell_listings_profile">
      {/* highlight*/}
      <div className="highlights">
        <div className="display_flex margin_x_-11 flex_wrap">
          <ImageViewPopup images={images} isOpen={isOpenViewImages} setIsOpen={setIsOpenViewImages}></ImageViewPopup>
          {highlightSellListings.lists?.map((post: SellListing) => {
            return (
              <div className="col_4 padding_x_11 col_xs_12" key={post.id}>
                <HighlightPostItem post={post} setViewImages={setViewImages}></HighlightPostItem>
              </div>
            );
          })}
        </div>
      </div>
      {/* end highlight*/}
      {/* selled */}
      <div className="sell_listings_by_project margin_top_36">
        <div className="title margin_bottom_24">
          <span className="font_weight_600 line_height_27 font_size_18">Tất cả tin rao</span>
        </div>
        {!highlightSellListings.loadingLists ? (
          <div className="list display_flex margin_x_-10 flex_wrap">
            {highlightSellListings.lists.map((post: SellListing) => {
              return (
                <div key={post.id} className="padding_x_10 col_3 col_xs_12">
                  <PostItem post={post}></PostItem>
                </div>
              );
            })}
          </div>
        ) : null}
      </div>
      {/* end selled */}
    </div>
  );
};

export default SellerSellListings;
