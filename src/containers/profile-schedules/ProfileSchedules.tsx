import dayjs from 'dayjs';
import React, { useEffect, useRef, useState } from 'react';
import { useDispatch } from 'react-redux';
import customerApi from '../../api/customer';
import avatar from '../../images/user_avatar.png';
import { openNotification } from '../../reducers/notification/actions';
import { Schedule } from '../../types/type.schedule';
import classnames from 'classnames';
import useOutsideClick from '../../utils/customHooks/useOutsideClick';
import Popup from '../../components/popup/Popup';
import api from '../../api';

interface Props {
  list: Schedule[];
}

const ProfileSchedules: React.FC<Props> = (props) => {
  const [scheduleList, setScheduleList] = useState<Schedule[]>([]);
  const dispatch = useDispatch();
  const [status, setStatus] = useState({
    value: 'Chưa bắt đầu',
    scheduleId: 0,
  });
  const statusList = [
    {
      title: 'Chưa bắt đầu',
      value: 0,
    },
    {
      title: 'Hoàn thành',
      value: 1,
    },
    {
      title: 'Hủy',
      value: -1,
    },
  ];
  const [isOpenStatus, setIsOpenStatus] = useState<boolean>(false);
  const [chooseStatusPosition, setChooseStatusPosition] = useState<{ x: number; y: number }>({ x: 0, y: 0 });
  const [isShowDetail, setIsShowDetail] = useState<boolean>(false);
  const [showSchedule, setShowSchedule] = useState<Schedule>({});
  const $selectStatusRef = useRef(null);
  const isOpenCached = useRef(null);

  useEffect(() => {
    isOpenCached.current = isOpenStatus;
  }, [isOpenStatus]);

  useOutsideClick($selectStatusRef, () => {
    if (isOpenCached.current) {
      setIsOpenStatus(false);
    }
  });

  useEffect(() => {
    setScheduleList(props.list);
  }, [props.list]);

  const createCustomer = (scheduleId: number, isCustomer: boolean) => {
    if (isCustomer) {
      dispatch(
        openNotification({
          type: 'error',
          title: 'Khách hàng đã tồn tại!',
          isOpen: true,
        }),
      );
    } else {
      customerApi.create(scheduleId).then((res: any) => {
        if (res.success) {
          dispatch(
            openNotification({
              type: 'success',
              title: 'Tạo khách hàng thành công!',
              isOpen: true,
            }),
          );
        }
      });
    }
  };

  const onOpenChooseStatus = (e: React.MouseEvent, scheduleId: number, status: string) => {
    const $target = e.currentTarget;
    if ($target) {
      const screenX = $target.getBoundingClientRect().left;
      const screenY = $target.getBoundingClientRect().top;
      setChooseStatusPosition({
        x: screenX - 6,
        y: screenY + 24,
      });
      setIsOpenStatus(!isOpenStatus);
      setStatus({
        scheduleId,
        value: status,
      });
    }
  };

  const changeStatus = async (title: string, value: number) => {
    const res = (await api.post(`/schedule/${status.scheduleId}/update`, { status: value })) as any;
    if (res.success) {
      const newScheduleList = scheduleList.map((schedule: Schedule) => {
        if (schedule.id === status.scheduleId) {
          return {
            ...schedule,
            status: title,
          };
        }
        return schedule;
      });
      setScheduleList(newScheduleList);
      setIsOpenStatus(false);
    } else {
      dispatch(
        openNotification({
          title: 'Cập nhật thât bại',
          type: 'error',
          isOpen: true,
        }),
      );
    }
  };

  return (
    <div className="">
      <Popup isOpen={isShowDetail} setIsOpen={setIsShowDetail} className="border_radius_18 height_auto">
        <div className="padding_x_24 padding_y_36 width_574 width_xs_100_percent">
          <h2 className="margin_0 margin_bottom_24 font_size_16">Chi tiết lịch hẹn</h2>
          <div>
            <div className="display_flex align_items_center margin_bottom_24 display_xs_block">
              <div className="font_size_16 width_130 margin_bottom_xs_8 font_weight_xs_600">Tên: </div>
              <div className="text_align_left font_size_14 line_height_24 font_weight_400 border_width_0 width_153 display_flex align_items_center">
                <div className="display_flex margin_right_16">
                  <img src={avatar} alt="avatar" className="width_32 height_32 border_radius_50_percent" />
                </div>
                <span className="word_break" title={showSchedule.name}>
                  {showSchedule.name}
                </span>
              </div>
            </div>
            <div className="display_flex align_items_center margin_bottom_24 display_xs_block">
              <div className="font_size_16 width_130 margin_bottom_xs_8 font_weight_xs_600">Yêu cầu: </div>
              <div className="text_align_left font_size_16 line_height_24">{showSchedule.type}</div>
            </div>
            <div className="display_flex align_items_center margin_bottom_24 display_xs_block">
              <div className="font_size_16 width_130 margin_bottom_xs_8 font_weight_xs_600">SĐT: </div>
              <div className="text_align_left font_size_16 line_height_24">{showSchedule.phone}</div>
            </div>
            <div className="display_flex align_items_center margin_bottom_24 display_xs_block">
              <div className="font_size_16 width_130 margin_bottom_xs_8 font_weight_xs_600">Email: </div>
              <div className="text_align_left font_size_16 line_height_24">{showSchedule.email}</div>
            </div>
            <div className="display_flex align_items_center margin_bottom_24 display_xs_block">
              <div className="font_size_16 width_130 margin_bottom_xs_8 font_weight_xs_600">Thời gian: </div>
              <div className="text_align_left font_size_16 line_height_24">
                {showSchedule.time ? dayjs(showSchedule.time).format('HH:mm DD/MM/YYYY') : 'Không'}
              </div>
            </div>
            <div className="display_flex align_items_fs margin_bottom_24 display_xs_block">
              <div className="font_size_16 width_130 margin_bottom_xs_8 font_weight_xs_600">Bất động sản: </div>
              <div className="text_align_left font_size_16 line_height_24">{showSchedule.sellListing?.title}</div>
            </div>
            <div className="display_flex align_items_fs margin_bottom_24 display_xs_block">
              <div className="font_size_16 width_130 margin_bottom_xs_8 font_weight_xs_600">Tình trạng: </div>
              <div className="text_align_left font_size_16 line_height_24">{showSchedule.status}</div>
            </div>
            {showSchedule.content ? (
              <div className="display_flex align_items_fs margin_bottom_24 display_xs_block">
                <div className="font_size_16 width_130 margin_bottom_xs_8 font_weight_xs_600">Lời nhắn: </div>
                <div className="text_align_left font_size_16 line_height_24">{showSchedule.content}</div>
              </div>
            ) : null}
          </div>
        </div>
      </Popup>
      <div
        className={classnames(
          {
            display_none: !isOpenStatus,
          },
          'position_absolute bg_white border_width_1 border_color_grey border_style_solid border_bottom_left_radius_8 border_bottom_right_radius_8  padding_y_6 z_index_1 border_top_width_0',
        )}
        style={{ top: `${chooseStatusPosition.y}px`, left: `${chooseStatusPosition.x}px` }}
        ref={$selectStatusRef}
      >
        {statusList.map((item) => {
          return (
            <div
              key={item.value}
              className={classnames(
                {
                  'bg_orange color_white': item.title === status.value,
                  'bg_whitesmoke-hover': item.title !== status.value,
                },
                'padding_y_12 cursor_pointer padding_x_6 font_size_14',
              )}
              onClick={() => {
                changeStatus(item.title, item.value);
              }}
            >
              {item.title}
            </div>
          );
        })}
      </div>
      <div className="margin_top_30 overflow_auto scroll_bar_width_5 position_relative">
        <div className="width_1168 display_xs_none">
          {/* header */}
          <div className="display_flex border_width_0 border_bottom_width_1 border_style_solid border_color_grey padding_left_24 padding_bottom_10 margin_x_2">
            <div className="text_align_left font_size_14 line_height_24 font_weight_400 width_42">#</div>
            <div className="text_align_left font_size_14 line_height_24 font_weight_400 border_width_0 width_153">
              Tên
            </div>
            <div className="text_align_left font_size_14 line_height_24 font_weight_400 border_width_0 width_132">
              Yêu cầu
            </div>
            <div className="text_align_left font_size_14 line_height_24 font_weight_400 border_width_0 width_119">
              SĐT
            </div>
            <div className="text_align_left font_size_14 line_height_24 font_weight_400 border_width_0 width_190">
              Email
            </div>
            <div className="text_align_left font_size_14 line_height_24 font_weight_400 border_width_0 width_139">
              Thời gian
            </div>
            <div className="text_align_left font_size_14 line_height_24 font_weight_400 border_width_0 width_156">
              Bất động sản
            </div>
            <div className="text_align_left font_size_14 line_height_24 font_weight_400 border_width_0 width_115">
              Tình trạng
            </div>
            <div className="text_align_left font_size_14 line_height_24 font_weight_400 border_width_0 width_116 flex_1">
              Tác vụ
            </div>
          </div>
          {/* content */}
          {scheduleList.map((schedule: Schedule, index: number) => {
            return (
              <div
                className="display_flex padding_left_26 padding_y_12 margin_y_16 border_radius_18 bg_grey_3-hover box_shadow_primary-hover align_items_center margin_x_2"
                key={schedule.id}
              >
                <div className="text_align_left font_size_14 line_height_24 font_weight_400 width_42">{index + 1}</div>
                <div
                  className="text_align_left font_size_14 line_height_24 font_weight_400 border_width_0 width_153 display_flex align_items_center cursor_pointer"
                  onClick={() => {
                    setIsShowDetail(true);
                    setShowSchedule(schedule);
                  }}
                >
                  <div className="display_flex margin_right_16">
                    <img src={avatar} alt="avatar" className="width_32 height_32 border_radius_50_percent" />
                  </div>
                  <span className="text_over_flow_1 word_break" title={schedule.name}>
                    {schedule.name}
                  </span>
                </div>
                <div
                  className="text_align_left font_size_14 line_height_24 font_weight_400 border_width_0 width_132"
                  title={schedule.type as string}
                >
                  <span className="text_over_flow_1 word_break">{schedule.type}</span>
                </div>
                <div
                  className="text_align_left font_size_14 line_height_24 font_weight_400 border_width_0 width_119"
                  title={schedule.phone}
                >
                  <span className="text_over_flow_1 word_break">{schedule.phone}</span>
                </div>
                <div
                  className="text_align_left font_size_14 line_height_24 font_weight_400 border_width_0 width_190 padding_right_10"
                  title={schedule.email}
                >
                  <span className="text_over_flow_1 word_break">{schedule.email}</span>
                </div>
                <div
                  className="text_align_left font_size_14 line_height_24 font_weight_400 border_width_0 width_139"
                  title={dayjs(schedule.time).format('HH:mm DD/MM/YYYY')}
                >
                  <span className="text_over_flow_1 word_break">
                    {schedule.time ? dayjs(schedule.time).format('HH:mm DD/MM/YYYY') : 'Không'}
                  </span>
                </div>
                <div
                  className="text_align_left font_size_14 line_height_24 font_weight_400 border_width_0 width_156 padding_right_10"
                  title={schedule.sellListing.title}
                >
                  <span className="text_over_flow_1 word_break">{schedule.sellListing.title}</span>
                </div>
                <div
                  className={classnames(
                    {
                      'padding_x_6 padding_y_6 bg_white margin_left_-6 border_width_1 border_style_solid border_color_grey border_top_left_radius_8 border_top_right_radius_8 border_bottom_width_0 margin_right_12':
                        schedule.id === status.scheduleId && isOpenStatus,
                    },
                    'text_align_left font_size_14 line_height_24 font_weight_400 border_width_0 width_109 cursor_pointer no_select margin_right_6',
                  )}
                  onClick={(e: React.MouseEvent) => {
                    onOpenChooseStatus(e, schedule.id, schedule.status);
                  }}
                >
                  {schedule.id === status.scheduleId && isOpenStatus ? (
                    <span className="text_over_flow_1 word_break font_weight_600">Trạng thái</span>
                  ) : (
                    <span
                      className={classnames(
                        {
                          color_green: schedule.status === 'Hoàn thành',
                          color_orange: schedule.status === 'Chưa bắt đầu',
                        },
                        'text_over_flow_1 word_break',
                      )}
                    >
                      {schedule.status}
                    </span>
                  )}
                </div>
                <div className="text_align_left font_size_14 line_height_24 font_weight_400 border_width_0 width_116 flex_1">
                  <button
                    className={classnames(
                      {
                        'opacity_0_5 cursor_default': schedule.isCustomer,
                      },
                      'bg_white border_radius_25 border_width_1 border_style_solid border_color_grey display_flex padding_x_18 padding_y_6 font_size_14 line_height_24',
                    )}
                    onClick={() => {
                      createCustomer(schedule.id, schedule.isCustomer);
                    }}
                  >
                    Tạo KH
                  </button>
                </div>
              </div>
            );
          })}
        </div>
        {/* mobile */}
        <div className="display_xs_block display_none">
          {scheduleList.map((schedule: Schedule, index: number) => {
            return (
              <div
                className="display_flex padding_y_12 margin_y_16 border_radius_18 align_items_center margin_x_2"
                key={schedule.id}
              >
                <div
                  className="text_align_left font_size_14 line_height_24 font_weight_400 border_width_0 width_153 display_flex align_items_center cursor_pointer"
                  onClick={() => {
                    setIsShowDetail(true);
                    setShowSchedule(schedule);
                  }}
                >
                  <div className="display_flex margin_right_16">
                    <img src={avatar} alt="avatar" className="width_32 height_32 border_radius_50_percent" />
                  </div>
                  <span className="text_over_flow_1 word_break" title={schedule.name}>
                    {schedule.name}
                  </span>
                </div>
                <div
                  className="text_align_left font_size_14 line_height_24 font_weight_400 border_width_0 width_139"
                  title={dayjs(schedule.time).format('HH:mm DD/MM/YYYY')}
                >
                  <span className="text_over_flow_1 word_break">
                    {schedule.time ? dayjs(schedule.time).format('HH:mm DD/MM/YYYY') : 'Không'}
                  </span>
                </div>
                <div className="display_flex line_height_24 font_weight_400 border_width_0 flex_1 justify_content_fe">
                  <svg
                    width={20}
                    height={20}
                    viewBox="0 0 20 20"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                    onClick={() => {
                      createCustomer(schedule.id, schedule.isCustomer);
                    }}
                    className={classnames(
                      {
                        opacity_0_5: schedule.isCustomer,
                      },
                      'display_flex',
                    )}
                  >
                    <path
                      d="M7.5 13.0152C11.5668 13.0152 15 13.6844 15 16.2687C15 18.8531 11.5448 19.5 7.5 19.5C3.4332 19.5 0 18.8298 0 16.2464C0 13.6621 3.45422 13.0152 7.5 13.0152ZM16.999 5.5C17.4952 5.5 17.8979 5.90945 17.8979 6.41162V7.58786H19.101C19.5962 7.58786 20 7.99731 20 8.49948C20 9.00165 19.5962 9.4111 19.101 9.4111H17.8979V10.5884C17.8979 11.0906 17.4952 11.5 16.999 11.5C16.5038 11.5 16.1 11.0906 16.1 10.5884V9.4111H14.899C14.4027 9.4111 14 9.00165 14 8.49948C14 7.99731 14.4027 7.58786 14.899 7.58786H16.1V6.41162C16.1 5.90945 16.5038 5.5 16.999 5.5ZM7.5 0.5C10.2546 0.5 12.4626 2.73663 12.4626 5.52684C12.4626 8.31705 10.2546 10.5537 7.5 10.5537C4.74543 10.5537 2.53737 8.31705 2.53737 5.52684C2.53737 2.73663 4.74543 0.5 7.5 0.5Z"
                      fill="#8C8C8C"
                    />
                  </svg>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
};

export default ProfileSchedules;
