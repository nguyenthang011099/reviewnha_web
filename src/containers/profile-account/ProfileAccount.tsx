import React, { forwardRef, useEffect, useState } from 'react';
import InputText from '../../components/base/input-text/InputText';
import banner from '../../images/banner_profile.png';
import avatarDefault from '../../images/user_avatar.png';
import DatePicker, { registerLocale } from 'react-datepicker';
import vi from 'date-fns/locale/vi';
import 'react-datepicker/dist/react-datepicker.css';
registerLocale('vi', vi);
import './ProfileAccount.scss';
import TextArea from '../../components/base/textarea/TextArea';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../reducers';
import { updateUserInfo } from '../../reducers/auth/actions';
import { UserInfo } from '../../types/type.auth';
import Link from 'next/link';
import dayjs from 'dayjs';

const CustomInputDatePicker: React.FC<any> = forwardRef(({ value, onClick }, ref: any) => {
  return (
    <div onClick={onClick} ref={ref} className="height_100_percent">
      <input
        type="text"
        name="birth"
        placeholder="Ngày sinh của bạn"
        className="border_none font_size_16 font_weight_500 line_height_28 width_100_percent height_100_percent padding_right_30 height_100_percent cursor_pointer"
        value={value}
        disabled={true}
      />
    </div>
  );
});

const ProfileAccount: React.FC = () => {
  const [phone, setPhone] = useState<string>('');
  const [name, setName] = useState<string>('');
  const [email, setEmail] = useState<string>('');
  const [address, setAddress] = useState<string>('');
  const [warningType, setWarningType] = useState<string>('');
  const [warningMessage, setWarningMessage] = useState<string>('');
  const [gender, setGender] = useState<string>('');
  const [dateOfBirth, setDateOfBirth] = useState<Date>();
  const [company, setCompany] = useState<string>('');
  const [avatar, setAvatar] = useState<any>(null);
  const [thumbnail, setThumbnail] = useState<any>(null);
  const [avatarUrl, setAvatarUrl] = useState<string>('');
  const [thumbnailUrl, setThumbnailUrl] = useState<string>('');
  // const [position, setPosition] = useState<string>('');
  const [desc, setDesc] = useState<string>('');
  const user: UserInfo = useSelector((state: RootState) => state.auth.user);
  const isUpdating = useSelector((state: RootState) => state.auth.isUpdating);
  const dispatch = useDispatch();

  useEffect(() => {
    if (user) {
      setName(user.name);
      setPhone(user.phone);
      setEmail(user.email);
      setGender(user.sex);
      setAddress(user.address);
      setCompany(user.company);
      // setPosition(user.position);
      setDesc(user.description);
      user.dateOfBirth && setDateOfBirth(new Date(user.dateOfBirth));
      setAvatarUrl(user.avatar as string);
      setThumbnailUrl(user.background);
    }
  }, [user]);

  /**
   * changePhone change và validate phone
   * @param value value change
   */
  const changePhone = (value: string) => {
    const validatedValue = value.replace(/[^0-9+()]/g, '');
    setPhone(validatedValue);
  };

  /**
   * checkGenderIcon render icon
   * @param sex male or female
   */
  const checkGenderIcon = (sex: 'male' | 'female') => {
    return gender === sex ? (
      <svg width={20} height={20} viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M10 0C15.514 0 20 4.486 20 10C20 15.514 15.514 20 10 20C4.486 20 0 15.514 0 10C0 4.486 4.486 0 10 0ZM10 1.5C5.313 1.5 1.5 5.313 1.5 10C1.5 14.687 5.313 18.5 10 18.5C14.687 18.5 18.5 14.687 18.5 10C18.5 5.313 14.687 1.5 10 1.5Z"
          fill="#3B4144"
        />
        <path
          d="M16.5 10C16.5 13.5899 13.5899 16.5 10 16.5C6.41015 16.5 3.5 13.5899 3.5 10C3.5 6.41015 6.41015 3.5 10 3.5C13.5899 3.5 16.5 6.41015 16.5 10Z"
          fill="#3B4144"
        />
      </svg>
    ) : (
      <svg width={20} height={20} viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M10 0C15.514 0 20 4.486 20 10C20 15.514 15.514 20 10 20C4.486 20 0 15.514 0 10C0 4.486 4.486 0 10 0ZM10 1.5C5.313 1.5 1.5 5.313 1.5 10C1.5 14.687 5.313 18.5 10 18.5C14.687 18.5 18.5 14.687 18.5 10C18.5 5.313 14.687 1.5 10 1.5Z"
          fill="#3B4144"
        />
      </svg>
    );
  };

  /**
   * setIconBirth: icon birth
   */
  const setIconBirth = () => {
    return (
      <svg width={20} height={20} viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M14.334 0C17.723 0 20 2.378 20 5.916V14.084C20 17.622 17.723 20 14.333 20H5.665C2.276 20 0 17.622 0 14.084V5.916C0 2.378 2.276 0 5.665 0H14.334ZM14.334 1.5H5.665C3.135 1.5 1.5 3.233 1.5 5.916V14.084C1.5 16.767 3.135 18.5 5.665 18.5H14.333C16.864 18.5 18.5 16.767 18.5 14.084V5.916C18.5 3.233 16.865 1.5 14.334 1.5ZM9.9991 4.8836C10.4131 4.8836 10.7491 5.2196 10.7491 5.6336V9.5696L13.7751 11.3726C14.1301 11.5856 14.2471 12.0456 14.0351 12.4016C13.8941 12.6366 13.6451 12.7676 13.3901 12.7676C13.2591 12.7676 13.1271 12.7336 13.0061 12.6626L9.6151 10.6396C9.3891 10.5036 9.2491 10.2586 9.2491 9.9956V5.6336C9.2491 5.2196 9.5851 4.8836 9.9991 4.8836Z"
          fill="black"
        />
      </svg>
    );
  };

  const onUploadImage = (e: React.ChangeEvent<HTMLInputElement>, type: 'avatar' | 'thumbnail') => {
    if (e.target.files?.length) {
      const file = e.target.files[0];
      const reader = new FileReader();
      reader.onload = function (e1) {
        if (type === 'avatar') {
          setAvatarUrl(e1.target.result as string);
        } else {
          console.log('set thumbnail');
          setThumbnailUrl(e1.target.result as string);
        }
      };
      reader.readAsDataURL(file);
      if (type === 'avatar') {
        setAvatar(file);
        console.log(file);
      } else {
        setThumbnail(file);
      }
    }
  };

  const onUpdateUser = () => {
    const data: UserInfo = {
      address: address,
      company: company,
      dateOfBirth: dateOfBirth ? dayjs(dateOfBirth).format('YYYY-MM-DD') : undefined,
      description: desc,
      // position: position,
      sex: gender ? gender : undefined,
    };
    if (email !== user.email) {
      data.email = email;
    }
    if (phone !== user.phone) {
      data.phone = phone;
    }
    if (name !== user.name) {
      data.name = name;
    }
    if (avatar) {
      data.avatar = avatar;
    }
    if (thumbnail) {
      data.background = thumbnail;
    }
    dispatch(updateUserInfo(data));
  };

  return (
    <div className="account">
      <div className="banner display_flex position_relative">
        <img
          src={thumbnailUrl ? thumbnailUrl : banner}
          alt="banner"
          className="height_270 object_fit_cover width_100_percent height_xs_200"
        />
        <div className="position_absolute top_0 left_0 height_100_percent width_100_percent">
          <div className="container_account height_100_percent display_flex justify_content_fe align_items_fe">
            <label
              className="display_flex width_52 height_52 border_radius_50_percent cursor_pointer no_select align_items_center justify_content_center margin_bottom_16"
              style={{
                background: 'linear-gradient(93.91deg, rgba(255, 255, 255, 0.75) 0%, rgba(255, 255, 255, 0.375) 100%)',
                backdropFilter: 'blur(60px)',
              }}
            >
              <svg width={20} height={20} viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path
                  d="M14.3338 0C17.7231 0 20 2.37811 20 5.91672V14.0833C20 17.6219 17.7231 20 14.3328 20H5.66618C2.2769 20 0 17.6219 0 14.0833V5.91672C0 2.37811 2.2769 0 5.66618 0H14.3338ZM15.4366 10.5501C14.3646 9.88132 13.5371 10.8204 13.3138 11.1207C13.0986 11.4107 12.9136 11.7306 12.7185 12.0506C12.2419 12.84 11.6958 13.7503 10.7506 14.2797C9.37699 15.0402 8.3342 14.3395 7.58404 13.8297C7.30248 13.6398 7.02897 13.4603 6.75645 13.3406C6.08473 13.0506 5.48038 13.3808 4.5834 14.5201C4.11279 15.1156 3.6462 15.7059 3.17358 16.2941C2.89102 16.646 2.95839 17.1889 3.3395 17.4241C3.94788 17.7988 4.68999 18 5.52864 18H13.9564C14.432 18 14.9087 17.935 15.3632 17.7864C16.3869 17.452 17.1994 16.6863 17.6237 15.6749C17.9817 14.8246 18.1557 13.7926 17.8208 12.934C17.7092 12.6491 17.5423 12.3839 17.308 12.1507C16.6936 11.5408 16.1194 10.9711 15.4366 10.5501ZM6.49886 4C5.12021 4 4 5.12173 4 6.5C4 7.87827 5.12021 9 6.49886 9C7.8765 9 8.99772 7.87827 8.99772 6.5C8.99772 5.12173 7.8765 4 6.49886 4Z"
                  fill="#272727"
                />
              </svg>
              <input
                type="file"
                className="display_none"
                accept="image/*"
                onChange={(e) => {
                  onUploadImage(e, 'thumbnail');
                }}
              />
            </label>
          </div>
        </div>
      </div>
      <div className="container_account padding_bottom_100 padding_x_xs_22">
        {/* avatar */}
        <div className="display_flex justify_content_sb align_items_fs margin_bottom_64 margin_bottom_xs_24">
          <div className="margin_top_-108 width_216 height_216 position_relative width_xs_108 height_xs_108 margin_top_xs_-54">
            <label
              className="position_absolute display_flex right_0 bottom_0 width_52 height_52 bg_white border_radius_50_percent align_items_center justify_content_center
              cursor_pointer no_select width_xs_32 height_xs_32"
            >
              <svg
                width={20}
                height={20}
                viewBox="0 0 20 20"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
                className="width_xs_15 height_xs_15"
              >
                <path
                  d="M14.3338 0C17.7231 0 20 2.37811 20 5.91672V14.0833C20 17.6219 17.7231 20 14.3328 20H5.66618C2.2769 20 0 17.6219 0 14.0833V5.91672C0 2.37811 2.2769 0 5.66618 0H14.3338ZM15.4366 10.5501C14.3646 9.88132 13.5371 10.8204 13.3138 11.1207C13.0986 11.4107 12.9136 11.7306 12.7185 12.0506C12.2419 12.84 11.6958 13.7503 10.7506 14.2797C9.37699 15.0402 8.3342 14.3395 7.58404 13.8297C7.30248 13.6398 7.02897 13.4603 6.75645 13.3406C6.08473 13.0506 5.48038 13.3808 4.5834 14.5201C4.11279 15.1156 3.6462 15.7059 3.17358 16.2941C2.89102 16.646 2.95839 17.1889 3.3395 17.4241C3.94788 17.7988 4.68999 18 5.52864 18H13.9564C14.432 18 14.9087 17.935 15.3632 17.7864C16.3869 17.452 17.1994 16.6863 17.6237 15.6749C17.9817 14.8246 18.1557 13.7926 17.8208 12.934C17.7092 12.6491 17.5423 12.3839 17.308 12.1507C16.6936 11.5408 16.1194 10.9711 15.4366 10.5501ZM6.49886 4C5.12021 4 4 5.12173 4 6.5C4 7.87827 5.12021 9 6.49886 9C7.8765 9 8.99772 7.87827 8.99772 6.5C8.99772 5.12173 7.8765 4 6.49886 4Z"
                  fill="#272727"
                />
              </svg>
              <input
                type="file"
                className="display_none"
                accept="image/*"
                onChange={(e) => {
                  onUploadImage(e, 'avatar');
                }}
              />
            </label>
            <img
              src={avatarUrl ? avatarUrl : avatarDefault}
              alt=""
              className="border_radius_50_percent object_fit_cover width_100_percent height_100_percent"
            />
          </div>
          <div className="display_flex margin_top_24 display_xs_none">
            <Link href={{ pathname: '/home-page' }} as="/">
              <button
                className="bg_white border_radius_25 border_width_1 border_style_solid border_color_grey display_flex padding_x_44 padding_y_6 
              font_size_14 line_height_20 margin_right_20"
              >
                Hủy
              </button>
            </Link>
            <button
              className="bg_black color_white border_radius_25 border_width_1 border_style_solid border_color_black display_flex padding_x_44 padding_y_6 
              font_size_14 line_height_20"
              onClick={() => {
                onUpdateUser();
              }}
            >
              {isUpdating ? 'Đang lưu...' : 'Lưu'}
            </button>
          </div>
        </div>
        {/* content */}
        <div>
          <div className="display_flex align_items_center margin_bottom_30">
            <span className="margin_right_40 font_weight_600">Loại tài khoản:</span>
            <div className="width_360 display_flex align_items_center justify_content_center border_radius_25 bg_orange height_32 width_xs_auto flex_xs_1">
              <span className="font_size_14 line_height_20 color_white font_weight_600">
                {user.type === 'seller' ? 'Chuyên gia tư vấn' : 'Người dùng'}
              </span>
            </div>
          </div>
          <div className="display_flex margin_x_-10 flex_wrap">
            {/* infomation */}
            <div className="col_6 padding_x_10 col_xs_12">
              <h3 className="margin_0 margin_bottom_24 font_size_16 font_weight_600 line_height_21">
                Thông tin cá nhân
              </h3>
              <div className="margin_bottom_16">
                <InputText
                  name="name"
                  placeholder="Họ và tên của bạn"
                  required={true}
                  type="text"
                  label="Họ tên:"
                  value={name}
                  setValue={setName}
                  isWarning={warningType === 'name'}
                  setIsWarning={setWarningType}
                />
              </div>
              <div className="margin_bottom_16">
                <div className="height_48 display_flex align_items_center padding_x_24">
                  <label className="display_flex font_size_14 line_height_24 width_95 flex_none opacity_0_5">
                    Giới tính
                  </label>
                  <div
                    className="display_flex align_items_center margin_right_66 cursor_pointer no_select"
                    onClick={() => {
                      setGender('male');
                    }}
                  >
                    <div className="display_flex margin_right_10 cursor_pointer">{checkGenderIcon('male')}</div>
                    <span>Nam</span>
                  </div>
                  <div
                    className="display_flex align_items_center cursor_pointer no_select"
                    onClick={() => {
                      setGender('female');
                    }}
                  >
                    <div className="display_flex margin_right_10">{checkGenderIcon('female')}</div>
                    <span>Nữ</span>
                  </div>
                </div>
              </div>

              <div className="margin_bottom_16">
                <div
                  className="input_text_base margin_bottom_16 border_width_1 border_color_black border_style_solid border_radius_8 padding_x_24 display_flex 
                  height_48 align_items_center position_relative"
                >
                  <div className="display_flex align_items_center width_100_percent height_100_percent">
                    <label className="display_flex align_items_center font_size_14 line_height_24 opacity_0_5 color_black width_95 flex_none height_100_percent">
                      Ngày sinh:
                    </label>
                    <DatePicker
                      className="date_picker"
                      selected={dateOfBirth}
                      onChange={(date: any) => setDateOfBirth(date)}
                      peekNextMonth
                      showMonthDropdown
                      showYearDropdown
                      dropdownMode="select"
                      dateFormat="P"
                      locale="vi"
                      customInput={<CustomInputDatePicker></CustomInputDatePicker>}
                    />
                  </div>
                  <div className={'display_flex cursor_pointer'}>{setIconBirth()}</div>
                </div>
              </div>
              <div className="margin_bottom_16">
                <InputText
                  label="SĐT:"
                  type="tel"
                  name="phone"
                  placeholder="Số điện thoại"
                  value={phone}
                  setValue={changePhone}
                  required={true}
                  isWarning={warningType === 'phone'}
                  setIsWarning={setWarningType}
                  disabled={true}
                />
              </div>
              <div className="margin_bottom_16">
                <InputText
                  label="Email:"
                  type="text"
                  name="email"
                  placeholder="Email"
                  required={true}
                  value={email}
                  setValue={setEmail}
                  isWarning={warningType === 'email'}
                  setIsWarning={setWarningType}
                  disabled={true}
                />
              </div>
              <div className="margin_bottom_16">
                <InputText
                  label="Địa chỉ:"
                  name="address"
                  placeholder="Địa chỉ của bạn"
                  type="address"
                  value={address}
                  setValue={setAddress}
                  isWarning={warningType === 'address'}
                  setIsWarning={setWarningType}
                />
              </div>
            </div>
            {/* company */}
            <div className="col_6 padding_x_10 col_xs_12">
              <h3 className="margin_0 margin_bottom_24 font_size_16 font_weight_600 line_height_21">Công tác</h3>
              <div className="margin_bottom_16">
                <InputText
                  label="Công ty:"
                  name=""
                  placeholder="Công ty của bạn"
                  type="text"
                  value={company}
                  setValue={setCompany}
                />
              </div>
              <h3 className="margin_0 margin_bottom_24 font_size_16 font_weight_600 line_height_21">Mô tả thêm</h3>
              <div>
                <TextArea
                  value={desc}
                  setValue={setDesc}
                  label="Giới thiệu"
                  placeholder="Giới thiệu bản thân"
                  className="height_180"
                ></TextArea>
              </div>
            </div>
          </div>
          {warningType ? (
            <div className="margin_bottom_16 margin_top_-8 padding_left_24 text_align_center">
              <span className="font_size_12 line_height_18 color_red">{warningMessage}</span>
            </div>
          ) : null}
          <div className="margin_top_24 display_none display_xs_flex">
            <Link href={{ pathname: '/home-page' }} as="/">
              <button
                className="bg_white border_radius_25 border_width_1 border_style_solid border_color_grey display_flex padding_y_10 justify_content_center
              font_size_14 line_height_20 margin_right_20 width_xs_50_percent"
              >
                Hủy
              </button>
            </Link>
            <button
              className="bg_black color_white border_radius_25 border_width_1 border_style_solid border_color_black display_flex padding_y_10 justify_content_center
              font_size_14 line_height_20 width_xs_50_percent"
              onClick={() => {
                onUpdateUser();
              }}
            >
              {isUpdating ? 'Đang lưu...' : 'Lưu'}
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ProfileAccount;
