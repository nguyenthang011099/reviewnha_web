import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import customerApi from '../../api/customer';
import Confirm from '../../components/confirm/Confirm';
import avatar from '../../images/user_avatar.png';
import { openNotification } from '../../reducers/notification/actions';
import { Customer } from '../../types/type.customer';

interface Props {
  list: Customer[];
}

const ProfileCustomers: React.FC<Props> = (props) => {
  const [customerList, setCustomerList] = useState<Customer[]>([]);
  const [isOpenConfirm, setIsOpenConfirm] = useState<boolean>(false);
  const [deleteId, setDeleteId] = useState<number>(0);
  const dispatch = useDispatch();

  useEffect(() => {
    setCustomerList(props.list);
  }, [props.list]);

  const onDeleteCustomer = () => {
    if (deleteId) {
      customerApi.delete(deleteId).then((res) => {
        if (res.success) {
          const newCustomerList = customerList.filter((item: Customer) => {
            return item.id !== deleteId;
          });
          setCustomerList(newCustomerList);
          setDeleteId(0);
          setIsOpenConfirm(false);
          dispatch(
            openNotification({
              type: 'success',
              title: 'Xóa khách hàng thành công!',
              isOpen: true,
            }),
          );
        }
      });
    }
  };

  return (
    <div className="margin_top_30 overflow_auto scroll_bar_width_5">
      <Confirm
        isOpen={isOpenConfirm}
        setIsOpen={setIsOpenConfirm}
        onAccept={onDeleteCustomer}
        onReject={() => {
          setIsOpenConfirm(false);
        }}
        title="Bạn có chắc chắn muốn xóa khách hàng này không?"
      ></Confirm>
      <div className="width_1168 display_xs_none">
        {/* header */}
        <div className="display_flex border_width_0 border_bottom_width_1 border_style_solid border_color_grey padding_left_24 padding_bottom_10 margin_x_2">
          <div className="text_align_left font_size_14 line_height_24 font_weight_400 width_42">#</div>
          <div className="text_align_left font_size_14 line_height_24 font_weight_400 border_width_0 width_487">
            Tên
          </div>
          <div className="text_align_left font_size_14 line_height_24 font_weight_400 border_width_0 width_219">
            SĐT
          </div>
          <div className="text_align_left font_size_14 line_height_24 font_weight_400 border_width_0 width_316">
            Email
          </div>
          <div className="text_align_left font_size_14 line_height_24 font_weight_400 border_width_0 width_116 flex_1">
            Tác vụ
          </div>
        </div>
        {/* content */}
        {customerList.map((customer: Customer, index: number) => {
          return (
            <div
              className="display_flex padding_left_26 padding_y_12 margin_y_16 border_radius_18 bg_grey_3-hover box_shadow_primary-hover align_items_center margin_x_2"
              key={customer.id}
            >
              <div className="text_align_left font_size_14 line_height_24 font_weight_400 width_42">{index + 1}</div>
              <div className="text_align_left font_size_14 line_height_24 font_weight_400 border_width_0 width_487 display_flex align_items_center">
                <div className="display_flex margin_right_16">
                  <img src={avatar} alt="avatar" className="width_32 height_32 border_radius_50_percent" />
                </div>
                <span className="text_over_flow_1">{customer.name}</span>
              </div>
              <div className="text_align_left font_size_14 line_height_24 font_weight_400 border_width_0 width_219 text_over_flow_1">
                {customer.phone}
              </div>
              <div className="text_align_left font_size_14 line_height_24 font_weight_400 border_width_0 width_316 text_over_flow_1">
                {customer.email}
              </div>
              <div className="text_align_left font_size_14 line_height_24 font_weight_400 border_width_0 width_116 flex_1">
                <div
                  className="padding_y_8 cursor_pointer no_select display_flex"
                  onClick={() => {
                    setDeleteId(customer.id);
                    setIsOpenConfirm(true);
                  }}
                >
                  <svg width={15} height={16} viewBox="0 0 15 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path
                      fillRule="evenodd"
                      clipRule="evenodd"
                      d="M12.5385 5.53978C12.8482 5.56528 13.0792 5.83603 13.0544 6.14578C13.0499 6.19678 12.6435 11.23 12.4095 13.3413C12.264 14.6515 11.3984 15.4488 10.0919 15.4728C9.0922 15.49 8.1277 15.4998 7.18495 15.4998C6.1687 15.4998 5.17795 15.4885 4.1977 15.4683C2.9437 15.4435 2.07595 14.6305 1.9342 13.3465C1.69795 11.2165 1.2937 6.19603 1.28995 6.14578C1.26445 5.83603 1.49545 5.56453 1.8052 5.53978C2.11045 5.53153 2.38645 5.74603 2.4112 6.05503C2.41359 6.08758 2.57886 8.1378 2.75895 10.1663L2.79512 10.5711C2.88582 11.5794 2.97777 12.5483 3.05245 13.2228C3.1327 13.9525 3.52645 14.329 4.22095 14.3433C6.09595 14.383 8.0092 14.3853 10.0717 14.3478C10.8097 14.3335 11.2087 13.9645 11.2912 13.2175C11.5237 11.122 11.9287 6.10603 11.9332 6.05503C11.9579 5.74603 12.2317 5.53003 12.5385 5.53978ZM8.75905 0.5C9.44755 0.5 10.0528 0.96425 10.2305 1.6295L10.4211 2.57525C10.4826 2.88528 10.7547 3.11169 11.0697 3.11667L13.531 3.11675C13.8415 3.11675 14.0935 3.36875 14.0935 3.67925C14.0935 3.98975 13.8415 4.24175 13.531 4.24175L11.0917 4.24164C11.0879 4.24171 11.0841 4.24175 11.0803 4.24175L11.062 4.241L3.28121 4.24166C3.27517 4.24172 3.26911 4.24175 3.26305 4.24175L3.2515 4.241L0.8125 4.24175C0.502 4.24175 0.25 3.98975 0.25 3.67925C0.25 3.36875 0.502 3.11675 0.8125 3.11675L3.27325 3.116L3.34902 3.1112C3.63123 3.07459 3.86578 2.86025 3.92305 2.57525L4.1053 1.66325C4.29055 0.96425 4.8958 0.5 5.5843 0.5H8.75905ZM8.75905 1.625H5.5843C5.4043 1.625 5.24605 1.74575 5.2003 1.919L5.02555 2.7965C5.00334 2.90765 4.971 3.01476 4.92961 3.11695H9.41396C9.37252 3.01476 9.34011 2.90765 9.3178 2.7965L9.13555 1.8845C9.0973 1.74575 8.93905 1.625 8.75905 1.625Z"
                      fill="black"
                    />
                  </svg>
                </div>
              </div>
            </div>
          );
        })}
      </div>
      {/* mobile  */}
      <div className="display_none display_xs_block">
        {customerList.map((customer: Customer, index: number) => {
          return (
            <div
              className="display_flex padding_y_12 margin_y_16 border_radius_18 align_items_center margin_x_2"
              key={customer.id}
            >
              <div className="text_align_left font_size_14 line_height_24 font_weight_400 border_width_0 width_153 display_flex align_items_center">
                <div className="display_flex margin_right_16">
                  <img src={avatar} alt="avatar" className="width_32 height_32 border_radius_50_percent" />
                </div>
                <span className="text_over_flow_1">{customer.name}</span>
              </div>
              <div className="text_align_left font_size_14 line_height_24 font_weight_400 border_width_0 width_139 text_over_flow_1">
                {customer.phone}
              </div>
              <div className="font_size_14 line_height_24 font_weight_400 border_width_0 width_116 flex_1">
                <div
                  className="padding_y_8 cursor_pointer no_select display_flex justify_content_fe"
                  onClick={() => {
                    setDeleteId(customer.id);
                    setIsOpenConfirm(true);
                  }}
                >
                  <svg width={15} height={16} viewBox="0 0 15 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path
                      fillRule="evenodd"
                      clipRule="evenodd"
                      d="M12.5385 5.53978C12.8482 5.56528 13.0792 5.83603 13.0544 6.14578C13.0499 6.19678 12.6435 11.23 12.4095 13.3413C12.264 14.6515 11.3984 15.4488 10.0919 15.4728C9.0922 15.49 8.1277 15.4998 7.18495 15.4998C6.1687 15.4998 5.17795 15.4885 4.1977 15.4683C2.9437 15.4435 2.07595 14.6305 1.9342 13.3465C1.69795 11.2165 1.2937 6.19603 1.28995 6.14578C1.26445 5.83603 1.49545 5.56453 1.8052 5.53978C2.11045 5.53153 2.38645 5.74603 2.4112 6.05503C2.41359 6.08758 2.57886 8.1378 2.75895 10.1663L2.79512 10.5711C2.88582 11.5794 2.97777 12.5483 3.05245 13.2228C3.1327 13.9525 3.52645 14.329 4.22095 14.3433C6.09595 14.383 8.0092 14.3853 10.0717 14.3478C10.8097 14.3335 11.2087 13.9645 11.2912 13.2175C11.5237 11.122 11.9287 6.10603 11.9332 6.05503C11.9579 5.74603 12.2317 5.53003 12.5385 5.53978ZM8.75905 0.5C9.44755 0.5 10.0528 0.96425 10.2305 1.6295L10.4211 2.57525C10.4826 2.88528 10.7547 3.11169 11.0697 3.11667L13.531 3.11675C13.8415 3.11675 14.0935 3.36875 14.0935 3.67925C14.0935 3.98975 13.8415 4.24175 13.531 4.24175L11.0917 4.24164C11.0879 4.24171 11.0841 4.24175 11.0803 4.24175L11.062 4.241L3.28121 4.24166C3.27517 4.24172 3.26911 4.24175 3.26305 4.24175L3.2515 4.241L0.8125 4.24175C0.502 4.24175 0.25 3.98975 0.25 3.67925C0.25 3.36875 0.502 3.11675 0.8125 3.11675L3.27325 3.116L3.34902 3.1112C3.63123 3.07459 3.86578 2.86025 3.92305 2.57525L4.1053 1.66325C4.29055 0.96425 4.8958 0.5 5.5843 0.5H8.75905ZM8.75905 1.625H5.5843C5.4043 1.625 5.24605 1.74575 5.2003 1.919L5.02555 2.7965C5.00334 2.90765 4.971 3.01476 4.92961 3.11695H9.41396C9.37252 3.01476 9.34011 2.90765 9.3178 2.7965L9.13555 1.8845C9.0973 1.74575 8.93905 1.625 8.75905 1.625Z"
                      fill="black"
                    />
                  </svg>
                </div>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default ProfileCustomers;
