import React, { useEffect, useState } from 'react';
import logo from '../../images/logo.png';
import Link from 'next/link';
import { useDispatch, useSelector } from 'react-redux';
import { login, resetStatusAuth } from '../../reducers/auth/actions';
import { Status } from '../../types/type.status';
import { RootState } from '../../reducers';
import { useRouter } from 'next/router';
import InputText from '../../components/base/input-text/InputText';
import { CallbackURLType } from '../../types/type.auth';

const Login: React.FC = () => {
  const [username, setUsername] = useState<string>('');
  const [password, setPassword] = useState<string>('');
  const dispatch = useDispatch();
  const loginStatus: Status = useSelector((state: RootState) => state.auth.loginStatus as Status);
  const loginMessageError: string = useSelector((state: RootState) => state.auth.loginMessageError);
  const router = useRouter();
  const googleLink: string = useSelector((state: RootState) => state.auth.googleLink);
  const facebookLink: string = useSelector((state: RootState) => state.auth.facebookLink);
  const callbackUrl: CallbackURLType = useSelector((state: RootState) => state.auth.callbackUrl);
  const authenticated: boolean = useSelector((state: RootState) => state.auth.authenticated);

  /**
   * lắng nghe khi đăng nhập
   */
  useEffect(() => {
    if (loginStatus === 'success') {
      dispatch(resetStatusAuth('login'));
      router.push(
        { pathname: callbackUrl.pathname || '/home-page', query: callbackUrl.query },
        callbackUrl.asPath || '/',
      );
    }
  }, [loginStatus]);

  useEffect(() => {
    if (authenticated) {
      router.push({ pathname: '/home-page' }, '/');
    }
  }, []);

  /**
   * changeName
   * @param value value cua input
   */
  const changeUsername = (value: string) => {
    setUsername(value);
  };

  /**
   * changePassword
   * @param value value cua input
   */
  const changePassword = (value: string) => {
    setPassword(value);
  };

  /**
   * onLogin login
   */
  const onLogin = (e: React.FormEvent<HTMLButtonElement>) => {
    if (username && password) {
      e.preventDefault();
      const data = {
        username,
        password,
      };
      dispatch(login(data));
    }
  };

  if (authenticated) {
    return null;
  }

  return (
    <div className="login_page display_flex height_100_percent flex_wrap_xs align_items_xs_fs flex_direction_xs_col">
      <div className="logo col_6 display_flex align_items_center justify_content_fe col_xs_12 display_xs_block padding_y_xs_24">
        <Link href={{ pathname: '/home-page' }} as="/">
          <a>
            <div className="logo_image_auth padding_right_150 padding_x_xs_22">
              <div className="margin_bottom_15 margin_bottom_xs_4">
                <img src={logo} alt="review nha" className="height_xs_30" />
              </div>
              <div>
                <span className="font_size_36 line_height_57 font_logo font_size_xs_16 line_height_xs_16">
                  Finding your best home
                </span>
              </div>
            </div>
          </a>
        </Link>
      </div>
      <div className="form_login col_6 display_flex align_items_center margin_left_12 col_xs_12 margin_left_xs_0 padding_x_xs_22">
        <div className="width_xs_100_percent">
          <form className="width_575 width_xs_100_percent">
            <h2 className="margin_y_0 margin_bottom_36 font_size_32 line_height_39 font_weight_600">Đăng nhập</h2>
            <div className="padding_bottom_48 border_width_0 border_bottom_width_1 border_color_black border_style_solid padding_bottom_xs_24">
              <InputText
                label="SĐT/ Mail:"
                type="text"
                value={username}
                setValue={changeUsername}
                name="email"
                placeholder="Số điện thoại/ Email"
                required={true}
              />
              <InputText
                label="Mật khẩu:"
                name="password"
                placeholder="Mật khẩu của bạn"
                type="password"
                required={true}
                value={password}
                setValue={changePassword}
              />
              <div className="margin_top_46 display_flex justify_content_sb align_items_center display_xs_block margin_top_xs_12">
                <span className="font_size_15 line_height_22">Quên mật khẩu</span>
                <div className="margin_top_xs_12">
                  <button
                    className="font_size_14 line_height_17 font_weight_600 height_32 width_215 bg_red border_none color_white
                  border_radius_25 width_xs_100_percent"
                    type="submit"
                    onClick={onLogin}
                  >
                    Đăng nhập
                  </button>
                </div>
              </div>
            </div>
          </form>
          <div className="margin_top_26 display_flex align_items_center width_xs_100_percent display_xs_block text_align_xs_center margin_top_xs_18">
            <span className="font_size_15 line_height_22 margin_right_18 display_xs_flex justify_content_xs_center margin_bottom_xs_8">
              Đăng nhập với
            </span>
            {facebookLink ? (
              <a href={facebookLink}>
                <button
                  className="display_flex align_items_center color_white font_size_14 font_weight_600 line_height_17 width_215 height_32 border_radius_16
              border_none justify_content_center margin_right_16 width_xs_100_percent margin_bottom_xs_8"
                  style={{ background: '#395185' }}
                >
                  <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path
                      fillRule="evenodd"
                      clipRule="evenodd"
                      d="M0 10.0558C0 15.0275 3.61083 19.1617 8.33333 20V12.7775H5.83333V10H8.33333V7.7775C8.33333 5.2775 9.94417 3.88917 12.2225 3.88917C12.9442 3.88917 13.7225 4 14.4442 4.11083V6.66667H13.1667C11.9442 6.66667 11.6667 7.2775 11.6667 8.05583V10H14.3333L13.8892 12.7775H11.6667V20C16.3892 19.1617 20 15.0283 20 10.0558C20 4.525 15.5 0 10 0C4.5 0 0 4.525 0 10.0558Z"
                      fill="white"
                    />
                  </svg>
                  <span className="margin_left_8">Facebook</span>
                </button>
              </a>
            ) : (
              <button
                className="display_flex align_items_center color_white font_size_14 font_weight_600 line_height_17 width_215 height_32 border_radius_16
              border_none justify_content_center margin_right_16 width_xs_100_percent margin_bottom_xs_8"
                style={{ background: '#395185' }}
              >
                <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path
                    fillRule="evenodd"
                    clipRule="evenodd"
                    d="M0 10.0558C0 15.0275 3.61083 19.1617 8.33333 20V12.7775H5.83333V10H8.33333V7.7775C8.33333 5.2775 9.94417 3.88917 12.2225 3.88917C12.9442 3.88917 13.7225 4 14.4442 4.11083V6.66667H13.1667C11.9442 6.66667 11.6667 7.2775 11.6667 8.05583V10H14.3333L13.8892 12.7775H11.6667V20C16.3892 19.1617 20 15.0283 20 10.0558C20 4.525 15.5 0 10 0C4.5 0 0 4.525 0 10.0558Z"
                    fill="white"
                  />
                </svg>
                <span className="margin_left_8">Facebook</span>
              </button>
            )}
            {googleLink ? (
              <a href={googleLink}>
                <button
                  className="display_flex align_items_center font_size_14 font_weight_600 line_height_17 width_215 height_32 border_radius_16
                  border_width_1 border_color_black border_style_solid bg_white justify_content_center margin_right_16 width_xs_100_percent margin_bottom_xs_8"
                >
                  <svg width="20" height="16" viewBox="0 0 20 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <g clipPath="url(#clip0)">
                      <path
                        d="M4.54547 15.0039V7.27657L2.14898 5.08415L0 3.86751V13.6402C0 14.3948 0.611328 15.0039 1.36367 15.0039H4.54547Z"
                        fill="#4285F4"
                      />
                      <path
                        d="M15.4541 15.0039H18.6359C19.3905 15.0039 19.9996 14.3925 19.9996 13.6402V3.86757L17.5655 5.26109L15.4541 7.27655V15.0039Z"
                        fill="#34A853"
                      />
                      <path
                        d="M4.54582 7.27657L4.21973 4.25727L4.54582 1.36751L10.0004 5.45845L15.4549 1.36751L15.8197 4.10126L15.4549 7.27657L10.0004 11.3675L4.54582 7.27657Z"
                        fill="#EA4335"
                      />
                      <path
                        d="M15.4541 1.36751V7.27657L19.9996 3.86751V2.0493C19.9996 0.362975 18.0746 -0.598353 16.7269 0.412975L15.4541 1.36751Z"
                        fill="#FBBC04"
                      />
                      <path
                        d="M0 3.86736L2.09055 5.43533L4.54547 7.27642V1.36736L3.27266 0.412828C1.92266 -0.598578 0 0.362828 0 2.04908V3.86728V3.86736Z"
                        fill="#C5221F"
                      />
                    </g>
                    <defs>
                      <clipPath id="clip0">
                        <rect width="20" height="15.0781" fill="white" />
                      </clipPath>
                    </defs>
                  </svg>
                  <span className="margin_left_8">Gmail</span>
                </button>
              </a>
            ) : (
              <button
                className="display_flex align_items_center font_size_14 font_weight_600 line_height_17 width_215 height_32 border_radius_16
              border_width_1 border_color_black border_style_solid bg_white justify_content_center margin_right_16 width_xs_100_percent margin_bottom_xs_8"
              >
                <svg width="20" height="16" viewBox="0 0 20 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <g clipPath="url(#clip0)">
                    <path
                      d="M4.54547 15.0039V7.27657L2.14898 5.08415L0 3.86751V13.6402C0 14.3948 0.611328 15.0039 1.36367 15.0039H4.54547Z"
                      fill="#4285F4"
                    />
                    <path
                      d="M15.4541 15.0039H18.6359C19.3905 15.0039 19.9996 14.3925 19.9996 13.6402V3.86757L17.5655 5.26109L15.4541 7.27655V15.0039Z"
                      fill="#34A853"
                    />
                    <path
                      d="M4.54582 7.27657L4.21973 4.25727L4.54582 1.36751L10.0004 5.45845L15.4549 1.36751L15.8197 4.10126L15.4549 7.27657L10.0004 11.3675L4.54582 7.27657Z"
                      fill="#EA4335"
                    />
                    <path
                      d="M15.4541 1.36751V7.27657L19.9996 3.86751V2.0493C19.9996 0.362975 18.0746 -0.598353 16.7269 0.412975L15.4541 1.36751Z"
                      fill="#FBBC04"
                    />
                    <path
                      d="M0 3.86736L2.09055 5.43533L4.54547 7.27642V1.36736L3.27266 0.412828C1.92266 -0.598578 0 0.362828 0 2.04908V3.86728V3.86736Z"
                      fill="#C5221F"
                    />
                  </g>
                  <defs>
                    <clipPath id="clip0">
                      <rect width="20" height="15.0781" fill="white" />
                    </clipPath>
                  </defs>
                </svg>
                <span className="margin_left_8">Gmail</span>
              </button>
            )}
          </div>
          <div className="margin_top_78 display_flex align_items_center margin_top_xs_24">
            <span className="font_size_15 line_height_22 margin_right_16">Bạn chưa có tài khoản?</span>
            <Link href={{ pathname: '/register-page' }} as="/register">
              <a>
                <span className="font_size_14 line_height_17 font_weight_600 margin_right_16 color_red">Đăng ký</span>
              </a>
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Login;
