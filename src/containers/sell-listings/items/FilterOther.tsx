import React, { useEffect, useRef, useState } from 'react';
import classnames from 'classnames';
import { OptionInputRadio, OptionRangeDouble } from '../../../types/type.base';
import InputRangeDouble from '../../../components/base/input-range-double';
import InputSegment from '../../../components/base/input-segment';
import InputRadio from '../../../components/base/input-radio/InputRadio';
import useOutsideClick from '../../../utils/customHooks/useOutsideClick';

interface Props {
  area: OptionRangeDouble;
  setArea: (_value: OptionRangeDouble) => void;
  bedroom: OptionInputRadio;
  setBedroom: (_value: OptionInputRadio) => void;
  bathroom: OptionInputRadio;
  setBathroom: (_value: OptionInputRadio) => void;
  direction: OptionInputRadio;
  setDirection: (_value: OptionInputRadio) => void;
  listRoomNumber: OptionInputRadio[];
  directions: OptionInputRadio[];
  onOutsideClick?: () => void;
  productType: OptionInputRadio;
}

const FilterOther: React.FC<Props> = (props) => {
  // state
  const [isOpen, setIsOpen] = useState<boolean>(false);
  const $selectRef = useRef(null);
  const isOpenCached: any = useRef(null);
  const [missingProductType, setMissingProductType] = useState<boolean>(false);

  useEffect(() => {
    if (props.productType.value) {
      setMissingProductType(false);
    }
  }, [props.productType]);

  useOutsideClick($selectRef, () => {
    if (isOpenCached.current) {
      setIsOpen(false);
      props.onOutsideClick && props.onOutsideClick();
    }
  });

  useEffect(() => {
    isOpenCached.current = isOpen;
  }, [isOpen]);

  return (
    <div className="position_relative" ref={$selectRef}>
      <div
        className="display_flex height_45 align_items_center justify_content_center padding_x_18 bg_white border_radius_16 cursor_pointer margin_right_16 no_select"
        onClick={() => {
          setIsOpen(!isOpen);
        }}
      >
        <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path
            d="M12.9782 0.249911C15.9444 0.249911 17.9307 2.33241 17.9307 5.42991V12.5787C17.9307 15.6674 15.9444 17.7499 12.9782 17.7499H5.39191C2.42566 17.7499 0.430664 15.6674 0.430664 12.5787V5.42991C0.430664 2.33241 2.42566 0.249911 5.39191 0.249911H12.9782ZM13.1007 7.95079C12.5232 7.95079 12.0507 8.42241 12.0507 8.99991C12.0507 9.57741 12.5232 10.0499 13.1007 10.0499C13.6782 10.0499 14.1419 9.57741 14.1419 8.99991C14.1419 8.42241 13.6782 7.95079 13.1007 7.95079ZM9.18066 7.95079C8.60316 7.95079 8.13066 8.42241 8.13066 8.99991C8.13066 9.57741 8.60316 10.0499 9.18066 10.0499C9.75816 10.0499 10.2307 9.57741 10.2307 8.99991C10.2307 8.42241 9.75816 7.95079 9.18066 7.95079ZM5.26066 7.95079C4.68316 7.95079 4.21066 8.42241 4.21066 8.99991C4.21066 9.57741 4.68316 10.0499 5.26066 10.0499C5.83816 10.0499 6.31066 9.57741 6.31066 8.99991C6.31066 8.42241 5.83816 7.95079 5.26066 7.95079Z"
            fill="#3B4144"
          />
        </svg>
        <span className="margin_x_18 font_size_14 line_height_17 font_weight_600">Khác</span>
        <svg
          width="14"
          height="14"
          viewBox="0 0 14 14"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
          className={classnames({ rotate_180: isOpen })}
          style={{ transition: 'all 0.3s ease' }}
        >
          <path
            fillRule="evenodd"
            clipRule="evenodd"
            d="M6.99967 0.333333C10.6757 0.333333 13.6663 3.324 13.6663 7C13.6663 10.676 10.6757 13.6667 6.99967 13.6667C3.32367 13.6667 0.333008 10.676 0.333008 7C0.333008 3.324 3.32367 0.333333 6.99967 0.333333ZM6.99967 1.33333C3.87501 1.33333 1.33301 3.87533 1.33301 7C1.33301 10.1247 3.87501 12.6667 6.99967 12.6667C10.1243 12.6667 12.6663 10.1247 12.6663 7C12.6663 3.87533 10.1243 1.33333 6.99967 1.33333ZM9.66701 5.68393C9.86234 5.8786 9.86301 6.19527 9.66834 6.39127L7.35367 8.71527C7.27901 8.791 7.18215 8.83985 7.07847 8.85637L6.99967 8.8626C6.86701 8.8626 6.73901 8.80993 6.64567 8.71527L4.33167 6.39127C4.13634 6.19527 4.13767 5.8786 4.33301 5.68393C4.52901 5.48927 4.84567 5.48927 5.04034 5.68527L6.99967 7.6546L8.95967 5.68527C9.15434 5.48927 9.47101 5.48927 9.66701 5.68393Z"
            fill="black"
          />
        </svg>
      </div>
      <div
        className={classnames(
          { active: isOpen },
          'drop_down position_absolute right_-115 top_56 padding_x_16 padding_y_24 z_index_999 bg_white border_radius_18 border_color_grey border_style_solid border_width_1 display_flex',
        )}
        style={{ boxShadow: '1px 1px 2px 1px rgba(23, 23, 23, 0.16)' }}
      >
        <div className="padding_16 border_radius_8 border_width_1 border_style_solid border_color_black margin_right_16">
          <div className="margin_bottom_8">
            <span className="font_size_14 line_height_24 opacity_0_5">Diện tích (m2)</span>
          </div>
          <InputRangeDouble value={props.area} setValue={props.setArea} min={0} max={500} step={50}></InputRangeDouble>
        </div>
        <div className="">
          <div className="margin_bttom_16">
            <InputSegment
              value={props.bedroom}
              setValue={props.setBedroom}
              options={props.listRoomNumber}
              label="Số phòng ngủ:"
              className="width_464"
            ></InputSegment>
          </div>
          <div className="margin_bottom_16">
            <InputSegment
              value={props.bathroom}
              setValue={props.setBathroom}
              options={props.listRoomNumber}
              label="Số WC:"
              className="width_464"
            ></InputSegment>
          </div>
          <div
            onClick={() => {
              if (!props.productType.value) {
                setMissingProductType(true);
              }
            }}
          >
            <InputRadio
              value={props.direction}
              setValue={props.setDirection}
              label="Hướng:"
              options={props.directions}
              placeholder="Chọn hướng"
              disabledClick={!props.productType.value}
              isWarning={missingProductType}
            ></InputRadio>
          </div>
          {missingProductType ? (
            <span className="font_size_14 line_height_24 margin_top_10 color_red margin_left_5">
              Vui lòng chọn loại hình trước!
            </span>
          ) : null}
        </div>
      </div>
    </div>
  );
};

export default FilterOther;
