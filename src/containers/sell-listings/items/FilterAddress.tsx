import React, { useState, useEffect, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import InputRadio from '../../../components/base/input-radio/InputRadio';
import { RootState } from '../../../reducers';
import { OptionInputRadio } from '../../../types/type.base';
import { District, Province, Ward } from '../../../types/type.location';
import classnames from 'classnames';
import { getDistricts, getProvinces, getWards } from '../../../reducers/location/actions';
import useOutsideClick from '../../../utils/customHooks/useOutsideClick';

interface Props {
  province: OptionInputRadio;
  setProvince: (value: OptionInputRadio) => void;
  district: OptionInputRadio;
  setDistrict: (value: OptionInputRadio) => void;
  ward: OptionInputRadio;
  setWard: (value: OptionInputRadio) => void;
  onOutsideClick?: () => void;
}

const FilterAddress: React.FC<Props> = (props) => {
  const { province, setProvince, district, setDistrict, ward, setWard } = props;
  const provinces: Province[] = useSelector((state: RootState) => state.location.provinces);
  const districts: District[] = useSelector((state: RootState) => state.location.districts);
  const wards: Ward[] = useSelector((state: RootState) => state.location.wards);
  // warning
  const [warningProvince, setWarningProvince] = useState<boolean>(false);
  const [warningDistrict, setWarningDistrict] = useState<boolean>(false);
  const [warningWard, setWarningWard] = useState<boolean>(false);
  // state
  const [isOpen, setIsOpen] = useState<boolean>(false);
  const dispatch = useDispatch();
  const $selectRef = useRef(null);
  const isOpenCached: any = useRef(null);

  useEffect(() => {
    if (!provinces?.length) {
      dispatch(getProvinces());
    }
  }, []);

  useEffect(() => {
    if (province.value) {
      dispatch(getDistricts(province.value));
      setDistrict({
        value: '',
        title: '',
      });
      setWard({
        value: '',
        title: '',
      });
    }
  }, [province]);

  useEffect(() => {
    if (district.value) {
      dispatch(getWards(district.value));
      setWard({
        value: '',
        title: '',
      });
    }
  }, [district]);

  useOutsideClick($selectRef, () => {
    if (isOpenCached.current) {
      setIsOpen(false);
      props.onOutsideClick && props.onOutsideClick();
    }
  });

  useEffect(() => {
    isOpenCached.current = isOpen;
  }, [isOpen]);

  const renderWarning = () => {
    if (warningProvince) {
      return 'Vui lòng nhập thông tin tỉnh thành';
    } else if (warningDistrict) {
      return 'Vui lòng nhập thông tin quuận / huyện';
    } else if (warningWard) {
      return 'Vui lòng nhập thông tin xã / phường';
    }
  };

  return (
    <div className="position_relative" ref={$selectRef}>
      <div
        className="display_flex height_45 align_items_center justify_content_center padding_x_18 bg_white border_radius_16 cursor_pointer margin_right_16 no_select"
        onClick={() => {
          setIsOpen(!isOpen);
        }}
      >
        <svg width="14" height="20" viewBox="0 0 14 20" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path
            d="M3.53162 0.936771C5.71648 -0.332726 8.40203 -0.310537 10.5664 0.994894C12.7095 2.32691 14.012 4.70418 13.9999 7.26144C13.95 9.80194 12.5533 12.19 10.8075 14.0361C9.79983 15.1064 8.67261 16.0528 7.44884 16.856C7.32281 16.9289 7.18475 16.9777 7.04148 17C6.9036 16.9941 6.76932 16.9534 6.65075 16.8814C4.78243 15.6746 3.14333 14.134 1.81233 12.334C0.698587 10.8314 0.0658345 9.01601 0 7.13442L0.00498622 6.86069C0.0959221 4.40464 1.4248 2.16093 3.53162 0.936771ZM7.90726 5.03477C7.01907 4.65723 5.99504 4.86235 5.31332 5.55435C4.63159 6.24635 4.42664 7.28872 4.79416 8.19478C5.16168 9.10084 6.02918 9.69184 6.9916 9.69185C7.6221 9.69638 8.22819 9.44383 8.67481 8.99048C9.12143 8.53712 9.37148 7.92064 9.36926 7.27838C9.37261 6.29804 8.79546 5.41231 7.90726 5.03477Z"
            fill="#3B4144"
          />
          <path
            d="M7 20C9.76143 20 12 19.5523 12 19C12 18.4477 9.76143 18 7 18C4.23858 18 2 18.4477 2 19C2 19.5523 4.23858 20 7 20Z"
            fill="#3B4144"
          />
        </svg>
        <span className="margin_x_18 font_size_14 line_height_17 font_weight_600">Vị trí</span>
        <svg
          width="14"
          height="14"
          viewBox="0 0 14 14"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
          className={classnames({ rotate_180: isOpen })}
          style={{ transition: 'all 0.3s ease' }}
        >
          <path
            fillRule="evenodd"
            clipRule="evenodd"
            d="M6.99967 0.333333C10.6757 0.333333 13.6663 3.324 13.6663 7C13.6663 10.676 10.6757 13.6667 6.99967 13.6667C3.32367 13.6667 0.333008 10.676 0.333008 7C0.333008 3.324 3.32367 0.333333 6.99967 0.333333ZM6.99967 1.33333C3.87501 1.33333 1.33301 3.87533 1.33301 7C1.33301 10.1247 3.87501 12.6667 6.99967 12.6667C10.1243 12.6667 12.6663 10.1247 12.6663 7C12.6663 3.87533 10.1243 1.33333 6.99967 1.33333ZM9.66701 5.68393C9.86234 5.8786 9.86301 6.19527 9.66834 6.39127L7.35367 8.71527C7.27901 8.791 7.18215 8.83985 7.07847 8.85637L6.99967 8.8626C6.86701 8.8626 6.73901 8.80993 6.64567 8.71527L4.33167 6.39127C4.13634 6.19527 4.13767 5.8786 4.33301 5.68393C4.52901 5.48927 4.84567 5.48927 5.04034 5.68527L6.99967 7.6546L8.95967 5.68527C9.15434 5.48927 9.47101 5.48927 9.66701 5.68393Z"
            fill="black"
          />
        </svg>
      </div>
      <div
        className={classnames(
          { active: isOpen },
          'drop_down position_absolute left_0 top_56 width_440 padding_x_16 padding_y_24 z_index_999 bg_white border_radius_18 border_color_grey border_style_solid border_width_1',
        )}
        style={{ boxShadow: '1px 1px 2px 1px rgba(23, 23, 23, 0.16)' }}
      >
        <div className="margin_bottom_16">
          <InputRadio
            value={province}
            setValue={setProvince}
            label="Tỉnh thành:"
            options={provinces.map((province: Province) => {
              return {
                value: province.code,
                title: province.name,
              };
            })}
            placeholder="Chọn tỉnh thành"
            isWarning={warningProvince}
            setIsWarning={setWarningProvince}
          ></InputRadio>
        </div>
        <div
          className="margin_bottom_16"
          onClick={() => {
            if (!province.value) {
              setWarningProvince(true);
            }
          }}
        >
          <InputRadio
            value={district}
            setValue={setDistrict}
            label="Quận/ huyện:"
            options={districts.map((district: District) => {
              return {
                value: district.code,
                title: district.nameWithType,
              };
            })}
            placeholder="Chọn quận/ huyện"
            isWarning={warningDistrict}
            setIsWarning={setWarningDistrict}
            disabledClick={!province.value}
          ></InputRadio>
        </div>
        <div
          className=""
          onClick={() => {
            if (!district.value) {
              setWarningDistrict(true);
            }
          }}
        >
          <InputRadio
            value={ward}
            setValue={setWard}
            label="Xã/ Phường:"
            options={wards.map((ward: Ward) => {
              return {
                value: ward.code,
                title: ward.nameWithType,
              };
            })}
            placeholder="Chọn xã/ phường"
            isWarning={warningWard}
            setIsWarning={setWarningWard}
            disabledClick={!district.value}
          ></InputRadio>
        </div>
        {renderWarning() ? (
          <div className="margin_top_16">
            <span className="font_size_12 font_weight_400 color_red">{renderWarning()}</span>
          </div>
        ) : null}
      </div>
    </div>
  );
};

export default FilterAddress;
