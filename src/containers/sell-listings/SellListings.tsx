import React from 'react';
import { useSelector } from 'react-redux';
import { NUMBER_ALL_SELL_LISTINGS_PER_REQ } from '../../constants/sell-listings';
import { RootState } from '../../reducers';
import AllPosts from './components/AllPosts';
import Banner from './components/Banner';
import HighlightPostsGroup from './components/HighlightPostsGroup';

const SellListings: React.FC<any> = () => {
  const highlightSellListings = useSelector((state: RootState) => state.sellListing.highlightSellListings);
  const allSellListings = useSelector((state: RootState) => state.sellListing.allSellListings);

  return (
    <div className="container ">
      <Banner></Banner>
      <div className=" margin_x_xs_22">
        {!highlightSellListings.loadingLists &&
        !highlightSellListings.lists?.length &&
        !allSellListings.loadingLists &&
        !allSellListings.lists?.length ? (
          <div className="text_align_center">
            <span className="font_size_22 font_weight_600">Không có kết quả</span>
          </div>
        ) : (
          <>
            <HighlightPostsGroup
              loading={highlightSellListings.loadingLists}
              list={highlightSellListings.lists}
            ></HighlightPostsGroup>
            <AllPosts
              loading={allSellListings.loadingLists}
              list={allSellListings.lists}
              total={allSellListings.total}
              amountPerPage={NUMBER_ALL_SELL_LISTINGS_PER_REQ}
            ></AllPosts>
          </>
        )}
      </div>
    </div>
  );
};

export default SellListings;
