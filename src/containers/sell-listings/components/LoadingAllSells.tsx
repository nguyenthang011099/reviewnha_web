import React from 'react';

const LoadingAllSells: React.FC = () => {
  return (
    <div className="margin_bottom_50 display_xs_none">
      <div className="display_flex margin_x_-10">
        {Array.from(Array(4).keys()).map((item) => {
          return (
            <div key={item} className="padding_x_10 flex_1 height_345">
              <div className="width_100_percent height_100_percent loading_placeholder border_radius_18"></div>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default LoadingAllSells;
