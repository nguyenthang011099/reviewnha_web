import React, { useState } from 'react';
import HighlightPostItem from '../../../components/highlight-post-item/HighlightPostItem';
import { SellListing } from '../../../types/type.sell-listing';
import ImageViewPopup from '../../../components/image-view/ImageViewPopup';
import LoadingHighlightSells from './LoadingHighlightSells';

interface Props {
  list: SellListing[];
  loading: boolean;
}

const HighlightPostsGroup: React.FC<Props> = (props) => {
  const [images, setImages] = useState<{ url: string }[]>([]);
  const [isOpenViewImages, setIsOpenViewImages] = useState<boolean>(false);

  const setViewImages = (images: { url: string }[]) => {
    setImages(images);
    setIsOpenViewImages(true);
  };

  if (props.loading) {
    return <LoadingHighlightSells></LoadingHighlightSells>;
  }

  return (
    <div className="highlight_post_group ">
      <ImageViewPopup images={images} isOpen={isOpenViewImages} setIsOpen={setIsOpenViewImages}></ImageViewPopup>
      <div className="display_flex  margin_x_-11 display_xs_block">
        {props.list.map((post: SellListing) => {
          return (
            <div className="col_4 padding_x_11 col_xs_12 margin_bottom_xs_8" key={post.id}>
              <HighlightPostItem post={post} setViewImages={setViewImages}></HighlightPostItem>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default HighlightPostsGroup;
