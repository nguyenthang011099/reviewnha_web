import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import InputSelect from '../../../components/base/input-select/InputSelect';
import {
  LIST_PRICE_RANGE,
  LIST_ROOM_NUMBER,
  NUMBER_ALL_SELL_LISTINGS_PER_REQ,
  NUMBER_HIGHLIGHT_SELL_LISTINGS_PER_REQ,
} from '../../../constants/sell-listings';
import bgBanner from '../../../images/banner-advertising.png';
import { RootState } from '../../../reducers';
import {
  changeFilterData,
  changeIsFiltered,
  getSellListingPartTwo,
  getSellListingsList,
} from '../../../reducers/sell-listing/actions';
import { OptionInputRadio, OptionRangeDouble } from '../../../types/type.base';
import FilterAddress from '../items/FilterAddress';
import FilterOther from '../items/FilterOther';
import bannerLogo from '../../../images/logo_banner.png';
import '../styles/Banner.scss';

const Banner: React.FC = () => {
  const [province, setProvince] = useState<OptionInputRadio>({ value: '', title: '' });
  const [district, setDistrict] = useState<OptionInputRadio>({ value: '', title: '' });
  const [ward, setWard] = useState<OptionInputRadio>({ value: '', title: '' });
  const [productType, setProductType] = useState<OptionInputRadio>({ value: '', title: '' });
  const [price, setPrice] = useState<OptionInputRadio>({ value: '', title: '' });
  const [area, setArea] = useState<OptionRangeDouble>({ min: 0, max: 500 });
  const [bedroom, setBedroom] = useState<OptionInputRadio>({ value: null, title: '' });
  const [bathroom, setBathroom] = useState<OptionInputRadio>({ value: null, title: '' });
  const [direction, setDirection] = useState<OptionInputRadio>({ value: '', title: '' });
  const [typeTransaction, setTypeTransaction] = useState<string>('');
  const dispatch = useDispatch();
  const dataType = useSelector((state: RootState) => state.sellListing.stepOneData);
  const directions = useSelector((state: RootState) => state.sellListing.stepTwoData.directions);
  const isFiltered = useSelector((state: RootState) => state.sellListing.isFiltered);
  const [query, setQuery] = useState<string>('');
  const router = useRouter();

  useEffect(() => {
    if (router.query.transactionType) {
      setTypeTransaction(router.query.transactionType as string);
    }
  }, [router.query.transactionType]);

  useEffect(() => {
    if (router.query.provinceCode) {
      setProvince({ title: '', value: router.query.provinceCode as string });
    }
  }, [router.query.provinceCode]);

  useEffect(() => {
    if (router.query.districtCode) {
      setDistrict({ title: '', value: router.query.districtCode as string });
    }
  }, [router.query.districtCode]);

  useEffect(() => {
    if (router.query.wardCode) {
      setWard({ title: '', value: router.query.wardCode as string });
    }
  }, [router.query.wardCode]);

  const onChangeProductType = (type: OptionInputRadio) => {
    setProductType(type);
    dispatch(getSellListingPartTwo(type.value));
  };

  const onClickFilter = () => {
    const data = {
      provinceCode: province.value,
      districtCode: district.value,
      wardCode: ward.value,
      typeProduct: productType.value,
      typeTransaction: typeTransaction || undefined,
      price: {
        min: price.value?.includes('-') ? price.value.split('-')[0] : null,
        max: price.value?.includes('-') ? price.value.split('-')[1] : null,
      },
      area: {
        min: area.min,
        max: area.max,
      },
      direction: direction.value,
      bedroom: Number(bedroom.value),
      bathroom: Number(bathroom.value),
      query: query,
    };
    dispatch(changeFilterData(data));
    if (!isFiltered) {
      dispatch(changeIsFiltered(true));
    }
    dispatch(
      getSellListingsList({
        ...data,
        type: 'all',
        limit: NUMBER_ALL_SELL_LISTINGS_PER_REQ,
      }),
    );
    dispatch(
      getSellListingsList({
        ...data,
        type: 'highlight',
        limit: NUMBER_HIGHLIGHT_SELL_LISTINGS_PER_REQ,
      }),
    );
  };

  return (
    <div className="margin_bottom_46 margin_top_30 padding_x_xs_22 banner_sell_listings">
      <div
        className="banner_image height_390 padding_top_114 padding_left_121 border_radius_18 height_xs_90 padding_x_xs_33 padding_top_xs_30"
        style={{ background: `url(${bgBanner})` }}
      >
        <div className="font_logo color_white">
          <img src={bannerLogo} alt="logo" className="margin_left_-15 height_xs_15 margin_left_xs_-5"></img>
          <p className="margin_y_0 line_height_100_percent font_size_28 font_weight_600 font_size_xs_10">
            Finding your best home
          </p>
        </div>
      </div>
      <div className="display_xs_none">
        <div
          className="banner_container border_radius_25  margin_x_md_30 margin_top_-50 padding_x_36 
          border_width_2 border_color_white border_style_solid position_relative z_index_2 padding_y_24"
          style={{ background: 'rgba(255,255,225,0.3)', backdropFilter: 'blur(94px)' }}
        >
          <div className="display_flex align_items_center flex_wrap justify_content_sb">
            <div className="display_flex align_items_center">
              <div
                className="display_flex height_45 align_items_center justify_content_center padding_x_18 bg_white 
            border_radius_16 cursor_pointer margin_right_16"
              >
                <svg width="19" height="18" viewBox="0 0 19 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path
                    d="M14.4181 13.9475C14.7563 13.6116 15.2985 13.6116 15.6367 13.9475L17.872 15.7519H17.9108C18.3631 16.209 18.3631 16.9501 17.9108 17.4072C17.4586 17.8643 16.7254 17.8643 16.2731 17.4072L14.4181 15.2812L14.3477 15.2017C14.2166 15.0358 14.1442 14.8288 14.1442 14.6143C14.1442 14.3641 14.2427 14.1241 14.4181 13.9475ZM8.25543 0.25C10.246 0.25 12.155 1.04926 13.5626 2.47194C14.9701 3.89463 15.7609 5.8242 15.7609 7.83618C15.7609 12.0259 12.4006 15.4224 8.25543 15.4224C4.1103 15.4224 0.75 12.0259 0.75 7.83618C0.75 3.64645 4.1103 0.25 8.25543 0.25Z"
                    fill="#3B4144"
                  />
                </svg>
                <input
                  placeholder="Nhập từ khóa..."
                  className="border_none bg_white margin_left_18 width_112 input_search_banner"
                  onChange={(e) => {
                    setQuery(e.target.value);
                  }}
                />
              </div>
              <FilterAddress
                province={province}
                setProvince={setProvince}
                district={district}
                setDistrict={setDistrict}
                ward={ward}
                setWard={setWard}
              ></FilterAddress>
              <div className="margin_right_16 max_width_180">
                <InputSelect
                  value={productType}
                  options={dataType.productTypes}
                  setValue={onChangeProductType}
                  label="Loại hình"
                  className="display_flex height_45 align_items_center justify_content_center cursor_pointer padding_x_18"
                >
                  <svg width="17" height="18" viewBox="0 0 17 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path
                      d="M5.91285 16.6767V14.0012C5.91285 13.3182 6.47057 12.7645 7.15855 12.7645H9.67346C10.0038 12.7645 10.3207 12.8948 10.5543 13.1267C10.7879 13.3586 10.9192 13.6732 10.9192 14.0012V16.6767C10.9171 16.9606 11.0292 17.2336 11.2307 17.4352C11.4323 17.6367 11.7065 17.75 11.9925 17.75H13.7083C14.5096 17.7521 15.2788 17.4375 15.8462 16.8757C16.4136 16.3139 16.7324 15.5511 16.7324 14.7556V7.1335C16.7324 6.4909 16.4455 5.88136 15.949 5.46909L10.1122 0.841385C9.09687 0.0299915 7.64215 0.0561893 6.65714 0.903607L0.953558 5.46909C0.43357 5.86921 0.12278 6.48056 0.107422 7.1335V14.7478C0.107422 16.4059 1.46138 17.75 3.13157 17.75H4.80818C5.40225 17.75 5.88504 17.2742 5.88935 16.6845L5.91285 16.6767Z"
                      fill="#3B4144"
                    />
                  </svg>
                </InputSelect>
              </div>
              <div className="margin_right_16  max_width_180">
                <InputSelect
                  value={price}
                  options={LIST_PRICE_RANGE}
                  setValue={setPrice}
                  label="Giá"
                  className="display_flex height_45 align_items_center justify_content_center cursor_pointer padding_x_18"
                >
                  <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path
                      d="M13.291 0.125C16.3087 0.125 18.0898 1.86152 18.0898 4.83408H14.3876V4.86441C12.6694 4.86441 11.2765 6.22243 11.2765 7.89763C11.2765 9.57283 12.6694 10.9308 14.3876 10.9308H18.0898V11.2038C18.0898 14.1385 16.3087 15.875 13.291 15.875H5.38873C2.37096 15.875 0.589844 14.1385 0.589844 11.2038V4.79616C0.589844 1.86152 2.37096 0.125 5.38873 0.125H13.291ZM17.4365 6.13836C17.7973 6.13836 18.0898 6.42354 18.0898 6.77534V8.98959C18.0856 9.33967 17.7956 9.62247 17.4365 9.62657H14.4576C13.5878 9.63798 12.8271 9.05733 12.6298 8.23128C12.531 7.71851 12.6697 7.18937 13.0088 6.78569C13.3478 6.38201 13.8525 6.14507 14.3876 6.13836H17.4365ZM14.8076 7.16207H14.5198C14.3432 7.16005 14.173 7.22706 14.0473 7.34816C13.9217 7.46926 13.851 7.63436 13.851 7.80663C13.8509 8.16805 14.1492 8.4622 14.5198 8.46636H14.8076C15.177 8.46636 15.4765 8.17438 15.4765 7.81422C15.4765 7.45405 15.177 7.16207 14.8076 7.16207ZM9.67429 3.52979H4.7354C4.369 3.52977 4.07077 3.81715 4.06651 4.17435C4.06651 4.53577 4.36473 4.82992 4.7354 4.83408H9.67429C10.0437 4.83408 10.3432 4.5421 10.3432 4.18193C10.3432 3.82176 10.0437 3.52979 9.67429 3.52979Z"
                      fill="#3B4144"
                    />
                  </svg>
                </InputSelect>
              </div>
              <FilterOther
                area={area}
                setArea={setArea}
                bedroom={bedroom}
                setBedroom={setBedroom}
                bathroom={bathroom}
                setBathroom={setBathroom}
                direction={direction}
                setDirection={setDirection}
                listRoomNumber={LIST_ROOM_NUMBER}
                directions={directions}
                productType={productType}
              ></FilterOther>
            </div>
            <div
              className="display_flex height_45 align_items_center justify_content_sb padding_x_24 bg_black
            border_radius_16 cursor_pointer color_white flex_md_none width_180 no_select btn_filter_action"
              onClick={onClickFilter}
            >
              <span className="color_white font_size_14 font_weight_600">Tìm kiếm</span>
              <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path
                  d="M17.0029 15.9922L17.0027 15.9921L14.6979 13.6921C15.8999 12.3005 16.6281 10.4877 16.6281 8.50823C16.6281 4.13026 13.0674 0.569568 8.68945 0.569568C4.31152 0.569568 0.75 4.13023 0.75 8.50823C0.75 12.8862 4.31148 16.4477 8.68945 16.4477C10.4815 16.4477 12.1371 15.8506 13.467 14.8454L15.8126 17.1845L15.813 17.1849C15.9776 17.3484 16.1932 17.4304 16.4076 17.4304C16.6221 17.4304 16.8395 17.3488 17.0043 17.1821C17.3331 16.8527 17.3312 16.3206 17.0029 15.9922ZM2.43433 8.50823C2.43433 5.0594 5.24054 2.2539 8.68945 2.2539C12.1375 2.2539 14.9438 5.05938 14.9438 8.50823C14.9438 11.9571 12.1375 14.7633 8.68945 14.7633C5.24058 14.7633 2.43433 11.9571 2.43433 8.50823Z"
                  fill="white"
                  stroke="white"
                  strokeWidth="0.5"
                />
              </svg>
            </div>
          </div>
        </div>
      </div>
      <div className="display_none display_xs_flex">
        <div
          className="display_flex height_48 align_items_center justify_content_center padding_x_24 bg_white border_radius_16 cursor_pointer
        border_color_grey border_style_solid border_width_1 width_100_percent margin_top_16"
        >
          <input
            placeholder="Nhập từ khóa..."
            className="border_none bg_white margin_right_18 width_100_percent"
            onChange={(e) => {
              setQuery(e.target.value);
            }}
          />
          <svg
            width={19}
            height={18}
            viewBox="0 0 19 18"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            onClick={onClickFilter}
          >
            <path
              d="M17.9936 15.6726L17.9934 15.6724L15.61 13.3752C16.8523 11.9845 17.6061 10.172 17.6061 8.19159C17.6061 3.8055 13.915 0.25293 9.39524 0.25293C4.87549 0.25293 1.18359 3.80546 1.18359 8.19159C1.18359 12.5777 4.87545 16.131 9.39524 16.131C11.251 16.131 12.9659 15.5325 14.3431 14.524L16.7799 16.8709L16.7803 16.8713C16.9496 17.0337 17.1696 17.1138 17.3866 17.1138C17.6037 17.1138 17.8256 17.0341 17.995 16.8685C18.3362 16.5384 18.3342 16.0015 17.9936 15.6726ZM2.90985 8.19159C2.90985 4.75089 5.81109 1.93726 9.39524 1.93726C12.9785 1.93726 15.8798 4.75087 15.8798 8.19159C15.8798 11.6323 12.9785 14.4467 9.39524 14.4467C5.81113 14.4467 2.90985 11.6323 2.90985 8.19159Z"
              fill="#3B4144"
              stroke="white"
              strokeWidth="0.5"
            />
          </svg>
        </div>
      </div>
    </div>
  );
};

export default Banner;
