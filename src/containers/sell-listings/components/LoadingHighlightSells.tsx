import React from 'react';

const LoadingHighlightSells: React.FC = () => {
  return (
    <div className="display_flex margin_x_-11 display_xs_block">
      {Array.from(Array(3).keys()).map((item) => {
        return (
          <div
            key={item}
            className="padding_x_11 flex_1 height_495 width_xs_100_percent height_xs_250 margin_bottom_xs_24"
          >
            <div className="width_100_percent height_100_percent loading_placeholder border_radius_18"></div>
          </div>
        );
      })}
    </div>
  );
};

export default LoadingHighlightSells;
