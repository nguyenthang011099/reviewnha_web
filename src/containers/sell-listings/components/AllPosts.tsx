import { useRouter } from 'next/router';
import React, { useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Pagination from '../../../components/pagination/Pagination';
import PostItem from '../../../components/post-item/PostItem';
import { RootState } from '../../../reducers';
import { getSellListingsList } from '../../../reducers/sell-listing/actions';
import { SellListing } from '../../../types/type.sell-listing';
import LoadingAllSells from './LoadingAllSells';
interface Props {
  list: SellListing[];
  loading: boolean;
  total: number;
  amountPerPage: number;
}

const AllPosts: React.FC<Props> = (props) => {
  const filterData = useSelector((state: RootState) => state.sellListing.filterData);
  const dispatch = useDispatch();
  const topDomRef = useRef(null);
  const router = useRouter();

  const getDataByPage = (data: any) => {
    dispatch(
      getSellListingsList({
        ...data,
        ...filterData,
      }),
    );
  };

  const scrollTop = () => {
    if (topDomRef && topDomRef.current) {
      topDomRef.current.scrollIntoView({ behavior: 'smooth', block: 'start' });
    }
  };

  if (!props.list.length && !props.loading) {
    return null;
  }

  return (
    <div className="all_posts margin_top_80 margin_bottom_130">
      <h2 className="margin_y_0 margin_bottom_44 font_size_18 line_height_27 font_weight_600">Tất cả tin rao</h2>
      {props.loading ? (
        <LoadingAllSells></LoadingAllSells>
      ) : (
        <>
          <div className="display_flex flex_wrap margin_x_-10" ref={topDomRef}>
            {props.list.map((post: SellListing) => {
              return (
                <div className="col_3 padding_x_10 margin_bottom_50 col_xs_12" key={post.id}>
                  <PostItem post={post}></PostItem>
                </div>
              );
            })}
          </div>
        </>
      )}
      {props.total > props.amountPerPage ? (
        <Pagination
          totalItem={props.total}
          pageAmount={props.amountPerPage}
          fetchDataEachPage={getDataByPage}
          data={{
            type: 'all',
            limit: props.amountPerPage,
            typeTransaction: router.query.transactionType,
            provinceCode: router.query.provinceCode,
            districtCode: router.query.districtCode,
            wardCode: router.query.wardCode,
          }}
          scrollTop={scrollTop}
          checkRecalculateTotalPage={props.total}
        ></Pagination>
      ) : null}
    </div>
  );
};

export default AllPosts;
