import React from 'react';
import { useSelector } from 'react-redux';
import { RootState } from '../../reducers';
import { Blog, BlogsAllData, BlogsCategoryData } from '../../types/type.blog';
import Content from './components/Content';
import FeatureBlogs from './components/FeatureBlogs';
import RelatedBlogs from './components/RelatedBlogs';

const BlogDetail: React.FC = () => {
  const loadingBlogDetail: boolean = useSelector((state: RootState) => state.blog.loadingBlogDetail);
  const blogDetail: Blog = useSelector((state: RootState) => state.blog.blogDetail);
  const blogsAll: BlogsAllData = useSelector((state: RootState) => state.blog.blogsAll);
  const blogsByCategory: BlogsCategoryData = useSelector((state: RootState) => state.blog.blogsByCategory);
  const activeCategory: string = useSelector((state: RootState) => state.blog.activeCategory);
  // relatedblogs
  let relatedBlogs = {
    blogs: [],
  };
  if (blogDetail?.category?.slug) {
    relatedBlogs = blogsByCategory[blogDetail.category.slug];
  }

  return (
    <div className="container_blog padding_x_240 margin_top_70 padding_bottom_265 padding_x_md_0 width_xs_100_percent padding_x_xs_24 margin_top_xs_30">
      {!loadingBlogDetail ? (
        <>
          <Content blogDetail={blogDetail} activeCategory={activeCategory}></Content>
          <RelatedBlogs
            blogs={relatedBlogs?.blogs || []}
            blogId={blogDetail.id}
            category={blogDetail.category}
          ></RelatedBlogs>
          <FeatureBlogs blogs={blogsAll.featureBlogs.blogs} activeBlogId={blogDetail.id}></FeatureBlogs>
        </>
      ) : (
        <div className="loading width_100_percent height_100_vh loading_placeholder"></div>
      )}
    </div>
  );
};

export default BlogDetail;
