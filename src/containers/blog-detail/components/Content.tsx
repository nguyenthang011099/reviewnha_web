import HTMLReactParser from 'html-react-parser';
import Link from 'next/link';
import React, { useEffect, useState } from 'react';
import { Blog, BlogTag } from '../../../types/type.blog';
import '../styles/Content.scss';
import { FacebookShareButton, EmailShareButton } from 'react-share';
import { useRouter } from 'next/router';
import { formatDateBlog } from '../../../utils/format-date-blog';

interface Props {
  blogDetail: Blog;
  activeCategory: string;
}

const Content: React.FC<Props> = (props) => {
  const router = useRouter();
  const [linkShare, setLinkShare] = useState<string>('');

  useEffect(() => {
    const hostname = window.location.origin;
    setLinkShare(hostname + router.asPath);
  }, []);

  return (
    <div className="content_blog width_xs_100_percent">
      <div className="nav display_flex align_items_center justify_content_sb">
        <Link
          href={{ pathname: '/blogs-page', query: { categorySlug: props.activeCategory } }}
          as={`/blogs/${props.activeCategory === 'all' ? '' : props.activeCategory}`}
        >
          <a className="display_flex align_items_center ">
            <svg width="8" height="11" viewBox="0 0 8 11" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path
                d="M6.33331 10.1668L1.66665 5.50016L6.33331 0.833496"
                stroke="#3B4144"
                strokeWidth="1.5"
                strokeLinecap="round"
                strokeLinejoin="round"
              />
            </svg>
            <span className="font_size_14 line_height_17 font_weight_600 margin_left_21 color_red font_size_xs_13 line_height_xs_15">
              Quay lại
            </span>
          </a>
        </Link>
        {props.blogDetail.category ? (
          <div className="display_flex align_items_center">
            <span className="font_size_15 line_height_22 display_xs_none">Chuyên mục:</span>
            <Link
              href={{ pathname: '/blogs-page', query: { categorySlug: props.blogDetail.category.slug } }}
              as={`/blogs/${props.blogDetail.category.slug}`}
            >
              <a className="bg_green height_32 padding_x_36 border_radius_25 color_white align_items_center display_flex margin_left_16 margin_left_xs_0">
                <span className="font_size_16 line_height_19 font_weight_600 font_size_xs_14 line_height_xs_21 text_over_flow_1">
                  {props.blogDetail.category.name}
                </span>
              </a>
            </Link>
          </div>
        ) : null}
      </div>
      <div className="margin_top_61 margin_top_xs_30">
        <h2 className="font_weight_700 font_size_32 line_height_42 margin_y_0 margin_bottom_24 font_size_xs_18 line_height_xs_27 ">
          {props.blogDetail.title}
        </h2>
        <div className="display_flex align_items_center justify_content_sb display_xs_block">
          <div className="display_flex align_items_center  margin_bottom_xs_18">
            {props.blogDetail.category ? (
              <>
                <span className="font_weight_600 font_size_14 line_height_22 color_green font_size_xs_12 line_height_xs_18 text_over_flow_1">
                  {props.blogDetail.category.name}
                </span>
                <svg
                  width="3"
                  height="3"
                  viewBox="0 0 3 3"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                  className="margin_x_8 "
                >
                  <circle cx="1.5" cy="1.5" r="1.5" fill="#3B4144" />
                </svg>
              </>
            ) : null}
            <span className="font_size_15 line_height_22 font_size_xs_12 line_height_xs_18 text_over_flow_1">
              {props.blogDetail.authorName}
            </span>
            <svg
              width="3"
              height="3"
              viewBox="0 0 3 3"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
              className="margin_x_8 "
            >
              <circle cx="1.5" cy="1.5" r="1.5" fill="#3B4144" />
            </svg>
            <span className="font_size_15 line_height_22 opacity_0_5 font_size_xs_12 line_height_xs_18 text_over_flow_1">
              {formatDateBlog(props.blogDetail.publishedAt)}
            </span>
          </div>
          <div className="display_flex">
            <div>
              <EmailShareButton url={linkShare} className="email">
                <span className="height_32 width_145 display_flex align_items_center  justify_content_center bg_gray border_radius_25 border_none">
                  <svg width="20" height="18" viewBox="0 0 20 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path
                      d="M14.9399 0C16.2809 0 17.5709 0.53 18.5199 1.481C19.4699 2.43 20.0009 3.71 20.0009 5.05V12.95C20.0009 15.74 17.7309 18 14.9399 18H5.06085C2.26985 18 0.000854492 15.74 0.000854492 12.95V5.05C0.000854492 2.26 2.25985 0 5.06085 0H14.9399ZM16.0709 5.2C15.8609 5.189 15.6609 5.26 15.5099 5.4L11.0009 9C10.4209 9.481 9.58985 9.481 9.00085 9L4.50085 5.4C4.18985 5.17 3.75985 5.2 3.50085 5.47C3.23085 5.74 3.20085 6.17 3.42985 6.47L3.56085 6.6L8.11085 10.15C8.67085 10.59 9.34985 10.83 10.0609 10.83C10.7699 10.83 11.4609 10.59 12.0199 10.15L16.5309 6.54L16.6109 6.46C16.8499 6.17 16.8499 5.75 16.5999 5.46C16.4609 5.311 16.2699 5.22 16.0709 5.2Z"
                      fill="white"
                    />
                  </svg>
                  <span className="font_weight_600 font_size_14 line_height_17 margin_left_12 color_white font_size_xs_13 line_height_xs_15">
                    Gửi mail
                  </span>
                </span>
              </EmailShareButton>
            </div>
            <FacebookShareButton url={linkShare} className="facebook">
              <span
                className="height_32 width_145 display_flex align_items_center  justify_content_center border_radius_25 border_none margin_left_8"
                style={{ background: '#395185' }}
              >
                <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path
                    d="M10.001 0.00195312C4.47895 0.00195312 0.00195312 4.47895 0.00195312 10.001C0.00195312 14.991 3.65795 19.127 8.43895 19.88V12.892H5.89895V10.001H8.43895V7.79795C8.43895 5.28995 9.93195 3.90695 12.215 3.90695C13.309 3.90695 14.455 4.10195 14.455 4.10195V6.56095H13.191C11.951 6.56095 11.563 7.33295 11.563 8.12395V9.99895H14.334L13.891 12.89H11.563V19.878C16.344 19.129 20 14.992 20 10.001C20 4.47895 15.523 0.00195312 10.001 0.00195312Z"
                    fill="white"
                  />
                </svg>
                <span className="font_weight_600 font_size_14 line_height_17 margin_left_12 color_white font_size_xs_13 line_height_xs_15">
                  Chia sẻ
                </span>
              </span>
            </FacebookShareButton>
          </div>
        </div>
      </div>
      <div className="content margin_top_36">{HTMLReactParser(props.blogDetail.content)}</div>
      <div className="tags margin_top_20 display_flex flex_wrap">
        {props.blogDetail.tags?.map((tag: BlogTag) => {
          return (
            <Link
              href={{ pathname: '/custom-blogs-page', query: { tagSlug: tag.slug } }}
              as={`/blogs/tags/${tag.slug}`}
              key={tag.slug}
            >
              <a className="display_block">
                <div
                  className="height_31 padding_x_24 display_flex align_items_center margin_right_16 border_radius_24 cursor_pointer margin_top_16"
                  style={{ background: '#E5E5E5' }}
                >
                  {tag.name}
                </div>
              </a>
            </Link>
          );
        })}
      </div>
    </div>
  );
};

export default Content;
