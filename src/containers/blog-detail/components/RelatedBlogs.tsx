import Link from 'next/link';
import React from 'react';
import BlogItem from '../../../components/blog-item/BlogItem';
import { Blog, BlogCategory } from '../../../types/type.blog';

interface Props {
  blogs: Blog[];
  category: BlogCategory;
  blogId: number;
}

const RelatedBlogs: React.FC<Props> = (props) => {
  let countBlogs = 0;

  /**
   * nếu các blog trong danh mục đó chỉ có 1 thì không hiên thị vì đã là detail rồi. nếu bằng 0 thì không hiển thị khi không có category
   */
  if (!props.blogs.length || props.blogs.length === 1) {
    return null;
  }

  return (
    <div className="related_blogs margin_top_56  margin_top_xs_36">
      <div className="title display_flex align_items_center justify_content_sb margin_bottom_36 margin_bottom_xs_26">
        <div className="display_flex align_items_center">
          <span className="font_weight_600 font_size_18 line_height_27 font_size_xs_14 line_height_xs_17 white_space_nowrap">
            Tin cùng chuyên mục
          </span>
          <Link
            href={{ pathname: '/blogs-page', query: { categorySlug: props.category?.slug } }}
            as={`/blogs/${props.category?.slug}`}
          >
            <a className="bg_green height_32 padding_x_36 border_radius_25 color_white align_items_center display_flex margin_left_16 display_xs_none">
              <span className="font_size_16 line_height_19 font_weight_600  font_size_xs_14 line_height_xs_17">
                {props.category?.name}
              </span>
            </a>
          </Link>
        </div>
        <Link
          href={{ pathname: '/blogs-page', query: { categorySlug: props.category?.slug } }}
          as={`/blogs/${props.category?.slug}`}
        >
          <a>
            <span className="line_height_22 font_size_15 cursor_pointer display_xs_none">Xem tất cả</span>
            <span className="cursor_pointer display_xs_block padding_left_15">
              <svg width="8" height="12" viewBox="0 0 8 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path
                  d="M1.6665 1.21549L6.33317 5.88216L1.6665 10.5488"
                  stroke="#3B4144"
                  strokeWidth="1.5"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                />
              </svg>
            </span>
          </a>
        </Link>
      </div>
      {props.blogs.map((blog: Blog) => {
        if (blog.id !== props.blogId && countBlogs < 2) {
          countBlogs++;
          return (
            <div className="margin_bottom_36 margin_bottom_xs_24" key={blog.id}>
              <BlogItem blog={blog}></BlogItem>
            </div>
          );
        }
        return null;
      })}
    </div>
  );
};

export default RelatedBlogs;
