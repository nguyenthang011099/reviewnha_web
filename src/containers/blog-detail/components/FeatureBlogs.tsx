import Link from 'next/link';
import React from 'react';
import BlogItem from '../../../components/blog-item/BlogItem';
import { Blog } from '../../../types/type.blog';

interface Props {
  blogs: Blog[];
  activeBlogId: number;
}

const FeatureBlogs: React.FC<Props> = (props) => {
  let count = 0;

  return (
    <div className="related_blogs margin_top_56 margin_top_xs_36">
      <div className="title display_flex align_items_center justify_content_sb margin_bottom_36 margin_bottom_xs_26">
        <span className="font_weight_600 font_size_18 line_height_27 font_size_xs_14 line_height_xs_17">
          Tin thịnh hành
        </span>
        <Link href={{ pathname: '/custom-blogs-page' }} as="/blogs/feature">
          <a>
            <span className="line_height_22 font_size_15 cursor_pointer display_xs_none">Xem tất cả</span>
            <span className="cursor_pointer display_xs_block padding_left_15">
              <svg width="8" height="12" viewBox="0 0 8 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path
                  d="M1.6665 1.21549L6.33317 5.88216L1.6665 10.5488"
                  stroke="#3B4144"
                  strokeWidth="1.5"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                />
              </svg>
            </span>
          </a>
        </Link>
      </div>
      {props.blogs.map((blog: Blog) => {
        if (count < 2 && blog.id !== props.activeBlogId) {
          count++;
          return (
            <div className="margin_bottom_36 margin_bottom_xs_24" key={blog.id}>
              <BlogItem blog={blog}></BlogItem>
            </div>
          );
        }
        return null;
      })}
    </div>
  );
};

export default FeatureBlogs;
