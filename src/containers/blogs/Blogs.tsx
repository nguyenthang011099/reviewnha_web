import React from 'react';
import { useSelector } from 'react-redux';
import { RootState } from '../../reducers';
import { BlogCategory, BlogsAllData, BlogsCategoryData, VideoList } from '../../types/type.blog';
import Banner from './components/Banner';
import List from './components/list';

const Posts: React.FC = () => {
  const categories: BlogCategory[] = useSelector((state: RootState) => state.blog.categories);
  const blogsByCategory: BlogsCategoryData = useSelector((state: RootState) => state.blog.blogsByCategory);
  const blogsAll: BlogsAllData = useSelector((state: RootState) => state.blog.blogsAll);
  const activeCategory: string = useSelector((state: RootState) => state.blog.activeCategory);
  const videoList: VideoList = useSelector((state: RootState) => state.blog.videoList);

  return (
    <div className="posts margin_top_56 padding_bottom_184 padding_bottom_xs_50 margin_top_sm_46 margin_top_xs_24">
      <Banner></Banner>
      <List
        videoList={videoList}
        categories={categories}
        blogsByCategory={blogsByCategory}
        blogsAll={blogsAll}
        activeCategory={activeCategory}
      ></List>
    </div>
  );
};

export default Posts;
