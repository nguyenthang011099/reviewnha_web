import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../reducers';
import { getBlogsByTag, getBlogsByType } from '../../reducers/blog/actions';
import { BlogsByTag } from '../../types/type.blog';
import Banner from './components/Banner';
import OtherTabList from './components/list/OtherTabList';

interface Props {
  type: 'tag' | 'feature';
}

const FeatureBlogs: React.FC<Props> = (props) => {
  const featureBlogs = useSelector((state: RootState) => state.blog.blogsAll.featureBlogs);
  const blogsByTag: BlogsByTag = useSelector((state: RootState) => state.blog.blogsByTag);
  const dispatch = useDispatch();

  /**
   * getFeatureBlogsNextPage lấy bài viết nổi bật theo page
   * @param category không cần
   * @param offset offset
   */
  const getFeatureBlogsNextPage = (_category: string, offset: number) => {
    dispatch(getBlogsByType('featured', offset));
  };

  /**
   * getBlogsByTagNextPage lấy bài viết theo tag và theo offset
   * @param slug slug của tag
   * @param offset
   */
  const getBlogsByTagNextPage = (slug: string, offset: number) => {
    dispatch(getBlogsByTag(slug, offset));
  };

  // lấy các props truyền vào để render
  const getPropsToRender = () => {
    if (props.type === 'feature') {
      return {
        blogs: featureBlogs.blogs,
        total: featureBlogs.total,
        currentOffset: featureBlogs.currentOffset,
        getDataNextPage: getFeatureBlogsNextPage,
      };
    }
    return {
      blogs: blogsByTag.blogs,
      total: blogsByTag.total,
      currentOffset: blogsByTag.currentOffset,
      getDataNextPage: getBlogsByTagNextPage,
      category: blogsByTag.currentTag.slug,
    };
  };

  const propsToRender = getPropsToRender();

  return (
    <div className="feature_blogs margin_top_56 padding_bottom_184 padding_bottom_xs_50 margin_top_sm_46 margin_top_xs_24">
      <Banner></Banner>
      <div className="padding_x_240 margin_top_40 padding_x_md_0 container_blog margin_top_xs_16">
        <div className="title display_flex align_items_center justify_content_center ">
          <svg
            className="width_xs_18 height_xs_20 width_36 height_40"
            viewBox="0 0 36 40"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              d="M26.382 0C32.56 0 36 3.56 36 9.66V30.32C36 36.52 32.56 40 26.382 40H9.62C3.54 40 0 36.52 0 30.32V9.66C0 3.56 3.54 0 9.62 0H26.382ZM10.16 27.48C9.56 27.42 8.98 27.7 8.66 28.22C8.34 28.72 8.34 29.38 8.66 29.9C8.98 30.4 9.56 30.7 10.16 30.62H25.84C26.638 30.54 27.24 29.858 27.24 29.06C27.24 28.24 26.638 27.56 25.84 27.48H10.16ZM25.84 18.358H10.16C9.298 18.358 8.6 19.06 8.6 19.92C8.6 20.78 9.298 21.48 10.16 21.48H25.84C26.7 21.48 27.4 20.78 27.4 19.92C27.4 19.06 26.7 18.358 25.84 18.358ZM16.138 9.3H10.16V9.32C9.298 9.32 8.6 10.02 8.6 10.88C8.6 11.74 9.298 12.44 10.16 12.44H16.138C17 12.44 17.7 11.74 17.7 10.858C17.7 10 17 9.3 16.138 9.3Z"
              fill="#007882"
            />
          </svg>
          <span className="font_weight_700 line_height_42 font_size_32 margin_left_30 font_size_xs_18 line_height_xs_27 margin_left_xs_11 text_transform_capitalize">
            {props.type === 'feature' ? 'Tin thịnh hành' : blogsByTag.currentTag.name}
          </span>
        </div>
        <div className="content margin_top_xs_-24">
          <OtherTabList
            blogs={propsToRender.blogs}
            total={propsToRender.total}
            currentOffset={propsToRender.currentOffset}
            getDataNextPage={propsToRender.getDataNextPage}
            category={propsToRender.category}
          ></OtherTabList>
        </div>
      </div>
    </div>
  );
};

export default FeatureBlogs;
