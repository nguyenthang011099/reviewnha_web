import React, { useState } from 'react';
import { SliderMethod } from '../../../types/type.slider';
import bannerImage from '../../../images/banner-posts.png';
import Slider from '../../../components/slider/Slider';

const Banner: React.FC = () => {
  const [listImages, setListImages] = useState([
    {
      avatar: {
        url: bannerImage,
      },
    },
    {
      avatar: {
        url: bannerImage,
      },
    },
  ]);
  let slider: SliderMethod = {};

  return (
    <div className="container_blog padding_x_xs_22">
      <Slider
        className="width_100_percent"
        listImages={listImages as any}
        setListImages={setListImages}
        slidesNm={100}
        slidesMd={100}
        slidesSm={100}
        slidesXs={100}
        innerRef={slider}
        showButton={false}
        slidesPerView={1}
        margin={0}
        loop={true}
        timePlay={10}
      >
        {listImages.map((item, index) => {
          return (
            <div className="posts_banner position_relative flex_none width_100_percent" key={index}>
              <img
                src={item.avatar.url}
                alt="discovery"
                className="display_block border_radius_24 width_100_percent object_fit_cover height_390 height_md_257 height_sm_191 height_xs_90"
              />
              <div className="position_absolute left_131 top_50_percent translate_0_-50_percent display_md_none">
                <span className="font_weight_700 font_size_56 line_height_42 color_white font_logo">Banner</span>
              </div>
            </div>
          );
        })}
      </Slider>
    </div>
  );
};

export default Banner;
