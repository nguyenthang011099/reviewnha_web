import React from 'react';
import classnames from 'classnames';
import AllTabList from './AllTabList';
import OtherTabList from './OtherTabList';
import { BlogCategory, BlogsAllData, BlogsCategoryData, VideoList } from '../../../../types/type.blog';
import Link from 'next/link';
import { useDispatch } from 'react-redux';
import { getBlogsByCategory, getVideos } from '../../../../reducers/blog/actions';
import VideoTabList from './VideoTabList';

interface Props {
  categories: BlogCategory[];
  blogsByCategory: BlogsCategoryData;
  blogsAll: BlogsAllData;
  activeCategory: string;
  videoList: VideoList;
}

const List: React.FC<Props> = (props) => {
  const { activeCategory } = props;
  const dispatch = useDispatch();

  /**
   * renderTab khi chọn tab thì render nội dung
   */
  const renderTab = () => {
    const dataOtherTab = {
      blogs: [],
      total: 0,
      currentOffset: 0,
      loading: false,
    };
    if (activeCategory !== 'all') {
      const blogsData = props.blogsByCategory[props.activeCategory];
      if (blogsData) {
        dataOtherTab.blogs = blogsData.blogs || [];
        dataOtherTab.total = blogsData.total || 0;
        dataOtherTab.currentOffset = blogsData.currentOffset || 0;
        dataOtherTab.loading = blogsData.loading || false;
      }
    }
    switch (activeCategory) {
      case 'all': {
        return <AllTabList blogsAll={props.blogsAll} videoList={props.videoList}></AllTabList>;
      }
      case 'video': {
        return <VideoTabList videoList={props.videoList} getDataNextPage={getVideoNextPage}></VideoTabList>;
      }
      default: {
        return (
          <OtherTabList
            loading={dataOtherTab.loading}
            category={props.activeCategory}
            currentOffset={dataOtherTab.currentOffset}
            blogs={dataOtherTab.blogs}
            total={dataOtherTab.total}
            getDataNextPage={getDataNextPageByCategory}
          ></OtherTabList>
        );
      }
    }
  };

  /**
   * getDataNextPageByCategory lấy blog của page tiếp theo
   */
  const getDataNextPageByCategory = (categorySlug: string, offset: number) => {
    dispatch(getBlogsByCategory(categorySlug, offset));
  };

  /**
   * getVideoNextPage: lấy video của page tiếp theo
   */
  const getVideoNextPage = (_slug: string, offset: number) => {
    dispatch(getVideos(offset));
  };

  return (
    <div className="list margin_top_36 padding_x_240 container_blog padding_x_md_0 margin_top_xs_16">
      <div className="margin_x_xs_22">
        <div className="tabs display_flex overflow_auto scroll_bar_width_3 padding_bottom_xs_2 padding_bottom_8 ">
          <div className="padding_right_16 flex_none padding_right_xs_6">
            <Link href={{ pathname: '/blogs-page', query: { categorySlug: 'all' } }} as="/blogs" scroll={false}>
              <a
                className={classnames(
                  {
                    'bg_green color_white': activeCategory === 'all',
                  },
                  'display_flex height_39 padding_x_36 border_radius_25 align_items_center cursor_pointer height_xs_33',
                )}
              >
                <span className="font_weight_600 font_size_18 line_height_27 font_size_xs_14 line_height_xs_21">
                  Tất cả
                </span>
              </a>
            </Link>
          </div>
          {props.categories?.map((category: BlogCategory) => {
            return (
              <div className="padding_right_16 flex_none padding_right_xs_6" key={category.id}>
                <Link
                  href={{ pathname: '/blogs-page', query: { categorySlug: category.slug } }}
                  as={`/blogs/${category.slug}`}
                  scroll={false}
                >
                  <a
                    className={classnames(
                      {
                        'bg_green color_white': activeCategory === category.slug,
                      },
                      'display_flex height_39 padding_x_36 border_radius_25 align_items_center cursor_pointer height_xs_33',
                    )}
                  >
                    <span className="font_weight_600 font_size_18 line_height_27 font_size_xs_14 line_height_xs_21">
                      {category.name}
                    </span>
                  </a>
                </Link>
              </div>
            );
          })}
          <div className="flex_none">
            <Link href={{ pathname: '/blogs-page', query: { categorySlug: 'video' } }} as="/blogs/video" scroll={false}>
              <a
                className={classnames(
                  {
                    'bg_green color_white': activeCategory === 'video',
                  },
                  'display_flex height_39 padding_x_36 border_radius_25 align_items_center cursor_pointer height_xs_33',
                )}
              >
                <span className="font_weight_600 font_size_18 line_height_27 font_size_xs_14 line_height_xs_21">
                  Video
                </span>
              </a>
            </Link>
          </div>
        </div>
      </div>
      <div className="margin_top_32 margin_top_xs_14">{renderTab()}</div>
    </div>
  );
};

export default List;
