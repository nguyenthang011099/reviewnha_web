import React from 'react';
import BlogItem from '../../../../components/blog-item/BlogItem';
import BlogItemLoading from '../../../../components/blog-item/BlogItemLoading';
import { NUMBER_BLOGS_PER_REQUEST } from '../../../../constants/blog';
import { Blog } from '../../../../types/type.blog';

interface Props {
  blogs: Blog[];
  total: number;
  category?: any;
  currentOffset: number;
  getDataNextPage: (category: any, page: number) => void;
  loading?: boolean;
}

const OtherTabList: React.FC<Props> = (props) => {
  return (
    <div className="margin_top_36 padding_x_xs_22">
      {!props.loading
        ? props.blogs.map((item: Blog) => {
            return (
              <div className="margin_top_36 margin_top_xs_24" key={item.id}>
                <BlogItem blog={item}></BlogItem>
              </div>
            );
          })
        : Array.from(Array(3).keys()).map((item: number) => {
            return (
              <div className="margin_top_36 margin_top_xs_24" key={item}>
                <BlogItemLoading></BlogItemLoading>
              </div>
            );
          })}
      {props.blogs.length < props.total ? (
        <div className="margin_top_56 display_flex justify_content_center ">
          <button
            className="font_size_16 line_height_19 font_weight_600 display_flex align_items_center justify_content_center 
                height_56 width_216 border_radius_24 border_color_black border_style_solid border_width_1 bg_white"
            onClick={() => {
              props.getDataNextPage(props.category, props.currentOffset + NUMBER_BLOGS_PER_REQUEST);
            }}
          >
            Xem thêm
          </button>
        </div>
      ) : null}
    </div>
  );
};

export default OtherTabList;
