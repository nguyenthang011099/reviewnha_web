import Link from 'next/link';
import React from 'react';
import { useDispatch } from 'react-redux';
import BlogItem from '../../../../components/blog-item/BlogItem';
import { NUMBER_BLOGS_PER_REQUEST } from '../../../../constants/blog';
import { getBlogsByType } from '../../../../reducers/blog/actions';
import { Blog, BlogsAllData, VideoList } from '../../../../types/type.blog';
import { formatDateBlog } from '../../../../utils/format-date-blog';
import VideoBlock from '../item/VideoBlock';

interface Props {
  blogsAll: BlogsAllData;
  videoList: VideoList;
}

const AllTabListLoading: React.FC = () => {
  return (
    <div className="content display_flex margin_top_40 flex_direction_sm_col margin_top_xs_16">
      <div className="left flex_1">
        <div className="width_100_percent display_block">
          <div className="border_radius_24 width_100_percent height_455 object_fit_cover height_xs_216 loading_placeholder"></div>
          <div className="margin_top_16 height_27 border_radius_5 width_100_percent  loading_placeholder"></div>
          <div className="margin_top_6 display_flex align_items_center height_22 width_100 border_radius_5 loading_placeholder"></div>
          <div className="margin_top_6 width_100_percent height_62 border_radius_5 loading_placeholder"></div>
        </div>
      </div>
      <div className="right margin_left_24 display_sm_flex margin_x_sm_-12 width_sm_100_percent margin_top_sm_36 display_xs_block margin_x_xs_0 margin_top_xs_24 overflow_hidden">
        <div className="width_336 margin_bottom_16 display_block padding_x_sm_12 col_sm_6 width_sm_auto width_xs_100_percent padding_x_xs_0 margin_bottom_xs_24">
          <div className="display_block border_radius_24 width_336 height_220 width_xs_100_percent height_xs_216 loading_placeholder"></div>
          <div className="height_27 margin_top_16 width_100_percent border_radius_5 loading_placeholder"></div>
          <div className="margin_top_4 height_22 width_100_percent border_radius_5 loading_placeholder"></div>
        </div>
        <div className="width_336 margin_bottom_16 display_block padding_x_sm_12 col_sm_6 width_sm_auto width_xs_100_percent padding_x_xs_0 margin_bottom_xs_24">
          <div className="display_block border_radius_24 width_336 height_220 width_xs_100_percent height_xs_216 loading_placeholder"></div>
          <div className="height_27 margin_top_16 width_100_percent border_radius_5 loading_placeholder"></div>
          <div className="margin_top_4 height_22 width_100_percent border_radius_5 loading_placeholder"></div>
        </div>
      </div>
    </div>
  );
};

const AllTabList: React.FC<Props> = (props) => {
  const highlightBlogs = props?.blogsAll?.highlightBlogs;
  const normalBlogs = props?.blogsAll?.normalBlogs;
  const dispatch = useDispatch();

  /**
   * getNormalBlogsNextPage láy page sau của normal blogs
   */
  const getNormalBlogsNextPage = () => {
    dispatch(getBlogsByType('normal', normalBlogs.currentOffset + NUMBER_BLOGS_PER_REQUEST));
  };

  return (
    <div className="tab_all padding_x_xs_22">
      <div className="news">
        <div className="display_flex align_items_center justify_content_center">
          <div className="title display_flex align_items_center justify_content_center ">
            <svg
              className="width_xs_18 height_xs_20 width_36 height_40"
              viewBox="0 0 36 40"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M26.382 0C32.56 0 36 3.56 36 9.66V30.32C36 36.52 32.56 40 26.382 40H9.62C3.54 40 0 36.52 0 30.32V9.66C0 3.56 3.54 0 9.62 0H26.382ZM10.16 27.48C9.56 27.42 8.98 27.7 8.66 28.22C8.34 28.72 8.34 29.38 8.66 29.9C8.98 30.4 9.56 30.7 10.16 30.62H25.84C26.638 30.54 27.24 29.858 27.24 29.06C27.24 28.24 26.638 27.56 25.84 27.48H10.16ZM25.84 18.358H10.16C9.298 18.358 8.6 19.06 8.6 19.92C8.6 20.78 9.298 21.48 10.16 21.48H25.84C26.7 21.48 27.4 20.78 27.4 19.92C27.4 19.06 26.7 18.358 25.84 18.358ZM16.138 9.3H10.16V9.32C9.298 9.32 8.6 10.02 8.6 10.88C8.6 11.74 9.298 12.44 10.16 12.44H16.138C17 12.44 17.7 11.74 17.7 10.858C17.7 10 17 9.3 16.138 9.3Z"
                fill="#007882"
              />
            </svg>
            <span className="font_weight_700 line_height_42 font_size_32 margin_left_30 font_size_xs_18 line_height_xs_27 margin_left_xs_11">
              Tin nổi bật
            </span>
          </div>
        </div>
        {!highlightBlogs.loading ? (
          <div className="content display_flex margin_top_40 flex_direction_sm_col margin_top_xs_16">
            <div className="left flex_1">
              {highlightBlogs?.blogs?.length ? (
                <Link
                  href={{ pathname: '/blog-detail-page', query: { slug: highlightBlogs?.blogs[0]?.slug } }}
                  as={`/blogs/${highlightBlogs?.blogs[0]?.category?.slug}/${highlightBlogs?.blogs[0]?.slug}`}
                >
                  <a className="width_100_percent display_block">
                    <div className="position_relative">
                      <img
                        src={highlightBlogs.blogs[0].thumbnail}
                        alt={highlightBlogs.blogs[0].title}
                        className="border_radius_24 width_100_percent height_455 object_fit_cover height_xs_216"
                      />
                      <svg
                        width={24}
                        height={24}
                        viewBox="0 0 24 24"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                        className="position_absolute right_26 top_26 right_xs_18 top_xs_18"
                      >
                        <path
                          d="M8.27977 2.50015C8.90977 2.51931 9.51977 2.62931 10.1108 2.83031H10.1698C10.2098 2.84931 10.2398 2.87031 10.2598 2.88931C10.4808 2.96031 10.6898 3.04031 10.8898 3.15031L11.2698 3.32031C11.4198 3.40031 11.5998 3.54931 11.6998 3.61031C11.7998 3.66931 11.9098 3.73031 11.9998 3.79931C13.1108 2.95031 14.4598 2.49031 15.8498 2.50015C16.4808 2.50015 17.1108 2.58931 17.7098 2.79031C21.4008 3.99031 22.7308 8.04031 21.6198 11.5803C20.9898 13.3893 19.9598 15.0403 18.6108 16.3893C16.6798 18.2593 14.5608 19.9193 12.2798 21.3493L12.0298 21.5003L11.7698 21.3393C9.48077 19.9193 7.34977 18.2593 5.40077 16.3793C4.06077 15.0303 3.02977 13.3893 2.38977 11.5803C1.25977 8.04031 2.58977 3.99031 6.32077 2.76931C6.61077 2.66931 6.90977 2.59931 7.20977 2.56031H7.32977C7.61077 2.51931 7.88977 2.50015 8.16977 2.50015H8.27977ZM17.1898 5.66031C16.7798 5.51931 16.3298 5.74031 16.1798 6.16031C16.0398 6.58031 16.2598 7.04031 16.6798 7.18931C17.3208 7.42931 17.7498 8.06031 17.7498 8.75931V8.79031C17.7308 9.01931 17.7998 9.24031 17.9398 9.41031C18.0798 9.58031 18.2898 9.67931 18.5098 9.70031C18.9198 9.68931 19.2698 9.36031 19.2998 8.93931V8.82031C19.3298 7.41931 18.4808 6.15031 17.1898 5.66031Z"
                          fill="white"
                        />
                      </svg>
                    </div>
                    <h2
                      className="margin_y_0 font_size_18 line_height_27 font_weight_600 margin_top_16 text_over_flow_2 font_size_xs_14 line_height_xs_21
                      margin_top_xs_8"
                    >
                      {highlightBlogs.blogs[0].title}
                    </h2>
                    <div className="margin_top_6 display_flex align_items_center margin_top_xs_4">
                      {highlightBlogs.blogs[0]?.category ? (
                        <>
                          <span className="font_weight_600 font_size_14 line_height_22 color_green font_size_xs_13 line_height_xs_15">
                            {highlightBlogs.blogs[0]?.category?.name}
                          </span>
                          <svg
                            width="3"
                            height="3"
                            viewBox="0 0 3 3"
                            fill="none"
                            xmlns="http://www.w3.org/2000/svg"
                            className="margin_x_8"
                          >
                            <circle cx="1.5" cy="1.5" r="1.5" fill="#3B4144" />
                          </svg>
                        </>
                      ) : null}
                      <span className="font_size_15 line_height_22 font_size_xs_13 line_height_xs_15">
                        {highlightBlogs.blogs[0].authorName}
                      </span>
                      <svg
                        width="3"
                        height="3"
                        viewBox="0 0 3 3"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                        className="margin_x_8"
                      >
                        <circle cx="1.5" cy="1.5" r="1.5" fill="#3B4144" />
                      </svg>
                      <span className="font_size_15 line_height_22 opacity_0_5 font_size_xs_13 line_height_xs_15">
                        {formatDateBlog(highlightBlogs.blogs[0].publishedAt)}
                      </span>
                    </div>
                    <div className="margin_top_6 margin_top_xs_4">
                      <span className="font_size_16 line_height_28 opacity_0_5 text_over_flow_3 font_size_xs_14 line_height_xs_21">
                        {highlightBlogs.blogs[0].description}
                      </span>
                    </div>
                  </a>
                </Link>
              ) : null}
            </div>
            <div className="right margin_left_24 display_sm_flex margin_x_sm_-12 width_sm_100_percent margin_top_sm_36 display_xs_block margin_x_xs_0 margin_top_xs_24">
              {highlightBlogs.blogs.map((item: Blog, index: number) => {
                if (index !== 0 && index < 3) {
                  return (
                    <Link
                      href={{ pathname: '/blog-detail-page', query: { slug: item.slug } }}
                      as={`/blogs/${item?.category?.slug || 'none-category'}/${item.slug}`}
                      key={item.id}
                    >
                      <a className="width_336 margin_bottom_16 display_block padding_x_sm_12 col_sm_6 width_sm_auto width_xs_100_percent padding_x_xs_0 margin_bottom_xs_24">
                        <div className="position_relative">
                          <img
                            className="display_block border_radius_24 width_336 height_220 width_xs_100_percent height_xs_216"
                            src={item.thumbnail}
                            alt={item.title}
                          />
                          <svg
                            width={24}
                            height={24}
                            viewBox="0 0 24 24"
                            fill="none"
                            xmlns="http://www.w3.org/2000/svg"
                            className="position_absolute right_26 top_26 right_xs_18 top_xs_18"
                          >
                            <path
                              d="M8.27977 2.50015C8.90977 2.51931 9.51977 2.62931 10.1108 2.83031H10.1698C10.2098 2.84931 10.2398 2.87031 10.2598 2.88931C10.4808 2.96031 10.6898 3.04031 10.8898 3.15031L11.2698 3.32031C11.4198 3.40031 11.5998 3.54931 11.6998 3.61031C11.7998 3.66931 11.9098 3.73031 11.9998 3.79931C13.1108 2.95031 14.4598 2.49031 15.8498 2.50015C16.4808 2.50015 17.1108 2.58931 17.7098 2.79031C21.4008 3.99031 22.7308 8.04031 21.6198 11.5803C20.9898 13.3893 19.9598 15.0403 18.6108 16.3893C16.6798 18.2593 14.5608 19.9193 12.2798 21.3493L12.0298 21.5003L11.7698 21.3393C9.48077 19.9193 7.34977 18.2593 5.40077 16.3793C4.06077 15.0303 3.02977 13.3893 2.38977 11.5803C1.25977 8.04031 2.58977 3.99031 6.32077 2.76931C6.61077 2.66931 6.90977 2.59931 7.20977 2.56031H7.32977C7.61077 2.51931 7.88977 2.50015 8.16977 2.50015H8.27977ZM17.1898 5.66031C16.7798 5.51931 16.3298 5.74031 16.1798 6.16031C16.0398 6.58031 16.2598 7.04031 16.6798 7.18931C17.3208 7.42931 17.7498 8.06031 17.7498 8.75931V8.79031C17.7308 9.01931 17.7998 9.24031 17.9398 9.41031C18.0798 9.58031 18.2898 9.67931 18.5098 9.70031C18.9198 9.68931 19.2698 9.36031 19.2998 8.93931V8.82031C19.3298 7.41931 18.4808 6.15031 17.1898 5.66031Z"
                              fill="white"
                            />
                          </svg>
                        </div>
                        <h2
                          className="margin_y_0 font_size_18 line_height_27 font_weight_600 margin_top_16  text_over_flow_2 font_size_xs_14 line_height_xs_21
                      margin_top_xs_8"
                        >
                          {item.title}
                        </h2>
                        <div className="margin_top_4 display_flex align_items_center">
                          {item.category ? (
                            <>
                              <span className="font_weight_600 font_size_14 line_height_22 color_green white_space_nowrap font_size_xs_13 line_height_xs_15">
                                {item.category.name}
                              </span>
                              <svg
                                width="3"
                                height="3"
                                viewBox="0 0 3 3"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                                className="margin_x_8"
                              >
                                <circle cx="1.5" cy="1.5" r="1.5" fill="#3B4144" />
                              </svg>
                            </>
                          ) : null}
                          <span
                            className="font_size_15 line_height_22 white_space_nowrap font_size_xs_13 line_height_xs_15"
                            title={item.authorName}
                          >
                            {item.authorName.length > 15 ? item.authorName.substr(0, 15) + '...' : item.authorName}
                          </span>
                          <svg
                            width="3"
                            height="3"
                            viewBox="0 0 3 3"
                            fill="none"
                            xmlns="http://www.w3.org/2000/svg"
                            className="margin_x_8"
                          >
                            <circle cx="1.5" cy="1.5" r="1.5" fill="#3B4144" />
                          </svg>
                          <span className="font_size_15 line_height_22 opacity_0_5 white_space_nowrap font_size_15">
                            {formatDateBlog(item.publishedAt)}
                          </span>
                        </div>
                        <div className="margin_top_6 margin_top_xs_4 display_none display_xs_block">
                          <span className="font_size_16 line_height_28 opacity_0_5 text_over_flow_3 font_size_xs_14 line_height_xs_21">
                            {highlightBlogs.blogs[0].description}
                          </span>
                        </div>
                      </a>
                    </Link>
                  );
                }
              })}
            </div>
          </div>
        ) : (
          <AllTabListLoading></AllTabListLoading>
        )}
      </div>
      <VideoBlock videoList={props.videoList}></VideoBlock>
      <div className="blogs margin_top_56 margin_top_xs_44">
        <div className="title">
          <span className="font_size_18 line_height_27 font_weight_600 font_size_xs_14 line_height_xs_21">
            Danh sách tin
          </span>
        </div>
        <div className="contents margin_top_36 margin_top_xs_16">
          {normalBlogs.blogs.map((item: Blog) => {
            return (
              <div className="margin_bottom_36 margin_bottom_xs_24" key={item.id}>
                <BlogItem blog={item}></BlogItem>
              </div>
            );
          })}
          {normalBlogs.blogs.length !== normalBlogs.total ? (
            <div className="margin_top_56 display_flex justify_content_center ">
              <button
                className="font_size_16 line_height_19 font_weight_600 display_flex align_items_center justify_content_center 
                height_56 width_216 border_radius_24 border_color_black border_style_solid border_width_1 bg_white"
                onClick={() => {
                  getNormalBlogsNextPage();
                }}
              >
                Xem thêm
              </button>
            </div>
          ) : null}
        </div>
      </div>
    </div>
  );
};

export default AllTabList;
