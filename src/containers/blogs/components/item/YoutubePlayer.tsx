import React, { useEffect } from 'react';
import { getYoutubeId } from '../../../../utils/youtube';

interface Props {
  url: string;
  style?: any;
}

const YoutubePlayer: React.FC<Props> = (props) => {
  let player: any;

  useEffect(() => {
    if (!(window as any).YT) {
      const tag = document.createElement('script');
      tag.src = 'https://www.youtube.com/iframe_api';
      (window as any).onYouTubeIframeAPIReady = loadVideo;
      const firstScriptTag = document.getElementsByTagName('script')[0];
      firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
    } else {
      loadVideo();
    }
  }, []);

  const loadVideo = () => {
    const videoId = getYoutubeId(props.url);
    player = new (window as any).YT.Player(`youtube-player-${videoId}`, {
      videoId: videoId,
      events: {
        onReady: () => {
          player.playVideo();
        },
      },
    });
  };
  return (
    <div
      id={`youtube-player-${getYoutubeId(props.url)}`}
      className="width_100_percent height_100_percent border_radius_24 position_relative"
      style={{ ...props.style }}
    />
  );
};

export default YoutubePlayer;
