import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../../../reducers';
import { setCachedVideoCount } from '../../../../reducers/blog/actions';
import { VideoCount } from '../../../../types/type.blog';
import { getYoutubeId } from '../../../../utils/youtube';
import axios from 'axios';
import { numberWithDot } from '../../../../utils/format-number/index';

interface Props {
  videoUrl: string;
}

const YoutubeCount: React.FC<Props> = (props) => {
  const videoCountCached: VideoCount[] = useSelector((state: RootState) => state.blog.videoCountCached);
  const dispatch = useDispatch();
  const [count, setCount] = useState<number>(0);

  useEffect(() => {
    if (props.videoUrl) {
      const videoId = getYoutubeId(props.videoUrl);
      const existedVideo = videoCountCached.find((item: VideoCount) => item.id === videoId);
      if (!existedVideo) {
        getYoutubeViewCount(videoId);
      } else {
        setCount(existedVideo.count);
      }
    }
  }, [props.videoUrl]);

  const getYoutubeViewCount = (videoId: string) => {
    videoId &&
      axios
        .get(
          `https://www.googleapis.com/youtube/v3/videos?part=statistics&id=${videoId}&key=AIzaSyBQeup5_p075oz110LRqGDaFmpB8B-bjig`,
        )
        .then((res: any) => {
          const items = res?.data?.items;
          if (items && items.length) {
            const data = items[0];
            const viewCount = data?.statistics?.viewCount as number;
            setCount(viewCount);
            dispatch(
              setCachedVideoCount({
                id: videoId,
                count: viewCount,
              }),
            );
          }
        });
  };

  return <>{numberWithDot(count)}</>;
};

export default YoutubeCount;
