import Link from 'next/link';
import React, { useEffect, useState } from 'react';
import { VideoBlog, VideoList } from '../../../../types/type.blog';
import classnames from 'classnames';
import { renderYoutubeThumnail } from '../../../../utils/youtube';
import YoutubePlayer from './YoutubePlayer';
import YoutubeCount from './YoutubeCount';
import { formatDateBlog } from '../../../../utils/format-date-blog';

interface Props {
  videoList: VideoList;
}

const VideoBlock: React.FC<Props> = (props) => {
  const [activeVideo, setActiveVideo] = useState<VideoBlog>({} as VideoBlog);
  const [isPlaying, setIsPlaying] = useState<boolean>(false);

  useEffect(() => {
    const videos = props.videoList?.videos;
    if (videos?.length) {
      setActiveVideo(videos[0]);
    }
  }, [props.videoList]);

  const playVideo = () => {
    setIsPlaying(true);
  };

  const stopVideo = () => {
    setIsPlaying(false);
  };

  return (
    <div className="video margin_top_67 margin_top_xs_28">
      <div className="title display_flex align_items_center justify_content_sb">
        <span className="font_weight_600 font_size_18 line_height_27 cursor_pointer font_size_xs_14 line_height_xs_21">
          Video
        </span>
        <Link href={{ pathname: '/blogs-page', query: { categorySlug: 'video' } }} as="/blogs/video">
          <a>
            <span className="line_height_22 font_size_15 cursor_pointer display_xs_none">Xem tất cả</span>
            <span className="cursor_pointer display_xs_block padding_left_15">
              <svg width="8" height="12" viewBox="0 0 8 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path
                  d="M1.6665 1.21549L6.33317 5.88216L1.6665 10.5488"
                  stroke="#3B4144"
                  strokeWidth="1.5"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                />
              </svg>
            </span>
          </a>
        </Link>
      </div>
      <div className="content display_flex margin_top_40 margin_right_-19 flex_direction_sm_col margin_top_xs_16 width_xs_100_percent">
        <div className="left flex_1">
          {isPlaying ? (
            <div className="width_100_percent height_391 loading_placeholder border_radius_24 height_xs_186">
              <YoutubePlayer url={activeVideo.url}></YoutubePlayer>
            </div>
          ) : null}
          <div
            className={classnames(
              {
                display_none: isPlaying,
              },
              'width_100_percent position_relative cursor_pointer',
            )}
            onClick={playVideo}
          >
            <div
              className="width_96 height_96 border_radius_50_percent display_flex align_items_center justify_content_center position_absolute
                  left_50_percent top_50_percent translate_-50_-50_percent z_index_1 no_select width_xs_48 height_xs_48 top_xs_69 translate_xs_-50_0_percent"
              style={{
                background: 'linear-gradient(93.91deg, rgba(255, 255, 255, 0.5) 0%, rgba(255, 255, 255, 0.25) 100%)',
                backdropFilter: 'blur(75px)',
              }}
            >
              <svg
                className="width_34 height_41 width_xs_16 height_xs_20"
                viewBox="0 0 34 41"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M32.4363 16.98C32.2479 16.7867 31.5368 15.96 30.8743 15.28C26.9906 11.0033 16.8591 4.00333 11.5563 1.86667C10.7509 1.52333 8.71489 0.796667 7.62705 0.75C6.58467 0.75 5.59101 0.99 4.6428 1.47667C3.46079 2.15667 2.51259 3.22667 1.99302 4.49C1.65855 5.36667 1.13899 7.99 1.13899 8.03667C0.619423 10.9067 0.33366 15.57 0.33366 20.7233C0.33366 25.6333 0.619419 30.1067 1.04482 33.02C1.09352 33.0667 1.61309 36.3267 2.18136 37.4433C3.22374 39.4833 5.25978 40.75 7.43871 40.75H7.62705C9.04611 40.7 12.0304 39.4333 12.0304 39.3867C17.0474 37.2467 26.9451 30.59 30.923 26.1667C30.923 26.1667 32.0434 25.03 32.5304 24.32C33.2903 23.3 33.667 22.0367 33.667 20.7733C33.667 19.3633 33.2416 18.05 32.4363 16.98Z"
                  fill="white"
                />
              </svg>
            </div>
            <img
              src={renderYoutubeThumnail(activeVideo.url, 'high')}
              className="width_100_percent height_391 object_fit_cover border_radius_24 height_xs_186"
              alt={activeVideo.title}
            />
            <svg
              width={24}
              height={24}
              viewBox="0 0 24 24"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
              className="position_absolute right_26 top_26 right_xs_18 top_xs_18"
            >
              <path
                d="M8.27977 2.50015C8.90977 2.51931 9.51977 2.62931 10.1108 2.83031H10.1698C10.2098 2.84931 10.2398 2.87031 10.2598 2.88931C10.4808 2.96031 10.6898 3.04031 10.8898 3.15031L11.2698 3.32031C11.4198 3.40031 11.5998 3.54931 11.6998 3.61031C11.7998 3.66931 11.9098 3.73031 11.9998 3.79931C13.1108 2.95031 14.4598 2.49031 15.8498 2.50015C16.4808 2.50015 17.1108 2.58931 17.7098 2.79031C21.4008 3.99031 22.7308 8.04031 21.6198 11.5803C20.9898 13.3893 19.9598 15.0403 18.6108 16.3893C16.6798 18.2593 14.5608 19.9193 12.2798 21.3493L12.0298 21.5003L11.7698 21.3393C9.48077 19.9193 7.34977 18.2593 5.40077 16.3793C4.06077 15.0303 3.02977 13.3893 2.38977 11.5803C1.25977 8.04031 2.58977 3.99031 6.32077 2.76931C6.61077 2.66931 6.90977 2.59931 7.20977 2.56031H7.32977C7.61077 2.51931 7.88977 2.50015 8.16977 2.50015H8.27977ZM17.1898 5.66031C16.7798 5.51931 16.3298 5.74031 16.1798 6.16031C16.0398 6.58031 16.2598 7.04031 16.6798 7.18931C17.3208 7.42931 17.7498 8.06031 17.7498 8.75931V8.79031C17.7308 9.01931 17.7998 9.24031 17.9398 9.41031C18.0798 9.58031 18.2898 9.67931 18.5098 9.70031C18.9198 9.68931 19.2698 9.36031 19.2998 8.93931V8.82031C19.3298 7.41931 18.4808 6.15031 17.1898 5.66031Z"
                fill="white"
              />
            </svg>
            <div
              className="position_absolute bottom_0 left_0 color_white padding_x_36 padding_bottom_36 padding_top_117 width_100_percent border_bottom_left_radius_24 
              border_bottom_right_radius_24 display_xs_none"
              style={{ background: 'linear-gradient(0deg, #000000 0%, rgba(0, 0, 0, 0) 100%)', pointerEvents: 'none' }}
            >
              <h2 className="margin_y_0 font_size_18 line_height_27 font_weight_600">{activeVideo.title}</h2>
              <div className="margin_top_4 display_flex align_items_center">
                <span className="font_size_15 line_height_22 margin_right_8">
                  <YoutubeCount videoUrl={activeVideo.url}></YoutubeCount> lượt xem
                </span>
                <svg width="3" height="3" viewBox="0 0 3 3" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <circle cx="1.5" cy="1.5" r="1.5" fill="#ffffff" />
                </svg>
                <span className="font_size_15 line_height_22 opacity_0_5 margin_x_8">
                  {formatDateBlog(activeVideo.publishedAt)}
                </span>
              </div>
            </div>
          </div>
        </div>
        <div className="width_100_percent display_none display_xs_block">
          <h2 className="margin_y_0 font_size_14 line_height_21 margin_top_8 font_weight_600">{activeVideo.title}</h2>
          <div className="margin_top_4 display_flex align_items_center">
            <span className="font_size_12 line_height_18 margin_right_8">
              <YoutubeCount videoUrl={activeVideo.url}></YoutubeCount> lượt xem
            </span>
            <svg width="3" height="3" viewBox="0 0 3 3" fill="none" xmlns="http://www.w3.org/2000/svg">
              <circle cx="1.5" cy="1.5" r="1.5" fill="#3B4144" />
            </svg>
            <span className="font_size_12 line_height_18 opacity_0_5 margin_x_8">
              {formatDateBlog(activeVideo.publishedAt)}
            </span>
          </div>
        </div>
        <div
          className="right margin_left_24 max_height_395 overflow_auto scroll_bar_width_3 padding_right_16 display_sm_flex 
          margin_x_sm_-12 width_sm_100_percent margin_top_sm_36 margin_top_xs_16"
        >
          {props.videoList.videos.map((item: VideoBlog, index: number) => {
            if (index > 0 && index < 11) {
              return (
                <div className="padding_x_sm_12" key={item.id}>
                  <div
                    className="width_216 margin_bottom_16 cursor_pointer"
                    onClick={() => {
                      stopVideo();
                      setActiveVideo(item);
                    }}
                  >
                    <div className="position_relative">
                      <div
                        className="width_48 height_48 border_radius_50_percent display_flex align_items_center justify_content_center position_absolute
                  left_50_percent top_50_percent translate_-50_-50_percent z_index_1"
                        style={{
                          background:
                            'linear-gradient(93.91deg, rgba(255, 255, 255, 0.5) 0%, rgba(255, 255, 255, 0.25) 100%)',
                          backdropFilter: 'blur(75px)',
                        }}
                      >
                        <svg width="18" height="20" viewBox="0 0 18 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <path
                            d="M16.7176 8.115C16.6235 8.01833 16.2679 7.605 15.9367 7.265C13.9948 5.12667 8.92905 1.62667 6.27764 0.558333C5.87498 0.386667 4.85696 0.0233333 4.31304 0C3.79185 0 3.29502 0.12 2.82091 0.363333C2.22991 0.703333 1.75581 1.23833 1.49602 1.87C1.32879 2.30833 1.06901 3.62 1.06901 3.64333C0.809223 5.07833 0.666342 7.41 0.666342 9.98667C0.666342 12.4417 0.809221 14.6783 1.02192 16.135C1.04627 16.1583 1.30606 17.7883 1.59019 18.3467C2.11138 19.3667 3.1294 20 4.21887 20H4.31304C5.02257 19.975 6.51469 19.3417 6.51469 19.3183C9.02322 18.2483 13.9721 14.92 15.961 12.7083C15.961 12.7083 16.5212 12.14 16.7647 11.785C17.1447 11.275 17.333 10.6433 17.333 10.0117C17.333 9.30667 17.1203 8.65 16.7176 8.115Z"
                            fill="white"
                          />
                        </svg>
                      </div>
                      <svg
                        width={24}
                        height={24}
                        viewBox="0 0 24 24"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                        className="position_absolute right_18 top_18"
                      >
                        <path
                          d="M8.27977 2.50015C8.90977 2.51931 9.51977 2.62931 10.1108 2.83031H10.1698C10.2098 2.84931 10.2398 2.87031 10.2598 2.88931C10.4808 2.96031 10.6898 3.04031 10.8898 3.15031L11.2698 3.32031C11.4198 3.40031 11.5998 3.54931 11.6998 3.61031C11.7998 3.66931 11.9098 3.73031 11.9998 3.79931C13.1108 2.95031 14.4598 2.49031 15.8498 2.50015C16.4808 2.50015 17.1108 2.58931 17.7098 2.79031C21.4008 3.99031 22.7308 8.04031 21.6198 11.5803C20.9898 13.3893 19.9598 15.0403 18.6108 16.3893C16.6798 18.2593 14.5608 19.9193 12.2798 21.3493L12.0298 21.5003L11.7698 21.3393C9.48077 19.9193 7.34977 18.2593 5.40077 16.3793C4.06077 15.0303 3.02977 13.3893 2.38977 11.5803C1.25977 8.04031 2.58977 3.99031 6.32077 2.76931C6.61077 2.66931 6.90977 2.59931 7.20977 2.56031H7.32977C7.61077 2.51931 7.88977 2.50015 8.16977 2.50015H8.27977ZM17.1898 5.66031C16.7798 5.51931 16.3298 5.74031 16.1798 6.16031C16.0398 6.58031 16.2598 7.04031 16.6798 7.18931C17.3208 7.42931 17.7498 8.06031 17.7498 8.75931V8.79031C17.7308 9.01931 17.7998 9.24031 17.9398 9.41031C18.0798 9.58031 18.2898 9.67931 18.5098 9.70031C18.9198 9.68931 19.2698 9.36031 19.2998 8.93931V8.82031C19.3298 7.41931 18.4808 6.15031 17.1898 5.66031Z"
                          fill="white"
                        />
                      </svg>
                      <img
                        className="display_block border_radius_24 width_216 height_121 object_fit_cover"
                        src={renderYoutubeThumnail(item.url, 'low')}
                        alt={item.title}
                      />
                    </div>
                    <h2 className="margin_y_0 font_size_14 line_height_17 font_weight_600 margin_top_8 text_over_flow_2">
                      {item.title}
                    </h2>
                    <div className="margin_top_4 display_flex align_items_center">
                      <span className="font_size_12 line_height_18 margin_right_8">
                        <YoutubeCount videoUrl={item.url}></YoutubeCount> lượt xem
                      </span>
                      <svg width="3" height="3" viewBox="0 0 3 3" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <circle cx="1.5" cy="1.5" r="1.5" fill="#3B4144" />
                      </svg>
                      <span className="font_size_12 line_height_18 opacity_0_5 margin_x_8">
                        {formatDateBlog(item.publishedAt)}
                      </span>
                    </div>
                  </div>
                </div>
              );
            }
            return null;
          })}
        </div>
      </div>
    </div>
  );
};

export default VideoBlock;
