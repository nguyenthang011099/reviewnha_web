import React, { useEffect, useState } from 'react';
import Popup from '../../components/popup/Popup';
import BasicInfo from './components/BasicInfo';
import HeaderPopup from './components/HeaderPopup';
import classnames from 'classnames';
import DetailInfo from './components/DetailInfo';
import DescriptionProduct from './components/DescriptionProduct';
import ContactInfo from './components/ContactInfo';
import { useDispatch, useSelector } from 'react-redux';
import {
  getExperiencesByProject,
  getSellListingPartOne,
  getSellListingPartTwo,
  postSellListing,
  resetStatusPostSellListing,
  updateSellListing,
} from '../../reducers/sell-listing/actions';
import { OptionInputRadio } from '../../types/type.base';
import { PostBodySellListing, Project, SellListing } from '../../types/type.sell-listing';
import { Status } from '../../types/type.status';
import { RootState } from '../../reducers';
import { openNotification } from '../../reducers/notification/actions';
import { uploadImage } from '../../api/uploadImage';
import { numberWithDot } from '../../utils/format-number';

interface Props {
  innerRef?: any;
  editMode?: boolean;
  sellListing?: SellListing;
}

/* Default position Ha noi */
const defaultPosition = {
  lat: 21.028021768231802,
  lng: 105.83358650207519,
};

const SellCreatePopup: React.FC<Props> = (props) => {
  const [stage, setStage] = useState<number>(1);
  const [isOpen, setIsOpen] = useState<boolean>(false);
  const dispatch = useDispatch();
  // all data step
  // step 1
  const [province, setProvince] = useState<OptionInputRadio>({ value: '', title: '' });
  const [district, setDistrict] = useState<OptionInputRadio>({ value: '', title: '' });
  const [ward, setWard] = useState<OptionInputRadio>({ value: '', title: '' });
  const [transaction, setTransaction] = useState<OptionInputRadio>({ value: '', title: '' });
  const [productType, setProductType] = useState<OptionInputRadio>({ value: '', title: '' });
  const [project, setProject] = useState<OptionInputRadio>({ value: '', title: '' });
  const [address, setAddress] = useState<string>('');
  // step 2
  const [type, setType] = useState<OptionInputRadio>({ title: '', value: '' });
  const [area, setArea] = useState<number | string>('');
  const [direction, setDirection] = useState<OptionInputRadio>({ title: '', value: '' });
  const [furniture, setFurniture] = useState<OptionInputRadio>({ title: '', value: '' });
  const [juridical, setJuridical] = useState<OptionInputRadio>({ title: '', value: '' });
  const [numberOfBedroom, setNumberOfBedroom] = useState<number | string>('');
  const [numberOfBathroom, setNumberOfBathroom] = useState<number | string>('');
  const [price, setPrice] = useState<number | string>('');
  // step 3
  const [title, setTitle] = useState<string>('');
  const [desc, setDesc] = useState<string>('');
  const [images, setImages] = useState<{ id: number; url: string }[]>([]);
  const [videoUrl, setVideoUrl] = useState<string>('');
  const [experiences, setExperiences] = useState<OptionInputRadio[]>([]);
  // step 4
  const [fullname, setFullname] = useState<string>('');
  const [phone, setPhone] = useState<string>('');
  const [email, setEmail] = useState<string>('');
  const [service, setService] = useState<string>('');
  // store
  const statusPost: Status = useSelector((state: RootState) => state.sellListing.statusPost as Status);
  const messagePostFail: string = useSelector((state: RootState) => state.sellListing.messagePostFail);
  // pick map
  const [location, setLocation] = useState<{ lat: number; lng: number }>(defaultPosition);
  const dataType = useSelector((state: RootState) => state.sellListing.stepOneData);
  // header choose step
  const [stepHeaderChoose, setStepHeaderChoose] = useState<number | null>(null);
  const projectList = useSelector((state: RootState) => state.sellListing.projectList);

  useEffect(() => {
    if (!dataType.productTypes?.length || !dataType.transactionTypes?.length) {
      dispatch(getSellListingPartOne());
    }
  }, []);

  /**
   * lắng nghe khi tao
   */
  useEffect(() => {
    if (statusPost === 'error') {
      dispatch(
        openNotification({
          type: 'error',
          title: messagePostFail,
          isOpen: true,
          reset: () => {
            dispatch(resetStatusPostSellListing());
          },
        }),
      );
    } else if (statusPost === 'success') {
      if (!props.editMode) {
        resetState();
      } else {
        setStage(1);
      }
      setIsOpen(false);
      dispatch(resetStatusPostSellListing());
    }
  }, [statusPost]);

  useEffect(() => {
    if (props.editMode) {
      updateState(props.sellListing);
    }
  }, [props.sellListing?.id]);

  /**
   * chooseStage chuyển stage
   * @param stageNumber statge
   */
  const chooseStage = (stageNumber: number) => {
    setStage(stageNumber);
  };

  /**
   * renderTitle: render title
   */
  const renderTitle = (): string => {
    switch (stage) {
      case 1:
        return 'Bước 1: Thông tin cơ bản';
      case 2:
        return 'Bước 2: Thông tin chi tiết';
      case 3:
        return 'Bước 3: Mô tả sản phẩm';
      default:
        return 'Bước 4: Thông tin liên hệ / Gói đăng';
    }
  };

  /**
   * createSellListing: post to create sell listing
   */
  const createSellListing = () => {
    const data: PostBodySellListing = {
      address: address,
      area: Number(area),
      contact_email: email,
      contact_name: fullname,
      contact_phone: phone,
      description: desc,
      direction: direction.value,
      district_code: district.value,
      images: images.map((image) => image.id),
      juridical: juridical.value,
      province_code: province.value,
      service_code: service,
      title: title,
      type: type.value,
      type_product: productType.value,
      type_transaction: transaction.value,
      video_url: videoUrl,
      ward_code: ward.value,
      bathroom: Number(numberOfBathroom ? numberOfBathroom : 0),
      bedroom: Number(numberOfBedroom ? numberOfBedroom : 0),
      furniture: furniture.value,
      type_project: project.value,
      price: Number(String(price ? price : 0).replace(/\./g, '')),
      map_payload: {
        latitude: location.lat,
        longitude: location.lng,
      },
      project_id: project.value || undefined,
      project_experiences: experiences.map((ex) => ex.data),
    };
    if (project.title && !project.value) {
      data.project_name = project.title;
    }
    if (props.editMode) {
      dispatch(updateSellListing(props.sellListing.id, data));
    } else {
      dispatch(postSellListing(data));
    }
  };

  /**
   * resetState
   */
  const resetState = () => {
    setStage(1);
    // step 1
    setProvince({ value: '', title: '' });
    setDistrict({ value: '', title: '' });
    setWard({ value: '', title: '' });
    setTransaction({ value: '', title: '' });
    setProductType({ value: '', title: '' });
    setProject({ value: '', title: '' });
    setAddress('');
    // step 2
    setType({ title: '', value: '' });
    setArea('');
    setDirection({ title: '', value: '' });
    setFurniture({ title: '', value: '' });
    setJuridical({ title: '', value: '' });
    setNumberOfBedroom('');
    setNumberOfBathroom('');
    setPrice('');
    // step 3
    setTitle('');
    setDesc('');
    setVideoUrl('');
    setImages([]);
    // step 4
    setFullname('');
    setPhone('');
    setEmail('');
    setService('');
  };

  /**
   * updateState
   */
  const updateState = (sell: SellListing) => {
    if (sell.id) {
      setStage(1);
      // step 1
      setProvince({ value: sell.province?.code, title: sell.province?.name });
      setDistrict({ value: sell.district?.code, title: sell.district?.name });
      setWard({ value: sell.ward?.code, title: sell.ward?.name });
      setTransaction({ value: sell.typeTransaction?.id, title: sell.typeTransaction?.content ?? '' });
      setProductType({ value: sell.typeProduct?.id, title: sell.typeProduct?.content ?? '' });
      // get data part 2
      dispatch(getSellListingPartTwo(sell?.typeProduct?.id));
      // project
      const currentPrj: Project = projectList.find((item: Project) => Number(item.id) === Number(sell.projectId));
      setProject({ value: sell.projectId as string, title: currentPrj?.name || '' });
      if (sell.projectId) {
        dispatch(getExperiencesByProject(sell.projectId as string, true));
      }
      setAddress(sell.address);
      // step 2
      setType({ title: sell.type?.content, value: sell.type?.id ?? '' });
      setArea(sell.area);
      setDirection({ title: sell.direction?.content, value: sell.direction?.id });
      setFurniture({ title: sell.furniture?.content, value: sell.furniture?.id });
      setJuridical({ title: sell.juridical?.content, value: sell.juridical?.id });
      setNumberOfBedroom(sell.bedroom);
      setNumberOfBathroom(sell.bathroom);
      setPrice(numberWithDot(Number(String(sell.price ? sell.price : 0).replace(/\./g, ''))) || '');
      // step 3
      setTitle(sell.title);
      setDesc(sell.description ? sell.description : '');
      setVideoUrl(sell.videoUrl);
      setImages(sell.images);
      setExperiences(
        sell.projectExperiences?.map((exp) => {
          return {
            title: exp.name,
            value: exp.id,
            data: exp,
          };
        }) || [],
      );
      // step 4
      setFullname(sell.seller?.name ?? '');
      setPhone(sell.seller?.phone ?? '');
      setEmail(sell.seller?.email ?? '');
      setService(sell.serviceCode);
    }
  };

  const uploadImageDesc = (data: any) => {
    uploadImage(data).then((res) => {
      if (res.success && res?.data?.id) {
        const image = res?.data;
        setImages([...images, image]);
      } else {
        dispatch(
          openNotification({
            title: 'Tải ảnh lên không thành công. Ảnh phải có dung lượng dưới 2MB',
            type: 'error',
            isOpen: true,
          }),
        );
      }
    });
  };

  if (props.innerRef && props.innerRef.current) {
    props.innerRef.current = {
      togglePopup: setIsOpen,
    };
  }

  const headerChooseStep = (step: number) => {
    setStepHeaderChoose(step);
  };

  const changeProject = (project: OptionInputRadio) => {
    setProject(project);
    if (project.value) {
      dispatch(getExperiencesByProject(project.value, true));
    }
  };

  return (
    <div className="post_advertising_popup">
      <button
        className="padding_x_32 color_white bg_red border_radius_25 border_none height_32 font_weight_600 width_xs_100_percent padding_y_xs_10 height_xs_auto"
        onClick={() => {
          setIsOpen(true);
        }}
      >
        Đăng bài
      </button>
      <Popup
        className="border_radius_18  margin_top_-35 margin_top_xs_0"
        isOpen={isOpen}
        setIsOpen={setIsOpen}
        isHiddenExitButton={true}
      >
        <div
          className="width_1176 height_950 padding_top_38 padding_bottom_71 display_flex flex_direction_col width_xs_100_percent height_xs_auto
        padding_top_xs_12"
        >
          <HeaderPopup
            setIsOpenPopup={setIsOpen}
            title={renderTitle()}
            step={stage}
            chooseStep={headerChooseStep}
            backStep={chooseStage}
          ></HeaderPopup>
          <div className={classnames({ display_none: stage !== 1 }, 'flex_1')}>
            <BasicInfo
              province={province}
              setProvince={setProvince}
              district={district}
              setDistrict={setDistrict}
              ward={ward}
              setWard={setWard}
              transaction={transaction}
              setTransaction={setTransaction}
              productType={productType}
              setProductType={setProductType}
              project={project}
              setProject={changeProject}
              address={address}
              setAddress={setAddress}
              nextStage={chooseStage}
              editMode={props.editMode}
              location={location}
              setLocation={setLocation}
              isOpen={isOpen}
              editId={props.sellListing?.id}
              stepHeaderChoose={stepHeaderChoose}
              setStepHeaderChoose={setStepHeaderChoose}
            ></BasicInfo>
          </div>
          <div className={classnames({ display_none: stage !== 2 }, 'flex_1')}>
            <DetailInfo
              nextStage={chooseStage}
              type={type}
              setType={setType}
              area={area}
              setArea={setArea}
              direction={direction}
              setDirection={setDirection}
              furniture={furniture}
              setFurniture={setFurniture}
              juridical={juridical}
              setJuridical={setJuridical}
              numberOfBedroom={numberOfBedroom}
              setNumberOfBedroom={setNumberOfBedroom}
              numberOfBathroom={numberOfBathroom}
              setNumberOfBathroom={setNumberOfBathroom}
              price={price}
              setPrice={setPrice}
              stepHeaderChoose={stepHeaderChoose}
              setStepHeaderChoose={setStepHeaderChoose}
            ></DetailInfo>
          </div>
          <div className={classnames({ display_none: stage !== 3 }, 'flex_1')}>
            <DescriptionProduct
              title={title}
              setTitle={setTitle}
              desc={desc}
              setDesc={setDesc}
              videoUrl={videoUrl}
              setVideoUrl={setVideoUrl}
              nextStage={chooseStage}
              images={images}
              uploadImage={uploadImageDesc}
              setImages={setImages}
              stepHeaderChoose={stepHeaderChoose}
              setStepHeaderChoose={setStepHeaderChoose}
              experiences={experiences}
              setExperiences={setExperiences}
            ></DescriptionProduct>
          </div>
          <div className={classnames({ display_none: stage !== 4 }, 'flex_1')}>
            <ContactInfo
              fullname={fullname}
              setFullname={setFullname}
              phone={phone}
              setPhone={setPhone}
              email={email}
              setEmail={setEmail}
              service={service}
              setService={setService}
              onSubmit={createSellListing}
              nextStage={chooseStage}
              editMode={props.editMode}
            ></ContactInfo>
          </div>
        </div>
      </Popup>
    </div>
  );
};

export default SellCreatePopup;
