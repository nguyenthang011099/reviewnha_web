import React, { useEffect, useRef, useState } from 'react';
import classnames from 'classnames';
import InputText from '../../../components/base/input-text/InputText';
import { useSelector } from 'react-redux';
import { RootState } from '../../../reducers';
import { OptionInputRadio } from '../../../types/type.base';
import dayjs from 'dayjs';

interface Props {
  nextStage: (stage: number) => void;
  fullname: string;
  setFullname: (value: string) => void;
  phone: string;
  setPhone: (value: string) => void;
  email: string;
  setEmail: (value: string) => void;
  service: string;
  setService: (value: string) => void;
  onSubmit: (..._args: any[]) => void;
  editMode: boolean;
}

const ContactInfo: React.FC<Props> = (props) => {
  const { fullname, setFullname, email, setEmail, phone, setPhone, service, setService } = props;
  const stepFourData = useSelector((state: RootState) => state.sellListing.stepFourData);
  const [warningName, setWarningName] = useState<boolean>(false);
  const [warningPhone, setWarningPhone] = useState<boolean>(false);
  const [warningEmail, setWarningEmail] = useState<boolean>(false);

  useEffect(() => {
    if (stepFourData.services?.length && !props.editMode) {
      setService(stepFourData.services[0].value);
    }
  }, [stepFourData]);

  const renderWarning = () => {
    if (warningName) {
      return 'Họ và tên phải có ít nhất 3 kí tự';
    } else if (warningPhone) {
      return 'Vui lòng nhập số điện thoại';
    } else if (warningEmail) {
      return 'Vui lòng nhập email';
    }
  };

  const submit = () => {
    if (fullname.length >= 3 && email && phone) {
      props.onSubmit();
    } else if (fullname.length < 3) {
      setWarningName(true);
    } else if (!phone) {
      setWarningPhone(true);
    } else if (!email) {
      setWarningEmail(true);
    }
  };

  return (
    <div className="detail_info padding_x_240 display_flex flex_direction_col justify_content_sb height_100_percent padding_x_xs_22">
      <div>
        <div className="margin_bottom_16">
          <InputText
            label="Họ tên"
            value={fullname}
            setValue={(value: string) => {
              if (value.length <= 70) {
                setFullname(value);
              }
            }}
            name=""
            type="text"
            required={true}
            placeholder="Nhập họ tên người liên hệ "
            isWarning={warningName}
            setIsWarning={setWarningName}
          ></InputText>
        </div>
        <div className="margin_bottom_16">
          <InputText
            label="SĐT:"
            value={phone}
            setValue={(value: string) => {
              const validatedValue = value.replace(/[^0-9+()]/g, '');
              setPhone(validatedValue);
            }}
            name=""
            type="text"
            required={true}
            placeholder="Nhập SĐT liên hệ"
            isWarning={warningPhone}
            setIsWarning={setWarningPhone}
          ></InputText>
        </div>
        <div className="margin_bottom_16">
          <InputText
            label="Email:"
            value={email}
            setValue={setEmail}
            name=""
            type="text"
            required={true}
            placeholder="Nhập Email người liên hệ"
            isWarning={warningEmail}
            setIsWarning={setWarningEmail}
          ></InputText>
        </div>
        <div className="margin_bottom_16 padding_left_24">
          <span className="display_block font_size_12 line_height_18">Chọn gói tin:</span>
        </div>
        <div className="display_flex margin_x_-8 margin_bottom_16 display_xs_block">
          {stepFourData.services.map((item: OptionInputRadio) => {
            return (
              <div
                className="padding_x_8 flex_1 margin_bottom_xs_16 opacity_0_7"
                key={item.value}
                onClick={() => {}}
              >
                <div
                  className="border_color_black border_style_solid border_width_1 padding_x_24 height_40 display_flex justify_content_sb
                  border_radius_8 align_items_center cursor_pointer"
                >
                  <div className="display_flex align_items_center">
                    {item.value?.toLowerCase() === service.toLowerCase() ? (
                      <svg width={20} height={20} viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path
                          fillRule="evenodd"
                          clipRule="evenodd"
                          d="M10 0C15.514 0 20 4.486 20 10C20 15.514 15.514 20 10 20C4.486 20 0 15.514 0 10C0 4.486 4.486 0 10 0ZM10 1.5C5.313 1.5 1.5 5.313 1.5 10C1.5 14.687 5.313 18.5 10 18.5C14.687 18.5 18.5 14.687 18.5 10C18.5 5.313 14.687 1.5 10 1.5Z"
                          fill="#3B4144"
                        />
                        <path
                          d="M16.5 10C16.5 13.5899 13.5899 16.5 10 16.5C6.41015 16.5 3.5 13.5899 3.5 10C3.5 6.41015 6.41015 3.5 10 3.5C13.5899 3.5 16.5 6.41015 16.5 10Z"
                          fill="#3B4144"
                        />
                      </svg>
                    ) : (
                      <svg width={20} height={20} viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path
                          fillRule="evenodd"
                          clipRule="evenodd"
                          d="M10 0C15.514 0 20 4.486 20 10C20 15.514 15.514 20 10 20C4.486 20 0 15.514 0 10C0 4.486 4.486 0 10 0ZM10 1.5C5.313 1.5 1.5 5.313 1.5 10C1.5 14.687 5.313 18.5 10 18.5C14.687 18.5 18.5 14.687 18.5 10C18.5 5.313 14.687 1.5 10 1.5Z"
                          fill="#3B4144"
                        />
                      </svg>
                    )}
                    <span className="font_size_14 line_height_17 font_weight_600 display_block margin_left_18">
                      {item.title}
                    </span>
                  </div>
                  <svg width={20} height={20} viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path
                      fillRule="evenodd"
                      clipRule="evenodd"
                      d="M10 0.750199C15.109 0.750199 19.25 4.8922 19.25 10.0002C19.25 15.1082 15.109 19.2502 10 19.2502C4.892 19.2502 0.75 15.1082 0.75 10.0002C0.75 4.8922 4.892 0.750199 10 0.750199Z"
                      stroke="#3B4144"
                      strokeWidth="1.5"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                    />
                    <path
                      d="M9.99609 6.2043V10.6233"
                      stroke="#3B4144"
                      strokeWidth="1.5"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                    />
                    <path
                      d="M9.99609 13.7961H10.0061"
                      stroke="#3B4144"
                      strokeWidth="1.5"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                    />
                  </svg>
                </div>
              </div>
            );
          })}
        </div>
        {/* <div
          className="input_select display_flex align_items_center justify_content_sb height_48 border_color_black border_style_solid border_radius_24 
            border_width_1 padding_x_24 margin_bottom_16"
        >
          <div className="display_flex align_items_center">
            <span className="width_97 font_size_12 line_height_18 display_block">Trong:</span>
            <input className="border_none font_size_16 font_weight_500 line_height_28" value="2 tuần" disabled />
          </div>
          <div className="display_flex cursor_pointer">
            <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path
                fillRule="evenodd"
                clipRule="evenodd"
                d="M10 0C15.514 0 20 4.486 20 10C20 15.514 15.514 20 10 20C4.486 20 0 15.514 0 10C0 4.486 4.486 0 10 0ZM10 1.5C5.313 1.5 1.5 5.313 1.5 10C1.5 14.687 5.313 18.5 10 18.5C14.687 18.5 18.5 14.687 18.5 10C18.5 5.313 14.687 1.5 10 1.5ZM14.001 8.0259C14.294 8.3179 14.295 8.7929 14.003 9.0869L10.531 12.5729C10.419 12.6865 10.2737 12.7598 10.1182 12.7845L10 12.7939C9.801 12.7939 9.609 12.7149 9.469 12.5729L5.998 9.0869C5.705 8.7929 5.707 8.3179 6 8.0259C6.294 7.7339 6.769 7.7339 7.061 8.0279L10 10.9819L12.94 8.0279C13.232 7.7339 13.707 7.7339 14.001 8.0259Z"
                fill="black"
              />
            </svg>
          </div>
        </div>
        <div className="display_flex margin_x_-8">
          <div className="col_6 padding_x_8">
            <div
              className="display_flex align_items_center justify_content_sb height_48 border_color_black border_style_solid border_radius_24  
              border_width_1 padding_x_24 margin_bottom_16"
            >
              <div className="display_flex align_items_center flex_1">
                <span className="width_97 font_size_12 line_height_18 display_block flex_none">Từ:</span>
                <input
                  className="border_none font_size_16 font_weight_500 line_height_28 width_100_percent"
                  value={dayjs(new Date()).format('DD/MM/YYYY')}
                  disabled
                />
              </div>
              <div className="display_flex cursor_pointer">
                <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path
                    fillRule="evenodd"
                    clipRule="evenodd"
                    d="M14.334 0C17.723 0 20 2.378 20 5.916V14.084C20 17.622 17.723 20 14.333 20H5.665C2.276 20 0 17.622 0 14.084V5.916C0 2.378 2.276 0 5.665 0H14.334ZM14.334 1.5H5.665C3.135 1.5 1.5 3.233 1.5 5.916V14.084C1.5 16.767 3.135 18.5 5.665 18.5H14.333C16.864 18.5 18.5 16.767 18.5 14.084V5.916C18.5 3.233 16.865 1.5 14.334 1.5ZM9.9991 4.8836C10.4131 4.8836 10.7491 5.2196 10.7491 5.6336V9.5696L13.7751 11.3726C14.1301 11.5856 14.2471 12.0456 14.0351 12.4016C13.8941 12.6366 13.6451 12.7676 13.3901 12.7676C13.2591 12.7676 13.1271 12.7336 13.0061 12.6626L9.6151 10.6396C9.3891 10.5036 9.2491 10.2586 9.2491 9.9956V5.6336C9.2491 5.2196 9.5851 4.8836 9.9991 4.8836Z"
                    fill="black"
                  />
                </svg>
              </div>
            </div>
          </div>
          <div className="col_6 padding_x_8">
            <div
              className="display_flex align_items_center justify_content_sb height_48 border_color_black border_style_solid border_radius_24  
              border_width_1 padding_x_24 margin_bottom_16"
            >
              <div className="display_flex align_items_center flex_1">
                <span className="width_97 font_size_12 line_height_18 display_block flex_none">Đến:</span>
                <input
                  className="border_none font_size_16 font_weight_500 line_height_28 width_100_percent"
                  value={dayjs(new Date()).add(14, 'day').format('DD/MM/YYYY')}
                  disabled
                />
              </div>
              <div className="display_flex cursor_pointer">
                <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path
                    fillRule="evenodd"
                    clipRule="evenodd"
                    d="M14.334 0C17.723 0 20 2.378 20 5.916V14.084C20 17.622 17.723 20 14.333 20H5.665C2.276 20 0 17.622 0 14.084V5.916C0 2.378 2.276 0 5.665 0H14.334ZM14.334 1.5H5.665C3.135 1.5 1.5 3.233 1.5 5.916V14.084C1.5 16.767 3.135 18.5 5.665 18.5H14.333C16.864 18.5 18.5 16.767 18.5 14.084V5.916C18.5 3.233 16.865 1.5 14.334 1.5ZM9.9991 4.8836C10.4131 4.8836 10.7491 5.2196 10.7491 5.6336V9.5696L13.7751 11.3726C14.1301 11.5856 14.2471 12.0456 14.0351 12.4016C13.8941 12.6366 13.6451 12.7676 13.3901 12.7676C13.2591 12.7676 13.1271 12.7336 13.0061 12.6626L9.6151 10.6396C9.3891 10.5036 9.2491 10.2586 9.2491 9.9956V5.6336C9.2491 5.2196 9.5851 4.8836 9.9991 4.8836Z"
                    fill="black"
                  />
                </svg>
              </div>
            </div>
          </div>
      </div> */}
        {/* <div className="margin_bottom_16">
          <InputText
            label="Giá:"
            value={'2.000.000'}
            setValue={(_value: string) => {}}
            name=""
            type="text"
            setIcon={() => {
              return (
                <div className="display_flex">
                  <span className="font_size_12 line_height_18">VNĐ</span>
                </div>
              );
            }}
            isHiddenClear={true}
          ></InputText>
        </div> */}
        {renderWarning() ? (
          <div className="margin_left_25">
            <span className="font_size_12 font_weight_400 color_red">{renderWarning()}</span>
          </div>
        ) : null}
      </div>
      <div className="display_flex justify_content_sb">
        <button
          className="border_none width_216 height_32 font_size_14 line_height_17 font_weight_600 bg_red color_white border_radius_25 opacity_0
        width_xs_1"
        >
          Preview
        </button>
        <div className="display_flex">
          <button
            className="margin_right_16 border_color_black border_width_1 width_120 height_32 font_size_14 line_height_17 font_weight_600 bg_white border_radius_25"
            onClick={() => {
              props.nextStage(3);
            }}
          >
            Quay lại
          </button>
          <button
            className="border_none width_120 height_32 font_size_14 line_height_17 font_weight_600 bg_black color_white border_radius_25"
            onClick={submit}
          >
            Đăng bài
          </button>
        </div>
      </div>
    </div>
  );
};

export default ContactInfo;
