import React, { useEffect, useRef, useState } from 'react';
import InputText from '../../../components/base/input-text/InputText';
import Slider from '../../../components/slider/Slider';
import useOutsideClick from '../../../utils/customHooks/useOutsideClick';
import './styles/DescriptionProduct.scss';
import classnames from 'classnames';
import { useSelector } from 'react-redux';
import { RootState } from '../../../reducers';
import InputTag from '../../../components/base/input-tag/InputTag';
import { OptionInputRadio } from '../../../types/type.base';

interface Props {
  nextStage: (stage: number) => void;
  title: string;
  setTitle: (value: string) => void;
  desc: string;
  setDesc: (value: string) => void;
  videoUrl: string;
  setVideoUrl: (value: string) => void;
  images: { id: number; url: string }[];
  setImages: (images: { id: number; url: string }[]) => void;
  uploadImage: (data: any) => void;
  stepHeaderChoose: number | null;
  setStepHeaderChoose: (value: number | null) => void;
  experiences: OptionInputRadio[];
  setExperiences: (value: OptionInputRadio[]) => void;
}

const DescriptionProduct: React.FC<Props> = (props) => {
  const { title, setTitle, desc, setDesc, videoUrl, setVideoUrl, images, setImages, uploadImage } = props;
  const $textareaRef = useRef(null);
  const [isFocus, setIsFocus] = useState<boolean>(false);
  const isFocusState: any = useRef(false);
  const [warningTitle, setWarningTitle] = useState<boolean>(false);
  const [warningDesc, setWarningDesc] = useState<boolean>(false);
  const [warningVideoUrl, setWarningVideoUrl] = useState<boolean>(false);
  const [warningImages, setWarningImages] = useState<boolean>(false);
  const $nextStepBtn = useRef(null);
  const stepThreeData = useSelector((state: RootState) => state.sellListing.stepThreeData);

  useEffect(() => {
    isFocusState.current = isFocus;
  }, [isFocus]);

  useOutsideClick($textareaRef, () => {
    if (isFocusState.current) {
      setIsFocus(false);
    }
  });

  useEffect(() => {
    if (props.stepHeaderChoose === 4) {
      props.setStepHeaderChoose(null);
      $nextStepBtn && $nextStepBtn.current.click();
    }
  }, [props.stepHeaderChoose]);

  const renderLengthTitle = () => {
    return (
      <div className="display_flex">
        <span className="font_size_12 line_height_18">{title.length}/70</span>
      </div>
    );
  };

  const nextStep = (e: React.FormEvent<HTMLButtonElement>) => {
    e.preventDefault();
    if (title.length >= 6 && desc.length >= 50 && images.length >= 5) {
      props.nextStage(4);
    } else if (title.length < 6) {
      setWarningTitle(true);
    } else if (desc.length < 50) {
      setWarningDesc(true);
    } else if (images.length < 5) {
      setWarningImages(true);
    } else if (!videoUrl) {
      setWarningVideoUrl(true);
    }
  };

  /**
   * renderWarning: render warning
   */
  const renderWarning = () => {
    if (warningTitle) {
      return 'Tiêu đề phải có ít nhất 6 kí tự';
    } else if (warningDesc) {
      return 'Mô tả phải có ít nhất 50 kí tự';
    } else if (warningVideoUrl) {
      return 'Vui lòng nhập video url';
    } else if (warningImages) {
      return 'Phải có ít nhất 5 ảnh';
    }
  };

  const onUploadImage = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (e.target.files?.length) {
      const file = e.target.files[0];
      const data = new FormData();
      data.append('type', 'sell_listing');
      data.append('image', file);
      uploadImage(data);
      setWarningImages(false);
    }
  };

  const onRemoveImage = (imageId: number) => {
    const newImages = images.filter((image) => image.id !== imageId);
    setImages(newImages);
  };

  return (
    <div className="description_product padding_x_240 display_flex flex_direction_col justify_content_sb height_100_percent padding_x_xs_22">
      <div>
        <div className="margin_bottom_16">
          <InputText
            label="Tiêu đề:"
            placeholder="Nhập tiêu đề bài đăng"
            required
            value={title}
            setValue={(value) => {
              if (value.length <= 70) {
                setTitle(value);
              }
            }}
            type="text"
            name="title"
            setIcon={renderLengthTitle}
            isHiddenClear={true}
            isWarning={warningTitle}
            setIsWarning={setWarningTitle}
          ></InputText>
        </div>
        <div
          className={classnames(
            { focus: isFocus, warning: warningDesc },
            `description_input input_select display_flex justify_content_sb border_color_black border_style_solid border_radius_8
            border_width_1 padding_x_24 margin_bottom_16 height_100`,
          )}
          ref={$textareaRef}
        >
          <div className="display_flex flex_1">
            <span className="label width_97 font_size_12 line_height_18 display_block padding_top_15 opacity_0_5 width_xs_80">
              Mô tả thêm:
            </span>
            <textarea
              className="border_none font_size_16 font_weight_500 line_height_28 padding_y_10 flex_1 height_100_percent scroll_bar_width_4 
              width_xs_100_percent"
              placeholder="Nhập mô tả chi tiết về dự án"
              value={desc}
              required
              onChange={(e: React.ChangeEvent<HTMLTextAreaElement>) => {
                if (e.target.value.length <= 1000) {
                  setDesc(e.target.value);
                }
                warningDesc && setWarningDesc(false);
              }}
              onFocus={() => {
                setIsFocus(true);
              }}
            ></textarea>
          </div>
          <div className="display_flex align_items_fe margin_bottom_16 margin_left_10 display_xs_none">
            <span className="font_size_12 line_height_18">{desc.length}/1000</span>
          </div>
        </div>
        <div className="images margin_bottom_16">
          <div className="margin_bottom_16 padding_left_24">
            <span className="display_block font_size_12 line_height_18 opacity_0_5">Hình ảnh:</span>
          </div>
          <div className="display_flex display_xs_block">
            <div
              className="width_120 height_120 border_color_black border_style_solid border_width_1 border_radius_8
              display_flex align_items_center justify_content_center cursor_pointer margin_right_42 flex_none
              height_xs_40 width_xs_100_percent"
            >
              <label className="display_flex align_items_center cursor_pointer height_100_percent width_100_percent justify_content_center">
                <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path
                    d="M14.3338 0C17.7231 0 20 2.37811 20 5.91672V14.0833C20 17.6219 17.7231 20 14.3328 20H5.66618C2.2769 20 0 17.6219 0 14.0833V5.91672C0 2.37811 2.2769 0 5.66618 0H14.3338ZM15.4366 10.5501C14.3646 9.88132 13.5371 10.8204 13.3138 11.1207C13.0986 11.4107 12.9136 11.7306 12.7185 12.0506C12.2419 12.84 11.6958 13.7503 10.7506 14.2797C9.37699 15.0402 8.3342 14.3395 7.58404 13.8297C7.30248 13.6398 7.02897 13.4603 6.75645 13.3406C6.08473 13.0506 5.48038 13.3808 4.5834 14.5201C4.11279 15.1156 3.6462 15.7059 3.17358 16.2941C2.89102 16.646 2.95839 17.1889 3.3395 17.4241C3.94788 17.7988 4.68999 18 5.52864 18H13.9564C14.432 18 14.9087 17.935 15.3632 17.7864C16.3869 17.452 17.1994 16.6863 17.6237 15.6749C17.9817 14.8246 18.1557 13.7926 17.8208 12.934C17.7092 12.6491 17.5423 12.3839 17.308 12.1507C16.6936 11.5408 16.1194 10.9711 15.4366 10.5501ZM6.49886 4C5.12021 4 4 5.12173 4 6.5C4 7.87827 5.12021 9 6.49886 9C7.8765 9 8.99772 7.87827 8.99772 6.5C8.99772 5.12173 7.8765 4 6.49886 4Z"
                    fill="#272727"
                  />
                </svg>
                <span className="font_size_15 line_height_22 margin_left_10">Tải lên</span>
                <input className="display_none" type="file" onChange={onUploadImage} accept="image/*" />
              </label>
            </div>
            <div className={classnames({ display_none: !images.length }, 'position_relative display_xs_none')}>
              <Slider
                className="border_top_left_radius_8 border_top_right_radius_8 width_516"
                listImages={images as any}
                setListImages={setImages}
                slidesNm={25}
                slidesMd={100}
                slidesSm={100}
                slidesXs={100}
                showButton={true}
                isStopAutoPlay={true}
                slidesPerView={4}
                margin={12}
              >
                {images.map((item, index) => {
                  return (
                    <div className="discovery_image_item padding_x_6 position_relative" key={item.id}>
                      <div
                        className="cursor_pointer position_absolute right_10 top_10"
                        onClick={() => {
                          onRemoveImage(item.id);
                        }}
                      >
                        <svg
                          width="20"
                          height="20"
                          viewBox="0 0 20 20"
                          fill="none"
                          xmlns="http://www.w3.org/2000/svg"
                          className=" bg_white border_radius_50_percent"
                        >
                          <path
                            fillRule="evenodd"
                            clipRule="evenodd"
                            d="M10 0C15.514 0 20 4.486 20 10C20 15.514 15.514 20 10 20C4.486 20 0 15.514 0 10C0 4.486 4.486 0 10 0ZM10 1.5C5.313 1.5 1.5 5.313 1.5 10C1.5 14.687 5.313 18.5 10 18.5C14.687 18.5 18.5 14.687 18.5 10C18.5 5.313 14.687 1.5 10 1.5Z"
                            fill="black"
                          />
                          <path
                            fillRule="evenodd"
                            clipRule="evenodd"
                            d="M7.58073 6.46447C7.27242 6.15616 6.77255 6.15616 6.46424 6.46447C6.15594 6.77278 6.15594 7.27264 6.46424 7.58095L8.88329 10L6.46424 12.4191C6.15594 12.7274 6.15594 13.2272 6.46424 13.5355C6.77255 13.8438 7.27242 13.8438 7.58073 13.5355L9.99978 11.1165L12.4188 13.5355C12.7271 13.8438 13.227 13.8438 13.5353 13.5355C13.8436 13.2272 13.8436 12.7274 13.5353 12.4191L11.1163 10L13.5353 7.58095C13.8436 7.27264 13.8436 6.77278 13.5353 6.46447C13.227 6.15616 12.7271 6.15616 12.4188 6.46447L9.99978 8.88352L7.58073 6.46447Z"
                            fill="black"
                          />
                        </svg>
                      </div>
                      <img
                        src={item?.url}
                        alt="discovery"
                        className="display_block border_radius_8 width_120 height_120 object_fit_cover"
                      />
                    </div>
                  );
                })}
              </Slider>
            </div>
            {/* mobile */}
            <div className="display_none display_xs_flex width_100_percent overflow_auto margin_top_16">
              {images.map((item, index) => {
                return (
                  <div className="discovery_image_item padding_right_12 position_relative" key={item.id}>
                    <div
                      className="cursor_pointer position_absolute right_15 top_10"
                      onClick={() => {
                        onRemoveImage(item.id);
                      }}
                    >
                      <svg
                        width="20"
                        height="20"
                        viewBox="0 0 20 20"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                        className=" bg_white border_radius_50_percent"
                      >
                        <path
                          fillRule="evenodd"
                          clipRule="evenodd"
                          d="M10 0C15.514 0 20 4.486 20 10C20 15.514 15.514 20 10 20C4.486 20 0 15.514 0 10C0 4.486 4.486 0 10 0ZM10 1.5C5.313 1.5 1.5 5.313 1.5 10C1.5 14.687 5.313 18.5 10 18.5C14.687 18.5 18.5 14.687 18.5 10C18.5 5.313 14.687 1.5 10 1.5Z"
                          fill="black"
                        />
                        <path
                          fillRule="evenodd"
                          clipRule="evenodd"
                          d="M7.58073 6.46447C7.27242 6.15616 6.77255 6.15616 6.46424 6.46447C6.15594 6.77278 6.15594 7.27264 6.46424 7.58095L8.88329 10L6.46424 12.4191C6.15594 12.7274 6.15594 13.2272 6.46424 13.5355C6.77255 13.8438 7.27242 13.8438 7.58073 13.5355L9.99978 11.1165L12.4188 13.5355C12.7271 13.8438 13.227 13.8438 13.5353 13.5355C13.8436 13.2272 13.8436 12.7274 13.5353 12.4191L11.1163 10L13.5353 7.58095C13.8436 7.27264 13.8436 6.77278 13.5353 6.46447C13.227 6.15616 12.7271 6.15616 12.4188 6.46447L9.99978 8.88352L7.58073 6.46447Z"
                          fill="black"
                        />
                      </svg>
                    </div>
                    <img
                      src={item?.url}
                      alt="discovery"
                      className="display_block border_radius_8 width_120 height_120 object_fit_cover"
                    />
                  </div>
                );
              })}
            </div>
          </div>
        </div>
        {stepThreeData.experiences?.length ? (
          <div className="margin_bottom_16">
            <InputTag
              value={props.experiences}
              setValue={props.setExperiences}
              label="Bài tư vấn:"
              options={stepThreeData.experiences}
            ></InputTag>
          </div>
        ) : null}
        <div className="margin_bottom_-5">
          <InputText
            value={videoUrl}
            setValue={setVideoUrl}
            type="text"
            name=""
            label="Video:"
            isWarning={warningVideoUrl}
            setIsWarning={setWarningVideoUrl}
          ></InputText>
        </div>
        {renderWarning() ? (
          <div className="margin_left_25 margin_top_-15">
            <span className="font_size_12 font_weight_400 color_red">{renderWarning()}</span>
          </div>
        ) : null}
        <div className="padding_left_24">
          <p className="margin_y_0 font_size_12 line_height_18 margin_bottom_5">Lưu ý:</p>
          <p className="margin_y_0 font_size_12 line_height_18 margin_bottom_5">Bạn cần đăng ít nhất 5 ảnh:</p>
          <ul className="margin_y_0 margin_bottom_5 padding_left_25">
            <li className="font_size_12 line_height_18 margin_bottom_5">Chọn hình ảnh đẹp để bán nhanh hơn.</li>
            <li className="font_size_12 line_height_18 margin_bottom_5">
              Nên chọn ảnh khổ ngang, ảnh có dung lượng dưới 2MB.
            </li>
            <li className="font_size_12 line_height_18 margin_bottom_5">
              Cập nhật thêm video để tin bài sinh động và trực quan bằng cách gắn link youtube.
            </li>
          </ul>
          <p className="margin_y_0 font_size_12 line_height_18 margin_bottom_5">Tin sẽ bị từ chối nếu:</p>
          <ul className="margin_y_0 margin_bottom_5 padding_left_25">
            <li className="font_size_12 line_height_18 margin_bottom_5">Sử dụng hình ảnh lặp, chất lượng kém.</li>
            <li className="font_size_12 line_height_18 margin_bottom_5">chèn sđt, mail vào hình.</li>
          </ul>
        </div>
      </div>
      <div className="display_flex justify_content_sb">
        <button
          className="border_none width_216 height_32 font_size_14 line_height_17 font_weight_600 bg_red color_white border_radius_25 opacity_0
          width_xs_1"
          type="button"
        >
          Preview
        </button>
        <div className="display_flex">
          <button
            type="button"
            className="margin_right_16 border_color_black border_width_1 width_120 height_32 font_size_14 line_height_17 font_weight_600 bg_white border_radius_25"
            onClick={() => {
              props.nextStage(2);
            }}
          >
            Quay lại
          </button>
          <button
            type="submit"
            className="border_none width_120 height_32 font_size_14 line_height_17 font_weight_600 bg_black color_white border_radius_25"
            onClick={nextStep}
            ref={$nextStepBtn}
          >
            Tiếp theo
          </button>
        </div>
      </div>
    </div>
  );
};

export default DescriptionProduct;
