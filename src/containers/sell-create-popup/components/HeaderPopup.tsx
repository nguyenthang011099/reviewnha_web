import React, { useEffect, useRef, useState } from 'react';
import classnames from 'classnames';

interface Props {
  setIsOpenPopup: (isOpen: boolean) => void;
  title: string;
  step: number;
  chooseStep: (stage: number) => void;
  backStep: (stage: number) => void;
}

const HeaderPopup: React.FC<Props> = (props) => {
  const steps = [
    {
      id: 1,
      title: 'Thông tin cơ bản',
    },
    {
      id: 2,
      title: 'Thông tin chi tiết ',
    },
    {
      id: 3,
      title: 'Mô tả sản phẩm',
    },
    {
      id: 4,
      title: 'Thông tin liên hệ Gói đăng',
    },
  ];
  const $disablebar = useRef(null);
  const [activebarWidth, setActivebarWidth] = useState<string>('0px');
  const [device, setDevice] = useState<string>('desktop');

  useEffect(() => {
    setDevice(getDeviceType());
  }, []);

  useEffect(() => {
    setActivebarWidth(getWidthActiveBar());
  }, [props.step]);

  const getDeviceType = () => {
    var userAgent = navigator.userAgent;
    if (/(tablet|ipad|playbook|silk)|(android(?!.*mobi))/i.test(userAgent)) {
      return 'tablet';
    }
    if (
      /Mobile|iP(hone|od)|Android|BlackBerry|IEMobile|Kindle|Silk-Accelerated|(hpw|web)OS|Opera M(obi|ini)/.test(
        userAgent,
      )
    ) {
      return 'mobile';
    }
    return 'desktop';
  };

  const getWidthActiveBar = () => {
    if ($disablebar.current) {
      const disableBarWidth = $disablebar.current.offsetWidth;
      let activeWidth = disableBarWidth;
      if (props.step === 2 || props.step === 3) {
        activeWidth = ((disableBarWidth - (device === 'mobile' ? 23 : 46)) / 3) * (props.step - 1);
      } else if (props.step === 1) {
        activeWidth = 0;
      }
      return activeWidth + 'px';
    }
    return '0px';
  };

  return (
    <div className="header text_align_center position_relative margin_bottom_36 margin_x_40 margin_x_xs_0">
      <div className="subtitle position_absolute left_50_percent translate_-50_0_percent display_flex align_items_center">
        <svg width="24" height="28" viewBox="0 0 24 28" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path
            d="M13.6693 0.666656C13.9993 0.666656 14.2766 0.946657 14.2766 1.27999V5.57332C14.2766 8.01332 16.2568 10.0133 18.6858 10.0267C19.6759 10.0267 20.468 10.04 21.0753 10.04C21.4845 10.04 22.1578 10.0267 22.7254 10.0267C23.0555 10.0267 23.3327 10.2933 23.3327 10.6267V21.3467C23.3327 24.6533 20.666 27.3333 17.3921 27.3333H6.89704C3.45149 27.3333 0.666016 24.52 0.666016 21.0533V6.67999C0.666016 3.37332 3.31948 0.666656 6.61981 0.666656H13.6693ZM11.5175 9.65332C11.3855 9.65332 11.2535 9.67999 11.1347 9.73332C11.0159 9.78666 10.9102 9.85332 10.8178 9.94666L7.04225 13.7867C6.65942 14.1733 6.65942 14.8 7.04225 15.1867C7.42509 15.5733 8.04555 15.5733 8.42839 15.1867L10.5274 13.0533V19.4933C10.5274 20.04 10.963 20.48 11.5175 20.48C12.0588 20.48 12.4944 20.04 12.4944 19.4933V13.0533L14.5934 15.1867C14.9762 15.5733 15.5967 15.5733 15.9795 15.1867C16.3756 14.8 16.3756 14.1733 15.9927 13.7867L12.204 9.94666C12.1116 9.85332 12.006 9.78666 11.8871 9.73332C11.7683 9.67999 11.6495 9.65332 11.5175 9.65332ZM16.193 1.87466C16.193 1.29999 16.8821 1.01466 17.2768 1.42932C18.7052 2.92799 21.199 5.54799 22.593 7.01199C22.9772 7.41599 22.6947 8.08666 22.1389 8.08799C21.0537 8.09199 19.7759 8.08799 18.8557 8.07866C17.3957 8.07866 16.193 6.86399 16.193 5.38932V1.87466Z"
            fill="black"
          />
        </svg>
        <span className="font_size_18 line_height_27 font_weight_600 margin_left_20">Đăng bài</span>
      </div>
      <div
        className="close display_flex position_absolute right_0 cursor_pointer right_xs_10"
        onClick={() => {
          props.setIsOpenPopup(false);
        }}
      >
        <svg width="28" height="28" viewBox="0 0 28 28" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path
            fillRule="evenodd"
            clipRule="evenodd"
            d="M13.9993 0.666656C21.3513 0.666656 27.3327 6.64799 27.3327 14C27.3327 21.352 21.3513 27.3333 13.9993 27.3333C6.64735 27.3333 0.666016 21.352 0.666016 14C0.666016 6.64799 6.64735 0.666656 13.9993 0.666656ZM13.9993 2.66666C7.75002 2.66666 2.66602 7.75066 2.66602 14C2.66602 20.2493 7.75002 25.3333 13.9993 25.3333C20.2487 25.3333 25.3327 20.2493 25.3327 14C25.3327 7.75066 20.2487 2.66666 13.9993 2.66666Z"
            fill="black"
          />
          <path
            fillRule="evenodd"
            clipRule="evenodd"
            d="M10.7737 9.28594C10.3626 8.87487 9.69609 8.87487 9.28501 9.28594C8.87393 9.69702 8.87393 10.3635 9.28501 10.7746L12.5104 14L9.28501 17.2254C8.87393 17.6365 8.87393 18.303 9.28501 18.714C9.69609 19.1251 10.3626 19.1251 10.7737 18.714L13.9991 15.4886L17.2245 18.714C17.6355 19.1251 18.302 19.1251 18.7131 18.714C19.1242 18.303 19.1242 17.6365 18.7131 17.2254L15.4877 14L18.7131 10.7746C19.1242 10.3635 19.1242 9.69702 18.7131 9.28594C18.302 8.87487 17.6355 8.87487 17.2245 9.28594L13.9991 12.5113L10.7737 9.28594Z"
            fill="black"
          />
        </svg>
      </div>
      <div className="margin_top_60 display_flex margin_x_260 justify_content_sb position_relative z_index_1 margin_x_xs_22">
        {steps.map((step) => {
          return (
            <div className="width_124 display_flex flex_direction_col align_items_center no_select" key={step.id}>
              <div
                className={classnames(
                  { bg_red: step.id <= props.step, bg_grey: step.id > props.step },
                  `width_48 height_48 border_radius_50_percent display_flex justify_content_center align_items_center cursor_pointer
                  height_xs_24 width_xs_24`,
                )}
                onClick={() => {
                  if (step.id < props.step) {
                    props.backStep(step.id);
                  } else {
                    props.chooseStep(step.id);
                  }
                }}
              >
                <span className="color_white font_size_22 font_weight_600 margin_bottom_4 font_size_xs_16">
                  {step.id}
                </span>
              </div>
              <div
                className="padding_top_16 cursor_pointer"
                onClick={() => {
                  if (step.id < props.step) {
                    props.backStep(step.id);
                  } else {
                    props.chooseStep(step.id);
                  }
                }}
              >
                <span
                  className={classnames(
                    { color_grey: step.id > props.step },
                    'font_size_14 font_weight_600 font_size_xs_12',
                  )}
                >
                  {step.title}
                </span>
              </div>
            </div>
          );
        })}
        <div
          className="disable_bar position_absolute top_23 left_0 translate_0_-50_percent width_100_percent padding_x_40 z_index_-2 
        top_xs_12 padding_x_xs_27"
        >
          <div className="bg_grey height_10 height_xs_6 width_100_percent" ref={$disablebar}></div>
        </div>
        <div
          className="disable_bar position_absolute top_23 left_0 translate_0_-50_percent width_100_percent padding_x_40 z_index_-1 
        top_xs_12 padding_x_xs_27"
        >
          <div className="bg_red height_10 height_xs_6" style={{ width: activebarWidth }}></div>
        </div>
      </div>
    </div>
  );
};
export default HeaderPopup;
