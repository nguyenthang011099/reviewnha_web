import React, { useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import InputRadio from '../../../components/base/input-radio/InputRadio';
import InputText from '../../../components/base/input-text/InputText';
import { RootState } from '../../../reducers';
import { getDistricts, getProvinces, getWards } from '../../../reducers/location/actions';
import { getProjectList, getSellListingPartTwo } from '../../../reducers/sell-listing/actions';
import { OptionInputRadio } from '../../../types/type.base';
import { District, Province, Ward } from '../../../types/type.location';
import { Project } from '../../../types/type.sell-listing';
import dynamic from 'next/dynamic';
const LocationPicker: any = dynamic(() => import('react-location-picker'), { ssr: false });
import axios from 'axios';
import { convertURL } from '../../../utils/format-url';

interface Props {
  nextStage: (stage: number) => void;
  province: OptionInputRadio;
  setProvince: (value: OptionInputRadio) => void;
  district: OptionInputRadio;
  setDistrict: (value: OptionInputRadio) => void;
  ward: OptionInputRadio;
  setWard: (value: OptionInputRadio) => void;
  transaction: OptionInputRadio;
  setTransaction: (value: OptionInputRadio) => void;
  productType: OptionInputRadio;
  setProductType: (value: OptionInputRadio) => void;
  project: OptionInputRadio;
  setProject: (value: OptionInputRadio) => void;
  address: string;
  setAddress: (value: string) => void;
  editMode?: boolean;
  location: { lat: number; lng: number };
  setLocation: (_value: { lat: number; lng: number }) => void;
  isOpen?: boolean;
  editId?: number;
  stepHeaderChoose: number | null;
  setStepHeaderChoose: (value: number | null) => void;
}

const BasicInfo: React.FC<Props> = (props) => {
  const {
    province,
    setProvince,
    district,
    setDistrict,
    ward,
    setWard,
    transaction,
    setTransaction,
    productType,
    setProductType,
    project,
    setProject,
    address,
    setAddress,
    editMode,
    isOpen,
    location,
    setLocation,
    editId,
  } = props;
  const dispatch = useDispatch();
  const provinces: Province[] = useSelector((state: RootState) => state.location.provinces);
  const districts: District[] = useSelector((state: RootState) => state.location.districts);
  const wards: Ward[] = useSelector((state: RootState) => state.location.wards);
  const stepOneData = useSelector((state: RootState) => state.sellListing.stepOneData);
  // warning
  const [warningTransaction, setWarningTransaction] = useState<boolean>(false);
  const [warningProduct, setWarningProduct] = useState<boolean>(false);
  const [warningProvince, setWarningProvince] = useState<boolean>(false);
  const [warningDistrict, setWarningDistrict] = useState<boolean>(false);
  const [warningWard, setWarningWard] = useState<boolean>(false);
  const [warningAddress, setWarningAddress] = useState<boolean>(false);
  const projectList = useSelector((state: RootState) => state.sellListing.projectList);
  const [projectFilter, setProjectFilter] = useState<OptionInputRadio[]>([]);
  const $nextStepBtn = useRef(null);

  useEffect(() => {
    setProjectFilter(
      projectList.map((prj: Project) => {
        return {
          value: prj.id,
          title: prj.name,
          hidden: false,
        };
      }),
    );
  }, [projectList]);

  useEffect(() => {
    if (!projectList.length) {
      dispatch(getProjectList());
    }
    dispatch(getProvinces());
  }, []);

  useEffect(() => {
    if (props.stepHeaderChoose === 2) {
      props.setStepHeaderChoose(null);
      $nextStepBtn && $nextStepBtn.current.click();
    }
  }, [props.stepHeaderChoose]);

  const changeProvince = (province: OptionInputRadio) => {
    setProvince(province);
    const formatProvince = province.title ? province.title.replace(/ /g, '+') : '';
    const location = [formatProvince, '+viet+nam'].join(',');
    getPositionWhenSelect(location);
    // get district
    dispatch(getDistricts(province.value));
    setDistrict({
      value: '',
      title: '',
    });
    setWard({
      value: '',
      title: '',
    });
  };

  useEffect(() => {
    if (editMode && isOpen) {
      dispatch(getDistricts(province.value));
      dispatch(getWards(district.value));
    }
  }, [editId]);

  const changeDistrict = (district: OptionInputRadio) => {
    setDistrict(district);
    const formatDistrict = district.title ? district.title.replace(/ /g, '+') : '';
    const formatProvince = province.title ? province.title.replace(/ /g, '+') : '';
    const locationText = [formatDistrict, formatProvince, '+viet+nam'].join(',');
    getPositionWhenSelect(locationText);
    // get ward
    dispatch(getWards(district.value));
    setWard({
      value: '',
      title: '',
    });
  };

  const changeWard = (ward: OptionInputRadio) => {
    setWard(ward);
    const formatWard = ward.title ? ward.title.replace(/ /g, '+') : '';
    const formatDistrict = district.title ? district.title.replace(/ /g, '+') : '';
    const formatProvince = province.title ? province.title.replace(/ /g, '+') : '';
    const location = [formatWard, formatDistrict, formatProvince, '+viet+nam'].join(',');
    getPositionWhenSelect(location);
  };

  const handleLocationChange = ({ position }) => {
    setLocation({
      lat: position.lat,
      lng: position.lng,
    });
  };

  const onChangeProductType = (type: OptionInputRadio) => {
    setProductType(type);
    dispatch(getSellListingPartTwo(type.value));
  };

  const setIconAddressInput = () => {
    return (
      <div className="padding_right_3">
        <svg width="14" height="20" viewBox="0 0 14 20" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path
            d="M3.53162 0.936771C5.71648 -0.332726 8.40203 -0.310537 10.5664 0.994894C12.7095 2.32691 14.012 4.70418 13.9999 7.26144C13.95 9.80194 12.5533 12.19 10.8075 14.0361C9.79983 15.1064 8.67261 16.0528 7.44884 16.856C7.32281 16.9289 7.18475 16.9777 7.04148 17C6.9036 16.9941 6.76932 16.9534 6.65075 16.8814C4.78243 15.6746 3.14333 14.134 1.81233 12.334C0.698587 10.8314 0.0658345 9.01601 0 7.13442L0.00498622 6.86069C0.0959221 4.40464 1.4248 2.16093 3.53162 0.936771ZM7.90726 5.03477C7.01907 4.65723 5.99504 4.86235 5.31332 5.55435C4.63159 6.24635 4.42664 7.28872 4.79416 8.19478C5.16168 9.10084 6.02918 9.69184 6.9916 9.69185C7.6221 9.69638 8.22819 9.44383 8.67481 8.99048C9.12143 8.53712 9.37148 7.92064 9.36926 7.27838C9.37261 6.29804 8.79546 5.41231 7.90726 5.03477Z"
            fill="#272727"
          />
          <path
            d="M7 20C9.76142 20 12 19.5523 12 19C12 18.4477 9.76142 18 7 18C4.23858 18 2 18.4477 2 19C2 19.5523 4.23858 20 7 20Z"
            fill="#272727"
          />
        </svg>
      </div>
    );
  };

  const renderWarning = () => {
    if (warningTransaction) {
      return 'Vui lòng nhập thông tin giao dịch';
    } else if (warningProduct) {
      return 'Vui lòng nhập thông tin loại sản phẩm';
    } else if (warningProvince) {
      return 'Vui lòng nhập thông tin tỉnh thành';
    } else if (warningDistrict) {
      return 'Vui lòng nhập thông tin quuận / huyện';
    } else if (warningWard) {
      return 'Vui lòng nhập thông tin xã / phường';
    } else if (warningAddress) {
      return 'Vui lòng nhập địa chỉ';
    }
  };

  const nextStep = (e: React.FormEvent<HTMLButtonElement>) => {
    e.preventDefault();
    if (transaction.value && productType.value && province.value && district.value && ward.value && address) {
      props.nextStage(2);
    } else if (!transaction.value) {
      setWarningTransaction(true);
    } else if (!productType.value) {
      setWarningProduct(true);
    } else if (!province.value) {
      setWarningProvince(true);
    } else if (!district.value) {
      setWarningDistrict(true);
    } else if (!ward.value) {
      setWarningWard(true);
    } else if (!address) {
      setWarningAddress(true);
    }
  };

  const getPositionWhenSelect = async (location: string) => {
    const res = await axios.get(
      `https://maps.googleapis.com/maps/api/geocode/json?address=${location}&key=${process.env.NEXT_PUBLIC_GOOGLE_API_KEY}`,
    );
    if (res.status === 200) {
      if (res?.data?.results) {
        const location = res.data.results[0];
        setLocation({
          lat: location?.geometry?.location?.lat,
          lng: location?.geometry?.location?.lng,
        });
      }
    }
  };

  const onProjectAutoComplete = (project: OptionInputRadio) => {
    setProject(project);
    searchProject(convertURL(project.title));
  };

  const searchProject = (q: string) => {
    let bestQueryIndex = +Infinity;

    const newProjectFilter = projectFilter.map((prj: OptionInputRadio) => {
      const queryIndex = convertURL(prj.title).indexOf(q);
      if (queryIndex > -1 && queryIndex < bestQueryIndex) {
        bestQueryIndex = queryIndex;
      }
      return {
        ...prj,
        hidden: q ? queryIndex === -1 : false,
        queryIndex,
        bestQueryIndex,
      };
    });

    newProjectFilter.sort((w1, w2) => {
      const query = convertURL(q.trim());
      if (w1.queryIndex > w2.queryIndex) {
        return 1;
      }
      if (w1.queryIndex === w2.queryIndex) {
        return editDistance(w1.title, query) - editDistance(w2.title, query);
      }
      return -1;
    }) || [];

    if (q) {
      newProjectFilter.sort((prj1: any, prj2: any) => {
        return prj1.bestQueryIndex - prj2.bestQueryIndex;
      });
    }

    setProjectFilter(newProjectFilter);
  };

  const editDistance = (s1: string, s2: string) => {
    if (!s1 || !s2) {
      return 0;
    }
    s1 = s1.toLowerCase();
    s2 = s2.toLowerCase();

    const costs = new Array();
    for (let i = 0; i <= s1.length; i++) {
      let lastValue = i;
      for (let j = 0; j <= s2.length; j++) {
        if (i == 0) costs[j] = j;
        else {
          if (j > 0) {
            let newValue = costs[j - 1];
            if (s1.charAt(i - 1) != s2.charAt(j - 1)) newValue = Math.min(Math.min(newValue, lastValue), costs[j]) + 1;
            costs[j - 1] = lastValue;
            lastValue = newValue;
          }
        }
      }
      if (i > 0) costs[s2.length] = lastValue;
    }
    return costs[s2.length];
  };

  return (
    <form className="basic_info padding_x_217 display_flex flex_direction_col justify_content_sb height_100_percent padding_x_xs_22">
      <div className="display_flex flex_wrap">
        <div className="flex_1 width_xs_100_percent">
          <div className="margin_bottom_16">
            <InputRadio
              value={transaction}
              setValue={setTransaction}
              label="Giao dịch:"
              options={stepOneData.transactionTypes}
              placeholder="Bán nhà"
              isWarning={warningTransaction}
              setIsWarning={setWarningTransaction}
            ></InputRadio>
          </div>
          <div className="margin_bottom_16">
            <InputRadio
              value={productType}
              setValue={onChangeProductType}
              label="Sản phẩm:"
              options={stepOneData.productTypes}
              placeholder="Căn hộ/ Chung cư"
              isWarning={warningProduct}
              setIsWarning={setWarningProduct}
            ></InputRadio>
          </div>
          <div className="margin_bottom_16">
            <InputRadio
              value={province}
              setValue={changeProvince}
              label="Tỉnh thành:"
              options={provinces.map((province: Province) => {
                return {
                  value: province.code,
                  title: province.name,
                };
              })}
              placeholder="Chọn tỉnh thành"
              isWarning={warningProvince}
              setIsWarning={setWarningProvince}
            ></InputRadio>
          </div>
          <div
            className="margin_bottom_16"
            onClick={() => {
              if (!province.value) {
                setWarningProvince(true);
              }
            }}
          >
            <InputRadio
              value={district}
              setValue={changeDistrict}
              label="Quận/ huyện:"
              options={districts.map((district: District) => {
                return {
                  value: district.code,
                  title: district.nameWithType,
                };
              })}
              placeholder="Chọn quận/ huyện"
              isWarning={warningDistrict}
              setIsWarning={setWarningDistrict}
              disabledClick={!province.value}
            ></InputRadio>
          </div>
          <div
            className="margin_bottom_16"
            onClick={() => {
              if (!district.value) {
                setWarningDistrict(true);
              }
            }}
          >
            <InputRadio
              value={ward}
              setValue={changeWard}
              label="Xã/ Phường:"
              options={wards.map((ward: Ward) => {
                return {
                  value: ward.code,
                  title: ward.nameWithType,
                };
              })}
              placeholder="Chọn xã/ phường"
              isWarning={warningWard}
              setIsWarning={setWarningWard}
              disabledClick={!district.value}
            ></InputRadio>
          </div>
          <div className="margin_bottom_16">
            <InputRadio
              value={project}
              setValue={onProjectAutoComplete}
              label="Dự án:"
              options={projectFilter}
              placeholder="Chọn Dự án"
              inputEditable={true}
              isSearch={true}
            ></InputRadio>
          </div>
          <div className="margin_bottom_16">
            <InputText
              required={true}
              value={address}
              setValue={setAddress}
              label="Địa chỉ:"
              name="address"
              type="text"
              setIcon={setIconAddressInput}
              placeholder="Nhập địa chỉ"
              isWarning={warningAddress}
              setIsWarning={setWarningAddress}
            ></InputText>
          </div>
          <div className="margin_bottom_16 margin_left_25">
            <span className="font_size_12 font_weight_400 color_red">{renderWarning()}</span>
          </div>
        </div>
        <div
          className="width_277 height_432 margin_left_20 border_radius_25 padding_8 width_xs_100_percent margin_left_xs_0 margin_bottom_xs_24
          height_xs_250"
          style={{ boxShadow: '1px 1px 2px 1px rgba(23, 23, 23, 0.16)' }}
        >
          <LocationPicker
            containerElement={(<div style={{ height: '100%' }} />) as any}
            mapElement={<div style={{ height: '100%', borderRadius: '25px' }} />}
            defaultPosition={location}
            onChange={handleLocationChange}
            radius={1}
          />
        </div>
      </div>
      <div className="display_flex justify_content_sb">
        <button
          className="border_none width_216 height_32 font_size_14 line_height_17 font_weight_600 bg_red color_white border_radius_25 opacity_0"
          type="button"
        >
          Preview
        </button>
        <button
          className="border_none width_120 height_32 font_size_14 line_height_17 font_weight_600 bg_black color_white border_radius_25"
          onClick={nextStep}
          ref={$nextStepBtn}
        >
          Tiếp theo
        </button>
      </div>
    </form>
  );
};

export default BasicInfo;
