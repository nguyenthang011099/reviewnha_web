import React, { useEffect, useRef, useState } from 'react';
import { useSelector } from 'react-redux';
import InputRadio from '../../../components/base/input-radio/InputRadio';
import InputText from '../../../components/base/input-text/InputText';
import { RootState } from '../../../reducers';
import { OptionInputRadio } from '../../../types/type.base';
import { numberWithDot } from '../../../utils/format-number';
// import { Status } from '../../../types/type.status';

interface Props {
  nextStage: (stage: number) => void;
  type: OptionInputRadio;
  setType: (value: OptionInputRadio) => void;
  area: number | string;
  setArea: (value: number | string) => void;
  direction: OptionInputRadio;
  setDirection: (value: OptionInputRadio) => void;
  furniture: OptionInputRadio;
  setFurniture: (value: OptionInputRadio) => void;
  juridical: OptionInputRadio;
  setJuridical: (value: OptionInputRadio) => void;
  numberOfBedroom: number | string;
  setNumberOfBedroom: (value: number | string) => void;
  numberOfBathroom: number | string;
  setNumberOfBathroom: (value: number | string) => void;
  price: number | string;
  setPrice: (value: number | string) => void;
  stepHeaderChoose: number | null;
  setStepHeaderChoose: (value: number | null) => void;
}

const DetailInfo: React.FC<Props> = (props) => {
  const {
    type,
    setType,
    area,
    setArea,
    direction,
    setDirection,
    furniture,
    setFurniture,
    juridical,
    setJuridical,
    numberOfBedroom,
    setNumberOfBedroom,
    numberOfBathroom,
    setNumberOfBathroom,
    price,
    setPrice,
  } = props;
  const stepTwoData = useSelector((state: RootState) => state.sellListing.stepTwoData);
  // const statusPostStepTwo: Status = useSelector((state: RootState) => state.sellListing.submitStatusStepTwo as Status);
  const [warningType, setWarningType] = useState<boolean>(false);
  const [warningArea, setWarningArea] = useState<boolean>(false);
  const [warningDirection, setWarningDirection] = useState<boolean>(false);
  const [warningJuridical, setWarningJuridical] = useState<boolean>(false);
  const $nextStepBtn = useRef(null);

  useEffect(() => {
    if (props.stepHeaderChoose === 3) {
      props.setStepHeaderChoose(null);
      $nextStepBtn && $nextStepBtn.current.click();
    }
  }, [props.stepHeaderChoose]);

  /**
   * renderIconArea: icon diện tích
   */
  const renderIconArea = () => {
    return (
      <div>
        <span>/m2</span>
      </div>
    );
  };

  /**
   * renderIconPrice: icon price
   */
  const renderIconPrice = () => {
    return (
      <div>
        <span>VND</span>
      </div>
    );
  };

  /**
   * setAreaValue
   * @param value value cua area
   */
  const setAreaValue = (value: number | string) => {
    if (value && Number(value) > 0) {
      setArea(Number(value));
    } else {
      setArea('');
    }
  };

  const setIconRoom = () => {
    return (
      <div>
        <span>phòng</span>
      </div>
    );
  };

  /**
   * setBedRoomValue
   * @param value value cua bedroom
   */
  const setBedRoomValue = (value: number | string) => {
    if (value && Number(value) > 0) {
      setNumberOfBedroom(Number(value));
    } else {
      setNumberOfBedroom('');
    }
  };

  /**
   * setBathRoomValue
   * @param value value cua bathroom
   */
  const setBathRoomValue = (value: number | string) => {
    if (value && Number(value) > 0) {
      setNumberOfBathroom(Number(value));
    } else {
      setNumberOfBathroom('');
    }
  };

  /**
   * setPriceValue
   * @param value value cua bathroom
   */
  const setPriceValue = (value: number) => {
    if (value && Number(String(value).replace(/\./g, '')) > 0) {
      setPrice(numberWithDot(Number(String(value).replace(/\./g, ''))));
    } else if (!value) {
      setPrice('');
    }
  };

  /**
   * renderWarning: render warning
   */
  const renderWarning = () => {
    if (warningType) {
      return 'Vui lòng nhập thông tin loại hình';
    } else if (warningArea) {
      return 'Vui lòng nhập thông tin diện tích';
    } else if (warningDirection) {
      return 'Vui lòng nhập thông tin hướng';
    } else if (warningJuridical) {
      return 'Vui lòng nhập thông tin pháp lý';
    }
  };

  const nextStep = (e: React.FormEvent<HTMLButtonElement>) => {
    e.preventDefault();
    if (type.value && direction.value && area && juridical.value) {
      props.nextStage(3);
    } else if (!type.value) {
      setWarningType(true);
    } else if (!area) {
      setWarningArea(true);
    } else if (!direction.value) {
      setWarningDirection(true);
    } else if (!juridical.value) {
      setWarningJuridical(true);
    }
  };

  return (
    <div className="detail_info padding_x_240 display_flex flex_direction_col justify_content_sb height_100_percent padding_x_xs_22">
      <div>
        <div className="margin_bottom_16">
          <InputRadio
            value={type}
            setValue={setType}
            label="Loại hình:"
            options={stepTwoData.types}
            placeholder="Chọn loại hình"
            isWarning={warningType}
            setIsWarning={setWarningType}
          ></InputRadio>
        </div>
        <div className="margin_bottom_16">
          <InputText
            value={area}
            setValue={setAreaValue}
            label="Diện tích:"
            type="number"
            min={0}
            name="area"
            setIcon={renderIconArea}
            required={true}
            placeholder="Nhập diện tích"
            isWarning={warningArea}
            setIsWarning={setWarningArea}
            isHiddenClear={true}
          ></InputText>
        </div>
        <div className="margin_bottom_16">
          <InputRadio
            value={direction}
            setValue={setDirection}
            label="Hướng:"
            options={stepTwoData.directions}
            placeholder="Chọn hướng"
            isWarning={warningDirection}
            setIsWarning={setWarningDirection}
          ></InputRadio>
        </div>
        <div className="margin_bottom_16">
          <InputText
            value={numberOfBedroom}
            setValue={setBedRoomValue}
            label="Phòng ngủ:"
            type="number"
            name="area"
            placeholder="Nhập số phòng ngủ"
            isHiddenClear={true}
            setIcon={setIconRoom}
          ></InputText>
        </div>
        <div className="margin_bottom_16">
          <InputText
            value={numberOfBathroom}
            setValue={setBathRoomValue}
            label="Vệ sinh:"
            type="number"
            name="area"
            placeholder="Nhập số nhà vệ sinh"
            isHiddenClear={true}
            setIcon={setIconRoom}
          ></InputText>
        </div>
        <div className="margin_bottom_16">
          <InputRadio
            value={furniture}
            setValue={setFurniture}
            label="Nội thất:"
            options={stepTwoData.furnitures}
            placeholder="Chọn tình trạng bàn giao nội thất"
          ></InputRadio>
        </div>
        <div className="margin_bottom_16">
          <InputRadio
            value={juridical}
            setValue={setJuridical}
            label="Pháp lý:"
            options={stepTwoData.juridicals}
            placeholder="Chọn/ điền giấy tờ pháp lý hiện có"
            inputEditable={true}
            isWarning={warningJuridical}
            setIsWarning={setWarningJuridical}
          ></InputRadio>
        </div>
        <div className="margin_bottom_16">
          <InputText
            value={price}
            setValue={setPriceValue}
            label="Giá:"
            type="text"
            name="price"
            placeholder="Nhập giá"
            setIcon={renderIconPrice}
            isHiddenClear={true}
          ></InputText>
        </div>
        <div className="margin_bottom_16 margin_left_25">
          <span className="font_size_12 font_weight_400 color_red">{renderWarning()}</span>
        </div>
      </div>
      <div className="display_flex justify_content_sb">
        <button
          className="border_none width_216 height_32 font_size_14 line_height_17 font_weight_600 bg_red color_white border_radius_25 opacity_0
          width_xs_1"
          type="button"
        >
          Preview
        </button>
        <div className="display_flex">
          <button
            type="button"
            className="margin_right_16 border_color_black border_width_1 width_120 height_32 font_size_14 line_height_17 font_weight_600 bg_white border_radius_25"
            onClick={() => {
              props.nextStage(1);
            }}
          >
            Quay lại
          </button>
          <button
            type="submit"
            className="border_none width_120 height_32 font_size_14 line_height_17 font_weight_600 bg_black color_white border_radius_25"
            onClick={nextStep}
            ref={$nextStepBtn}
          >
            Tiếp theo
          </button>
        </div>
      </div>
    </div>
  );
};

export default DetailInfo;
