import React, { useEffect, useState } from 'react';
import { SellListing } from '../../../types/type.sell-listing';
import { renderAddress, renderPrice } from '../../../utils/sell-listing-helper';
import dayjs from 'dayjs';
import Link from 'next/link';
import ImageViewPopup from '../../../components/image-view/ImageViewPopup';
import YoutubePlayer from '../../blogs/components/item/YoutubePlayer';
import { FacebookShareButton } from 'react-share';
import { useRouter } from 'next/router';

interface Props {
  postDetail: SellListing;
}

const Title: React.FC<Props> = (props) => {
  const [images, setImages] = useState<{ url: string }[]>([]);
  const [isOpenViewImages, setIsOpenViewImages] = useState<boolean>(false);
  const [showPhone, setShowPhone] = useState<boolean>(false);
  const [linkShare, setLinkShare] = useState<string>('');
  const router = useRouter();

  useEffect(() => {
    const hostname = window.location.origin;
    setLinkShare(hostname + router.asPath);
  }, []);

  const setViewImages = (images: { url: string }[]) => {
    setImages(images);
    setIsOpenViewImages(true);
  };

  return (
    <div className="container margin_bottom_96  padding_x_xs_22 margin_bottom_xs_36">
      <ImageViewPopup images={images} isOpen={isOpenViewImages} setIsOpen={setIsOpenViewImages}></ImageViewPopup>
      <div className="post_title width_100_percent">
        <div className="display_flex justify_content_sb width_100_percent">
          <div className="info width_100_percent">
            {/* name */}
            <div className="display_flex margin_bottom_24 align_items_fs flex_wrap margin_bottom_xs_4">
              <div className="name col_6 col_xs_12">
                <h2 className="margin_y_0 font_size_32 line_height_43 font_weight_700 font_weight_xs_600 font_size_xs_22">
                  {props.postDetail.title}
                </h2>
              </div>
              <div className="share col_6 display_flex align_items_center justify_content_fe col_xs_12 display_xs_none">
                <FacebookShareButton url={linkShare}>
                  <div
                    className="display_flex align_items_center justify_content_center padding_y_6 width_112 border_width_1 border_style_solid
                border_radius_25 cursor_pointer bg_white"
                    style={{ borderColor: 'rgba(59, 65, 68, 0.5)' }}
                  >
                    <svg width={13} height={11} viewBox="0 0 13 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path
                        d="M12.0664 6.65366C12.6072 6.00467 12.6072 5.06199 12.0664 4.41301L8.88172 0.591398C8.43265 0.052512 7.55556 0.370065 7.55556 1.07154V2.10096C7.55556 2.46628 7.29035 2.77565 6.93464 2.85888C3.38961 3.68842 1.25973 6.18248 0.053524 9.16152C-0.257778 9.93036 0.936957 10.5073 1.60053 10.0096C3.03231 8.93571 4.74563 8.36792 6.8056 8.25933C7.21924 8.23753 7.55556 8.57579 7.55556 8.99V9.99513C7.55556 10.6966 8.43265 11.0142 8.88172 10.4753L12.0664 6.65366Z"
                        fill="#3B4144"
                      />
                    </svg>
                    <span className="font_size_14 line_height_18 margin_left_12">Chia sẻ</span>
                  </div>
                </FacebookShareButton>
              </div>
            </div>
            <div className="display_flex width_100_percent flex_wrap">
              <div className="col_6 col_xs_12 margin_bottom_xs_16">
                {/* address  */}
                <div className="info_item display_flex align_items_fs margin_bottom_4">
                  <div className="width_30 display_flex margin_top_7 flex_none">
                    <svg width={12} height={16} viewBox="0 0 10 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path
                        d="M2.68779 0.957766C4.14436 0.111435 5.93473 0.126227 7.37763 0.996515C8.80635 1.88453 9.67468 3.46937 9.66665 5.17421C9.63334 6.86788 8.70224 8.45992 7.53835 9.69065C6.8666 10.4042 6.11512 11.0351 5.29927 11.5706C5.21524 11.6192 5.12321 11.6517 5.0277 11.6666C4.93577 11.6627 4.84625 11.6355 4.76721 11.5875C3.52166 10.783 2.42893 9.75594 1.54159 8.55589C0.799099 7.55416 0.377264 6.34392 0.333374 5.08953L0.336698 4.90705C0.397322 3.26968 1.28324 1.77387 2.68779 0.957766ZM5.60488 3.68977C5.01275 3.43808 4.33007 3.57482 3.87559 4.03615C3.4211 4.49748 3.28447 5.1924 3.52948 5.79644C3.7745 6.40048 4.35283 6.79448 4.99444 6.79448C5.41478 6.7975 5.81883 6.62914 6.11658 6.3269C6.41433 6.02467 6.58103 5.61368 6.57955 5.18551C6.58178 4.53194 6.19701 3.94146 5.60488 3.68977Z"
                        fill="#3B4144"
                      />
                      <path
                        d="M5.00008 13.6666C6.84103 13.6666 8.33341 13.3681 8.33341 12.9999C8.33341 12.6317 6.84103 12.3333 5.00008 12.3333C3.15913 12.3333 1.66675 12.6317 1.66675 12.9999C1.66675 13.3681 3.15913 13.6666 5.00008 13.6666Z"
                        fill="#3B4144"
                      />
                    </svg>
                  </div>
                  <span className="display_block line_height_27 text_transform_capitalize">
                    {renderAddress(props.postDetail)}
                  </span>
                </div>
                {/* publish date  */}
                <div className="info_item display_flex align_items_center margin_bottom_4">
                  <div className="width_30 display_flex">
                    <svg width={14} height={14} viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path
                        d="M7.00004 0.333496C10.6867 0.333496 13.6667 3.32016 13.6667 7.00016C13.6667 10.6868 10.6867 13.6668 7.00004 13.6668C3.32004 13.6668 0.333374 10.6868 0.333374 7.00016C0.333374 3.32016 3.32004 0.333496 7.00004 0.333496ZM6.76671 3.62016C6.49337 3.62016 6.26671 3.84016 6.26671 4.12016V7.48683C6.26671 7.66016 6.36004 7.82016 6.51337 7.9135L9.12671 9.4735C9.20671 9.52016 9.29337 9.54683 9.38671 9.54683C9.55337 9.54683 9.72004 9.46016 9.81337 9.30016C9.95337 9.06683 9.88004 8.76016 9.64004 8.6135L7.26671 7.20016V4.12016C7.26671 3.84016 7.04004 3.62016 6.76671 3.62016Z"
                        fill="#3B4144"
                      />
                    </svg>
                  </div>
                  <span className="display_block line_height_27">
                    Ngày đăng: {dayjs(props.postDetail.publishedDate).format('DD/MM/YYYY')}
                  </span>
                </div>
              </div>
              <div className="col_6 col_xs_12 display_flex justify_content_fe">
                {/* price */}
                <div className="display_flex width_xs_100_percent">
                  {!showPhone ? (
                    <div
                      className="border_width_1 border_style_solid font_size_14 font_weight_600 line_height_17 cursor_pointer margin_right_24 margin_right_xs_8
                  display_flex justify_content_center align_items_center border_radius_25 no_select height_40 width_178 width_xs_50_percent"
                      onClick={() => {
                        setShowPhone(true);
                      }}
                    >
                      Gọi ngay
                    </div>
                  ) : (
                    <div className="line_height_28 margin_right_24 margin_right_xs_8 display_flex height_40 align_items_center width_xs_50_percent">
                      <svg
                        width={14}
                        height={14}
                        viewBox="0 0 14 14"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                        className="margin_right_8"
                      >
                        <path
                          fillRule="evenodd"
                          clipRule="evenodd"
                          d="M6.68718 7.31488C9.34657 9.97353 9.94986 6.89777 11.6431 8.58983C13.2755 10.2218 14.2138 10.5488 12.1455 12.6164C11.8864 12.8246 10.2404 15.3295 4.45575 9.5464C-1.32964 3.76263 1.17374 2.11492 1.382 1.85593C3.45526 -0.217477 3.77659 0.726215 5.40901 2.35818C7.10226 4.05096 4.02779 4.65624 6.68718 7.31488Z"
                          fill="#3b4144"
                        />
                      </svg>
                      <a href={`tel:${props.postDetail?.contactPhone ?? props.postDetail?.seller?.phone}`}>
                        {props.postDetail?.contactPhone ?? props.postDetail?.seller?.phone}
                      </a>
                    </div>
                  )}
                  <div
                    className="display_flex justify_content_sb padding_x_24 padding_y_12 bg_white border_radius_25 width_200 align_items_center
                  height_40 width_178 bg_black color_white width_xs_50_percent"
                  >
                    <div className="display_flex align_items_center">
                      <div className="display_flex margin_right_10">
                        <svg width={20} height={18} viewBox="0 0 20 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <path
                            d="M14.5156 0C17.9644 0 20 1.98459 20 5.3818H15.7689V5.41647C13.8052 5.41647 12.2133 6.96849 12.2133 8.883C12.2133 10.7975 13.8052 12.3495 15.7689 12.3495H20V12.6615C20 16.0154 17.9644 18 14.5156 18H5.48444C2.03556 18 0 16.0154 0 12.6615V5.33847C0 1.98459 2.03556 0 5.48444 0H14.5156ZM19.2533 6.87241C19.6657 6.87241 20 7.19834 20 7.60039V10.131C19.9952 10.5311 19.6637 10.8543 19.2533 10.8589H15.8489C14.8548 10.872 13.9855 10.2084 13.76 9.26432C13.6471 8.67829 13.8056 8.07357 14.1931 7.61222C14.5805 7.15087 15.1573 6.88007 15.7689 6.87241H19.2533ZM16.2489 8.04237H15.92C15.7181 8.04005 15.5236 8.11664 15.38 8.25504C15.2364 8.39344 15.1556 8.58213 15.1556 8.77901C15.1555 9.19205 15.4964 9.52823 15.92 9.53298H16.2489C16.6711 9.53298 17.0133 9.1993 17.0133 8.78767C17.0133 8.37605 16.6711 8.04237 16.2489 8.04237ZM10.3822 3.89119H4.73778C4.31903 3.89116 3.9782 4.2196 3.97333 4.62783C3.97333 5.04087 4.31415 5.37705 4.73778 5.3818H10.3822C10.8044 5.3818 11.1467 5.04812 11.1467 4.6365C11.1467 4.22487 10.8044 3.89119 10.3822 3.89119Z"
                            fill="white"
                          />
                        </svg>
                      </div>
                      <span className="display_block font_size_14 line_height_17">Giá:</span>
                    </div>
                    <div className="price">
                      <span className="font_weight_600">{renderPrice(props?.postDetail?.price)}</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="height_574 border_radius_24 display_flex margin_top_52 margin_top_xs_36 height_xs_auto display_xs_block">
        <div className="thumnail height_100_percent flex_1 border_radius_18 padding_24 position_relative height_xs_220 margin_bottom_xs_8">
          <div
            className="position_absolute left_0 top_0 width_100_percent height_100_percent cursor_pointer"
            onClick={() => {
              if (!props.postDetail?.videoUrl) {
                setViewImages(props.postDetail.images);
              }
            }}
          >
            {props.postDetail?.videoUrl ? (
              <YoutubePlayer
                url={props.postDetail?.videoUrl}
                style={{ width: '100%', height: '100%', objectFit: 'cover', borderRadius: '18px' }}
              ></YoutubePlayer>
            ) : (
              <img
                src={props.postDetail.images?.length ? props.postDetail.images[0].url : props.postDetail.thumbnail}
                alt="thumbnail"
                className="width_100_percent height_100_percent object_fit_cover border_radius_18"
              ></img>
            )}
          </div>
          <Link
            href={{ pathname: '/seller-sell-listings-page', query: { sellerId: props.postDetail?.seller?.id } }}
            as={`/seller/${props.postDetail?.seller?.id}/sell-listings`}
          >
            <a
              className="post_user_info border_radius_25 padding_8 display_flex align_items_center padding_right_24"
              style={{
                background: 'linear-gradient(93.91deg, rgba(255, 255, 255, 0.75) 0%, rgba(255, 255, 255, 0.375) 100%)',
                backdropFilter: 'blur(60px)',
                display: 'inline-flex',
              }}
            >
              <div className="margin_right_10">
                <img
                  src={props.postDetail?.seller?.avatar}
                  alt="avatar user"
                  className="display_block width_36 height_36 border_radius_50_percent"
                />
              </div>
              <div>
                <span className="font_size_14 font_weight_600 line_height_17 color_white margin_bottom_3 display_block">
                  {props.postDetail?.seller?.name}
                </span>
                <div className="rate display_flex">
                  {Array.from(Array(5).keys()).map((item: number) => {
                    return (
                      <svg
                        width="14"
                        height="14"
                        key={item}
                        viewBox="0 0 14 14"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M10.9458 8.54675C10.7732 8.71408 10.6938 8.95608 10.7332 9.19341L11.3258 12.4734C11.3758 12.7514 11.2585 13.0327 11.0258 13.1934C10.7978 13.3601 10.4945 13.3801 10.2458 13.2467L7.29317 11.7067C7.1905 11.6521 7.0765 11.6227 6.95984 11.6194H6.77917C6.7165 11.6287 6.65517 11.6487 6.59917 11.6794L3.64584 13.2267C3.49984 13.3001 3.3345 13.3261 3.1725 13.3001C2.77784 13.2254 2.5145 12.8494 2.57917 12.4527L3.1725 9.17275C3.21184 8.93341 3.1325 8.69008 2.95984 8.52008L0.552502 6.18675C0.351169 5.99141 0.281169 5.69808 0.373169 5.43341C0.462502 5.16941 0.690502 4.97675 0.965836 4.93341L4.27917 4.45275C4.53117 4.42675 4.7525 4.27341 4.86584 4.04675L6.32584 1.05341C6.3605 0.986748 6.40517 0.925415 6.45917 0.873415L6.51917 0.826748C6.5505 0.792081 6.5865 0.763415 6.6265 0.740081L6.69917 0.713415L6.8125 0.666748H7.09317C7.34384 0.692748 7.5645 0.842748 7.67984 1.06675L9.15917 4.04675C9.26584 4.26475 9.47317 4.41608 9.7125 4.45275L13.0258 4.93341C13.3058 4.97341 13.5398 5.16675 13.6325 5.43341C13.7198 5.70075 13.6445 5.99408 13.4392 6.18675L10.9458 8.54675Z"
                          fill="#FEA82F"
                        />
                      </svg>
                    );
                  })}
                </div>
              </div>
            </a>
          </Link>
        </div>
        <div className="margin_left_20 margin_left_xs_0">
          <div className="images display_xs_flex margin_x_xs_-4">
            {props.postDetail?.images?.map((image, index) => {
              if (index === (props.postDetail?.videoUrl ? 0 : 1)) {
                return (
                  <div
                    className="margin_left_16 cursor_pointer width_277 height_277 margin_bottom_20 margin_xs_0 padding_x_xs_4 height_xs_160"
                    key={image.id}
                    onClick={() => {
                      setViewImages(props.postDetail?.images);
                    }}
                  >
                    <img
                      src={image.url}
                      alt="images"
                      className="border_radius_18 height_100_percent width_100_percent object_fit_cover"
                    />
                  </div>
                );
              } else if (index === (props.postDetail?.videoUrl ? 1 : 2)) {
                return (
                  <div
                    className="margin_left_16 cursor_pointer position_relative width_277 height_277 margin_xs_0 padding_x_xs_4 height_xs_160"
                    key={image.id}
                    onClick={() => {
                      setViewImages(props.postDetail?.images);
                    }}
                  >
                    <div
                      className="position_absolute width_245 height_52 border_width_1 border_color_white border_style_solid
              display_flex align_items_center justify_content_center border_radius_25 no_select left_16 bottom_16 width_xs_80_percent height_xs_30 left_xs_50_percent
              translate_xs_-50_0_percent"
                      style={{ background: 'rgba(39,39,39,0.7)' }}
                    >
                      <div className="display_flex">
                        <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <path
                            d="M14.3338 0C17.7231 0 20 2.37811 20 5.91672V14.0833C20 17.6219 17.7231 20 14.3328 20H5.66618C2.2769 20 0 17.6219 0 14.0833V5.91672C0 2.37811 2.2769 0 5.66618 0H14.3338ZM15.4366 10.5501C14.3646 9.88132 13.5371 10.8204 13.3138 11.1207C13.0986 11.4107 12.9136 11.7306 12.7185 12.0506C12.2419 12.84 11.6958 13.7503 10.7506 14.2797C9.37699 15.0402 8.3342 14.3395 7.58404 13.8297C7.30248 13.6398 7.02897 13.4603 6.75645 13.3406C6.08473 13.0506 5.48038 13.3808 4.5834 14.5201C4.11279 15.1156 3.6462 15.7059 3.17358 16.2941C2.89102 16.646 2.95839 17.1889 3.3395 17.4241C3.94788 17.7988 4.68999 18 5.52864 18H13.9564C14.432 18 14.9087 17.935 15.3632 17.7864C16.3869 17.452 17.1994 16.6863 17.6237 15.6749C17.9817 14.8246 18.1557 13.7926 17.8208 12.934C17.7092 12.6491 17.5423 12.3839 17.308 12.1507C16.6936 11.5408 16.1194 10.9711 15.4366 10.5501ZM6.49886 4C5.12021 4 4 5.12173 4 6.5C4 7.87827 5.12021 9 6.49886 9C7.8765 9 8.99772 7.87827 8.99772 6.5C8.99772 5.12173 7.8765 4 6.49886 4Z"
                            fill="white"
                          />
                        </svg>
                      </div>
                      <div className="margin_left_18">
                        {props.postDetail.images?.length > 3 ? (
                          <span className="color_white font_size_15 line_height_22">
                            +{props.postDetail.images.length - (props.postDetail?.videoUrl ? 2 : 3)} ảnh
                          </span>
                        ) : null}
                      </div>
                    </div>
                    <img
                      src={image.url}
                      alt="images"
                      className="border_radius_18 height_100_percent width_100_percent object_fit_cover"
                    />
                  </div>
                );
              }
              return null;
            })}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Title;
