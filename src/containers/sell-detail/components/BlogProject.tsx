import dayjs from 'dayjs';
import React, { useEffect, useState } from 'react';
import Slider from '../../../components/slider/Slider';
import avatar from '../../../images/user_avatar.png';
import { Experiences } from '../../../types/type.sell-listing';
import YoutubePlayer from '../../blogs/components/item/YoutubePlayer';

interface Props {
  list: Experiences[];
}

const BlogProject: React.FC<Props> = (props) => {
  const [listExp, setListExp] = useState([]);

  useEffect(() => {
    if (props.list) {
      setListExp(props.list);
    }
  }, [props.list]);

  if (!listExp.length) {
    return null;
  }

  return (
    <div className="margin_bottom_96  container padding_x_xs_22 margin_bottom_xs_36">
      <h2 className="font_size_16 margin_0 font_weight_600 margin_bottom_24">Tư vấn</h2>
      <div className="display_xs_none">
        {listExp.length ? (
          <Slider
            slidesPerView={4}
            className="border_top_left_radius_18 border_top_right_radius_18"
            listImages={listExp as any}
            setListImages={setListExp}
            slidesNm={25}
            slidesMd={100}
            slidesSm={100}
            slidesXs={100}
            showButton={true}
            isStopAutoPlay={true}
            margin={20}
            loop={false}
          >
            {listExp.map((exp: Experiences, idx: number) => {
              return (
                <a
                  className="item width_277 margin_x_10 display_block flex_none"
                  key={exp.id + idx}
                  href={exp.url}
                  target="_blank"
                >
                  <div className="height_185 margin_bottom_8 width_100_percent">
                    {exp.image ? (
                      <img
                        src={exp.image}
                        alt={exp.name}
                        className="width_100_percent height_100_percent object_fit_cover border_radius_18"
                      />
                    ) : (
                      <YoutubePlayer
                        url={exp.video}
                        style={{ width: '100%', height: '100%', objectFit: 'cover', borderRadius: '18px' }}
                      ></YoutubePlayer>
                    )}
                  </div>
                  <div className="name margin_bottom_8 padding_bottom_8 border_width_0 border_color_grey border_style_solid border_bottom_width_1">
                    <span className="font_weight_600 text_over_flow_2">{exp.name}</span>
                  </div>
                  <div>
                    <a className="display_flex align_items_center margin_bottom_8" href={exp.urlCus} target="_blank">
                      <img src={exp.cusAvatar ? exp.cusAvatar : avatar} alt="" className="margin_right_8" />
                      <span className="font_size_14">{exp.cusName}</span>
                    </a>
                    <div>
                      <span className="font_size_14 margin_right_16">{exp.totalView} lượt xem</span>
                      <span className="font_size_14 opacity_0_5">{dayjs(exp.publicDate).format('DD/MM/YYYY')}</span>
                    </div>
                  </div>
                </a>
              );
            })}
          </Slider>
        ) : null}
      </div>
      <div className="display_none display_xs_flex overflow_auto width_100_percent">
        {listExp.map((exp: Experiences, idx: number) => {
          return (
            <a
              className="item width_277 margin_x_10 display_block flex_none margin_x_xs_0 margin_right_xs_20"
              key={exp.id + idx}
              href={exp.url}
              target="_blank"
            >
              <div className="height_185 margin_bottom_8 width_100_percent">
                {exp.image ? (
                  <img
                    src={exp.image}
                    alt={exp.name}
                    className="width_100_percent height_100_percent object_fit_cover border_radius_18"
                  />
                ) : (
                  <YoutubePlayer
                    url={exp.video}
                    style={{ width: '100%', height: '100%', objectFit: 'cover', borderRadius: '18px' }}
                  ></YoutubePlayer>
                )}
              </div>
              <div className="name margin_bottom_8 padding_bottom_8 border_width_0 border_color_grey border_style_solid border_bottom_width_1">
                <span className="font_weight_600 text_over_flow_2">{exp.name}</span>
              </div>
              <div>
                <a className="display_flex align_items_center margin_bottom_8" href={exp.urlCus} target="_blank">
                  <img src={exp.cusAvatar ? exp.cusAvatar : avatar} alt="" className="margin_right_8" />
                  <span className="font_size_14">{exp.cusName}</span>
                </a>
                <div>
                  <span className="font_size_14 margin_right_16">{exp.totalView} lượt xem</span>
                  <span className="font_size_14 opacity_0_5">{dayjs(exp.publicDate).format('DD/MM/YYYY')}</span>
                </div>
              </div>
            </a>
          );
        })}
      </div>
    </div>
  );
};

export default BlogProject;
