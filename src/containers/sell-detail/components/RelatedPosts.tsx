import Link from 'next/link';
import React, { useEffect, useState } from 'react';
import PostItem from '../../../components/post-item/PostItem';
import Slider from '../../../components/slider/Slider';
import { SellListing } from '../../../types/type.sell-listing';

interface Props {
  type: 'user' | 'project';
  list: SellListing[];
  user?: {
    name: string;
    avatar: string;
    id: number;
  };
}

const RelatedPosts: React.FC<Props> = (props) => {
  const [listSell, setListSell] = useState<SellListing[]>([]);

  useEffect(() => {
    setListSell(props.list);
  }, [props.list]);

  if (!props.list?.length) {
    return null;
  }

  return (
    <div className="container margin_bottom_96  padding_x_xs_22 margin_bottom_xs_36">
      {props.type === 'user' ? (
        <div className="margin_bottom_24 display_flex align_items_center display_xs_block">
          <h2 className="margin_y_0 font_size_16 font_weight_600 margin_right_22 margin_bottom_xs_12 margin_right_xs_0">
            Bất động sản khác của{' '}
          </h2>
          <div className="display_flex align_items_center">
            <div className="margin_right_10">
              <img
                src={props.user?.avatar}
                alt="avatar user"
                className="display_block width_36 height_36 border_radius_50_percent"
              />
            </div>
            <Link
              href={{ pathname: '/seller-sell-listings-page', query: { sellerId: props.user?.id } }}
              as={`/seller/${props.user?.id}/sell-listings`}
            >
              <a className="display_block">
                <span className="font_size_14 font_weight_600 line_height_17 margin_bottom_3 display_block">
                  {props.user?.name}
                </span>
                <div className="rate display_flex">
                  {Array.from(Array(5).keys()).map((item: number) => {
                    return (
                      <svg
                        width="14"
                        height="14"
                        key={item}
                        viewBox="0 0 14 14"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M10.9458 8.54675C10.7732 8.71408 10.6938 8.95608 10.7332 9.19341L11.3258 12.4734C11.3758 12.7514 11.2585 13.0327 11.0258 13.1934C10.7978 13.3601 10.4945 13.3801 10.2458 13.2467L7.29317 11.7067C7.1905 11.6521 7.0765 11.6227 6.95984 11.6194H6.77917C6.7165 11.6287 6.65517 11.6487 6.59917 11.6794L3.64584 13.2267C3.49984 13.3001 3.3345 13.3261 3.1725 13.3001C2.77784 13.2254 2.5145 12.8494 2.57917 12.4527L3.1725 9.17275C3.21184 8.93341 3.1325 8.69008 2.95984 8.52008L0.552502 6.18675C0.351169 5.99141 0.281169 5.69808 0.373169 5.43341C0.462502 5.16941 0.690502 4.97675 0.965836 4.93341L4.27917 4.45275C4.53117 4.42675 4.7525 4.27341 4.86584 4.04675L6.32584 1.05341C6.3605 0.986748 6.40517 0.925415 6.45917 0.873415L6.51917 0.826748C6.5505 0.792081 6.5865 0.763415 6.6265 0.740081L6.69917 0.713415L6.8125 0.666748H7.09317C7.34384 0.692748 7.5645 0.842748 7.67984 1.06675L9.15917 4.04675C9.26584 4.26475 9.47317 4.41608 9.7125 4.45275L13.0258 4.93341C13.3058 4.97341 13.5398 5.16675 13.6325 5.43341C13.7198 5.70075 13.6445 5.99408 13.4392 6.18675L10.9458 8.54675Z"
                          fill="#FEA82F"
                        />
                      </svg>
                    );
                  })}
                </div>
              </a>
            </Link>
          </div>
        </div>
      ) : (
        <h2 className="margin_y_0 margin_bottom_24 font_size_16 font_weight_600">Bất động sản lân cận</h2>
      )}{' '}
      <div className="width_100_percent position_relative display_xs_none">
        {listSell.length ? (
          <Slider
            slidesPerView={4}
            className="border_radius_24 padding_bottom_5"
            listImages={listSell as any}
            setListImages={setListSell}
            slidesNm={25}
            slidesMd={100}
            slidesSm={100}
            slidesXs={100}
            showButton={true}
            isStopAutoPlay={true}
            margin={20}
            loop={false}
          >
            {listSell.map((post: SellListing, index: number) => {
              return (
                <div className="width_277 margin_x_10 flex_none" key={post.id + '_' + index}>
                  <PostItem post={post}></PostItem>
                </div>
              );
            })}
          </Slider>
        ) : null}
      </div>
      <div className="width_100_percent display_none display_xs_flex overflow_auto">
        {listSell.length
          ? listSell.map((post: SellListing, index: number) => {
              return (
                <div className="width_277 margin_right_10 flex_none" key={post.id + '_' + index}>
                  <PostItem post={post}></PostItem>
                </div>
              );
            })
          : null}
      </div>
    </div>
  );
};

export default RelatedPosts;
