import React, { useState } from 'react';
import InputRadio from '../../../components/base/input-radio/InputRadio';
import InputRange from '../../../components/base/input-range/InputRange';
import { OptionInputRadio } from '../../../types/type.base';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import dayjs from 'dayjs';

const ProfitCalculator: React.FC = () => {
  const [price, setPrice] = useState(1);
  const [percent, setPercent] = useState(1);
  const [time, setTime] = useState(1);
  const [profit, setProfit] = useState<any>();
  const [paymentType, setPaymentType] = useState<OptionInputRadio>({
    title: 'Thanh toán theo dư nợ giảm dần',
    value: '0',
  });
  const paymentList = [
    {
      title: 'Thanh toán theo dư nợ giảm dần',
      value: '0',
    },
    {
      title: 'Thanh toán đều hàng tháng',
      value: '1',
    },
  ];
  const fileType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
  const fileExtension = '.xlsx';

  /**
   * onChangeProfit
   */
  const onChangeProfit = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (Number(e.target.value) > 100) {
      setProfit(100);
    } else {
      setProfit(e.target.value);
    }
  };

  /**
   * calculate: tính số tiền phải trả 1 tháng
   */
  const calculate = () => {
    const _month = time * 12;
    const interest = profit ? profit : 7.6;
    const _percent = percent / 100;
    let result = 0;
    if (_month) {
      const money = price * _percent * 1000000000;
      //Lãi
      if (paymentType.value == '1') {
        //gốc = _money - amountMonthly
        result = process(money, _month, interest);
        //mỗi tháng tính lại lãi theo gốc
      } else {
        result = process(money, _month, interest);
      }
    }
    return result;
  };

  /**
   * process: tính tổng tiền phải trả 1 tháng
   * @param goc tiền gốc
   * @param month số tháng
   * @param percent phần trăm
   */
  const process = (goc: number, month: number, percent: number) => {
    const _amountMonthly = Math.round(goc / month);
    const _interestMoney = Math.round((goc * percent) / 100 / 12);
    return _amountMonthly + _interestMoney;
  };

  /**
   * numberWithCommas: format số
   * @param x số cần format
   */
  const numberWithCommas = (x: string | number) => {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
  };

  /**
   * exportToCSV
   */
  const exportToCSV = () => {
    const csvData: any[] = renderColumnCSV();
    const fileName: string = 'reviewnha_' + dayjs(new Date()).format('YYYYMMDDHHmm');
    const ws = XLSX.utils.json_to_sheet(csvData);
    const wb = { Sheets: { data: ws }, SheetNames: ['data'] };
    const excelBuffer = XLSX.write(wb, { bookType: 'xlsx', type: 'array' });
    const data = new Blob([excelBuffer], { type: fileType });
    FileSaver.saveAs(data, fileName + fileExtension);
  };

  const renderColumnCSV = () => {
    const data = [];
    const _month = time * 12;
    const interest = profit ? profit : 7.6;
    const _percent = percent / 100;
    const money = price * _percent * 1000000000;
    if (paymentType.value === '1') {
      for (let i = 0; i < _month; i++) {
        data.push({
          Tháng: i + 1,
          'Tổng tiền phải trả(Gốc + Lãi)': numberWithCommas(calculate()),
          'Gốc phải trả': numberWithCommas(Math.round(money / _month)),
          'Lãi phải trả': numberWithCommas(Math.round((money * interest) / 100 / 12)),
          'Gốc còn lại': numberWithCommas(money - (i + 1) * Math.round(money / _month)),
        });
      }
      data.push({
        Tháng: 'Tổng:',
        'Tổng tiền phải trả(Gốc + Lãi)': numberWithCommas(Math.round(calculate() * _month)),
        'Gốc phải trả': numberWithCommas(Math.round((money / _month) * _month)),
        'Lãi phải trả': numberWithCommas(Math.round(((money * interest) / 100 / 12) * _month)),
        'Gốc còn lại': '',
      });
    } else {
      const real = Math.round(money / _month);
      let realMoney = money;
      let totalPay = 0;
      let totalReal = 0;
      let totalProfit = 0;
      for (let i = 0; i < _month; i++) {
        const profit = Math.round((realMoney * interest) / 100 / 12);
        realMoney = realMoney - real;
        totalReal += real;
        totalProfit += profit;
        totalPay += real + profit;
        data.push({
          Tháng: i + 1,
          'Tổng tiền phải trả(Gốc + Lãi)': numberWithCommas(Math.round(real + profit)),
          'Gốc phải trả': numberWithCommas(real),
          'Lãi phải trả': numberWithCommas(profit),
          'Gốc còn lại': numberWithCommas(realMoney),
        });
      }
      data.push({
        Tháng: 'Tổng:',
        'Tổng tiền phải trả(Gốc + Lãi)': numberWithCommas(Math.round(totalPay)),
        'Gốc phải trả': numberWithCommas(Math.round(totalReal)),
        'Lãi phải trả': numberWithCommas(Math.round(totalProfit)),
        'Gốc còn lại': '',
      });
    }
    return data;
  };

  return (
    <div className="container margin_bottom_96  padding_x_xs_22 margin_bottom_xs_36">
      <h2 className="margin_y_0 font_size_16 font_weight_600 margin_bottom_36">Tính vay mua nhà</h2>
      <div className="display_flex flex_wrap_xs">
        <div className="input_box width_475 width_xs_100_percent">
          <div className="display_flex field margin_bottom_24">
            <div className="width_100_percent">
              <InputRange
                min={1}
                max={10}
                step={0.1}
                value={price}
                setValue={setPrice}
                label="Giá trị nhà đất (tỷ VNĐ)"
              ></InputRange>
            </div>
          </div>
          <div className="display_flex field margin_bottom_24">
            <div className="width_100_percent">
              <InputRange
                min={1}
                max={100}
                step={1}
                value={percent}
                setValue={setPercent}
                label="Tỷ lệ vay (%)"
              ></InputRange>
            </div>
          </div>
          <div className="display_flex field margin_bottom_24">
            <div className="width_100_percent ">
              <InputRange
                min={1}
                max={30}
                step={1}
                value={time}
                setValue={setTime}
                label="Thời hạn vay (năm)"
              ></InputRange>
            </div>
          </div>
        </div>
        <div className="result margin_left_20 flex_1 width_xs_100_percent margin_left_xs_0">
          <div className="margin_bottom_31">
            <div className="flex_none display_flex margin_bottom_10">
              <span className="font_size_14 line_height_17 font_weight_600">Lãi suất vay %/năm</span>
            </div>
            <input
              value={profit}
              onChange={(e) => {
                onChangeProfit(e);
              }}
              type="number"
              max={100}
              min={0.1}
              step={1}
              placeholder="Nhập lãi suất (Mặc định:7.6%)"
              className="border_none bg_light_gray height_50 padding_x_24 border_radius_8 font_size_14 width_100_percent"
            />
          </div>
          <div className="margin_bottom_28">
            <InputRadio
              options={paymentList}
              value={paymentType}
              setValue={setPaymentType}
              valueFontSize={14}
            ></InputRadio>
          </div>
          <div
            className="display_flex justify_content_sb align_items_center padding_x_24 height_50 border_width_1 border_color_black border_style_solid border_radius_8 flex_direction_xs_col
          align_items_xs_fs padding_y_xs_16 height_xs_auto"
          >
            <span className="font_size_14">Số tiền phải trả hàng tháng</span>
            <span className="font_weight_600 color_red">{numberWithCommas(calculate())} đ/tháng</span>
          </div>
          <div className="margin_top_40">
            <div className="display_flex justify_content_center align_items_center">
              <div
                className="display_flex cursor_pointer no_select"
                onClick={() => {
                  exportToCSV();
                }}
              >
                <svg width={12} height={18} viewBox="0 0 12 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path
                    d="M11.4101 9.14147C11.2839 8.90748 11.0438 8.7616 10.7827 8.7616H6.71729V0.734294C6.71729 0.328964 6.39596 0 6.00003 0C5.6041 0 5.28276 0.328964 5.28276 0.734294V8.7616H1.21731C0.955274 8.7616 0.71523 8.90748 0.589948 9.14147C0.462753 9.37547 0.471361 9.66135 0.610031 9.88752L5.39274 17.6573C5.52472 17.8708 5.75329 18 6.00003 18C6.24676 18 6.47533 17.8708 6.60731 17.6573L11.39 9.88752C11.4627 9.76807 11.5 9.63198 11.5 9.49589C11.5 9.37449 11.4694 9.25211 11.4101 9.14147Z"
                    fill="#D93C23"
                  />
                </svg>
                <span className="font_size_14 font_weight_600 color_red margin_left_12">Tải xuống chi tiết</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ProfitCalculator;
