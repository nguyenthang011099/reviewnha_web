import Link from 'next/link';
import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { RootState } from '../../../reducers';
import { Project, SellListing } from '../../../types/type.sell-listing';

interface Props {
  detail: SellListing;
}

const BreadCrumb: React.FC<Props> = (props) => {
  const projectList: Project[] = useSelector((state: RootState) => state.sellListing.projectList);
  const [project, setProject] = useState<Project>({});

  useEffect(() => {
    const activeProject = projectList.find((item) => Number(item.id) === Number(props.detail.projectId));
    setProject(activeProject);
  }, [props.detail, projectList]);

  return (
    <div className="container display_flex margin_bottom_24  align_items_center padding_x_xs_22 display_xs_block">
      <a
        href="https://reviewnha.vn"
        target="_blank"
        className="padding_x_16 padding_y_8 border_radius_25 border_color_grey border_width_1 border_style_solid display_flex align_items_center
      margin_right_36 cursor_pointer width_xs_125 margin_bottom_xs_16"
      >
        <svg width={8} height={12} viewBox="0 0 8 12" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path
            d="M6.33337 10.6666L1.66671 5.99992L6.33337 1.33325"
            stroke="#3B4144"
            strokeWidth="1.5"
            strokeLinecap="round"
            strokeLinejoin="round"
          />
        </svg>
        <span className="margin_left_12 font_size_14 font_weight_600">Trang chủ</span>
      </a>
      <div className="display_flex align_items_center flex_wrap">
        <Link
          href={{
            pathname: '/sell-listings-page',
            query: {
              provinceCode: props.detail?.province?.code,
            },
          }}
          as={`/sell-listings/?provinceCode=${props.detail?.province?.code}`}
        >
          <a className="">
            <span className="font_size_14 font_weight_600 line_height_17 margin_right_12 opacity_0_5 cursor_pointer">
              {props.detail?.province?.name}
            </span>
          </a>
        </Link>
        <div className="margin_right_12 opacity_0_5">
          <svg width={7} height={11} viewBox="0 0 7 11" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path
              d="M1.30634 0.833323L5.973 5.49999L1.30634 10.1667"
              stroke="#3B4144"
              strokeWidth="1.5"
              strokeLinecap="round"
              strokeLinejoin="round"
            />
          </svg>
          <Link
            href={{
              pathname: '/sell-listings-page',
              query: {
                provinceCode: props.detail?.province?.code,
                districtCode: props.detail?.district?.code,
              },
            }}
            as={`/sell-listings/?provinceCode=${props.detail?.province?.code}&districtCode=${props.detail?.district?.code}`}
          >
            <a className="">
              <span className="font_size_14 font_weight_600 line_height_17 margin_left_12 cursor_pointer">
                {props.detail?.district?.name}
              </span>
            </a>
          </Link>
        </div>
        <div className="margin_right_12 opacity_0_5">
          <svg width={7} height={11} viewBox="0 0 7 11" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path
              d="M1.30634 0.833323L5.973 5.49999L1.30634 10.1667"
              stroke="#3B4144"
              strokeWidth="1.5"
              strokeLinecap="round"
              strokeLinejoin="round"
            />
          </svg>
          <Link
            href={{
              pathname: '/sell-listings-page',
              query: {
                provinceCode: props.detail?.province?.code,
                districtCode: props.detail?.district?.code,
                wardCode: props.detail?.ward?.code,
              },
            }}
            as={`/sell-listings/?provinceCode=${props.detail?.province?.code}&districtCode=${props.detail?.district?.code}&wardCode=${props.detail?.ward?.code}`}
          >
            <a className="">
              <span className="font_size_14 font_weight_600 line_height_17 margin_left_12 cursor_pointer">
                {props.detail?.ward?.name}
              </span>
            </a>
          </Link>
        </div>
        {project?.id ? (
          <div className="opacity_0_5">
            <svg width={7} height={11} viewBox="0 0 7 11" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path
                d="M1.30634 0.833323L5.973 5.49999L1.30634 10.1667"
                stroke="#3B4144"
                strokeWidth="1.5"
                strokeLinecap="round"
                strokeLinejoin="round"
              />
            </svg>
            <Link
              href={{
                pathname: '/sell-listings-page',
                query: {
                  projectId: project.id,
                },
              }}
              as={`/sell-listings/?projectId=${project.id}`}
            >
              <a className="font_size_14 font_weight_600 line_height_17 margin_left_12 cursor_pointer">
                {project.name}
              </a>
            </Link>
          </div>
        ) : null}
      </div>
    </div>
  );
};

export default BreadCrumb;
