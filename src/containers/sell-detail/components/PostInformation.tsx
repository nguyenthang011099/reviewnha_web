import dayjs from 'dayjs';
import React, { useEffect, useRef, useState } from 'react';
import InputDateSlide from '../../../components/base/input-date-slide/InputDateSlide';
import InputTime from '../../../components/base/input-time/InputTime';
import classnames from 'classnames';
import InputText from '../../../components/base/input-text/InputText';
import { SellListing } from '../../../types/type.sell-listing';
import TextArea from '../../../components/base/textarea/TextArea';
import { CreateScheduleBody } from '../../../types/type.schedule';
import scheduleApi from '../../../api/schedule';
import { useDispatch } from 'react-redux';
import { openNotification } from '../../../reducers/notification/actions';
import classNames from 'classnames';

interface Props {
  postDetail: SellListing;
}

const PostInformation: React.FC<Props> = (props) => {
  const [date, setDate] = useState<string>(dayjs(new Date()).toString());
  const [time, setTime] = useState<string>('11:59PM');
  const [currentTab, setCurrentTab] = useState<'book' | 'receive'>('book');
  const [bookName, setBookName] = useState<string>('');
  const [bookPhone, setBookPhone] = useState<string>('');
  const [bookEmail, setBookEmail] = useState<string>('');
  const [receiveName, setReceiveName] = useState<string>('');
  const [receivePhone, setReceivePhone] = useState<string>('');
  const [receiveEmail, setReceiveEmail] = useState<string>('');
  const [receiveNote, setReceiveNote] = useState<string>('');
  const [currentTabBook, setCurrentTabBook] = useState<'directed' | 'video'>('directed');
  const dispatch = useDispatch();
  const [showMoreDescription, setShowMoreDescription] = useState<boolean>(false);
  const [showMoreButton, setShowMoreButton] = useState<boolean>(false);
  const $desc = useRef(null);

  useEffect(() => {
    setShowMoreButton(checkShowMoreButton());
  }, [$desc]);

  /**
   * changeTab dổi tab
   * @param value tên của tab
   */
  const changeTab = (value: 'book' | 'receive') => {
    setCurrentTab(value);
  };

  /**
   * renderIconRoom
   * @param code  code cua room
   * @returns
   */
  const renderIconRoom = (code: string) => {
    switch (code) {
      case 'BATHROOM': {
        return (
          <svg width={24} height={24} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path
              fillRule="evenodd"
              clipRule="evenodd"
              d="M7.92861 4.26877C8.21036 4.26877 8.46041 4.44752 8.55124 4.71297L8.70239 5.15463C8.12691 5.68567 7.86597 6.51465 8.10048 7.31796L8.11818 7.37851C8.23581 7.78139 8.65804 8.01212 9.06073 7.89499L11.702 7.12687C12.105 7.00971 12.3371 6.58829 12.2194 6.1852L12.2017 6.12467C11.9262 5.18072 11.0548 4.57348 10.1161 4.58938L9.99031 4.22177C9.6889 3.34125 8.86024 2.75 7.92861 2.75C6.72751 2.75 5.74994 3.72498 5.74994 4.92494V11.4896C5.74994 11.9096 6.09039 12.25 6.51034 12.25C6.93027 12.25 7.27069 11.9095 7.27069 11.4896V4.92494C7.27069 4.56379 7.56558 4.26877 7.92861 4.26877Z"
              fill="white"
            />
            <path
              d="M10.4523 13.3719H20.4726C20.7639 13.3719 21 13.6079 21 13.8992V14.8491C21 15.0238 20.9124 15.1855 20.7689 15.285C20.6622 15.359 20.5923 15.4826 20.5923 15.6219V17.3589C20.5923 18.5001 19.8163 19.4596 18.7633 19.7397L19.0552 20.1822C19.2156 20.4253 19.1486 20.7524 18.9054 20.9128C18.816 20.9717 18.7152 20.9999 18.6155 20.9999C18.4442 20.9999 18.2763 20.9166 18.1749 20.7629L17.601 19.893C17.586 19.8703 17.5736 19.8466 17.5626 19.8227H6.43735C6.42631 19.8466 6.41394 19.8703 6.39896 19.893L5.82507 20.7629C5.72368 20.9166 5.55567 20.9999 5.38438 20.9999C5.28471 20.9999 5.18389 20.9718 5.09448 20.9128C4.85138 20.7524 4.7843 20.4253 4.94468 20.1822L5.23659 19.7397C4.18362 19.4596 3.40761 18.5001 3.40761 17.3589V15.6219C3.40761 15.4826 3.33769 15.359 3.23102 15.285C3.08748 15.1855 2.99994 15.0238 2.99994 14.8491V13.8992C2.99994 13.6079 3.23605 13.3718 3.52728 13.3718H5.32437L10.4525 13.3787L10.4523 13.3719Z"
              fill="white"
            />
          </svg>
        );
      }
      default: {
        return (
          <svg width={24} height={24} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path
              d="M4.08326 9.91673H6.16658C6.03826 9.91673 5.9174 9.85755 5.83826 9.75673C5.75908 9.65591 5.73158 9.52423 5.76244 9.39923L6.02162 8.36341C6.1608 7.80592 6.65993 7.41674 7.23411 7.41674H9.9166C10.6058 7.41674 11.1666 7.97756 11.1666 8.66673V9.50005C11.1666 9.73005 10.9799 9.91673 10.7499 9.91673H13.2499C13.0199 9.91673 12.8332 9.73005 12.8332 9.50005V8.66673C12.8332 7.97756 13.394 7.41674 14.0832 7.41674H16.7657C17.3399 7.41674 17.839 7.80592 17.9782 8.36341L18.2374 9.39923C18.2691 9.52341 18.2407 9.65591 18.1616 9.75673C18.0824 9.85755 17.9616 9.91673 17.8332 9.91673H19.9166C20.1466 9.91673 20.3332 9.73005 20.3332 9.50005V7.00006C20.3332 5.85174 19.3982 4.91675 18.2499 4.91675H5.74994C4.60162 4.91675 3.66663 5.85174 3.66663 7.00006V9.50005C3.66663 9.73005 3.85327 9.91673 4.08326 9.91673Z"
              fill="white"
            />
            <path
              d="M19.9165 10.75H4.08319C2.93487 10.75 1.99988 11.685 1.99988 12.8333V18.6666C1.99988 18.8966 2.18656 19.0833 2.41656 19.0833C2.64655 19.0833 2.83323 18.8966 2.83323 18.6666V17.4166H21.1665V18.6666C21.1665 18.8966 21.3532 19.0833 21.5832 19.0833C21.8132 19.0833 21.9998 18.8966 21.9998 18.6666V12.8333C21.9998 11.685 21.0648 10.75 19.9165 10.75Z"
              fill="white"
            />
          </svg>
        );
      }
    }
  };

  const onSubmitSchedule = () => {
    let data: CreateScheduleBody;
    if (currentTab === 'receive') {
      data = {
        type: 'nhan_thong_tin',
        name: receiveName,
        email: receiveEmail,
        phone: receivePhone,
        content: receiveNote,
        sellListingId: props.postDetail.id,
      };
    } else {
      data = {
        name: bookName,
        email: bookEmail,
        phone: bookPhone,
        time: dayjs(date).format('MM/DD/YYYY') + ' ' + time.toLowerCase().replace(/(pm|am)/, ' $1'),
        sellListingId: props.postDetail.id,
      };
      if (currentTabBook === 'directed') {
        data.type = 'xem_truc_tiep';
      } else {
        data.type = 'video_call';
      }
    }
    scheduleApi.create(data).then((res: any) => {
      if (res.success) {
        clearForm();
        dispatch(
          openNotification({
            type: 'success',
            title: 'Đặt lịch thành công!',
            isOpen: true,
          }),
        );
      }
    });
  };

  const clearForm = () => {
    setDate(dayjs(new Date()).toString());
    setTime('11:59PM');
    setBookName('');
    setBookPhone('');
    setBookEmail('');
    setReceiveName('');
    setReceivePhone('');
    setReceiveEmail('');
    setReceiveNote('');
  };

  const checkShowMoreButton = () => {
    if ($desc && $desc.current) {
      const curOverflow = $desc.current.style.overflow;
      if (!curOverflow || curOverflow === 'visible') $desc.current.style.overflow = 'hidden';
      const isOverflowing =
        $desc.current.clientWidth < $desc.current.scrollWidth ||
        $desc.current.clientHeight < $desc.current.scrollHeight;
      $desc.current.style.overflow = curOverflow;
      return isOverflowing;
    }
    return false;
  };

  return (
    <div className="container margin_bottom_160  padding_x_xs_22 margin_bottom_xs_42">
      <h2 className="margin_y_0 font_size_16 font_weight_600 margin_bottom_24 ">Thông tin chi tiết</h2>
      <div className="display_flex align_items_fs display_xs_block">
        <div className="information margin_right_20 margin_right_xs_0">
          <div className="display_flex flex_wrap margin_bttom_56 margin_x_-10">
            {/* type  */}
            <div className="display_flex width_50_percent margin_bottom_24 padding_x_10 width_xs_100_percent margin_bottom_xs_16 padding_y_xs_0">
              <div className="display_flex align_items_center">
                <div className="width_36 height_36 display_flex align_items_center justify_content_center bg_green border_radius_14">
                  <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path
                      d="M7.13478 18.7733V15.7156C7.13478 14.9351 7.77217 14.3023 8.55844 14.3023H11.4326C11.8102 14.3023 12.1723 14.4512 12.4393 14.7163C12.7063 14.9813 12.8563 15.3408 12.8563 15.7156V18.7733C12.8539 19.0978 12.9821 19.4099 13.2124 19.6402C13.4427 19.8705 13.7561 20 14.0829 20H16.0438C16.9596 20.0023 17.8388 19.6428 18.4872 19.0008C19.1356 18.3588 19.5 17.487 19.5 16.5778V7.86686C19.5 7.13246 19.1721 6.43584 18.6046 5.96467L11.934 0.675869C10.7737 -0.251438 9.11111 -0.221498 7.98539 0.746979L1.46701 5.96467C0.872741 6.42195 0.517552 7.12064 0.5 7.86686V16.5689C0.5 18.4639 2.04738 20 3.95617 20H5.87229C6.55123 20 7.103 19.4562 7.10792 18.7822L7.13478 18.7733Z"
                      fill="white"
                    />
                  </svg>
                </div>
                <span className="display_block font_size_14 line_height_20 margin_left_16 opacity_0_5 margin_right_8">
                  Loại hình:
                </span>
              </div>
              <div className="display_flex align_items_center ">
                <span className="display_block font_size_14 line_height_20 font_weight_600">
                  {props.postDetail?.typeProduct?.content ?? ''}
                </span>
              </div>
            </div>
            {/* area  */}
            <div className="display_flex width_50_percent margin_bottom_24 padding_x_10 width_xs_100_percent margin_bottom_xs_16 padding_y_xs_0">
              <div className="display_flex align_items_center">
                <div className="width_36 height_36 display_flex align_items_center justify_content_center bg_green border_radius_14">
                  <svg width={24} height={24} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path
                      d="M21.7556 20.5773L19.639 18.4607C20.0757 17.8015 20.3332 17.0141 20.3332 16.1666C20.3332 13.8691 18.464 12 16.1666 12C13.8691 12 12 13.8691 12 16.1666C12 18.464 13.8691 20.3332 16.1666 20.3332C17.0141 20.3332 17.8015 20.0757 18.4607 19.639L20.5773 21.7556C20.9031 22.0815 21.4298 22.0815 21.7556 21.7556C22.0815 21.4298 22.0815 20.9031 21.7556 20.5773ZM16.1666 18.6665C14.7883 18.6665 13.6666 17.5449 13.6666 16.1666C13.6666 14.7883 14.7883 13.6666 16.1666 13.6666C17.5449 13.6666 18.6665 14.7883 18.6665 16.1666C18.6665 17.5449 17.5449 18.6665 16.1666 18.6665Z"
                      fill="white"
                    />
                    <path
                      d="M2 3.64706V14.3529C2 15.2613 2.7475 16 3.66667 16H7C7.91917 16 8.66667 15.2613 8.66667 14.3529V8.58824H20.3333C21.2525 8.58824 22 7.84953 22 6.94118V3.64706C22 2.73871 21.2525 2 20.3333 2H3.66667C2.7475 2 2 2.73871 2 3.64706Z"
                      fill="white"
                    />
                  </svg>
                </div>
                <span className="display_block font_size_14 line_height_20 margin_left_16 opacity_0_5 margin_right_8">
                  Diện tích:
                </span>
              </div>
              <div className="display_flex align_items_center ">
                <span className="display_block font_size_14 line_height_20 font_weight_600">
                  {props.postDetail?.area}m2
                </span>
              </div>
            </div>
            {/* room  */}
            <div className="display_flex width_50_percent margin_bottom_24 padding_x_10 width_xs_100_percent margin_bottom_xs_16 padding_y_xs_0">
              <div className="display_flex align_items_center">
                <div className="width_36 height_36 display_flex align_items_center justify_content_center bg_green border_radius_14">
                  {renderIconRoom('BEDROOM')}
                </div>
                <span className="display_block font_size_14 line_height_20 margin_left_16 opacity_0_5 margin_right_8">
                  Phòng ngủ:
                </span>
              </div>
              <div className="display_flex align_items_center ">
                <span className="display_block font_size_14 line_height_20 font_weight_600">
                  {props.postDetail.bedroom || 'Liên hệ'}
                </span>
              </div>
            </div>
            <div className="display_flex width_50_percent margin_bottom_24 padding_x_10 width_xs_100_percent margin_bottom_xs_16 padding_y_xs_0">
              <div className="display_flex align_items_center">
                <div className="width_36 height_36 display_flex align_items_center justify_content_center bg_green border_radius_14">
                  {renderIconRoom('BATHROOM')}
                </div>
                <span className="display_block font_size_14 line_height_20 margin_left_16 opacity_0_5 margin_right_8">
                  Vệ sinh:
                </span>
              </div>
              <div className="display_flex align_items_center ">
                <span className="display_block font_size_14 line_height_20 font_weight_600">
                  {props.postDetail.bathroom || 'Liên hệ'}
                </span>
              </div>
            </div>
            {/* phap ly  */}
            {props.postDetail?.juridical?.id ? (
              <div className="display_flex width_50_percent margin_bottom_24 padding_x_10 width_xs_100_percent margin_bottom_xs_16 padding_y_xs_0">
                <div className="display_flex align_items_center">
                  <div className="width_36 height_36 display_flex align_items_center justify_content_center bg_green border_radius_14">
                    <svg width={18} height={20} viewBox="0 0 18 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path
                        d="M13.191 0C16.28 0 18 1.78 18 4.83V15.16C18 18.26 16.28 20 13.191 20H4.81C1.77 20 0 18.26 0 15.16V4.83C0 1.78 1.77 0 4.81 0H13.191ZM5.08 13.74C4.78 13.71 4.49 13.85 4.33 14.11C4.17 14.36 4.17 14.69 4.33 14.95C4.49 15.2 4.78 15.35 5.08 15.31H12.92C13.319 15.27 13.62 14.929 13.62 14.53C13.62 14.12 13.319 13.78 12.92 13.74H5.08ZM12.92 9.179H5.08C4.649 9.179 4.3 9.53 4.3 9.96C4.3 10.39 4.649 10.74 5.08 10.74H12.92C13.35 10.74 13.7 10.39 13.7 9.96C13.7 9.53 13.35 9.179 12.92 9.179ZM8.069 4.65H5.08V4.66C4.649 4.66 4.3 5.01 4.3 5.44C4.3 5.87 4.649 6.22 5.08 6.22H8.069C8.5 6.22 8.85 5.87 8.85 5.429C8.85 5 8.5 4.65 8.069 4.65Z"
                        fill="white"
                      />
                    </svg>
                  </div>
                  <span className="display_block font_size_14 line_height_20 margin_left_16 opacity_0_5 margin_right_8">
                    Giấy tờ pháp lý:
                  </span>
                </div>
                <div className="display_flex align_items_center ">
                  <span className="display_block font_size_14 line_height_20 font_weight_600">
                    {props.postDetail?.juridical?.content}
                  </span>
                </div>
              </div>
            ) : null}
            {/* huong  */}
            {props.postDetail?.direction?.id ? (
              <div className="display_flex width_50_percent margin_bottom_24 padding_x_10 width_xs_100_percent margin_bottom_xs_16 padding_y_xs_0">
                <div className="display_flex align_items_center">
                  <div className="width_36 height_36 display_flex align_items_center justify_content_center bg_green border_radius_14">
                    <svg width={20} height={20} viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path
                        d="M9.99982 0C15.5198 0 19.9998 4.48 19.9998 10C19.9998 15.53 15.5198 20 9.99982 20C4.46982 20 -0.000183105 15.53 -0.000183105 10C-0.000183105 4.48 4.46982 0 9.99982 0ZM13.8498 6.71C13.9598 6.36 13.6398 6.03 13.2898 6.14L8.16982 7.74C7.95982 7.81 7.78982 7.97 7.72982 8.18L6.12982 13.31C6.01982 13.65 6.34982 13.98 6.68982 13.87L11.7898 12.27C11.9998 12.21 12.1698 12.04 12.2298 11.83L13.8498 6.71Z"
                        fill="white"
                      />
                    </svg>
                  </div>
                  <span className="display_block font_size_14 line_height_20 margin_left_16 opacity_0_5 margin_right_8">
                    Hướng:
                  </span>
                </div>
                <div className="display_flex align_items_center ">
                  <span className="display_block font_size_14 line_height_20 font_weight_600">
                    {props.postDetail?.direction?.content}
                  </span>
                </div>
              </div>
            ) : null}
            {/* noi that  */}
            {props.postDetail?.furniture?.id ? (
              <div className="display_flex width_50_percent margin_bottom_24 padding_x_10 width_xs_100_percent margin_bottom_xs_16 padding_y_xs_0">
                <div className="display_flex align_items_center">
                  <div className="width_36 height_36 display_flex align_items_center justify_content_center bg_green border_radius_14">
                    <svg width={24} height={24} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path
                        d="M19.6681 9.60941C18.8543 9.60941 18.1924 10.2714 18.1924 11.0852V14.7718C18.1924 15.4151 17.6688 15.9383 17.0259 15.9383H6.97462C6.33177 15.9383 5.80814 15.4151 5.80814 14.7718V11.0851C5.80814 10.2713 5.14619 9.60938 4.33237 9.60938C3.51854 9.60938 2.8562 10.2713 2.8562 11.0851V14.7718C2.8562 17.0427 4.70369 18.8902 6.97462 18.8902H17.0259C19.2968 18.8902 21.1443 17.0427 21.1443 14.7718V11.0851C21.1443 10.2714 20.482 9.60941 19.6681 9.60941Z"
                        fill="white"
                      />
                      <path
                        d="M6.97447 20.0829C6.77294 20.0829 6.57408 20.0713 6.3783 20.0493V21.4036C6.3783 21.733 6.64529 22 6.97466 22C7.30404 22 7.57103 21.733 7.57103 21.4036V20.0829H6.97447Z"
                        fill="white"
                      />
                      <path
                        d="M17.0261 20.0829H16.4296V21.4036C16.4296 21.733 16.6966 22 17.0259 22C17.3553 22 17.6223 21.733 17.6223 21.4036V20.0493C17.4265 20.0713 17.2276 20.0829 17.0261 20.0829Z"
                        fill="white"
                      />
                      <path
                        d="M16.6499 2H7.34972C5.35688 2 3.7356 3.62128 3.7356 5.61412V8.48469C3.92751 8.44067 4.12692 8.41669 4.33196 8.41669C5.80336 8.41669 7.00042 9.61375 7.00042 11.0852V14.7456H16.9992V11.0851C16.9992 9.61371 18.1963 8.41665 19.6677 8.41665C19.8727 8.41665 20.0722 8.44067 20.2641 8.48466V5.61412C20.264 3.62128 18.6427 2 16.6499 2ZM10.2903 11.306C9.956 11.306 9.70842 11.0328 9.69393 10.7096C9.67951 10.3876 9.97834 10.1133 10.2903 10.1133C10.6246 10.1133 10.8722 10.3865 10.8867 10.7096C10.9011 11.0317 10.6022 11.306 10.2903 11.306ZM10.2903 8.24149C9.956 8.24149 9.70842 7.96825 9.69393 7.64513C9.67951 7.32306 9.97834 7.04876 10.2903 7.04876C10.6246 7.04876 10.8722 7.32201 10.8867 7.64513C10.9011 7.9672 10.6022 8.24149 10.2903 8.24149ZM13.7093 11.306C13.375 11.306 13.1275 11.0328 13.113 10.7096C13.0986 10.3876 13.3974 10.1133 13.7093 10.1133C14.0437 10.1133 14.2912 10.3865 14.3057 10.7096C14.3202 11.0317 14.0213 11.306 13.7093 11.306ZM13.7093 8.24149C13.375 8.24149 13.1275 7.96825 13.113 7.64513C13.0986 7.32306 13.3974 7.04876 13.7093 7.04876C14.0437 7.04876 14.2912 7.32201 14.3057 7.64513C14.3202 7.9672 14.0213 8.24149 13.7093 8.24149Z"
                        fill="white"
                      />
                    </svg>
                  </div>
                  <span className="display_block font_size_14 line_height_20 margin_left_16 opacity_0_5 margin_right_8">
                    Nội thất:
                  </span>
                </div>
                <div className="display_flex align_items_center ">
                  <span className="display_block font_size_14 line_height_20 font_weight_600">
                    {props.postDetail?.furniture?.content}
                  </span>
                </div>
              </div>
            ) : null}
          </div>
          {/* line  */}
          <div className="height_1 width_100_percent bg_black opacity_0_5 margin_bottom_24 display_xs_none"></div>
          <div>
            <h2 className="margin_y_0 font_size_16 font_weight_600 margin_bottom_24 margin_top_xs_12">Mô tả thêm</h2>
            <ul
              className={classNames({ 'height_225 overflow_hidden': !showMoreDescription }, 'padding_left_25')}
              ref={$desc}
            >
              <li
                className="font_size_15 line_height_28 font_weight_500 margin_bottom_8"
                style={{ whiteSpace: 'pre-line' }}
              >
                {props.postDetail?.description}
              </li>
            </ul>
            {showMoreButton ? (
              <div className="text_align_center margin_top_36">
                <button
                  className="padding_x_32 padding_y_8 bg_white cursor_pointer border_width_1 border_color_black 
                border_style_solid border_radius_25 font_size_14 font_weight_600 line_height_17"
                  onClick={() => {
                    setShowMoreDescription(!showMoreDescription);
                  }}
                >
                  {showMoreDescription ? 'Thu gọn' : 'Xem thêm'}
                </button>
              </div>
            ) : null}
          </div>
        </div>
        <div
          className="contact width_475 flex_none padding_24 padding_bottom_36 border_width_1 border_color_black border_style_solid border_radius_18 height_610
        display_flex flex_direction_col width_xs_100_percent padding_xs_12 margin_top_xs_24 height_xs_570"
        >
          <div className="display_flex margin_x_-8 margin_bottom_16">
            <div className="width_50_percent padding_x_8">
              <button
                className={classnames(
                  {
                    'bg_black color_white': currentTab === 'book',
                    bg_white: currentTab !== 'book',
                  },
                  `padding_x_24 padding_y_12 font_size_14 line_height_17 font_weight_600 width_100_percent border_radius_8 cursor_pointer display_block border_none
                  font_size_xs_12 padding_x_xs_12`,
                )}
                onClick={() => {
                  changeTab('book');
                }}
              >
                Đặt lịch xem nhà
              </button>
            </div>
            <div className="width_50_percent padding_x_8">
              <button
                className={classnames(
                  {
                    'bg_black color_white': currentTab === 'receive',
                    bg_white: currentTab !== 'receive',
                  },
                  `padding_x_24 padding_y_12 font_size_14 line_height_17 font_weight_600 width_100_percent border_radius_8 cursor_pointer display_block border_none
                  font_size_xs_12`,
                )}
                onClick={() => {
                  changeTab('receive');
                }}
              >
                Nhận thông tin
              </button>
            </div>
          </div>
          <div className="flex_1 display_flex flex_direction_col justify_content_sb">
            <div>
              <div className={classnames({ display_none: currentTab !== 'book' }, 'calendar')}>
                <div className="calendar_type display_flex margin_x_-8 margin_bottom_16">
                  <div
                    className="width_50_percent padding_x_8 text_align_center cursor_pointer"
                    onClick={() => {
                      setCurrentTabBook('directed');
                    }}
                  >
                    <div
                      className={classnames({
                        'border_width_0 border_bottom_width_2 border_color_black border_style_solid padding_bottom_8':
                          currentTabBook === 'directed',
                      })}
                    >
                      <span className="display_block font_size_14 font_weight_600 line_height_17">Xem trực tiếp</span>
                    </div>
                  </div>
                  <div
                    className="width_50_percent padding_x_8  text_align_center cursor_pointer"
                    onClick={() => {
                      setCurrentTabBook('video');
                    }}
                  >
                    <div
                      className={classnames({
                        'border_width_0 border_bottom_width_2 border_color_black border_style_solid padding_bottom_8':
                          currentTabBook === 'video',
                      })}
                    >
                      <span className="display_block font_size_14 font_weight_600 line_height_17">Video Call</span>
                    </div>
                  </div>
                </div>
                <div className="content">
                  <div>
                    <div
                      className="input_box margin_bottom_16 display_flex align_items_center justify_content_center padding_y_16
                  border_width_1 border_color_black border_style_solid border_radius_8"
                    >
                      <InputDateSlide value={date} setValue={setDate}></InputDateSlide>
                    </div>
                    <div className="input_box margin_bottom_16">
                      <InputTime value={time} setValue={setTime} label="Thời gian:"></InputTime>
                    </div>
                    <InputText
                      label="Họ tên:"
                      type="text"
                      placeholder="Nhập họ tên"
                      value={bookName}
                      setValue={setBookName}
                      name="name"
                    />
                    <InputText
                      label="SĐT:"
                      type="text"
                      value={bookPhone}
                      setValue={setBookPhone}
                      name="phone"
                      placeholder="Nhập số điện thoại"
                    />
                    <InputText
                      label="Email:"
                      type="text"
                      value={bookEmail}
                      setValue={setBookEmail}
                      name="email"
                      placeholder="Nhập Email"
                    />
                  </div>
                </div>
              </div>
              <div
                className={classnames(
                  { display_none: currentTab !== 'receive' },
                  'receive_information height_100_percent',
                )}
              >
                <div className="margin_top_20 height_100_percent display_flex justify_content_sb flex_direction_col">
                  <div>
                    <InputText
                      label="Họ tên:"
                      type="text"
                      value={receiveName}
                      setValue={setReceiveName}
                      name="name"
                      placeholder="Nhập họ tên"
                    />
                    <InputText
                      label="SĐT:"
                      type="text"
                      value={receivePhone}
                      setValue={setReceivePhone}
                      name="phone"
                      placeholder="Nhập số điện thoại"
                    />
                    <InputText
                      label="Email:"
                      type="text"
                      value={receiveEmail}
                      setValue={setReceiveEmail}
                      name="email"
                      placeholder="Nhập Email"
                    />
                    <TextArea
                      label="Lời nhắn:"
                      value={receiveNote}
                      setValue={setReceiveNote}
                      placeholder="Gửi lời nhắn"
                      className="height_172"
                    />
                  </div>
                </div>
              </div>
            </div>
            <div className="text_align_center margin_top_20">
              <button
                className="font_size_14 font_weight_600 line_height_17 color_white border_none bg_red border_radius_25 cursor_pointer height_32 width_216"
                onClick={onSubmitSchedule}
              >
                Đặt lịch
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default PostInformation;
