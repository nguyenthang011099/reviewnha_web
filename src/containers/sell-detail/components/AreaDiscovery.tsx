import React, { useEffect, useRef, useState } from 'react';
import classnames from 'classnames';
import discoveryImg from '../../../images/post_detail_discovery_area.png';
import { SliderMethod } from '../../../types/type.slider';
import Slider from '../../../components/slider/Slider';
import { AreaAround, SellListing } from '../../../types/type.sell-listing';
import api from '../../../api';
import { renderAddress } from '../../../utils/sell-listing-helper';
import iconSchool from '../../../images/icon_school.png';
import iconRestaurant from '../../../images/icon_restaurant.png';
import iconMedicine from '../../../images/icon_medicine.png';
import iconShopping from '../../../images/icon_shopping.png';
import iconOffice from '../../../images/icon_office.png';
import iconOther from '../../../images/icon_other.png';
import imageSchool from '../../../images/image_school.png';
import imageRestaurant from '../../../images/image_restaurant.png';
import imageMedicine from '../../../images/image_medicine.png';
import imageShopping from '../../../images/image_shopping.png';
import imageOffice from '../../../images/image_office.png';
import imageOther from '../../../images/image_other.png';

interface Props {
  postDetail: SellListing;
}

const AreaDiscovery: React.FC<Props> = (props) => {
  const [listImages, setListImages] = useState<AreaAround[]>([]);
  let slider: SliderMethod = {};
  const [schools, setSchools] = useState<AreaAround[]>([]);
  const [medicines, setMedicines] = useState<AreaAround[]>([]);
  const [offices, setOffices] = useState<AreaAround[]>([]);
  const [restaurants, setRestaurants] = useState<AreaAround[]>([]);
  const [others, setOthers] = useState<AreaAround[]>([]);
  const [shoppings, setShoppings] = useState<AreaAround[]>([]);
  const [activeTab, setActiveTab] = useState<number>(1);
  const [activePositions, setActivePositions] = useState<AreaAround[]>([]);
  const [currentPosition, setCurrentPosition] = useState<{ lat: number; lon: number } | null>(null);
  const map = useRef(null);
  const [markers, setMarkers] = useState([]);
  const [infoWindows, setInfoWindows] = useState([]);

  useEffect(() => {
    if (currentPosition) {
      getPlacesAround(0);
      getPlacesAround(1);
      getPlacesAround(3);
      getPlacesAround(4);
      getPlacesAround(9);
      getPlacesAround(5);
    }
  }, [currentPosition]);

  useEffect(() => {
    if (props.postDetail?.mapPayload) {
      const position = props.postDetail.mapPayload;
      setCurrentPosition({ lat: position.latitude, lon: position.longitude });
    }
  }, [props.postDetail?.mapPayload]);

  useEffect(() => {
    setListImages(activePositions);
    initMap();
  }, [activePositions]);

  const initMap = () => {
    if (currentPosition) {
      window['infoWindows'] = [];

      const myLatLng = { lat: currentPosition.lat, lng: currentPosition.lon };
      const google: any = (window as any).google;
      map.current = new google.maps.Map(document.getElementById('map'), {
        zoom: 15,
        center: myLatLng,
      });
      // radius
      new google.maps.Circle({
        strokeColor: '#FF0000',
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: 'rgba(0,0,0,0)',
        fillOpacity: 0.35,
        map: map.current,
        center: myLatLng,
        radius: 1000,
      });
      // current position
      const marker: any = new google.maps.Marker({
        position: myLatLng,
        map: map.current,
        title: 'Hello World!',
      });
      const infowindow = new google.maps.InfoWindow({
        content: `<div>${renderAddress(props.postDetail)}<div>`,
      });
      marker.addListener('click', () => {
        window['infoWindows'].forEach((item) => {
          item.close();
        });
        infowindow.open({
          anchor: marker,
          map: map.current,
          shouldFocus: false,
        });
      });
      window['infoWindows'].push(infowindow);
      // around position
      const listMarker = [];
      const listInfoWindow = [];
      activePositions.forEach((item: AreaAround) => {
        const marker: any = new google.maps.Marker({
          position: { lat: Number(item.lat), lng: Number(item.lon) },
          map: map.current,
          icon: renderItem(activeTab).icon,
        });
        const infowindow = new google.maps.InfoWindow({
          content: `<div>
          <div style="font-weight: 600">${item.name}</div>
          ${item.address}
          <div><b>Khoảng cách: </b>${(Number(Number(item.distance).toFixed(3)) * 1000).toFixed(0)}m</div>
          <div>`,
        });
        marker.addListener('click', () => {
          window['infoWindows'].forEach((item) => {
            item.close();
          });
          infowindow.open({
            anchor: marker,
            map: map.current,
            shouldFocus: false,
          });
        });
        listMarker.push(marker);
        listInfoWindow.push(infowindow);
        window['infoWindows'].push(infowindow);
      });
      setMarkers(listMarker);
      setInfoWindows(listInfoWindow);
    }
  };

  const getPlacesAround = async (type: number) => {
    const data = new FormData();
    data.append('radius', '1000');
    data.append('radius_fix', '1000');
    data.append('types', String(type));
    data.append('lat', String(currentPosition.lat));
    data.append('lon', String(currentPosition.lon));
    data.append('m', 'pjdetail');
    data.append('v', '1628238552067');
    const res: any = await api.post('/project/places', data);
    if (res.success) {
      switch (type) {
        case 0: {
          setRestaurants(res?.data || []);
          break;
        }
        case 1: {
          setShoppings(res?.data || []);
          setActivePositions(res?.data || []);
          break;
        }
        case 3: {
          setOffices(res?.data || []);
          break;
        }
        case 4: {
          setSchools(res?.data || []);
          break;
        }
        case 5: {
          setMedicines(res?.data || []);
          break;
        }
        case 9: {
          setOthers(res?.data || []);
          break;
        }
      }
    }
  };

  const renderAreaIcon = (type: string) => {
    switch (Number(type)) {
      case 0: {
        return imageRestaurant;
      }
      case 1: {
        return imageShopping;
      }
      case 3: {
        return imageOffice;
      }
      case 4: {
        return imageSchool;
      }
      case 5: {
        return imageMedicine;
      }
      case 9: {
        return imageOther;
      }
    }
  };

  const renderItem = (type: number) => {
    switch (type) {
      case 1: {
        return {
          name: 'Giải trí & Mua sắm',
          count: shoppings.length < 10 ? '0' + shoppings.length : shoppings.length,
          icon: iconShopping,
        };
      }
      case 2: {
        return {
          name: 'Ăn uống',
          count: restaurants.length < 10 ? '0' + restaurants.length : restaurants.length,
          icon: iconRestaurant,
        };
      }
      case 3: {
        return {
          name: 'Y tế',
          count: medicines.length < 10 ? '0' + medicines.length : medicines.length,
          icon: iconMedicine,
        };
      }
      case 4: {
        return {
          name: 'Giáo dục',
          count: schools.length < 10 ? '0' + schools.length : schools.length,
          icon: iconSchool,
        };
      }
      case 5: {
        return {
          name: 'Hành chính & Tài chính',
          count: offices.length < 10 ? '0' + offices.length : offices.length,
          icon: iconOffice,
        };
      }
      case 6: {
        return {
          name: 'Khác',
          count: others.length < 10 ? '0' + others.length : others.length,
          icon: iconOther,
        };
      }
    }
  };

  const changeActivePosition = (type: number) => {
    switch (type) {
      case 1: {
        setActivePositions(shoppings);
        break;
      }
      case 2: {
        setActivePositions(restaurants);
        break;
      }
      case 3: {
        setActivePositions(medicines);
        break;
      }
      case 4: {
        setActivePositions(schools);
        break;
      }
      case 5: {
        setActivePositions(offices);
        break;
      }
      case 6: {
        setActivePositions(others);
        break;
      }
    }
  };

  if (!props.postDetail?.mapPayload) {
    return null;
  }

  return (
    <div className="area_discovery container margin_bottom_96  padding_x_xs_22 margin_bottom_xs_36">
      <div className="display_flex margin_x_-10 min_height_468 display_xs_block">
        <div className="flex_1 padding_x_10 width_1 width_xs_100_percent">
          <h2 className="margin_y_0 margin_bottom_24 font_size_16 font_weight_600">Khám phá tiện ích xung quanh</h2>
          <div className="area_number  border_width_0 border_bottom_width_1 border_color_black border_style_solid padding_bottom_8">
            <div className="display_flex margin_x_-12 flex_wrap flex_nowrap_xs overflow_auto width_xs_100_percent">
              {Array.from(Array(6).keys()).map((item: number) => {
                return (
                  <div className="col_4 padding_x_12 margin_bottom_16 width_xs_178 flex_xs_none" key={item}>
                    <div
                      className={classnames(
                        {
                          'bg_green  border_color_green': activeTab === item + 1,
                          border_color_black: activeTab !== item + 1,
                        },
                        `display_flex justify_content_sb padding_x_24 padding_y_18 align_items_fe height_96 border_radius_24 border_width_1 border_style_solid no_select cursor_pointer padding_x_xs_16
                        height_xs_78`,
                      )}
                      onClick={() => {
                        setActiveTab(item + 1);
                        changeActivePosition(item + 1);
                      }}
                    >
                      <div className="display_flex justify_content_sb flex_direction_col height_100_percent">
                        <div className="display_flex">
                          <svg
                            width="20"
                            height="16"
                            viewBox="0 0 20 16"
                            fill="none"
                            xmlns="http://www.w3.org/2000/svg"
                          >
                            <path
                              d="M18.1444 7.0786V2.62478C18.1444 1.64286 17.3455 0.843994 16.3636 0.843994H3.63641C2.65449 0.843994 1.85563 1.64286 1.85563 2.62478V7.0786C0.787031 7.37442 0 8.35489 0 9.5163V12.9004C0 13.2283 0.265859 13.4942 0.59375 13.4942H1.85563V14.5623C1.85563 14.8902 2.12148 15.156 2.44937 15.156C2.77727 15.156 3.04312 14.8902 3.04312 14.5623V13.4942H16.9569V14.5623C16.9569 14.8902 17.2227 15.156 17.5506 15.156C17.8785 15.156 18.1444 14.8902 18.1444 14.5623V13.4942H19.4062C19.7341 13.4942 20 13.2283 20 12.9004V9.5163C20 8.35489 19.2129 7.37442 18.1444 7.0786ZM3.04312 2.62478C3.04312 2.29767 3.3093 2.03149 3.63641 2.03149H16.3636C16.6907 2.03149 16.9569 2.29767 16.9569 2.62478V6.98673H15.5657V5.79489C15.5657 4.75853 14.7226 3.9154 13.6862 3.9154H11.2858C10.7887 3.9154 10.3365 4.10966 10 4.42587C9.66359 4.1097 9.21133 3.9154 8.7143 3.9154H6.31387C5.2775 3.9154 4.43437 4.75856 4.43437 5.79489V6.98673H3.04312V2.62478ZM14.3782 5.79489V6.98673H10.5938V5.79489C10.5938 5.41329 10.9042 5.1029 11.2857 5.1029H13.6862C14.0678 5.1029 14.3782 5.41333 14.3782 5.79489ZM9.40621 5.79489V6.98673H5.62176V5.79489C5.62176 5.41329 5.93223 5.1029 6.31375 5.1029H8.71418C9.09578 5.1029 9.40621 5.41333 9.40621 5.79489ZM18.8125 12.3067H1.1875V9.5163C1.1875 8.7763 1.78953 8.17423 2.52957 8.17423H17.4704C18.2104 8.17423 18.8125 8.77626 18.8125 9.5163V12.3067Z"
                              className={classnames({
                                fill_white: activeTab === item + 1,
                                fill_black: activeTab !== item + 1,
                              })}
                            />
                          </svg>
                        </div>
                        <span
                          className={classnames(
                            {
                              color_white: activeTab === item + 1,
                            },
                            'text_over_flow_2 display_block font_size_13 max_width_80 text_over_flow_xs_1',
                          )}
                        >
                          {renderItem(item + 1).name}
                        </span>
                      </div>
                      <div>
                        <span
                          className={classnames(
                            {
                              color_white: activeTab === item + 1,
                            },
                            'display_block font_size_32 line_height_39 font_weight_600 font_size_xs_24',
                          )}
                        >
                          {renderItem(item + 1).count}
                        </span>
                      </div>
                    </div>
                  </div>
                );
              })}
            </div>
          </div>
          <div
            className={classnames(
              { display_none: !listImages.length },
              'margin_top_24 position_relative display_xs_none',
            )}
          >
            <Slider
              slidesPerView={5}
              className="border_top_left_radius_15 border_top_right_radius_15"
              listImages={listImages as any}
              setListImages={setListImages}
              slidesNm={20}
              slidesMd={100}
              slidesSm={100}
              slidesXs={100}
              innerRef={slider}
              showButton={true}
              isStopAutoPlay={true}
              margin={24}
              reCalculate={activeTab}
            >
              {listImages.map((item, index) => {
                return (
                  <div
                    className="discovery_image_item width_96 flex_none margin_x_12 cursor_pointer no_select"
                    key={index}
                    onClick={() => {
                      if (map.current) {
                        map.current.setCenter({ lat: Number(item.lat), lng: Number(item.lon) });
                        for (let i = 0; i < markers.length; i++) {
                          const marker = markers[i];
                          const position = marker.getPosition();
                          if (
                            Number(position.lat()) === Number(item.lat) &&
                            Number(position.lng()) == Number(item.lon)
                          ) {
                            infoWindows.forEach((item) => {
                              item.close();
                            });
                            new (window as any).google.maps.event.trigger(marker, 'click');
                            break;
                          }
                        }
                      }
                    }}
                  >
                    <img
                      src={renderAreaIcon(item.typeid) ? renderAreaIcon(item.typeid) : discoveryImg}
                      alt="discovery"
                      className="display_block border_radius_15 height_96 object_fit_cover width_100_percent"
                    />
                    <div className="margin_top_8">
                      <span className="display_block font_size_14 line_height_17 font_weight_600 margin_bottom_4 text_over_flow_1">
                        {item.name}
                      </span>
                      <span className="display_block font_size_12 line_height_18">
                        {(Number(Number(item.distance).toFixed(3)) * 1000).toFixed(0)}m
                      </span>
                    </div>
                  </div>
                );
              })}
            </Slider>
          </div>
          {/* mobile */}
          <div className="display_xs_flex display_none width_100_percent overflow_auto margin_top_24">
            {listImages.map((item, index) => {
              return (
                <div
                  className="discovery_image_item width_96 flex_none margin_right_12 cursor_pointer no_select"
                  key={index}
                  onClick={() => {
                    if (map.current) {
                      map.current.setCenter({ lat: Number(item.lat), lng: Number(item.lon) });
                      for (let i = 0; i < markers.length; i++) {
                        const marker = markers[i];
                        const position = marker.getPosition();
                        if (Number(position.lat()) === Number(item.lat) && Number(position.lng()) == Number(item.lon)) {
                          infoWindows.forEach((item) => {
                            item.close();
                          });
                          new (window as any).google.maps.event.trigger(marker, 'click');
                          break;
                        }
                      }
                    }
                  }}
                >
                  <img
                    src={renderAreaIcon(item.typeid) ? renderAreaIcon(item.typeid) : discoveryImg}
                    alt="discovery"
                    className="display_block border_radius_15 height_96 object_fit_cover width_100_percent"
                  />
                  <div className="margin_top_8">
                    <span className="display_block font_size_14 line_height_17 font_weight_600 margin_bottom_4 text_over_flow_1">
                      {item.name}
                    </span>
                    <span className="display_block font_size_12 line_height_18">
                      {(Number(Number(item.distance).toFixed(3)) * 1000).toFixed(0)}m
                    </span>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
        <div className="width_574 margin_x_10 flex_none width_xs_100_percent height_xs_210 margin_x_xs_0 margin_top_xs_16">
          <div
            className="width_100_percent height_100_percent border_radius_25 padding_8 padding_xs_0"
            style={{ boxShadow: '1px 1px 2px 1px rgba(23, 23, 23, 0.16)' }}
          >
            <div className="width_100_percent height_100_percent border_radius_25" id="map"></div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AreaDiscovery;
