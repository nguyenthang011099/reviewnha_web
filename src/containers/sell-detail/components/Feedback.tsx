import React, { useEffect, useState } from 'react';
import Slider from '../../../components/slider/Slider';
import avatarImg from '../../../images/feedback_avatar.png';
import { Feedback } from '../../../types/type.sell-listing';
import { formatDateBlog } from '../../../utils/format-date-blog';
import FeedbackView from '../items/FeedbackView';

interface Props {
  list: Feedback[];
}

const FeedbackComponent: React.FC<Props> = (props) => {
  const [feedbackList, setFeedbackList] = useState<Feedback[]>([]);
  const [isOpen, setIsOpen] = useState<boolean>(false);
  const [activeFeedback, setActiveFeedback] = useState<Feedback>({});

  useEffect(() => {
    if (props.list) {
      setFeedbackList(props.list);
    }
  }, [props.list]);

  const onOpenFeedbackView = (feedback: Feedback) => {
    setIsOpen(true);
    setActiveFeedback(feedback);
  };

  if (!feedbackList.length) {
    return null;
  }

  return (
    <div className="feedback container margin_bottom_96  padding_x_xs_22 margin_bottom_xs_36">
      <FeedbackView isOpen={isOpen} setIsOpen={setIsOpen} feedback={activeFeedback}></FeedbackView>
      <div className="border_width_0 border_bottom_width_1 border_color_black border_style_solid border_width_xs_0">
        <div className="display_flex justify_content_sb margin_bottom_24 display_xs_block">
          <h2 className="margin_y_0 font_weight_600 font_size_16">Cảm nhận dân cư địa phương về dự án</h2>
          <span className="font_size_15 line_height_22">Dựa trên đánh giá của 213 người dân</span>
        </div>
        <div className="display_flex flex_wrap margin_x_-14">
          {Array.from(Array(6).keys()).map((item: number) => {
            return (
              <div className="col_4 padding_x_14 col_xs_12" key={item}>
                <div
                  className="display_flex justify_content_sb align_items_center height_46 border_radius_18 border_width_1 
            border_color_black border_style_solid padding_x_26 padding_y_4 padding_right_4 margin_bottom_24"
                >
                  <div className="display_flex align_items_center">
                    <div className="width_30">
                      <svg width="20" height="21" viewBox="0 0 20 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path
                          d="M5.42631 17.4093C6.25567 17.4093 6.93514 18.1033 6.93514 18.9605C6.93514 19.8075 6.25567 20.5015 5.42631 20.5015C4.58697 20.5015 3.9075 19.8075 3.9075 18.9605C3.9075 18.1033 4.58697 17.4093 5.42631 17.4093ZM16.6676 17.4093C17.4969 17.4093 18.1764 18.1033 18.1764 18.9605C18.1764 19.8075 17.4969 20.5015 16.6676 20.5015C15.8282 20.5015 15.1487 19.8075 15.1487 18.9605C15.1487 18.1033 15.8282 17.4093 16.6676 17.4093ZM0.778226 0.501584L0.880056 0.510203L3.2632 0.876566C3.60293 0.938817 3.85274 1.22354 3.88272 1.57051L4.07257 3.85645C4.10254 4.18404 4.36234 4.42896 4.68209 4.42896H18.1766C18.7861 4.42896 19.1858 4.64326 19.5855 5.1127C19.9852 5.58213 20.0551 6.25567 19.9652 6.86695L19.0159 13.5615C18.8361 14.8484 17.7569 15.7964 16.4879 15.7964H5.58639C4.25742 15.7964 3.15828 14.7565 3.04837 13.4094L2.12908 2.28487L0.620259 2.01954C0.22057 1.9481 -0.0592117 1.5501 0.0107338 1.1419C0.0806793 0.72451 0.470376 0.447952 0.880056 0.510203L0.778226 0.501584ZM14.8891 8.20382H12.1213C11.7016 8.20382 11.3719 8.54059 11.3719 8.9692C11.3719 9.38761 11.7016 9.73458 12.1213 9.73458H14.8891C15.3088 9.73458 15.6386 9.38761 15.6386 8.9692C15.6386 8.54059 15.3088 8.20382 14.8891 8.20382Z"
                          fill="#EBAA60"
                        />
                      </svg>
                    </div>
                    <span className="font_size_15 line_height_22">Nhiều hàng quán</span>
                  </div>
                  <div className="height_100_percent display_flex align_items_center ">
                    <span className="font_size_14 line_height_17 font_weight_600 ">75%</span>
                  </div>
                </div>
              </div>
            );
          })}
        </div>
      </div>
      <div className="margin_y_24 font_weight_600 margin_top_xs_0">
        <span>Đánh giá cộng đồng về dự án</span>
      </div>
      <div className="display_flex display_xs_none">
        <Slider
          slidesPerView={4}
          className="border_radius_24 padding_bottom_5"
          listImages={feedbackList as any}
          setListImages={setFeedbackList}
          slidesNm={25}
          slidesMd={100}
          slidesSm={100}
          slidesXs={100}
          showButton={true}
          isStopAutoPlay={true}
          margin={20}
          loop={false}
        >
          {feedbackList.map((feedback: Feedback) => {
            return (
              <div
                key={feedback.id}
                className="width_277 margin_x_10 margin_bottom_24 flex_none padding_x_1 cursor_pointer"
                onClick={() => {
                  onOpenFeedbackView(feedback);
                }}
              >
                <div
                  className="border_radius_24 padding_24 padding_bottom_36 height_388 display_flex flex_direction_col justify_content_sb "
                  style={{
                    boxShadow: '1px 1px 2px 1px rgba(23, 23, 23, 0.16)',
                  }}
                >
                  <div>
                    <div className="title display_flex justify_content_sb align_items_center margin_bottom_8">
                      <div className="display_flex align_items_center">
                        <img
                          src={feedback.cusAvatar ? feedback.cusAvatar : avatarImg}
                          alt="avatar"
                          className="width_32 height_32 margin_right_16 border_radius_50_percent"
                        />
                        <div>
                          <h5 className="font_size_14 font_weight_600 line_height_17 margin_y_0">{feedback.cusName}</h5>
                          <span className="font_size_12 line_height_18 display_block">
                            {formatDateBlog(feedback.createdAt)}
                          </span>
                        </div>
                      </div>
                    </div>
                    <div className="rate display_flex margin_bottom_16">
                      {Array.from(Array(5).keys()).map((item: number) => {
                        if (item + 1 <= feedback.point) {
                          return (
                            <svg
                              width="14"
                              height="14"
                              key={item}
                              viewBox="0 0 14 14"
                              fill="none"
                              xmlns="http://www.w3.org/2000/svg"
                            >
                              <path
                                d="M10.9458 8.54675C10.7732 8.71408 10.6938 8.95608 10.7332 9.19341L11.3258 12.4734C11.3758 12.7514 11.2585 13.0327 11.0258 13.1934C10.7978 13.3601 10.4945 13.3801 10.2458 13.2467L7.29317 11.7067C7.1905 11.6521 7.0765 11.6227 6.95984 11.6194H6.77917C6.7165 11.6287 6.65517 11.6487 6.59917 11.6794L3.64584 13.2267C3.49984 13.3001 3.3345 13.3261 3.1725 13.3001C2.77784 13.2254 2.5145 12.8494 2.57917 12.4527L3.1725 9.17275C3.21184 8.93341 3.1325 8.69008 2.95984 8.52008L0.552502 6.18675C0.351169 5.99141 0.281169 5.69808 0.373169 5.43341C0.462502 5.16941 0.690502 4.97675 0.965836 4.93341L4.27917 4.45275C4.53117 4.42675 4.7525 4.27341 4.86584 4.04675L6.32584 1.05341C6.3605 0.986748 6.40517 0.925415 6.45917 0.873415L6.51917 0.826748C6.5505 0.792081 6.5865 0.763415 6.6265 0.740081L6.69917 0.713415L6.8125 0.666748H7.09317C7.34384 0.692748 7.5645 0.842748 7.67984 1.06675L9.15917 4.04675C9.26584 4.26475 9.47317 4.41608 9.7125 4.45275L13.0258 4.93341C13.3058 4.97341 13.5398 5.16675 13.6325 5.43341C13.7198 5.70075 13.6445 5.99408 13.4392 6.18675L10.9458 8.54675Z"
                                fill="#FEA82F"
                              />
                            </svg>
                          );
                        }
                        return (
                          <svg
                            width="14"
                            height="14"
                            key={item}
                            viewBox="0 0 14 14"
                            fill="none"
                            xmlns="http://www.w3.org/2000/svg"
                          >
                            <path
                              d="M10.9458 8.54675C10.7732 8.71408 10.6938 8.95608 10.7332 9.19341L11.3258 12.4734C11.3758 12.7514 11.2585 13.0327 11.0258 13.1934C10.7978 13.3601 10.4945 13.3801 10.2458 13.2467L7.29317 11.7067C7.1905 11.6521 7.0765 11.6227 6.95984 11.6194H6.77917C6.7165 11.6287 6.65517 11.6487 6.59917 11.6794L3.64584 13.2267C3.49984 13.3001 3.3345 13.3261 3.1725 13.3001C2.77784 13.2254 2.5145 12.8494 2.57917 12.4527L3.1725 9.17275C3.21184 8.93341 3.1325 8.69008 2.95984 8.52008L0.552502 6.18675C0.351169 5.99141 0.281169 5.69808 0.373169 5.43341C0.462502 5.16941 0.690502 4.97675 0.965836 4.93341L4.27917 4.45275C4.53117 4.42675 4.7525 4.27341 4.86584 4.04675L6.32584 1.05341C6.3605 0.986748 6.40517 0.925415 6.45917 0.873415L6.51917 0.826748C6.5505 0.792081 6.5865 0.763415 6.6265 0.740081L6.69917 0.713415L6.8125 0.666748H7.09317C7.34384 0.692748 7.5645 0.842748 7.67984 1.06675L9.15917 4.04675C9.26584 4.26475 9.47317 4.41608 9.7125 4.45275L13.0258 4.93341C13.3058 4.97341 13.5398 5.16675 13.6325 5.43341C13.7198 5.70075 13.6445 5.99408 13.4392 6.18675L10.9458 8.54675Z"
                              fill="#808080"
                            />
                          </svg>
                        );
                      })}
                    </div>
                    <div className="content">
                      <span className="text_over_flow_6 line_height_28">"{feedback.content}"</span>
                    </div>
                  </div>
                  <div className="display_flex margin_x_-4">
                    {feedback.media.map((item: string, index: number) => {
                      if (index < 3) {
                        return (
                          <div className="padding_x_4" key={index}>
                            <img
                              src={item}
                              alt="feedback_image"
                              className="width_76 height_76 border_radius_16 object_fit_cover"
                            />
                          </div>
                        );
                      }
                      return null;
                    })}
                  </div>
                </div>
              </div>
            );
          })}
        </Slider>
      </div>
      <div className="display_xs_flex display_none width_100_percent overflow_auto">
        {feedbackList.map((feedback: Feedback) => {
          return (
            <div
              key={feedback.id}
              className="width_277 margin_right_20 margin_bottom_24 flex_none padding_x_1 cursor_pointer"
              onClick={() => {
                onOpenFeedbackView(feedback);
              }}
            >
              <div
                className="border_radius_24 padding_24 padding_bottom_36 height_388 display_flex flex_direction_col justify_content_sb "
                style={{
                  boxShadow: '1px 1px 2px 1px rgba(23, 23, 23, 0.16)',
                }}
              >
                <div>
                  <div className="title display_flex justify_content_sb align_items_center margin_bottom_8">
                    <div className="display_flex align_items_center">
                      <img
                        src={feedback.cusAvatar ? feedback.cusAvatar : avatarImg}
                        alt="avatar"
                        className="width_32 height_32 margin_right_16 border_radius_50_percent"
                      />
                      <div>
                        <h5 className="font_size_14 font_weight_600 line_height_17 margin_y_0">{feedback.cusName}</h5>
                        <span className="font_size_12 line_height_18 display_block">
                          {formatDateBlog(feedback.createdAt)}
                        </span>
                      </div>
                    </div>
                  </div>
                  <div className="rate display_flex margin_bottom_16">
                    {Array.from(Array(5).keys()).map((item: number) => {
                      if (item + 1 <= feedback.point) {
                        return (
                          <svg
                            width="14"
                            height="14"
                            key={item}
                            viewBox="0 0 14 14"
                            fill="none"
                            xmlns="http://www.w3.org/2000/svg"
                          >
                            <path
                              d="M10.9458 8.54675C10.7732 8.71408 10.6938 8.95608 10.7332 9.19341L11.3258 12.4734C11.3758 12.7514 11.2585 13.0327 11.0258 13.1934C10.7978 13.3601 10.4945 13.3801 10.2458 13.2467L7.29317 11.7067C7.1905 11.6521 7.0765 11.6227 6.95984 11.6194H6.77917C6.7165 11.6287 6.65517 11.6487 6.59917 11.6794L3.64584 13.2267C3.49984 13.3001 3.3345 13.3261 3.1725 13.3001C2.77784 13.2254 2.5145 12.8494 2.57917 12.4527L3.1725 9.17275C3.21184 8.93341 3.1325 8.69008 2.95984 8.52008L0.552502 6.18675C0.351169 5.99141 0.281169 5.69808 0.373169 5.43341C0.462502 5.16941 0.690502 4.97675 0.965836 4.93341L4.27917 4.45275C4.53117 4.42675 4.7525 4.27341 4.86584 4.04675L6.32584 1.05341C6.3605 0.986748 6.40517 0.925415 6.45917 0.873415L6.51917 0.826748C6.5505 0.792081 6.5865 0.763415 6.6265 0.740081L6.69917 0.713415L6.8125 0.666748H7.09317C7.34384 0.692748 7.5645 0.842748 7.67984 1.06675L9.15917 4.04675C9.26584 4.26475 9.47317 4.41608 9.7125 4.45275L13.0258 4.93341C13.3058 4.97341 13.5398 5.16675 13.6325 5.43341C13.7198 5.70075 13.6445 5.99408 13.4392 6.18675L10.9458 8.54675Z"
                              fill="#FEA82F"
                            />
                          </svg>
                        );
                      }
                      return (
                        <svg
                          width="14"
                          height="14"
                          key={item}
                          viewBox="0 0 14 14"
                          fill="none"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <path
                            d="M10.9458 8.54675C10.7732 8.71408 10.6938 8.95608 10.7332 9.19341L11.3258 12.4734C11.3758 12.7514 11.2585 13.0327 11.0258 13.1934C10.7978 13.3601 10.4945 13.3801 10.2458 13.2467L7.29317 11.7067C7.1905 11.6521 7.0765 11.6227 6.95984 11.6194H6.77917C6.7165 11.6287 6.65517 11.6487 6.59917 11.6794L3.64584 13.2267C3.49984 13.3001 3.3345 13.3261 3.1725 13.3001C2.77784 13.2254 2.5145 12.8494 2.57917 12.4527L3.1725 9.17275C3.21184 8.93341 3.1325 8.69008 2.95984 8.52008L0.552502 6.18675C0.351169 5.99141 0.281169 5.69808 0.373169 5.43341C0.462502 5.16941 0.690502 4.97675 0.965836 4.93341L4.27917 4.45275C4.53117 4.42675 4.7525 4.27341 4.86584 4.04675L6.32584 1.05341C6.3605 0.986748 6.40517 0.925415 6.45917 0.873415L6.51917 0.826748C6.5505 0.792081 6.5865 0.763415 6.6265 0.740081L6.69917 0.713415L6.8125 0.666748H7.09317C7.34384 0.692748 7.5645 0.842748 7.67984 1.06675L9.15917 4.04675C9.26584 4.26475 9.47317 4.41608 9.7125 4.45275L13.0258 4.93341C13.3058 4.97341 13.5398 5.16675 13.6325 5.43341C13.7198 5.70075 13.6445 5.99408 13.4392 6.18675L10.9458 8.54675Z"
                            fill="#808080"
                          />
                        </svg>
                      );
                    })}
                  </div>
                  <div className="content">
                    <span className="text_over_flow_6 line_height_28">"{feedback.content}"</span>
                  </div>
                </div>
                <div className="display_flex margin_x_-4">
                  {feedback.media.map((item: string, index: number) => {
                    if (index < 3) {
                      return (
                        <div className="padding_x_4" key={index}>
                          <img
                            src={item}
                            alt="feedback_image"
                            className="width_76 height_76 border_radius_16 object_fit_cover"
                          />
                        </div>
                      );
                    }
                    return null;
                  })}
                </div>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default FeedbackComponent;
