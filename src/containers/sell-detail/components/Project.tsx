import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import avatar from '../../../images/user_avatar.png';
import { RootState } from '../../../reducers';
import { Project, SellListing } from '../../../types/type.sell-listing';

interface Props {
  postDetail: SellListing;
}

const ProjectInfo: React.FC<Props> = (props) => {
  const projectList: Project[] = useSelector((state: RootState) => state.sellListing.projectList);
  const [project, setProject] = useState<Project>({});

  useEffect(() => {
    const activeProject = projectList.find((item) => Number(item.id) === Number(props.postDetail.projectId));
    setProject(activeProject);
  }, [props.postDetail, projectList]);

  if (!project) {
    if (props.postDetail?.projectName) {
      return (
        <div className="project container margin_bottom_96  padding_x_xs_22 margin_bottom_xs_36">
          <span className="font_size_16 font_weight_600">Thuộc dự án: {props.postDetail.projectName}</span>
        </div>
      );
    }
    return null;
  }

  return (
    <div className="project container margin_bottom_96  padding_x_xs_22 margin_bottom_xs_36">
      <div className="margin_bottom_24">
        <span className="font_size_16 font_weight_600">Thuộc dự án</span>
      </div>
      <div className="border_radius_18 border_color_grey border_width_1 border_style_solid padding_24 display_flex align_items_center display_xs_block">
        <div className="width_396 height_230 margin_right_56 width_xs_100_percent height_xs_164 margin_bottom_xs_24">
          <img src={project.image} alt="project" className="width_100_percent height_100_percent border_radius_18" />
        </div>
        <div>
          {/* address */}
          <div className="display_flex align_items_fs margin_bottom_8">
            <svg
              width={10}
              height={14}
              viewBox="0 0 10 14"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
              className="opacity_0_5 margin_top_3 flex_none"
            >
              <path
                d="M2.68773 0.652163C4.1443 -0.194168 5.93466 -0.179376 7.37757 0.690912C8.80628 1.57892 9.67462 3.16377 9.66659 4.86861C9.63328 6.56227 8.70217 8.15431 7.53829 9.38504C6.86653 10.0986 6.11505 10.7295 5.29921 11.265C5.21518 11.3136 5.12315 11.3461 5.02764 11.361C4.93571 11.3571 4.84619 11.3299 4.76715 11.2819C3.5216 10.4774 2.42887 9.45034 1.54153 8.25029C0.799038 7.24856 0.377203 6.03832 0.333313 4.78393L0.336637 4.60144C0.397261 2.96408 1.28318 1.46827 2.68773 0.652163ZM5.60482 3.38416C5.01269 3.13247 4.33001 3.26922 3.87552 3.73055C3.42104 4.19188 3.28441 4.8868 3.52942 5.49084C3.77444 6.09487 4.35277 6.48888 4.99438 6.48888C5.41471 6.4919 5.81877 6.32353 6.11652 6.0213C6.41427 5.71906 6.58097 5.30807 6.57949 4.8799C6.58172 4.22634 6.19695 3.63586 5.60482 3.38416Z"
                fill="#3B4144"
              />
              <path
                d="M4.99996 13.361C6.84091 13.361 8.33329 13.0625 8.33329 12.6943C8.33329 12.3261 6.84091 12.0276 4.99996 12.0276C3.15901 12.0276 1.66663 12.3261 1.66663 12.6943C1.66663 13.0625 3.15901 13.361 4.99996 13.361Z"
                fill="#3B4144"
              />
            </svg>
            <span className="font_size_14 opacity_0_5 margin_left_12">{project.address}</span>
          </div>
          {/* name */}
          <h2 className="font_size_24 margin_0 margin_bottom_16">{project.name}</h2>
          {/* rate */}
          <div className="display_flex  align_items_center ">
            <div className="display_flex align_items_center ">
              <div
                className="width_35 height_35 display_flex align_items_center justify_content_center bg_red"
                style={{ borderRadius: '8px 8px 8px 0px' }}
              >
                <span className="color_white font_weight_600">{project.pointRate}</span>
              </div>
              <div className="margin_left_10 padding_right_24 border_width_0 border_right_width_1 border_color_grey border_style_solid">
                <p className="margin_0 color_red font_size_14">Rất tốt</p>
                <p className="margin_0 font_size_12 opacity_0_5 ">{project.totalRate} đánh giá</p>
              </div>
            </div>
            <div className="padding_left_24">
              <span className="color_red margin_right_4 font_size_14">{project.countExperience}</span>
              <span className="opacity_0_5 font_size_14">Bài tư vấn</span>
            </div>
          </div>
          <div className="width_100_percent height_1 bg_grey margin_top_16 margin_bottom_10"></div>
          <div className="display_flex ">
            <div className="margin_right_10">
              <img
                src={props.postDetail?.seller?.avatar ?? avatar}
                alt="avatar user"
                className="display_block width_32 height_32 border_radius_50_percent"
              />
            </div>
            <div>
              <span className="font_size_14 font_weight_600 line_height_17 display_block">
                {props.postDetail?.seller?.name}
              </span>
              <div className="rate display_flex font_size_12">
                {props.postDetail?.seller?.description?.length > 50
                  ? props.postDetail?.seller?.description?.substr(0, 50) + '...'
                  : props.postDetail?.seller?.description}
              </div>
            </div>
          </div>
        </div>
        <div className="margin_left_56  margin_x_xs_0 margin_top_xs_32 display_xs_flex justify_content_xs_center">
          <a href={project.url} target="_blank" className="width_240 display_block">
            <button className="width_100_percent border_color_grey border_width_1 border_style_solid border_radius_25 bg_white padding_y_8">
              Tìm hiểu thêm
            </button>
          </a>
          {/*<a
            className="display_flex justify_content_center align_items_center margin_top_18"
            href={project.url}
            target="_blank"
          >
            <svg width={12} height={18} viewBox="0 0 12 18" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path
                d="M11.4101 9.14147C11.2839 8.90748 11.0438 8.7616 10.7827 8.7616H6.71729V0.734294C6.71729 0.328964 6.39596 0 6.00003 0C5.6041 0 5.28276 0.328964 5.28276 0.734294V8.7616H1.21731C0.955274 8.7616 0.71523 8.90748 0.589948 9.14147C0.462753 9.37547 0.471361 9.66135 0.610031 9.88752L5.39274 17.6573C5.52472 17.8708 5.75329 18 6.00003 18C6.24676 18 6.47533 17.8708 6.60731 17.6573L11.39 9.88752C11.4627 9.76807 11.5 9.63198 11.5 9.49589C11.5 9.37449 11.4694 9.25211 11.4101 9.14147Z"
                fill="#007882"
              />
            </svg>
            <span className="font_size_14 font_weight_600 color_green margin_left_12">Tải tài liệu đính kèm</span>
          </a>*/}
        </div>
      </div>
    </div>
  );
};

export default ProjectInfo;
