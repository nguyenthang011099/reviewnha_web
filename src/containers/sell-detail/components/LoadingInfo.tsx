import React from 'react';

const LoadingInfo: React.FC = () => {
  return (
    <div className="container  padding_x_xs_22">
      <div className="loading_placeholder height_45 width_100_percent border_radius_5 margin_bottom_24"></div>
      <div className="loading_placeholder height_27 width_50_percent border_radius_5 margin_bottom_4"></div>
      <div className="loading_placeholder height_27 width_50_percent border_radius_5 margin_bottom_52"></div>
      <div className="height_574 width_100_percent border_radius_5 margin_bottom_96 display_flex display_xs_none">
        <div className="loading_placeholder border_radius_18 height_100_percent flex_1"></div>
        <div className="margin_left_20">
          <div className="loading_placeholder border_radius_18 height_277 width_277 margin_bottom_20"></div>
          <div className="loading_placeholder border_radius_18 height_277 width_277"></div>
        </div>
      </div>
      <div className="width_100_percent loading_placeholder border_radius_5 height_610 margin_bottom_96"></div>
    </div>
  );
};

export default LoadingInfo;
