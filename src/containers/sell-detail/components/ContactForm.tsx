import React from 'react';
import contactLogo from '../../../images/contact_avatar.png';

const ContactForm: React.FC = () => {
  return (
    <div className="container  margin_bottom_240">
      <div
        className="border_radius_24 padding_left_100 padding_right_120 padding_y_84 display_flex"
        style={{
          background: 'linear-gradient(106.88deg, rgba(255, 255, 255, 0.75) 0%, rgba(255, 255, 255, 0.375) 100%)',
          backdropFilter: 'blur(24px)',
          border: '1px solid #e1e1e1',
        }}
      >
        <div className="width_300 margin_right_80">
          <p className="margin_y_0 margin_bottom_16 text_align_center font_size_24 line_height_36 font_weight_600">
            Nhận thông tin về bất động sản này
          </p>
          <div>
            <img src={contactLogo} alt="logo" />
          </div>
          <p className="margin_top_16 font_weight_500 font_size_16 line_height_28 text_align_center">
            Còn rất nhiều điều thú vị về căn nhà đang chờ bạn khám phá.
          </p>
        </div>
        <div className="margin_top_26 flex_1">
          <div
            className="display_flex align_items_center margin_bottom_14 border_radius_24 padding_x_24 padding_y_15 
            border_width_1 border_color_black border_style_solid width_100_percent"
          >
            <p className="margin_y_0 width_95 font_size_12 line_height_18">Họ tên:</p>
            <input
              className="border_none font_weight_500 font_size_16 line_height_28 width_100_percent"
              placeholder="Nhập họ tên"
            />
          </div>
          <div
            className="display_flex align_items_center margin_bottom_14 border_radius_24 padding_x_24 padding_y_15 
            border_width_1 border_color_black border_style_solid width_100_percent"
          >
            <p className="margin_y_0 width_95 font_size_12 line_height_18">Email:</p>
            <input
              className="border_none font_weight_500 font_size_16 line_height_28 width_100_percent"
              placeholder="Nhập email"
            />
          </div>
          <div
            className="display_flex align_items_fs margin_bottom_14 border_radius_24 padding_x_24 padding_y_15 
            border_width_1 border_color_black border_style_solid width_100_percent"
          >
            <p className="margin_y_0 width_95 font_size_12 line_height_18">Lời nhắn:</p>
            <textarea
              className="border_none font_weight_500 font_size_16 line_height_28 height_160 width_100_percent"
              placeholder="Viết lời nhắn của bạn"
            />
          </div>
          <div className="display_flex justify_content_center margin_top_20">
            <button
              className="font_size_14 font_weight_600 line_height_17 color_white border_none bg_red border_radius_25 width_336 
            height_30 display_flex justify_content_center align_items_center cursor_pointer"
            >
              Đặt lịch
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ContactForm;
