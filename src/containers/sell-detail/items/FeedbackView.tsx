import React from 'react';
import Popup from '../../../components/popup/Popup';
import { Feedback } from '../../../types/type.sell-listing';
import avatarImg from '../../../images/user_avatar.png';
import { formatDateBlog } from '../../../utils/format-date-blog';

interface Props {
  isOpen: boolean;
  setIsOpen: (_value: boolean) => void;
  feedback: Feedback;
}

const FeedbackView: React.FC<Props> = (props) => {
  const { isOpen, setIsOpen, feedback } = props;

  return (
    <Popup className="border_radius_18 height_auto" isOpen={isOpen} setIsOpen={setIsOpen} isHiddenExitButton={true}>
      <div className="width_574 height_100_percent padding_top_38 display_flex flex_direction_col width_xs_100_percent">
        {/* header */}
        <div className="header text_align_center position_relative margin_x_24">
          <div
            className="close display_flex position_absolute right_0 cursor_pointer"
            onClick={() => {
              setIsOpen(false);
            }}
          >
            <svg width="28" height="28" viewBox="0 0 28 28" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path
                fillRule="evenodd"
                clipRule="evenodd"
                d="M13.9993 0.666656C21.3513 0.666656 27.3327 6.64799 27.3327 14C27.3327 21.352 21.3513 27.3333 13.9993 27.3333C6.64735 27.3333 0.666016 21.352 0.666016 14C0.666016 6.64799 6.64735 0.666656 13.9993 0.666656ZM13.9993 2.66666C7.75002 2.66666 2.66602 7.75066 2.66602 14C2.66602 20.2493 7.75002 25.3333 13.9993 25.3333C20.2487 25.3333 25.3327 20.2493 25.3327 14C25.3327 7.75066 20.2487 2.66666 13.9993 2.66666Z"
                fill="black"
              />
              <path
                fillRule="evenodd"
                clipRule="evenodd"
                d="M10.7737 9.28594C10.3626 8.87487 9.69609 8.87487 9.28501 9.28594C8.87393 9.69702 8.87393 10.3635 9.28501 10.7746L12.5104 14L9.28501 17.2254C8.87393 17.6365 8.87393 18.303 9.28501 18.714C9.69609 19.1251 10.3626 19.1251 10.7737 18.714L13.9991 15.4886L17.2245 18.714C17.6355 19.1251 18.302 19.1251 18.7131 18.714C19.1242 18.303 19.1242 17.6365 18.7131 17.2254L15.4877 14L18.7131 10.7746C19.1242 10.3635 19.1242 9.69702 18.7131 9.28594C18.302 8.87487 17.6355 8.87487 17.2245 9.28594L13.9991 12.5113L10.7737 9.28594Z"
                fill="black"
              />
            </svg>
          </div>
        </div>
        {/* body */}
        <div>
          <div className="padding_x_24 padding_bottom_36">
            <a className="display_flex align_items_center margin_bottom_8" href={feedback.urlCus} target="_blank">
              <img
                src={feedback.cusAvatar ? feedback.cusAvatar : avatarImg}
                alt="avatar"
                className="width_32 height_32 margin_right_16 border_radius_50_percent"
              />
              <div>
                <h5 className="font_size_14 font_weight_600 line_height_17 margin_y_0">{feedback.cusName}</h5>
                <span className="font_size_12 line_height_18 display_block">{formatDateBlog(feedback.createdAt)}</span>
              </div>
            </a>
            <div className="rate display_flex margin_bottom_16">
              {Array.from(Array(5).keys()).map((item: number) => {
                if (item + 1 <= feedback.point) {
                  return (
                    <svg
                      width="14"
                      height="14"
                      key={item}
                      viewBox="0 0 14 14"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        d="M10.9458 8.54675C10.7732 8.71408 10.6938 8.95608 10.7332 9.19341L11.3258 12.4734C11.3758 12.7514 11.2585 13.0327 11.0258 13.1934C10.7978 13.3601 10.4945 13.3801 10.2458 13.2467L7.29317 11.7067C7.1905 11.6521 7.0765 11.6227 6.95984 11.6194H6.77917C6.7165 11.6287 6.65517 11.6487 6.59917 11.6794L3.64584 13.2267C3.49984 13.3001 3.3345 13.3261 3.1725 13.3001C2.77784 13.2254 2.5145 12.8494 2.57917 12.4527L3.1725 9.17275C3.21184 8.93341 3.1325 8.69008 2.95984 8.52008L0.552502 6.18675C0.351169 5.99141 0.281169 5.69808 0.373169 5.43341C0.462502 5.16941 0.690502 4.97675 0.965836 4.93341L4.27917 4.45275C4.53117 4.42675 4.7525 4.27341 4.86584 4.04675L6.32584 1.05341C6.3605 0.986748 6.40517 0.925415 6.45917 0.873415L6.51917 0.826748C6.5505 0.792081 6.5865 0.763415 6.6265 0.740081L6.69917 0.713415L6.8125 0.666748H7.09317C7.34384 0.692748 7.5645 0.842748 7.67984 1.06675L9.15917 4.04675C9.26584 4.26475 9.47317 4.41608 9.7125 4.45275L13.0258 4.93341C13.3058 4.97341 13.5398 5.16675 13.6325 5.43341C13.7198 5.70075 13.6445 5.99408 13.4392 6.18675L10.9458 8.54675Z"
                        fill="#FEA82F"
                      />
                    </svg>
                  );
                }
                return (
                  <svg
                    width="14"
                    height="14"
                    key={item}
                    viewBox="0 0 14 14"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M10.9458 8.54675C10.7732 8.71408 10.6938 8.95608 10.7332 9.19341L11.3258 12.4734C11.3758 12.7514 11.2585 13.0327 11.0258 13.1934C10.7978 13.3601 10.4945 13.3801 10.2458 13.2467L7.29317 11.7067C7.1905 11.6521 7.0765 11.6227 6.95984 11.6194H6.77917C6.7165 11.6287 6.65517 11.6487 6.59917 11.6794L3.64584 13.2267C3.49984 13.3001 3.3345 13.3261 3.1725 13.3001C2.77784 13.2254 2.5145 12.8494 2.57917 12.4527L3.1725 9.17275C3.21184 8.93341 3.1325 8.69008 2.95984 8.52008L0.552502 6.18675C0.351169 5.99141 0.281169 5.69808 0.373169 5.43341C0.462502 5.16941 0.690502 4.97675 0.965836 4.93341L4.27917 4.45275C4.53117 4.42675 4.7525 4.27341 4.86584 4.04675L6.32584 1.05341C6.3605 0.986748 6.40517 0.925415 6.45917 0.873415L6.51917 0.826748C6.5505 0.792081 6.5865 0.763415 6.6265 0.740081L6.69917 0.713415L6.8125 0.666748H7.09317C7.34384 0.692748 7.5645 0.842748 7.67984 1.06675L9.15917 4.04675C9.26584 4.26475 9.47317 4.41608 9.7125 4.45275L13.0258 4.93341C13.3058 4.97341 13.5398 5.16675 13.6325 5.43341C13.7198 5.70075 13.6445 5.99408 13.4392 6.18675L10.9458 8.54675Z"
                      fill="#808080"
                    />
                  </svg>
                );
              })}
            </div>
            <div className="content margin_bottom_24">
              <p className="margin_0 line_height_28">{feedback.content}</p>
            </div>
            <div>
              {feedback.media?.map((item: string, index: number) => {
                return (
                  <div className="width_250 height_172 border_radius_18" key={index}>
                    <img src={item} alt="feedback width_100_percent height_100_percent object_fit_cover" />
                  </div>
                );
              })}
            </div>
          </div>
        </div>
      </div>
    </Popup>
  );
};

export default FeedbackView;
