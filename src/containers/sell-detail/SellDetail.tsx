import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../reducers';
import { clearSellListingDetail } from '../../reducers/sell-listing/actions';
import { SellListing } from '../../types/type.sell-listing';
import AreaDiscovery from './components/AreaDiscovery';
import BlogProject from './components/BlogProject';
import BreadCrumb from './components/BreadCrumb';
import ContactForm from './components/ContactForm';
import Feedback from './components/Feedback';
import LoadingInfo from './components/LoadingInfo';
import PostInformation from './components/PostInformation';
import ProfitCalculator from './components/ProfitCalculator';
import Project from './components/Project';
import RelatedPosts from './components/RelatedPosts';
import Title from './components/Title';

const SellDetail: React.FC<any> = () => {
  const sellListingDetail = useSelector((state: RootState) => state.sellListing.sellListingDetail);
  const dispatch = useDispatch();

  useEffect(() => {
    return () => {
      dispatch(clearSellListingDetail());
    };
  }, []);

  /**
   * getSellListByUser
   */
  const getSellListByUser = () => {
    return sellListingDetail.listByUser.filter((item: SellListing) => item.id !== sellListingDetail.data.id);
  };

  const getRelatedSellListings = () => {
    return sellListingDetail.relatedList.filter((item: SellListing) => item.id !== sellListingDetail.data.id);
  };

  return (
    <div className="margin_top_36 margin_top_xs_12">
      {!sellListingDetail.loading ? (
        <>
          <BreadCrumb detail={sellListingDetail.data}></BreadCrumb>
          <Title postDetail={sellListingDetail.data}></Title>
          <PostInformation postDetail={sellListingDetail.data}></PostInformation>
          <BlogProject list={sellListingDetail.data?.projectExperiences || []}></BlogProject>
          <AreaDiscovery postDetail={sellListingDetail.data}></AreaDiscovery>
          <Project postDetail={sellListingDetail.data}></Project>
          <Feedback list={sellListingDetail.feedbackList}></Feedback>
          <RelatedPosts type="project" list={getRelatedSellListings()}></RelatedPosts>
          <RelatedPosts type="user" list={getSellListByUser()} user={sellListingDetail.data?.seller}></RelatedPosts>
          <ProfitCalculator></ProfitCalculator>
          {/*<ContactForm></ContactForm> */}
        </>
      ) : (
        <LoadingInfo></LoadingInfo>
      )}
    </div>
  );
};

export default SellDetail;
