import { combineReducers } from 'redux';
import authReducer from './auth/reducers';
import blogReducer from './blog/reducers';
import locationReducer from './location/reducers';
import notificationReducer from './notification/reducers';
import sellListingReducer from './sell-listing/reducers';
import userReducer from './user/reducer';

// COMBINED REDUCERS
const reducers = combineReducers({
  auth: authReducer,
  notification: notificationReducer,
  sellListing: sellListingReducer,
  location: locationReducer,
  blog: blogReducer,
  user: userReducer,
});

export type RootState = ReturnType<typeof reducers>;

export default reducers;
