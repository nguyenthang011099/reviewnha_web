import { District, Province, Ward } from '../../types/type.location';
import { Action } from '../../types/type.redux';
import {
  GET_DISTRICTS,
  GET_DISTRICTS_SUCCESS,
  GET_PROVINCES,
  GET_PROVINCES_SUCCESS,
  GET_WARDS,
  GET_WARDS_SUCCESS,
} from './constants';

interface State {
  provinces: Province[];
  districts: District[];
  wards: Ward[];
}

const initState: State = {
  provinces: [],
  districts: [],
  wards: [],
};

export default function locationReducer(state = initState, action: Action) {
  switch (action.type) {
    case GET_PROVINCES: {
      return state;
    }
    case GET_PROVINCES_SUCCESS: {
      return {
        ...state,
        provinces: action.data || [],
      };
    }
    case GET_DISTRICTS: {
      return state;
    }
    case GET_DISTRICTS_SUCCESS: {
      return {
        ...state,
        districts: action.data || [],
      };
    }
    case GET_WARDS: {
      return state;
    }
    case GET_WARDS_SUCCESS: {
      return {
        ...state,
        wards: action.data || [],
      };
    }
    default: {
      return state;
    }
  }
}
