import { Action } from '../../types/type.redux';
import { GET_DISTRICTS, GET_PROVINCES, GET_WARDS } from './constants';

/**
 * getProvinces
 */
export const getProvinces = (): Action => {
  return {
    type: GET_PROVINCES,
    payload: {
      query: 'provinces',
      method: 'GET',
    },
  };
};

/**
 * getDistricts
 * @param provinceId id cua province
 */
export const getDistricts = (provinceId: string): Action => {
  return {
    type: GET_DISTRICTS,
    payload: {
      query: 'districts',
      method: 'GET',
      body: {
        province_code: provinceId,
      },
    },
  };
};

/**
 * getWards
 * @param districtId district code
 */
export const getWards = (districtId: string): Action => {
  return {
    type: GET_WARDS,
    payload: {
      query: 'wards',
      method: 'GET',
      body: {
        district_code: districtId,
      },
    },
  };
};
