import {
  Blog,
  BlogCategory,
  BlogsAllData,
  BlogsByTag,
  BlogsCategoryData,
  VideoCount,
  VideoList,
} from '../../types/type.blog';
import { Action } from '../../types/type.redux';
import {
  GET_BLOGS_BY_CATEGORY,
  GET_BLOGS_BY_CATEGORY_SUCCESS,
  GET_BLOGS_BY_TAG,
  GET_BLOGS_BY_TAG_SUCCESS,
  GET_BLOG_BY_SLUG,
  GET_BLOG_BY_SLUG_SUCCESS,
  GET_BLOG_CATEGORIES,
  GET_BLOG_CATEGORIES_SUCCESS,
  GET_FEATURE_BLOGS,
  GET_FEATURE_BLOGS_SUCCESS,
  GET_HIGHLIGHT_BLOGS,
  GET_HIGHLIGHT_BLOGS_SUCCESS,
  GET_NORMAL_BLOGS,
  GET_NORMAL_BLOGS_SUCCESS,
  GET_VIDEOS,
  GET_VIDEOS_SUCCESS,
  RESET_LOADING_BLOG_DETAIL,
  SET_ACTIVE_CATEGORY,
  SET_CACHED_VIDEO_COUNT,
} from './constants';

interface State {
  categories: BlogCategory[];
  cacheCategoryBlogs: string[];
  blogsByCategory: BlogsCategoryData;
  blogsAll: BlogsAllData;
  blogDetail: Blog;
  loadingBlogDetail: boolean;
  activeCategory: string;
  videoList: VideoList;
  videoCountCached: VideoCount[];
  blogsByTag: BlogsByTag;
}

const initState: State = {
  categories: [],
  cacheCategoryBlogs: [],
  blogsByCategory: {},
  blogsAll: {
    featureBlogs: {
      blogs: [],
      total: 0,
      currentOffset: -1,
      loading: false,
    },
    videos: {
      blogs: [],
      total: 0,
      currentOffset: -1,
    },
    normalBlogs: {
      blogs: [],
      total: 0,
      currentOffset: -1,
    },
    highlightBlogs: {
      blogs: [],
      total: 0,
      currentOffset: -1,
      loading: false,
    },
  },
  blogDetail: {} as any,
  loadingBlogDetail: true,
  activeCategory: 'all',
  videoList: {
    videos: [],
    loading: false,
    currentOffset: -1,
    total: 0,
  },
  videoCountCached: [],
  blogsByTag: {
    total: 0,
    blogs: [],
    currentOffset: -1,
    loading: false,
    currentTag: {
      name: '',
      slug: '',
    },
  },
};

export default function blogReducer(state = initState, action: Action) {
  switch (action.type) {
    case GET_BLOG_CATEGORIES: {
      return state;
    }
    case GET_BLOG_CATEGORIES_SUCCESS: {
      return {
        ...state,
        categories: action.data,
      };
    }
    case GET_BLOGS_BY_CATEGORY: {
      const currentCateBlog = state.blogsByCategory[action.data];
      return {
        ...state,
        blogsByCategory: {
          ...state.blogsByCategory,
          [action.data]: {
            ...currentCateBlog,
            loading: state.cacheCategoryBlogs.includes(action.data) ? false : true,
          },
        },
      };
    }
    case GET_BLOGS_BY_CATEGORY_SUCCESS: {
      const currentCateBlog = state.blogsByCategory[action.old_action.data];
      let newBlogs = currentCateBlog?.blogs || [];
      const currentOffset = currentCateBlog?.currentOffset || -1;
      if (currentOffset < Number(action.data.offset)) {
        newBlogs = [...newBlogs, ...action.data.data];
      }
      return {
        ...state,
        cacheCategoryBlogs: [...state.cacheCategoryBlogs, action.old_action.data],
        blogsByCategory: {
          ...state.blogsByCategory,
          [action.old_action.data]: {
            blogs: newBlogs,
            total: action.data.total,
            currentOffset: Number(action.data.offset),
            loading: false,
          },
        },
      };
    }
    case GET_HIGHLIGHT_BLOGS: {
      let highlightBlogs = state.blogsAll.highlightBlogs;
      return {
        ...state,
        blogsAll: {
          ...state.blogsAll,
          highlightBlogs: {
            ...highlightBlogs,
            loading: highlightBlogs.blogs.length ? false : true,
          },
        },
      };
    }
    case GET_HIGHLIGHT_BLOGS_SUCCESS: {
      let newBlogs = state.blogsAll.highlightBlogs.blogs;
      if (state.blogsAll.highlightBlogs.currentOffset < Number(action.data.offset)) {
        newBlogs = [...newBlogs, ...action.data.data];
      }
      return {
        ...state,
        blogsAll: {
          ...state.blogsAll,
          highlightBlogs: {
            blogs: newBlogs,
            total: action.data.total,
            currentOffset: Number(action.data.offset),
            loading: false,
          },
        },
      };
    }
    case GET_FEATURE_BLOGS: {
      let featureBlogs = state.blogsAll.featureBlogs;
      return {
        ...state,
        blogsAll: {
          ...state.blogsAll,
          featureBlogs: {
            ...featureBlogs,
            loading: featureBlogs.blogs.length ? false : true,
          },
        },
      };
    }
    case GET_FEATURE_BLOGS_SUCCESS: {
      let newBlogs = state.blogsAll.featureBlogs.blogs;
      if (state.blogsAll.featureBlogs.currentOffset < Number(action.data.offset)) {
        newBlogs = [...newBlogs, ...action.data.data];
      }
      return {
        ...state,
        blogsAll: {
          ...state.blogsAll,
          featureBlogs: {
            blogs: newBlogs,
            total: action.data.total,
            currentOffset: Number(action.data.offset),
            loading: false,
          },
        },
      };
    }
    case GET_NORMAL_BLOGS: {
      return state;
    }
    case GET_NORMAL_BLOGS_SUCCESS: {
      let newBlogs = state.blogsAll.normalBlogs.blogs;
      if (state.blogsAll.normalBlogs.currentOffset < Number(action.data.offset)) {
        newBlogs = [...newBlogs, ...action.data.data];
      }
      return {
        ...state,
        blogsAll: {
          ...state.blogsAll,
          normalBlogs: {
            blogs: newBlogs,
            total: action.data.total,
            currentOffset: Number(action.data.offset),
          },
        },
      };
    }
    case GET_BLOG_BY_SLUG: {
      return {
        ...state,
        loadingBlogDetail: true,
      };
    }
    case GET_BLOG_BY_SLUG_SUCCESS: {
      return {
        ...state,
        loadingBlogDetail: false,
        blogDetail: action.data,
      };
    }
    case RESET_LOADING_BLOG_DETAIL: {
      return {
        ...state,
        loadingBlogDetail: true,
      };
    }
    case SET_ACTIVE_CATEGORY: {
      return {
        ...state,
        activeCategory: action.data,
      };
    }
    case GET_VIDEOS: {
      return {
        ...state,
        videoList: {
          ...state.videoList,
          loading: true,
        },
      };
    }
    case GET_VIDEOS_SUCCESS: {
      let newVideos = state.videoList.videos;
      if (state.videoList.currentOffset < Number(action.data.offset)) {
        newVideos = [...newVideos, ...action.data.data];
      }
      return {
        ...state,
        videoList: {
          videos: newVideos,
          loading: false,
          currentOffset: Number(action.data.offset),
          total: action.data.total,
        },
      };
    }
    case SET_CACHED_VIDEO_COUNT: {
      let currentCachedCount = state.videoCountCached;
      const isExisted = currentCachedCount.find((item: VideoCount) => item.id === action.data.id);
      if (!isExisted) {
        currentCachedCount = [...currentCachedCount, action.data];
      }
      return {
        ...state,
        videoCountCached: currentCachedCount,
      };
    }
    case GET_BLOGS_BY_TAG: {
      const blogsByTag = state.blogsByTag;
      if (blogsByTag.currentTag.slug !== action.data) {
        return {
          ...state,
          blogsByTag: {
            ...initState.blogsByTag,
            loading: true,
          },
        };
      }
      return {
        ...state,
        blogsByTag: {
          ...blogsByTag,
          loading: blogsByTag.blogs?.length ? false : true,
        },
      };
    }
    case GET_BLOGS_BY_TAG_SUCCESS: {
      let newBlogs = state.blogsByTag.blogs;
      if (state.blogsByTag.currentOffset < Number(action.data.offset)) {
        newBlogs = [...newBlogs, ...action.data.data];
      }
      return {
        ...state,
        blogsByTag: {
          ...state.blogsByTag,
          blogs: newBlogs,
          total: action.data.total,
          currentOffset: Number(action.data.offset),
          loading: false,
          currentTag: action.data.tag,
        },
      };
    }
    default: {
      return state;
    }
  }
}
