import {
  NUMBER_BLOGS_PER_REQUEST,
  NUMBER_VIDEOS_PER_FIRST_REQUEST,
  NUMBER_VIDEOS_PER_REQUEST,
} from '../../constants/blog';
import { VideoCount } from '../../types/type.blog';
import { Action } from '../../types/type.redux';
import {
  GET_FEATURE_BLOGS,
  GET_NORMAL_BLOGS,
  GET_BLOG_CATEGORIES,
  GET_BLOGS_BY_CATEGORY,
  GET_BLOG_BY_SLUG,
  RESET_LOADING_BLOG_DETAIL,
  SET_ACTIVE_CATEGORY,
  GET_VIDEOS,
  GET_VIDEOS_BY_CATEGORY,
  SET_CACHED_VIDEO_COUNT,
  GET_BLOGS_BY_TAG,
  GET_HIGHLIGHT_BLOGS,
} from './constants';

/**
 * getBlogCategories lấy danh mục của blog
 */
export const getBlogCategories = (): Action => {
  return {
    type: GET_BLOG_CATEGORIES,
    payload: {
      query: 'categories',
      method: 'GET',
    },
  };
};

/**
 * getBlogsByType lây blog có thịnh hành hay không
 * @param type thịnh hành hay nổi bật hay thông thường
 * @param offset thu tu
 */
export const getBlogsByType = (type: 'normal' | 'featured' | 'highlight', offset: number): Action => {
  let action: { type?: string; body?: any } = {};
  switch (type) {
    case 'featured': {
      action = {
        type: GET_FEATURE_BLOGS,
        body: {
          featured: true,
        },
      };
      break;
    }
    case 'highlight': {
      action = {
        type: GET_HIGHLIGHT_BLOGS,
        body: {
          highlight: true,
        },
      };
      break;
    }
    default: {
      action = {
        type: GET_NORMAL_BLOGS,
        body: {
          highlight: false,
        },
      };
    }
  }

  return {
    type: action.type,
    payload: {
      query: 'articles',
      method: 'GET',
      body: {
        limit: NUMBER_BLOGS_PER_REQUEST,
        offset: offset,
        ...action.body,
      },
    },
  };
};

/**
 * getBlogsByCategory lấy blogs theo danh mục
 * @param categorySlug slug của danh mục
 * @param offset thu tu
 */
export const getBlogsByCategory = (categorySlug: string, offset: number): Action => {
  return {
    type: GET_BLOGS_BY_CATEGORY,
    payload: {
      query: `categories/${categorySlug}/articles`,
      method: 'GET',
      body: {
        limit: NUMBER_BLOGS_PER_REQUEST,
        offset: offset,
      },
    },
    data: categorySlug,
  };
};

/**
 * getBlogBySlug lấy chi tiết bài viết theo slug
 * @param slug  slug của bài viết
 */
export const getBlogBySlug = (slug: string): Action => {
  return {
    type: GET_BLOG_BY_SLUG,
    payload: {
      query: `articles/${slug}`,
      method: 'GET',
    },
  };
};

/**
 * resetLoadingBlogDetail
 */
export const resetLoadingBlogDetail = (): Action => {
  return {
    type: RESET_LOADING_BLOG_DETAIL,
  };
};

export const setActiveCategory = (category: string): Action => {
  return {
    type: SET_ACTIVE_CATEGORY,
    data: category,
  };
};

/**
 * getVideos lấy các video
 * @param offset  thu tu
 */
export const getVideos = (offset: number): Action => {
  return {
    type: GET_VIDEOS,
    payload: {
      query: 'videos',
      method: 'GET',
      body: {
        limit: offset === 0 ? NUMBER_VIDEOS_PER_FIRST_REQUEST : NUMBER_VIDEOS_PER_REQUEST,
        offset: offset,
      },
    },
  };
};

/**
 * getVideosByCategory lấy video theo danh mục
 * @param slug slug của danh mục
 */
export const getVideosByCategory = (slug: string): Action => {
  return {
    type: GET_VIDEOS_BY_CATEGORY,
    payload: {
      query: `categories/${slug}/videos`,
      method: 'GET',
      body: {
        limit: NUMBER_VIDEOS_PER_REQUEST,
      },
    },
  };
};

/**
 * setCachedVideoCount cache lại video đã lấy count
 */
export const setCachedVideoCount = (videoCount: VideoCount): Action => {
  return {
    type: SET_CACHED_VIDEO_COUNT,
    data: videoCount,
  };
};

/**
 * getBlogsByTag lấy các blogs theo tag
 * @param tag slug cua tag
 * @param offset thu tu
 */
export const getBlogsByTag = (tagSlug: string, offset: number): Action => {
  return {
    type: GET_BLOGS_BY_TAG,
    payload: {
      query: `tags/${tagSlug}/articles`,
      method: 'GET',
      body: {
        limit: NUMBER_BLOGS_PER_REQUEST,
        offset: offset,
      },
    },
    data: tagSlug,
  };
};
