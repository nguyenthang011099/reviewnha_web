import { Action } from '../../types/type.redux';
import { GET_SELLER_INFORMATION } from './constants';

/**
 * getSellerInfo lấy thông tin của seller
 * @param id id của seller
 */
export const getSellerInfo = (id: number): Action => {
  return {
    type: GET_SELLER_INFORMATION,
    payload: {
      query: `/user/${id}`,
      method: 'GET',
    },
  };
};
