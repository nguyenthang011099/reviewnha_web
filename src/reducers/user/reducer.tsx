import { UserInfo } from '../../types/type.auth';
import { Action } from '../../types/type.redux';
import { GET_SELLER_INFORMATION_SUCCESS } from './constants';

interface State {
  info: UserInfo;
}

const initState: State = {
  info: {},
};

export default function userReducer(state = initState, action: Action) {
  switch (action.type) {
    case GET_SELLER_INFORMATION_SUCCESS: {
      return {
        ...state,
        info: action.data,
      };
    }
    default: {
      return state;
    }
  }
}
