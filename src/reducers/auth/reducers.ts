import { Action } from '../../types/type.redux';
import { Status } from '../../types/type.status';
import {
  GET_REDIRECT_LINK_OAUTH,
  GET_REDIRECT_LINK_OAUTH_SUCCESS,
  GET_USER_INFORMATION,
  GET_USER_INFORMATION_SUCCESS,
  LOGIN,
  LOGIN_FAIL,
  LOGIN_SUCCESS,
  LOGOUT,
  LOGOUT_SUCCESS,
  REGISTER,
  REGISTER_FAIL,
  REGISTER_SUCCESS,
  RESET_STATUS_LOGIN,
  RESET_STATUS_LOGOUT,
  RESET_STATUS_REGISTER,
  SET_CURRENT_URL,
  UPDATE_USER,
  UPDATE_USER_FAIL,
  UPDATE_USER_SUCCESS,
} from './constants';
import cookie from 'js-cookie';
import { CallbackURLType, UserInfo } from '../../types/type.auth';

interface State {
  registerStatus: Status;
  registerMessageError: string;
  loginStatus: Status;
  loginMessageError: string;
  authenticated: boolean;
  googleLink: string;
  facebookLink: string;
  callbackUrl: CallbackURLType;
  typeLogin: string;
  user: UserInfo;
  token: string;
  logoutStatus: Status;
  isUpdating: boolean;
}

const initState: State = {
  registerStatus: 'none',
  registerMessageError: '',
  loginStatus: 'none',
  loginMessageError: '',
  authenticated: false,
  googleLink: '',
  facebookLink: '',
  callbackUrl: {
    url: '',
    query: {},
    asPath: '',
    pathname: '',
  },
  typeLogin: '',
  user: {},
  token: cookie.get('auth') || '',
  logoutStatus: 'none',
  isUpdating: false,
};

export default function authReducer(state = initState, action: Action) {
  switch (action.type) {
    case REGISTER: {
      return {
        ...state,
        registerStatus: 'none',
      };
    }
    case REGISTER_SUCCESS: {
      return {
        ...state,
        registerStatus: 'success',
      };
    }
    case REGISTER_FAIL: {
      return {
        ...state,
        registerStatus: 'error',
        registerMessageError: action.data.message,
      };
    }
    case LOGIN: {
      return {
        ...state,
        loginStatus: 'none',
      };
    }
    case LOGIN_SUCCESS: {
      cookie.set('auth', action.data.token);
      return {
        ...state,
        authenticated: true,
        loginStatus: 'success',
        typeLogin: action.data?.type,
        token: action.data.token,
      };
    }
    case LOGIN_FAIL: {
      return {
        ...state,
        loginStatus: 'error',
        loginMessageError: action.data.message,
        typeLogin: action.data?.type,
      };
    }
    case RESET_STATUS_LOGIN: {
      return {
        ...state,
        loginStatus: 'none',
        typeLogin: '',
      };
    }
    case RESET_STATUS_REGISTER: {
      return {
        ...state,
        registerStatus: 'none',
      };
    }
    case GET_REDIRECT_LINK_OAUTH: {
      return {
        ...state,
      };
    }
    case GET_REDIRECT_LINK_OAUTH_SUCCESS: {
      if (action.old_action?.data === 'google') {
        return {
          ...state,
          googleLink: action.data.redirectUrl,
        };
      }
      return {
        ...state,
        facebookLink: action.data.redirectUrl,
      };
    }
    case SET_CURRENT_URL: {
      return {
        ...state,
        callbackUrl: action.data,
      };
    }
    case GET_USER_INFORMATION: {
      return state;
    }
    case GET_USER_INFORMATION_SUCCESS: {
      return {
        ...state,
        authenticated: true,
        user: action.data,
      };
    }
    case LOGOUT: {
      return {
        ...state,
      };
    }
    case LOGOUT_SUCCESS: {
      cookie.remove('auth');
      return {
        ...state,
        authenticated: false,
        user: {},
        logoutStatus: 'success',
      };
    }
    case RESET_STATUS_LOGOUT: {
      return {
        ...state,
        logoutStatus: 'none',
      };
    }
    case UPDATE_USER: {
      return {
        ...state,
        isUpdating: true,
      };
    }
    case UPDATE_USER_SUCCESS: {
      return {
        ...state,
        isUpdating: false,
        user: action.data,
      };
    }
    case UPDATE_USER_FAIL: {
      return {
        ...state,
        isUpdating: false,
      };
    }
    default: {
      return state;
    }
  }
}
