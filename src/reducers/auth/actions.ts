import { CallbackURLType, LoginParams, RegisterParams, UserInfo } from '../../types/type.auth';
import { Action } from '../../types/type.redux';
import {
  GET_REDIRECT_LINK_OAUTH,
  GET_USER_INFORMATION,
  LOGIN,
  LOGIN_FAIL,
  LOGIN_SUCCESS,
  LOGOUT,
  REGISTER,
  RESET_STATUS_LOGIN,
  RESET_STATUS_LOGOUT,
  RESET_STATUS_REGISTER,
  SET_CURRENT_URL,
  UPDATE_USER,
} from './constants';

/**
 * register action đăng ký
 * @param data dữ liệu đăng ký
 */
export const register = (data: RegisterParams): Action => {
  return {
    type: REGISTER,
    payload: {
      method: 'POST',
      query: 'register',
      body: data,
    },
  };
};

/**
 * login action đăng nhập
 * @param data dữ liệu đăng nhập
 */
export const login = (data: LoginParams): Action => {
  return {
    type: LOGIN,
    payload: {
      method: 'POST',
      query: 'login',
      body: data,
    },
  };
};

/**
 * resetStatusAuth reset trạng thái của thông báo
 * @param type login or register
 */
export const resetStatusAuth = (type: 'login' | 'register' | 'logout'): Action => {
  switch (type) {
    case 'login': {
      return {
        type: RESET_STATUS_LOGIN,
      };
    }
    case 'register': {
      return {
        type: RESET_STATUS_REGISTER,
      };
    }
    default: {
      return {
        type: RESET_STATUS_LOGOUT,
      };
    }
  }
};

/**
 * getRedirectLinkOauth: Lấy link oauth
 * @param type google hoặc fb
 * @param callbackUrl link callback khi đăng nhập thành công
 * @param userType normal hoặc seller
 */
export const getRedirectLinkOauth = (type: 'google' | 'facebook', callbackUrl: string, userType: string): Action => {
  return {
    type: GET_REDIRECT_LINK_OAUTH,
    payload: {
      query: `oauth/${type}/redirect`,
      method: 'GET',
      body: {
        user_type: userType,
        redirect_url: callbackUrl,
      },
    },
    data: type,
  };
};

/**
 * setCurrentUrl
 * @param url url hiện tại để callback khi đăng nhập bằng google, fackbook
 */
export const setCurrentUrl = (url: CallbackURLType): Action => {
  return {
    type: SET_CURRENT_URL,
    data: url,
  };
};

/**
 * setLoginStatusWhenCallbackUrlOauth
 * @param status success hoặc fail
 * @param token token đăng nhập thành công
 */
export const setLoginStatusWhenCallbackUrlOauth = (status: 'success' | 'fail', token?: string): Action => {
  if (status === 'fail') {
    return {
      type: LOGIN_FAIL,
      data: {
        message: 'Đăng nhập không thành công, vui lòng đăng nhập lại',
        type: 'OAuth',
      },
    };
  }
  return {
    type: LOGIN_SUCCESS,
    data: {
      token,
      type: 'OAuth',
    },
  };
};

/**
 * getUserInformation
 */
export const getUserInformation = (): Action => {
  return {
    type: GET_USER_INFORMATION,
    payload: {
      method: 'GET',
      query: 'user/get-info',
    },
  };
};

/**
 * logout
 */
export const logout = (): Action => {
  return {
    type: LOGOUT,
    payload: {
      method: 'POST',
      query: 'logout',
    },
  };
};

export const updateUserInfo = (data: UserInfo): Action => {
  const body = new FormData();
  data.name && body.append('name', data.name);
  data.phone && body.append('phone', data.phone);
  data.email && body.append('email', data.email);
  data.avatar && body.append('avatar', data.avatar as string);
  data.background && body.append('background', data.background as string);
  data.sex && body.append('sex', data.sex);
  data.type && body.append('type', data.type);
  data.dateOfBirth && body.append('date_of_birth', data.dateOfBirth);
  data.address && body.append('address', data.address);
  data.company && body.append('company', data.company);
  data.description && body.append('description', data.description);
  return {
    type: UPDATE_USER,
    payload: {
      method: 'POST',
      query: '/user/update',
      body,
    },
  };
};
