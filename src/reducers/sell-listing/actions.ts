import { Action } from '../../types/type.redux';
import { PostBodySellListing, SellListingFilter } from '../../types/type.sell-listing';
import {
  GET_SELL_LISINGS_LIST,
  GET_SELL_LISTING_PART_ONE,
  GET_SELL_LISTING_PART_TWO,
  POST_SELL_LISTING,
  RESET_STATUS_POST_SELL_LISTING,
  GET_SELL_LISING_DETAIL,
  CHANGE_FILTER_DATA,
  CHANGE_IS_FILTERED,
  GET_SELL_LISING_LIST_BY_USER_ID,
  GET_SELL_LISTINGS_BY_AUTH,
  DELETE_SELL_LISTINGS,
  GET_PROJECT_LIST,
  GET_FEEDBACK_LIST,
  GET_RELATED_SELL_LISTING,
  GET_EXPERIENCES_BY_PROJECT,
  UPDATE_SELL_LISTING,
  CLEAR_SELL_LISTING_DETAIL,
  GET_EXPERIENCES_BY_PROJECT_IN_CREATE_SELL_LISTING,
} from './constants';

/**
 * getSellListingPartOne: lấy ra loại sản phẩm và loại giao dịch
 */
export const getSellListingPartOne = (): Action => {
  return {
    type: GET_SELL_LISTING_PART_ONE,
    payload: {
      query: 'sell-listings/create/part-one',
      method: 'GET',
    },
  };
};

/**
 * getSellListingPartOne: lấy ra các field còn lại theo product type
 */
export const getSellListingPartTwo = (productType: string): Action => {
  return {
    type: GET_SELL_LISTING_PART_TWO,
    payload: {
      query: 'sell-listings/create/part-two',
      method: 'GET',
      body: {
        type_product: productType,
      },
    },
  };
};

/**
 * postSellListing
 * @param data data post sell listing
 */
export const postSellListing = (data: PostBodySellListing): Action => {
  return {
    type: POST_SELL_LISTING,
    payload: {
      query: 'sell-listing/store',
      method: 'POST',
      body: data,
    },
  };
};

export const resetStatusPostSellListing = (): Action => {
  return {
    type: RESET_STATUS_POST_SELL_LISTING,
  };
};

// get sell listings

/**
 * getSellListingsList: lấy danh sách tin rao
 * @param type loai danh sach can get
 * @param isGetByUser get by user hay khong (dùng cho trang sell detail)
 * @param isGetBySeller get by seller (dùng cho trang  sell list of seller )
 */
export const getSellListingsList = (
  data: SellListingFilter,
  isGetByUser?: boolean,
  isGetBySeller?: boolean,
): Action => {
  if (isGetByUser) {
    return {
      type: GET_SELL_LISING_LIST_BY_USER_ID,
      payload: {
        query: 'sell-listings',
        method: 'GET',
        body: {
          limit: data.limit,
          user_id: data.userId,
        },
      },
    };
  }
  return {
    type: GET_SELL_LISINGS_LIST,
    payload: {
      query: '/sell-listings',
      method: 'GET',
      body: {
        highlight: data.type === 'highlight',
        limit: data.limit,
        page: data.page ? data.page : 1,
        user_id: data.userId,
        project_id: data.projectId,
        type_transaction: data.typeTransaction,
        query: data.query,
        province_code: data.provinceCode ? data.provinceCode : undefined,
        district_code: data.districtCode ? data.districtCode : undefined,
        ward_code: data.wardCode ? data.wardCode : undefined,
        type_product: data.typeProduct ? data.typeProduct : undefined,
        price:
          data.price?.min || data.price?.max
            ? {
                min: Number(data.price?.min) * Math.pow(10, 9),
                max: Number(data.price?.max) * Math.pow(10, 9),
              }
            : undefined,
        area:
          data.area?.min || data.area?.max
            ? {
                min: data.area?.min,
                max: data.area?.max,
              }
            : undefined,
        direction: data.direction ? data.direction : undefined,
        bedroom: Number(data.bedroom) ? Number(data?.bedroom) : undefined,
        bathroom: Number(data.bathroom) ? Number(data?.bathroom) : undefined,
      },
    },
    data: {
      type: data.type,
      isGetBySeller: isGetBySeller,
    },
  };
};

/**
 * getSellListingDetail
 * @param id id cua sell listing
 * @returns
 */
export const getSellListingDetail = (id: number): Action => {
  return {
    type: GET_SELL_LISING_DETAIL,
    payload: {
      query: `sell-listings/${id}`,
      method: 'GET',
    },
  };
};

/**
 * changeFilterData: hàm để update lại những data đã filter trong store
 * @param data data cần filter
 */
export const changeFilterData = (data: SellListingFilter): Action => {
  return {
    type: CHANGE_FILTER_DATA,
    data,
  };
};

/**
 * changeIsFiltered: Hàm thay đổi xem user đã chọn filter chưa
 * @param isFiltered đã filter chưa
 */
export const changeIsFiltered = (isFiltered: boolean): Action => {
  return {
    type: CHANGE_IS_FILTERED,
    data: isFiltered,
  };
};

/**
 * getSellListingsByCurrentUser: lấy danh sách tin rao theo user đăng nhập
 */
export const getSellListingsByCurrentUser = (): Action => {
  return {
    type: GET_SELL_LISTINGS_BY_AUTH,
    payload: {
      query: '/user/sell-listings',
      method: 'GET',
    },
  };
};

/**
 * deleteSellListing: Xóa tin rao
 * @param id id của tin rao cần xóa
 */
export const deleteSellListing = (id: number): Action => {
  return {
    type: DELETE_SELL_LISTINGS,
    payload: {
      query: `/user/sell-listings/${id}`,
      method: 'DELETE',
    },
    data: {
      id,
    },
  };
};

/**
 * getProjectList: lấy danh sách project
 */
export const getProjectList = (): Action => {
  return {
    type: GET_PROJECT_LIST,
    payload: {
      query: `/project`,
      method: 'GET',
    },
  };
};

export const getFeedback = (projectId: number | string): Action => {
  return {
    type: GET_FEEDBACK_LIST,
    payload: {
      query: `/project/${projectId}/rate`,
      method: 'GET',
    },
  };
};

/**
 * getRelatedSellListings lấy các tin rao liên quan tới 1 tin rao
 * @param projectId
 * @param districtCode
 */
export const getRelatedSellListings = (projectId?: number | string, districtCode?: number | string): Action => {
  return {
    type: GET_RELATED_SELL_LISTING,
    payload: {
      query: `/sell-listings`,
      method: 'GET',
      body: projectId ? { project_id: projectId } : { district_code: districtCode },
    },
  };
};

export const getExperiencesByProject = (projectId: string, inCreateSell?: boolean): Action => {
  return {
    type: inCreateSell ? GET_EXPERIENCES_BY_PROJECT_IN_CREATE_SELL_LISTING : GET_EXPERIENCES_BY_PROJECT,
    payload: {
      query: `/project/${projectId}/experiences`,
      method: 'GET',
    },
  };
};

/**
 * updateSellListing
 * @param data data post sell listing
 */
export const updateSellListing = (id: number, data: PostBodySellListing): Action => {
  return {
    type: UPDATE_SELL_LISTING,
    payload: {
      query: `/user/sell-listings/${id}`,
      method: 'PUT',
      body: data,
    },
  };
};

export const clearSellListingDetail = (): Action => {
  return {
    type: CLEAR_SELL_LISTING_DETAIL,
  };
};
