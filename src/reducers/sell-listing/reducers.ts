import { OptionInputRadio } from '../../types/type.base';
import { Action } from '../../types/type.redux';
import { Experiences, Feedback, Project, SellListing, SellListingFilter } from '../../types/type.sell-listing';
import { Status } from '../../types/type.status';
import {
  CHANGE_FILTER_DATA,
  CHANGE_IS_FILTERED,
  CLEAR_SELL_LISTING_DETAIL,
  DELETE_SELL_LISTINGS,
  DELETE_SELL_LISTINGS_SUCCESS,
  GET_EXPERIENCES_BY_PROJECT_IN_CREATE_SELL_LISTING,
  GET_EXPERIENCES_BY_PROJECT_IN_CREATE_SELL_LISTING_SUCCESS,
  GET_EXPERIENCES_BY_PROJECT_SUCCESS,
  GET_FEEDBACK_LIST_SUCCESS,
  GET_PROJECT_LIST,
  GET_PROJECT_LIST_SUCCESS,
  GET_RELATED_SELL_LISTING,
  GET_RELATED_SELL_LISTING_SUCCESS,
  GET_SELL_LISINGS_LIST,
  GET_SELL_LISINGS_LIST_SUCCESS,
  GET_SELL_LISING_DETAIL,
  GET_SELL_LISING_DETAIL_SUCCESS,
  GET_SELL_LISING_LIST_BY_USER_ID,
  GET_SELL_LISING_LIST_BY_USER_ID_SUCCESS,
  GET_SELL_LISTINGS_BY_AUTH,
  GET_SELL_LISTINGS_BY_AUTH_SUCCESS,
  GET_SELL_LISTING_PART_ONE,
  GET_SELL_LISTING_PART_ONE_FAIL,
  GET_SELL_LISTING_PART_ONE_SUCCESS,
  GET_SELL_LISTING_PART_TWO,
  GET_SELL_LISTING_PART_TWO_FAIL,
  GET_SELL_LISTING_PART_TWO_SUCCESS,
  POST_SELL_LISTING,
  POST_SELL_LISTING_FAIL,
  POST_SELL_LISTING_SUCCESS,
  RESET_STATUS_POST_SELL_LISTING,
  UPDATE_SELL_LISTING,
  UPDATE_SELL_LISTING_FAIL,
  UPDATE_SELL_LISTING_SUCCESS,
  GET_SELL_LISINGS_LIST_FAIL,
} from './constants';

interface State {
  stepOneData: {
    productTypes: OptionInputRadio[];
    transactionTypes: OptionInputRadio[];
  };
  stepTwoData: {
    types: OptionInputRadio[];
    directions: OptionInputRadio[];
    furnitures: OptionInputRadio[];
    juridicals: OptionInputRadio[];
  };
  stepThreeData: {
    experiences: OptionInputRadio[];
  };
  stepFourData: {
    services: OptionInputRadio[];
  };
  statusPost: Status;
  messagePostFail: string;
  // sell listings
  allSellListings: {
    lists: SellListing[];
    loadingLists: boolean;
    total: number;
    currentPage: number;
  };
  highlightSellListings: {
    lists: SellListing[];
    loadingLists: boolean;
  };
  sellListingDetail: {
    data: SellListing;
    loading: boolean;
    listByUser: SellListing[];
    feedbackList: Feedback[];
    relatedList: SellListing[];
    experiences: Experiences[];
  };
  filterData: SellListingFilter;
  isFiltered: boolean;
  seller: {
    allSellListings: {
      lists: SellListing[];
      loadingLists: boolean;
      total: number;
      currentPage: number;
    };
    highlightSellListings: {
      lists: SellListing[];
      loadingLists: boolean;
    };
  };
  auth: {
    allSellListings: {
      lists: SellListing[];
      loadingLists: boolean;
      total: number;
      currentPage: number;
    };
  };
  projectList: Project[];
}

const initState: State = {
  stepOneData: {
    productTypes: [],
    transactionTypes: [],
  },
  stepTwoData: {
    types: [],
    directions: [],
    furnitures: [],
    juridicals: [],
  },
  stepThreeData: {
    experiences: [],
  },
  stepFourData: {
    services: [],
  },
  statusPost: 'none',
  messagePostFail: 'Tạo tin rao thất bại. Vui lòng thử lại',
  allSellListings: {
    lists: [],
    loadingLists: false,
    total: 0,
    currentPage: 0,
  },
  highlightSellListings: {
    lists: [],
    loadingLists: false,
  },
  sellListingDetail: {
    data: {},
    loading: false,
    listByUser: [],
    feedbackList: [],
    relatedList: [],
    experiences: [],
  },
  filterData: {
    area: {
      min: 0,
      max: 500,
    },
  },
  isFiltered: false,
  seller: {
    allSellListings: {
      lists: [],
      loadingLists: false,
      total: 0,
      currentPage: 0,
    },
    highlightSellListings: {
      lists: [],
      loadingLists: false,
    },
  },
  auth: {
    allSellListings: {
      lists: [],
      loadingLists: false,
      total: 0,
      currentPage: 0,
    },
  },
  projectList: [],
};

/**
 * mapDataToOptions: format lại data đầu vào để đúng với props của select base
 * @param data data cần format
 */
const mapDataToOptions = (data: { key: string; value: string }[], isCacheData?: boolean) => {
  const newData: OptionInputRadio[] = data.map((item: { key: string; value: string }) => {
    if (isCacheData) {
      return {
        title: item.value,
        value: item.key,
        data: item,
      };
    }
    return {
      title: item.value,
      value: item.key,
    };
  });
  return newData;
};

export default function sellListingReducer(state: State = initState, action: Action) {
  switch (action.type) {
    case GET_SELL_LISTING_PART_ONE: {
      return {
        ...state,
      };
    }
    case GET_SELL_LISTING_PART_ONE_SUCCESS: {
      return {
        ...state,
        stepOneData: {
          ...state.stepOneData,
          transactionTypes: mapDataToOptions(action.data.typeTransactions),
          productTypes: mapDataToOptions(action.data.typeProducts),
        },
      };
    }
    case GET_SELL_LISTING_PART_ONE_FAIL: {
      return state;
    }
    case GET_SELL_LISTING_PART_TWO: {
      return {
        ...state,
      };
    }
    case GET_SELL_LISTING_PART_TWO_SUCCESS: {
      return {
        ...state,
        stepTwoData: {
          types: mapDataToOptions(action.data.type),
          directions: mapDataToOptions(action.data.direction),
          furnitures: mapDataToOptions(action.data.furniture),
          juridicals: mapDataToOptions(action.data.juridical),
        },
        stepFourData: {
          services: mapDataToOptions(action.data.services),
        },
      };
    }
    case GET_SELL_LISTING_PART_TWO_FAIL: {
      return state;
    }
    case GET_EXPERIENCES_BY_PROJECT_IN_CREATE_SELL_LISTING: {
      return {
        ...state,
      };
    }
    case GET_EXPERIENCES_BY_PROJECT_IN_CREATE_SELL_LISTING_SUCCESS: {
      return {
        ...state,
        stepThreeData: {
          ...state.stepThreeData,
          experiences: (action.data || []).map((item: Experiences) => {
            return {
              title: item.name,
              value: item.id,
              data: item,
            };
          }),
        },
      };
    }
    case POST_SELL_LISTING: {
      return state;
    }
    case POST_SELL_LISTING_SUCCESS: {
      return {
        ...state,
        statusPost: 'success',
      };
    }
    case POST_SELL_LISTING_FAIL: {
      return {
        ...state,
        statusPost: 'error',
        messagePostFail: action.data.message,
      };
    }
    case RESET_STATUS_POST_SELL_LISTING: {
      return {
        ...state,
        statusPost: 'none',
      };
    }
    case GET_SELL_LISINGS_LIST: {
      if (action.data.isGetBySeller) {
        if (action.data.type === 'all') {
          return {
            ...state,
            seller: {
              ...state.seller,
              allSellListings: {
                ...state.seller.allSellListings,
                loadingLists: true,
              },
            },
          };
        }
        return {
          ...state,
          seller: {
            ...state.seller,
            highlightSellListings: {
              ...state.seller.highlightSellListings,
              loadingLists: true,
            },
          },
        };
      }
      if (action.data.type === 'all') {
        return {
          ...state,
          allSellListings: {
            ...state.allSellListings,
            loadingLists: true,
          },
        };
      }
      return {
        ...state,
        highlightSellListings: {
          ...state.highlightSellListings,
          loadingLists: true,
        },
      };
    }
    case GET_SELL_LISINGS_LIST_SUCCESS: {
      if (action?.old_action?.data?.isGetBySeller) {
        if (action?.old_action?.data?.type === 'all') {
          return {
            ...state,
            seller: {
              ...state.seller,
              allSellListings: {
                ...state.seller.allSellListings,
                lists: action.data?.data,
                loadingLists: false,
                total: action.data?.total,
                currentPage: action.data?.currentPage,
              },
            },
          };
        }
        return {
          ...state,
          seller: {
            ...state.seller,
            highlightSellListings: {
              ...state.seller.highlightSellListings,
              lists: action.data?.data,
              loadingLists: false,
            },
          },
        };
      }
      if (action?.old_action?.data?.type === 'all') {
        return {
          ...state,
          allSellListings: {
            ...state.allSellListings,
            lists: action.data?.data,
            loadingLists: false,
            total: action.data?.total,
            currentPage: action.data?.currentPage,
          },
        };
      }
      return {
        ...state,
        highlightSellListings: {
          ...state.highlightSellListings,
          lists: action.data?.data,
          loadingLists: false,
        },
      };
    }
    case GET_SELL_LISINGS_LIST_FAIL: {
      if (action?.old_action?.data?.isGetBySeller) {
        if (action?.old_action?.data?.type === 'all') {
          return {
            ...state,
            seller: {
              ...state.seller,
              allSellListings: {
                lists: [],
                loadingLists: false,
              },
            },
          };
        }
        return {
          ...state,
          seller: {
            ...state.seller,
            highlightSellListings: {
              ...state.seller.highlightSellListings,
              lists: [],
              loadingLists: false,
            },
          },
        };
      }
      if (action?.old_action?.data?.type === 'all') {
        return {
          ...state,
          allSellListings: {
            ...state.allSellListings,
            lists: [],
            loadingLists: false,
          },
        };
      }
      return {
        ...state,
        highlightSellListings: {
          ...state.highlightSellListings,
          lists: [],
          loadingLists: false,
        },
      };
    }
    case GET_SELL_LISING_DETAIL: {
      return {
        ...state,
        sellListingDetail: {
          ...state.sellListingDetail,
          loading: true,
        },
      };
    }
    case GET_SELL_LISING_DETAIL_SUCCESS: {
      return {
        ...state,
        sellListingDetail: {
          ...state.sellListingDetail,
          data: action.data,
          loading: false,
        },
      };
    }
    case CHANGE_FILTER_DATA: {
      return {
        ...state,
        filterData: {
          ...state.filterData,
          ...action.data,
        },
      };
    }
    case CHANGE_IS_FILTERED: {
      return {
        ...state,
        isFiltered: action.data,
      };
    }
    case GET_SELL_LISING_LIST_BY_USER_ID: {
      return {
        ...state,
      };
    }
    case GET_SELL_LISING_LIST_BY_USER_ID_SUCCESS: {
      return {
        ...state,
        sellListingDetail: {
          ...state.sellListingDetail,
          listByUser: action.data?.data ?? [],
        },
      };
    }
    case GET_SELL_LISTINGS_BY_AUTH: {
      return {
        ...state,
        auth: {
          ...state.auth,
          allSellListings: {
            ...state.auth.allSellListings,
            loadingLists: true,
          },
        },
      };
    }
    case GET_SELL_LISTINGS_BY_AUTH_SUCCESS: {
      return {
        ...state,
        auth: {
          ...state.auth,
          allSellListings: {
            ...state.auth.allSellListings,
            lists: action.data?.data,
            loadingLists: false,
            total: action.data?.total,
            currentPage: action.data?.currentPage,
          },
        },
      };
    }
    case DELETE_SELL_LISTINGS: {
      return {
        ...state,
      };
    }
    case DELETE_SELL_LISTINGS_SUCCESS: {
      const newSellListings = state.auth.allSellListings.lists.filter((sell: SellListing) => {
        return sell.id !== action?.old_action?.data?.id;
      });
      return {
        ...state,
        auth: {
          ...state.auth,
          allSellListings: {
            ...state.allSellListings,
            lists: [...newSellListings],
            total: state.auth.allSellListings.total - 1,
          },
        },
      };
    }
    case GET_PROJECT_LIST: {
      return {
        ...state,
      };
    }
    case GET_PROJECT_LIST_SUCCESS: {
      return {
        ...state,
        projectList: action.data,
      };
    }
    case GET_FEEDBACK_LIST_SUCCESS: {
      return {
        ...state,
        sellListingDetail: {
          ...state.sellListingDetail,
          feedbackList: action.data,
        },
      };
    }
    case GET_RELATED_SELL_LISTING: {
      return {
        ...state,
      };
    }
    case GET_RELATED_SELL_LISTING_SUCCESS: {
      return {
        ...state,
        sellListingDetail: {
          ...state.sellListingDetail,
          relatedList: action?.data?.data ?? [],
        },
      };
    }
    case GET_EXPERIENCES_BY_PROJECT_SUCCESS: {
      return {
        ...state,
        sellListingDetail: {
          ...state.sellListingDetail,
          experiences: action?.data ?? [],
        },
      };
    }
    case UPDATE_SELL_LISTING: {
      return {
        ...state,
      };
    }
    case UPDATE_SELL_LISTING_SUCCESS: {
      const newAuthSells = state.auth.allSellListings.lists.map((sell: SellListing) => {
        if (Number(action.data.id) === Number(sell.id)) {
          return action.data;
        }
        return sell;
      });
      return {
        ...state,
        statusPost: 'success',
        auth: {
          ...state.auth,
          allSellListings: {
            ...state.auth.allSellListings,
            lists: [...newAuthSells],
          },
        },
      };
    }
    case UPDATE_SELL_LISTING_FAIL: {
      return {
        ...state,
        statusPost: 'error',
        messagePostFail: action.data.message,
      };
    }
    case CLEAR_SELL_LISTING_DETAIL: {
      return {
        ...state,
        sellListingDetail: {
          ...state.sellListingDetail,
          data: {},
        },
      };
    }
    default: {
      return state;
    }
  }
}
