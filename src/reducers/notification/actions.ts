import { NotificationType } from '../../types/type.notification';
import { Action } from '../../types/type.redux';
import { CLOSE_NOTIFICATION, OPEN_NOTIFICATION } from './constants';

/**
 * openNotification mở thông báo
 * @param data data bật thông báo
 */
export const openNotification = (data: NotificationType): Action => {
  return {
    type: OPEN_NOTIFICATION,
    data: data,
  };
};

/**
 * closeNotification tắt thông báo
 */
export const closeNotification = (): Action => {
  return {
    type: CLOSE_NOTIFICATION,
  };
};
