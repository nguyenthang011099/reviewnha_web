import { NotificationType } from '../../types/type.notification';
import { Action } from '../../types/type.redux';
import { CLOSE_NOTIFICATION, OPEN_NOTIFICATION } from './constants';

const initialState: NotificationType = {
  isOpen: false,
  title: '',
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  reset: null,
  type: 'success',
};

const notificationReducer = (state = initialState, action: Action) => {
  switch (action.type) {
    case OPEN_NOTIFICATION: {
      return {
        ...state,
        ...action.data,
      };
    }
    case CLOSE_NOTIFICATION: {
      return {
        ...initialState,
      };
    }
    default: {
      return state;
    }
  }
};

export default notificationReducer;
