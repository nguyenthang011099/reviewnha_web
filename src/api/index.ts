import axios from 'axios';
import camelCase from 'camelcase-keys';
import { handleErr } from './handleError';
import cookie from 'js-cookie';
import Config from '../configs';

const axiosClient = axios.create({
  baseURL: Config.API_URL,
  responseType: 'json',
  timeout: 15 * 1000,
  transformResponse: [(data) => camelCase(data, { deep: true })],
});

axiosClient.interceptors.request.use(async (config) => {
  const accessToken = cookie.get('auth');
  if (accessToken) {
    config.headers.authorization = `Bearer ${accessToken}`;
  }
  return config;
});

axiosClient.interceptors.response.use(
  (response) => {
    return response.data;
  },
  (error) => {
    // Handle error
    console.error(error);
    return handleErr(error);
  },
);

export default axiosClient;
