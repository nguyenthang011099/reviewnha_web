import cookie from 'js-cookie';
/* eslint-disable @typescript-eslint/no-explicit-any */
const redirectToLogin = () => {
  cookie.remove('auth');
  cookie.remove('userId');
  cookie.remove('name');
  cookie.remove('email');
  cookie.remove('avatar');
  window.location.href = '/login';
};

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export const handleErr = (err: any): any => {
  if (err) {
    console.log('err: ', err);
    if (+err?.response?.status === 403 || +err?.response?.status === 401) {
      redirectToLogin();
    }
    return err?.response?.data ?? {};
  }
};
