import { CreateScheduleBody } from '../types/type.schedule';
import { handleErr } from './handleError';
import api from './index';

const scheduleApi = {
  async getList() {
    return api.get('/schedule').catch(handleErr);
  },
  async create(data: CreateScheduleBody) {
    const body = {
      name: data.name,
      phone: data.phone,
      email: data.email,
      time: data.time,
      type: data.type,
      sell_listing_id: data.sellListingId,
      content: data.content,
    };
    return api.post('/schedule', body);
  },
};

export default scheduleApi;
