import { handleErr } from './handleError';
import api from './index';

export const uploadImage = async (data: any) => {
  return api.post('images/upload', data).catch(handleErr);
};
