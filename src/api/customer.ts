import { handleErr } from './handleError';
import api from './index';

const customerApi = {
  async getList() {
    return api.get('/customer').catch(handleErr);
  },
  async create(scheduleId: number) {
    return api.post(`/customer/create/${scheduleId}`).catch(handleErr);
  },
  async delete(id: number) {
    return api.post(`/customer/delete/${id}`).catch(handleErr);
  },
};

export default customerApi;
