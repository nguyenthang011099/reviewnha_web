import { handleErr } from './handleError';
import api from './index';

const notificationApi = {
  async getList(offset: number, limit: number) {
    return api.get('/user/notifications', { params: { offset, limit } }).catch(handleErr);
  },
};

export default notificationApi;
