interface ConfigType {
  API_URL: string;
}

const Config: ConfigType = {
  API_URL: process.env.NEXT_PUBLIC_API_DOMAIN,
};

export default Config;
