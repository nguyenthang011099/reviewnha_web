import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import banner from '../images/banner_profile.png';
import avatar from '../images/user_avatar_high.png';
import { RootState } from '../reducers';
import { UserInfo } from '../types/type.auth';
import classnames from 'classnames';
import Link from 'next/link';

interface Props {
  isHideHeader?: boolean;
  isCurrentUser?: boolean;
}

const LayoutProfile: React.FC<Props> = (props) => {
  const [user, setUser] = useState<UserInfo>({});
  const seller: UserInfo = useSelector((state: RootState) => state.user.info);
  const auth: UserInfo = useSelector((state: RootState) => state.auth.user);
  const router = useRouter();

  useEffect(() => {
    if (props.isCurrentUser) {
      setUser(auth);
    } else {
      setUser(seller);
    }
  }, [props.isCurrentUser, seller, user]);

  const checkActiveTab = (
    tab: '/profile-customers-page' | '/profile-sell-listings-page' | '/profile-schedules-page',
  ): boolean => {
    return tab === router.pathname;
  };

  return (
    <div className="layout_profile">
      <div className="banner display_flex">
        <img
          src={user?.background ? user.background : banner}
          alt="banner"
          className="height_270 object_fit_cover width_100_percent height_xs_200"
        />
      </div>
      <div className="container_profile display_flex align_items_fs padding_bottom_100 display_xs_block">
        <div
          className="info padding_y_36 padding_x_24 margin_top_-144 width_386 border_radius_25 margin_right_20 flex_none display_xs_none"
          style={{
            background: 'rgba(255, 255, 255, 0.3)',
            backdropFilter: 'blur(94px)',
            border: '1px solid #cccccc',
          }}
        >
          <div className="avatar display_flex align_items_center flex_direction_col">
            <img
              src={user.avatar ? (user.avatar as string) : avatar}
              alt="user_avatar"
              className="border_radius_50_percent width_144 height_144 object_fit_cover"
            />
            <h2 className="margin_y_0 margin_top_16 font_weight_600 font_size_24 line_height_36">{user.name}</h2>
            <div className="margin_top_2">
              <span className="font_size_16 line_height_28 opacity_0_5">Tư vấn viên tại Reviewnha</span>
            </div>
          </div>
          <div className="actions margin_top_16">
            {props.isCurrentUser ? (
              <Link href={{ pathname: '/profile-account-page' }} as="/account">
                <a>
                  <button
                    className="display_flex width_100_percent padding_x_24 height_40 bg_black border_radius_25 color_white font_size_14 line_height_17 font_weight_600
              border_width_1 border_color_black border_style_solid justify_content_center align_items_center"
                  >
                    Sửa thông tin
                  </button>
                </a>
              </Link>
            ) : (
              <button
                className="display_flex width_100_percent padding_x_24 height_40 bg_black border_radius_25 color_white font_size_14 line_height_17 font_weight_600
              border_width_1 border_color_black border_style_solid justify_content_center align_items_center"
              >
                Liên hệ
              </button>
            )}
          </div>
          <div className="account_info margin_top_16">
            <div className="display_flex justify_content_sb align_items_center">
              <span className="font_size_12 line_height_18">Loại TK:</span>
              <div className="display_flex padding_x_24 height_24 align_items_center color_white bg_orange border_radius_25 font_size_14 line_height_17 font_weight_600">
                Chuyên gia tư vấn
              </div>
            </div>
            <div className="display_flex justify_content_sb align_items_center margin_top_16">
              <span className="font_size_12 line_height_18">Bất động sản:</span>
              <div className="font_size_16 line_height_28 font_weight_400">{user.sellListingsCount} tin đăng</div>
            </div>
            {user.description ? (
              <div className="margin_top_16">
                <span className="font_size_12 line_height_18">Giới thiệu:</span>
                <div className="font_size_14 line_height_21 font_weight_400 margin_top_16">
                  <span className="text_over_flow_6">{user.description}</span>
                </div>
                {user.description?.length > 500 ? (
                  <div className="margin_top_24 display_flex justify_content_center ">
                    <button
                      className="display_flex width_131 height_32 border_radius_25 font_size_14 line_height_17 font_weight_600
              border_width_1 border_color_black border_style_solid justify_content_center align_items_center bg_white margin_top_8"
                    >
                      Xem thêm
                    </button>
                  </div>
                ) : null}
              </div>
            ) : null}
          </div>
        </div>
        <div className="display_none display_xs_flex padding_x_xs_22 justify_content_xs_sb">
          <div className="avatar">
            <img
              src={user.avatar ? (user.avatar as string) : avatar}
              alt="user_avatar"
              className="border_radius_50_percent width_108 height_108 object_fit_cover display_flex margin_top_-54"
            />
            <h2 className="margin_y_0 margin_top_16 font_weight_600 font_size_16">{user.name}</h2>
            <div className="margin_top_2">
              <span className="font_size_14 opacity_0_5">Tư vấn viên tại Reviewnha</span>
            </div>
          </div>
          {props.isCurrentUser ? (
            <Link href={{ pathname: '/profile-account-page' }} as="/account">
              <a className="display_block">
                <div className="padding_5 margin_top_14">
                  <svg width={18} height={18} viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path
                      d="M16.9898 15.9533C17.5469 15.9533 18 16.4123 18 16.9766C18 17.5421 17.5469 18 16.9898 18H11.2797C10.7226 18 10.2695 17.5421 10.2695 16.9766C10.2695 16.4123 10.7226 15.9533 11.2797 15.9533H16.9898ZM13.0299 0.699064L14.5049 1.87078C15.1097 2.34377 15.513 2.96726 15.6509 3.62299C15.8101 4.3443 15.6403 5.0527 15.1628 5.66544L6.3764 17.0279C5.97316 17.5439 5.37891 17.8341 4.74222 17.8449L1.24039 17.8879C1.04939 17.8879 0.890213 17.7589 0.847767 17.5761L0.0518984 14.1255C-0.086052 13.4912 0.0518984 12.8355 0.455138 12.3303L6.68413 4.26797C6.79025 4.13898 6.98126 4.11855 7.1086 4.21422L9.72966 6.29967C9.89944 6.43942 10.1329 6.51467 10.377 6.48242C10.8969 6.41792 11.2471 5.94493 11.1941 5.43969C11.1622 5.1817 11.0349 4.96671 10.8651 4.80546C10.812 4.76246 8.31832 2.76301 8.31832 2.76301C8.15914 2.63401 8.12731 2.39752 8.25465 2.23735L9.24152 0.957057C10.1541 -0.214663 11.7459 -0.322161 13.0299 0.699064Z"
                      fill="#3B4144"
                    />
                  </svg>
                </div>
              </a>
            </Link>
          ) : null}
        </div>
        <div className="content_profile width_891 flex_1 padding_top_36 width_xs_100_percent padding_x_xs_22">
          {!props.isHideHeader ? (
            <div className="all_tab display_flex overflow_auto width_xs_100_percent scroll_bar_width_xs_0">
              <Link href={{ pathname: '/profile-sell-listings-page' }} as="/profile/sell-listings">
                <a
                  className={classnames(
                    { 'color_white bg_black': checkActiveTab('/profile-sell-listings-page') },
                    'height_40 padding_x_28 display_flex align_items_center border_radius_8 cursor_pointer margin_right_16 flex_xs_none',
                  )}
                >
                  <span className="font_weight_600 font_size_14 line_height_17">Bất động sản</span>
                </a>
              </Link>
              <Link href={{ pathname: '/profile-schedules-page' }} as="/profile/appointments">
                <a
                  className={classnames(
                    { 'color_white bg_black': checkActiveTab('/profile-schedules-page') },
                    'height_40 padding_x_28 display_flex align_items_center border_radius_8 cursor_pointer margin_right_16 flex_xs_none',
                  )}
                >
                  <span className="font_weight_600 font_size_14 line_height_17">Lịch hẹn</span>
                </a>
              </Link>
              <Link href={{ pathname: '/profile-customers-page' }} as="/profile/customers">
                <a
                  className={classnames(
                    { 'color_white bg_black': checkActiveTab('/profile-customers-page') },
                    'height_40 padding_x_28 display_flex align_items_center border_radius_8 cursor_pointer margin_right_16 flex_xs_none',
                  )}
                >
                  <span className="font_weight_600 font_size_14 line_height_17">Khách hàng</span>
                </a>
              </Link>
            </div>
          ) : null}
          <div className="tab_content">{props.children}</div>
        </div>
      </div>
    </div>
  );
};

export default LayoutProfile;
