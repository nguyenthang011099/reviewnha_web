import React, { useEffect } from 'react';
import Head from 'next/head';
import Header from '../components/header/Header';
import { useRouter } from 'next/router';
import Notification from '../components/modal-notification/Notification';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../reducers';
import {
  getUserInformation,
  resetStatusAuth,
  setCurrentUrl,
  setLoginStatusWhenCallbackUrlOauth,
} from '../reducers/auth/actions';
import { Status } from '../types/type.status';
import { openNotification } from '../reducers/notification/actions';
import HeaderMobile from '../components/header/HeaderMobile';
import { NextSeo } from 'next-seo';
import seo from '../constants/seo';
import { Blog } from '../types/type.blog';
import { initGA, logPageView } from '../utils/analytisc';
import Footer from '../components/footer/Footer';
import classNames from 'classnames';

interface Props {
  children: React.ReactNode;
}

const Layout: React.FC<Props> = (props) => {
  const router = useRouter();
  const dispatch = useDispatch();
  const authenticated: boolean = useSelector((state: RootState) => state.auth.authenticated);
  const token: string = useSelector((state: RootState) => state.auth.token);
  const typeLogin: string = useSelector((state: RootState) => state.auth.typeLogin);
  const loginStatus: Status = useSelector((state: RootState) => state.auth.loginStatus as Status);
  const loginMessageError: string = useSelector((state: RootState) => state.auth.loginMessageError);
  const blogDetail: Blog = useSelector((state: RootState) => state.blog.blogDetail);

  useEffect(() => {
    if (!(window as any).GA_INITIALIZED) {
      initGA();
      (window as any).GA_INITIALIZED = true;
    }
    logPageView();
  }, [router.asPath]);

  useEffect(() => {
    if (!checkHiddenHeaderFooter() && token && !authenticated) {
      dispatch(getUserInformation());
    }
  }, []);

  useEffect(() => {
    if (!authenticated && !checkHiddenHeaderFooter() && !router.query.token) {
      dispatch(
        setCurrentUrl({
          url: window.location.origin + router.asPath,
          pathname: router.pathname,
          query: router.query,
          asPath: router.asPath,
        }),
      );
    }
    if (router.query.token) {
      if (router.query.status === 'true') {
        dispatch(setLoginStatusWhenCallbackUrlOauth('success', router.query.token as string));
        window.location.href = window.location.origin + window.location.pathname;
      } else {
        dispatch(setLoginStatusWhenCallbackUrlOauth('fail'));
      }
    }
  }, [router.asPath, authenticated]);

  useEffect(() => {
    // chỉ khi đăng nhập với oauth mới thông báo ở layout
    if (typeLogin === 'OAuth') {
      if (loginStatus === 'error') {
        dispatch(
          openNotification({
            type: 'error',
            title: loginMessageError,
            isOpen: true,
            reset: () => {
              dispatch(resetStatusAuth('login'));
            },
          }),
        );
      }
    } else {
      if (loginStatus === 'success') {
        dispatch(getUserInformation());
      }
    }
  }, [loginStatus]);

  const checkHiddenHeaderFooter = () => {
    const hiddenAtRouters = ['/login-page', '/register-page'];
    return hiddenAtRouters.indexOf(router.route) !== -1;
  };

  const renderSeoTag = () => {
    if (router.pathname === '/blog-detail-page') {
      return {
        title: blogDetail.title + ' | ' + seo.title,
        description: blogDetail.description,
        logo: blogDetail.thumbnail,
        mainUrl: seo.mainUrl + router.asPath,
      };
    }
    return {
      title: seo.title,
      description: seo.description,
      logo: seo.logo,
      mainUrl: seo.mainUrl,
    };
  };

  const seoInfo = renderSeoTag();

  return (
    <div className="review-nha">
      <NextSeo
        title={seoInfo.title}
        description={seoInfo.description}
        canonical={seo.mainUrl + router.asPath}
        openGraph={{
          url: seoInfo.mainUrl,
          title: seoInfo.title,
          description: seoInfo.description,
          images: [
            {
              url: seoInfo.logo,
              width: 800,
              height: 600,
              alt: seoInfo.title,
            },
            {
              url: seoInfo.logo,
              width: 900,
              height: 800,
              alt: seoInfo.title,
            },
            { url: seoInfo.logo },
            { url: seoInfo.logo },
          ],
          site_name: seo.title,
        }}
        twitter={{
          handle: seoInfo.title,
          site: seo.title,
          cardType: seoInfo.logo,
        }}
      />
      <Head>
        <meta name="keywords" content=""></meta>
        <script
          src={`https://maps.googleapis.com/maps/api/js?v=3.exp&key=${process.env.NEXT_PUBLIC_GOOGLE_API_KEY}`}
        ></script>
        <link rel="preload" href="/fonts/SVN-Gotham_Regular.otf" as="font" crossOrigin="" />
        <link rel="preload" href="/fonts/SVN-Gotham_Book.otf" as="font" crossOrigin="" />
      </Head>
      <Notification></Notification>
      {checkHiddenHeaderFooter() ? null : (
        <>
          <div className="display_md_none">
            <Header></Header>
          </div>
          <div className="display_md_block display_none">
            <HeaderMobile></HeaderMobile>
          </div>
        </>
      )}
      <div className={classNames({ padding_top_75: !checkHiddenHeaderFooter() })}>{props.children}</div>
      {checkHiddenHeaderFooter() ? null : (
        <>
          <Footer></Footer>
        </>
      )}
    </div>
  );
};

export default Layout;
