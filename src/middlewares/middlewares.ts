import api from '../api';
import { Action } from '../types/type.redux';
import camelCase from 'camelcase-keys';
import { OPEN_NOTIFICATION } from '../reducers/notification/constants';

interface ParamsFetch {
  method:
    | 'POST'
    | 'PUT'
    | 'GET'
    | 'get'
    | 'delete'
    | 'DELETE'
    | 'head'
    | 'HEAD'
    | 'options'
    | 'OPTIONS'
    | 'post'
    | 'put'
    | 'patch'
    | 'PATCH'
    | 'purge'
    | 'PURGE'
    | 'link'
    | 'LINK'
    | 'unlink'
    | 'UNLINK'
    | undefined;
  data?: any;
  url: string;
  params?: any;
  headers?: any;
}

const middleware = (cookies: any) => {
  // eslint-disable-next-line no-unused-vars
  return (store: { dispatch: (arg0: { type: string; messages?: any; old_action: Action; data?: any }) => void }) => {
    // eslint-disable-next-line no-unused-vars
    return (next: (arg0: Action) => void) => {
      return async (action: Action) => {
        if (typeof action.payload === 'object' && action.payload.query) {
          // allow call normal action
          next(action);
          const paramsFetch: ParamsFetch = {
            method: action.payload.method,
            url: action.payload.query,
          };
          if (cookies?.auth) {
            paramsFetch.headers = {
              authorization: `Bearer ${cookies.auth}`,
            };
          }
          if (action.payload.body) {
            if (action.payload.method === 'POST' || action.payload.method === 'PUT') {
              paramsFetch.data = action.payload.body;
            } else {
              paramsFetch.params = action.payload.body;
            }
          }
          await api(paramsFetch)
            .then(async (data: any) => {
              if (typeof data === 'string') {
                try {
                  const response: any = camelCase(JSON.parse(data), { deep: true });
                  if (response.success) {
                    store.dispatch({
                      type: [action.type, 'SUCCESS'].join('_'),
                      messages: response.message,
                      data: response.data,
                      old_action: action,
                    });
                    openAlert('success', action?.payload?.method, store, action, response?.data?.message);
                  } else {
                    store.dispatch({
                      type: [action.type, 'FAIL'].join('_'),
                      messages: response.message,
                      old_action: action,
                      data: response,
                    });
                    openAlert('error', action?.payload?.method, store, action, response?.data?.message);
                  }
                } catch (e) {
                  store.dispatch({
                    type: [action.type, 'FAIL'].join('_'),
                    old_action: action,
                    data: data,
                  });
                  openAlert('error', action?.payload?.method, store, action, e?.message);
                }
              } else {
                if (data.success) {
                  store.dispatch({
                    type: [action.type, 'SUCCESS'].join('_'),
                    messages: data.message,
                    data: data.data,
                    old_action: action,
                  });
                  openAlert('success', action?.payload?.method, store, action, data.message);
                } else {
                  store.dispatch({
                    type: [action.type, 'FAIL'].join('_'),
                    messages: data.message,
                    old_action: action,
                    data: data,
                  });
                  openAlert('error', action?.payload?.method, store, action, data?.message);
                }
              }
            })
            .catch((error) => {
              console.error(error);
              store.dispatch({
                type: action.type + '_FAIL',
                old_action: action,
                data: error,
              });
              openAlert('error', action?.payload?.method, store, action, error?.message);
            });
        } else {
          next(action);
        }
      };
    };
  };
};

const openAlert = (type: 'success' | 'error', method: string, store: any, action: any, message?: string) => {
  if (['put', 'post', 'delete'].includes(method?.toLowerCase())) {
    store.dispatch({
      type: OPEN_NOTIFICATION,
      data: {
        title: message ? message : generateMessage(type),
        type: type,
        isOpen: true,
      },
      old_action: action,
    });
  }
};

const generateMessage = (type: 'success' | 'error') => {
  return 'Yêu cầu ' + type === 'success' ? 'thành công' : 'thất bại';
};

export default middleware;
