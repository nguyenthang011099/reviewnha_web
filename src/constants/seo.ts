export default {
  title: 'Reviewnha.vn | Đánh giá khu vực - Đánh giá dự án Bất động sản',
  description:
    'Reviewnha.vn giúp bạn khám phá khu vực mà bạn muốn sống, đánh giá dự án bất động sản một cách khách quan và sinh động. Tổng hợp danh sách các bất động bán và cho thuê...',
  mainUrl: 'https://reviewnha.vn',
  logo: 'https://reviewnha-pro.s3-ap-southeast-1.amazonaws.com/logo.jpg',
};
