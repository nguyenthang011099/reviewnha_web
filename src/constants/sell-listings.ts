export const NUMBER_ALL_SELL_LISTINGS_PER_REQ = 16;
export const NUMBER_HIGHLIGHT_SELL_LISTINGS_PER_REQ = 3;
export const NUMBER_SELL_LISTINGS_BY_USER_PER_REQ = 16;

export const LIST_ROOM_NUMBER = [
  {
    value: '1',
    title: '1',
  },
  {
    value: '2',
    title: '2',
  },
  {
    value: '3',
    title: '3',
  },
  {
    value: '4',
    title: '4',
  },
  {
    value: '+5',
    title: '+5',
  },
];
export const LIST_PRICE_RANGE = [
  {
    value: '0-2',
    title: 'Dưới 2 tỷ',
  },
  {
    value: '2-3',
    title: 'Từ 2 đến 3 tỷ',
  },
  {
    value: '3-5',
    title: 'Từ 3 đến 5 tỷ',
  },
  {
    value: '5-7',
    title: 'Từ 5 đến 7 tỷ',
  },
  {
    value: '7-10',
    title: 'Từ 7 đến 10 tỷ',
  },
  {
    value: '10-1000',
    title: 'Trên 10 tỷ',
  },
];
