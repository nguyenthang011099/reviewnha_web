import { NextPage } from 'next';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import scheduleApi from '../../src/api/schedule';
import ProfileSchedules from '../../src/containers/profile-schedules/ProfileSchedules';
import LayoutProfile from '../../src/layouts/LayoutProfile';
import { RootState } from '../../src/reducers';
import { getUserInformation } from '../../src/reducers/auth/actions';
import { getSellListingPartOne } from '../../src/reducers/sell-listing/actions';
import { PageContext } from '../../src/types/type.appcontext';
import { Schedule } from '../../src/types/type.schedule';
import { initializeStore } from '../../store';

const ProfileSchedulesPage: NextPage = () => {
  const router = useRouter();
  const auth = useSelector((state: RootState) => state.auth);
  const [scheduleList, setScheduleList] = useState<Schedule[]>([]);

  useEffect(() => {
    if (auth.authenticated) {
      getScheduleList();
    } else {
      router.push({ pathname: '/login-page' }, '/login');
    }
  }, [auth.user?.id]);

  const getScheduleList = () => {
    scheduleApi.getList().then((res) => {
      if (res.success && res.data?.length) {
        setScheduleList(res.data);
      }
    });
  };

  return (
    <div>
      <LayoutProfile isCurrentUser={true}>
        <ProfileSchedules list={scheduleList}></ProfileSchedules>
      </LayoutProfile>
    </div>
  );
};

ProfileSchedulesPage.getInitialProps = async ({ req }: PageContext) => {
  const isServer = !!req;
  if (isServer) {
    const store = initializeStore({}, req.cookies);
    await store.dispatch(getUserInformation());
    await store.dispatch(getSellListingPartOne());
    return {
      isServer,
      initialReduxState: store.getState(),
    };
  }
  return {
    isServer,
  };
};

export default ProfileSchedulesPage;
