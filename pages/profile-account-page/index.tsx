import { NextPage } from 'next';
import { useRouter } from 'next/router';
import { useEffect } from 'react';
import { useSelector } from 'react-redux';
import ProfileAccount from '../../src/containers/profile-account/ProfileAccount';
import { RootState } from '../../src/reducers';
import { getUserInformation } from '../../src/reducers/auth/actions';
import { getSellListingPartOne } from '../../src/reducers/sell-listing/actions';
import { PageContext } from '../../src/types/type.appcontext';
import { initializeStore } from '../../store';

const ProfileAccountPage: NextPage = () => {
  const router = useRouter();
  const auth = useSelector((state: RootState) => state.auth);

  useEffect(() => {
    if (!auth.authenticated) {
      router.push({ pathname: '/login-page' }, '/login');
    }
  }, [auth.user?.id]);

  return (
    <div>
      <ProfileAccount></ProfileAccount>
    </div>
  );
};

ProfileAccountPage.getInitialProps = async ({ req }: PageContext) => {
  const isServer = !!req;
  if (isServer) {
    const store = initializeStore({}, req.cookies);
    await store.dispatch(getUserInformation());
    await store.dispatch(getSellListingPartOne());
    return {
      isServer,
      initialReduxState: store.getState(),
    };
  }
  return {
    isServer,
  };
};

export default ProfileAccountPage;
