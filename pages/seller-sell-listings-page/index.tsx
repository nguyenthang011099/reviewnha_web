import { NextPage } from 'next';
import { useRouter } from 'next/router';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  NUMBER_ALL_SELL_LISTINGS_PER_REQ,
  NUMBER_HIGHLIGHT_SELL_LISTINGS_PER_REQ,
} from '../../src/constants/sell-listings';
import SellerSellListings from '../../src/containers/seller-sell-listings/SellerSellListings';
import LayoutProfile from '../../src/layouts/LayoutProfile';
import { RootState } from '../../src/reducers';
import { getUserInformation } from '../../src/reducers/auth/actions';
import { getProjectList, getSellListingPartOne, getSellListingsList } from '../../src/reducers/sell-listing/actions';
import { getSellerInfo } from '../../src/reducers/user/action';
import { PageContext } from '../../src/types/type.appcontext';
import { initializeStore } from '../../store';

interface Props {
  isServer: boolean;
}

const SellerSellListingsPage: NextPage<Props> = (props) => {
  const router = useRouter();
  const dispatch = useDispatch();
  const projectList = useSelector((state: RootState) => state.sellListing.projectList);

  useEffect(() => {
    if (!props.isServer) {
      const sellerId = router.query.sellerId;
      if (sellerId) {
        dispatch(getSellerInfo(Number(sellerId)));
        dispatch(
          getSellListingsList(
            { type: 'highlight', limit: NUMBER_HIGHLIGHT_SELL_LISTINGS_PER_REQ, userId: Number(sellerId) },
            false,
            true,
          ),
        );
        dispatch(
          getSellListingsList(
            { type: 'all', limit: NUMBER_ALL_SELL_LISTINGS_PER_REQ, userId: Number(sellerId) },
            false,
            true,
          ),
        );
      }
      if (!projectList.length) {
        dispatch(getProjectList());
      }
    }
  }, [router.query.sellerId]);

  return (
    <div>
      <LayoutProfile isHideHeader={true}>
        <SellerSellListings></SellerSellListings>
      </LayoutProfile>
    </div>
  );
};

SellerSellListingsPage.getInitialProps = async ({ req }: PageContext) => {
  const isServer = !!req;
  if (isServer) {
    const store = initializeStore({}, req.cookies);
    await store.dispatch(getUserInformation());
    await store.dispatch(getSellListingPartOne());
    await store.dispatch(getProjectList());
    const sellerId = req.params.sellerId;
    await store.dispatch(getSellerInfo(Number(sellerId)));
    await store.dispatch(
      getSellListingsList(
        { type: 'highlight', limit: NUMBER_HIGHLIGHT_SELL_LISTINGS_PER_REQ, userId: Number(sellerId) },
        false,
        true,
      ),
    );
    await store.dispatch(
      getSellListingsList(
        { type: 'all', limit: NUMBER_ALL_SELL_LISTINGS_PER_REQ, userId: Number(sellerId) },
        false,
        true,
      ),
    );
    return {
      isServer,
      initialReduxState: store.getState(),
    };
  }
  return {
    isServer,
  };
};

export default SellerSellListingsPage;
