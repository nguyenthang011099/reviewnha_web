import { NextPage } from 'next';
import { useRouter } from 'next/router';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { NUMBER_SELL_LISTINGS_BY_USER_PER_REQ } from '../../src/constants/sell-listings';
import SellDetail from '../../src/containers/sell-detail/SellDetail';
import { RootState } from '../../src/reducers';
import { getUserInformation } from '../../src/reducers/auth/actions';
import {
  getFeedback,
  getProjectList,
  getRelatedSellListings,
  getSellListingDetail,
  getSellListingPartOne,
  getSellListingsList,
} from '../../src/reducers/sell-listing/actions';
import { PageContext } from '../../src/types/type.appcontext';
import { initializeStore } from '../../store';

interface Props {
  isServer?: boolean;
}

const SellListingDetailPage: NextPage<Props> = (props) => {
  const dispatch = useDispatch();
  const router = useRouter();
  const sellListingDetail = useSelector((state: RootState) => state.sellListing.sellListingDetail);
  const projectList = useSelector((state: RootState) => state.sellListing.projectList);

  useEffect(() => {
    if (!props.isServer) {
      const postId = router.query?.postId;
      if (postId) {
        dispatch(getSellListingDetail(Number(postId)));
      }
      if (!projectList.length) {
        dispatch(getProjectList());
      }
    }
  }, [router?.query?.postId]);

  useEffect(() => {
    const userId = sellListingDetail?.data?.seller?.id;
    const listByUser = sellListingDetail?.listByUser;
    if (userId && (!listByUser.length || (listByUser.length && listByUser[0]?.seller?.id !== userId))) {
      dispatch(getSellListingsList({ limit: NUMBER_SELL_LISTINGS_BY_USER_PER_REQ, userId }, true));
    }
    const projectId = sellListingDetail.data?.projectId;
    if (projectId) {
      dispatch(getFeedback(projectId));
      dispatch(getRelatedSellListings(projectId));
    } else {
      dispatch(getRelatedSellListings(null, sellListingDetail.data?.districtCode));
    }
  }, [sellListingDetail.data]);

  return (
    <div className="post_detail_page min_height_80_vh">
      <SellDetail></SellDetail>
    </div>
  );
};

SellListingDetailPage.getInitialProps = async ({ req }: PageContext) => {
  const isServer = !!req;
  if (isServer) {
    const store = initializeStore({}, req.cookies);
    await store.dispatch(getUserInformation());
    await store.dispatch(getSellListingPartOne());
    const postId: string = req.params.postId;
    await store.dispatch(getSellListingDetail(Number(postId)));
    await store.dispatch(getProjectList());
    return {
      isServer,
      initialReduxState: store.getState(),
    };
  }
  return {
    isServer,
  };
};

export default SellListingDetailPage;
