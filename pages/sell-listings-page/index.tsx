import { NextPage } from 'next';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import SellListings from '../../src/containers/sell-listings/SellListings';
import { RootState } from '../../src/reducers';
import { getUserInformation } from '../../src/reducers/auth/actions';
import { getProjectList, getSellListingPartOne, getSellListingsList } from '../../src/reducers/sell-listing/actions';
import { PageContext } from '../../src/types/type.appcontext';
import { initializeStore } from '../../store';
import {
  NUMBER_ALL_SELL_LISTINGS_PER_REQ,
  NUMBER_HIGHLIGHT_SELL_LISTINGS_PER_REQ,
} from '../../src/constants/sell-listings';
import { useRouter } from 'next/router';
import { SellListingFilter } from '../../src/types/type.sell-listing';

interface Props {
  isServer?: boolean;
}

const SellListingsPage: NextPage<Props> = (props) => {
  // const isFiltered = useSelector((state: RootState) => state.sellListing.isFiltered);
  // const highlightSellListings = useSelector((state: RootState) => state.sellListing.highlightSellListings);
  // const allSellListings = useSelector((state: RootState) => state.sellListing.allSellListings);
  const dispatch = useDispatch();
  const router = useRouter();
  const projectList = useSelector((state: RootState) => state.sellListing.projectList);

  useEffect(() => {
    if (!props.isServer) {
      const dataQuery: SellListingFilter = {
        typeTransaction: router.query.transactionType as string,
        provinceCode: router.query.provinceCode as string,
        districtCode: router.query.districtCode as string,
        wardCode: router.query.wardCode as string,
        projectId: Number(router.query.projectId) || undefined,
      };
      dispatch(
        getSellListingsList({
          type: 'highlight',
          limit: NUMBER_HIGHLIGHT_SELL_LISTINGS_PER_REQ,
          ...dataQuery,
        }),
      );
      dispatch(
        getSellListingsList({
          type: 'all',
          typeTransaction: router.query.transactionType as string,
          limit: NUMBER_ALL_SELL_LISTINGS_PER_REQ,
          ...dataQuery,
        }),
      );
      if (!projectList.length) {
        dispatch(getProjectList());
      }
    }
  }, [router.query.transactionType]);

  return (
    <div className="home_page min_height_80_vh">
      <SellListings></SellListings>
    </div>
  );
};

SellListingsPage.getInitialProps = async ({ req }: PageContext) => {
  const isServer = !!req;
  if (isServer) {
    const store = initializeStore({}, req.cookies);
    await store.dispatch(getUserInformation());
    await store.dispatch(getSellListingPartOne());
    await store.dispatch(getProjectList());
    const dataQuery: SellListingFilter = {
      typeTransaction: req.query.transactionType as string,
      provinceCode: req.query.provinceCode as string,
      districtCode: req.query.districtCode as string,
      wardCode: req.query.wardCode as string,
      projectId: Number(req.query.projectId) || undefined,
    };
    await store.dispatch(
      getSellListingsList({
        type: 'highlight',
        limit: NUMBER_HIGHLIGHT_SELL_LISTINGS_PER_REQ,
        ...dataQuery,
      }),
    );
    await store.dispatch(
      getSellListingsList({
        type: 'all',
        limit: NUMBER_ALL_SELL_LISTINGS_PER_REQ,
        ...dataQuery,
      }),
    );
    return {
      isServer,
      initialReduxState: store.getState(),
    };
  }
  return {
    isServer,
  };
};

export default SellListingsPage;
