import { NextPage } from 'next';
import Register from '../../src/containers/register/Register';
import { getUserInformation } from '../../src/reducers/auth/actions';
import { PageContext } from '../../src/types/type.appcontext';
import { initializeStore } from '../../store';

interface Props {
  isServer?: boolean;
}

const RegisterPage: NextPage<Props> = () => {
  return (
    <div className="home_page height_100_vh">
      <Register></Register>
    </div>
  );
};

RegisterPage.getInitialProps = async ({ req }: PageContext) => {
  const isServer = !!req;
  if (isServer) {
    const store = initializeStore({}, req.cookies);
    await store.dispatch(getUserInformation());
    return {
      isServer,
      initialReduxState: store.getState(),
    };
  }
  return {
    isServer,
  };
};

export default RegisterPage;
