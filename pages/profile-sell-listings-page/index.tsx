import { NextPage } from 'next';
import { useRouter } from 'next/router';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import ProfileSellListings from '../../src/containers/profile-sell-listings/ProfileSellListings';
import LayoutProfile from '../../src/layouts/LayoutProfile';
import { RootState } from '../../src/reducers';
import { getUserInformation } from '../../src/reducers/auth/actions';
import {
  getProjectList,
  getSellListingPartOne,
  getSellListingsByCurrentUser,
} from '../../src/reducers/sell-listing/actions';
import { PageContext } from '../../src/types/type.appcontext';
import { initializeStore } from '../../store';

interface Props {
  isServer: boolean;
}

const ProfileSellListingsPage: NextPage<Props> = (props) => {
  const router = useRouter();
  const dispatch = useDispatch();
  const auth = useSelector((state: RootState) => state.auth);
  const projectList = useSelector((state: RootState) => state.sellListing.projectList);

  useEffect(() => {
    if (!props.isServer) {
      if (auth.authenticated) {
        dispatch(getSellListingsByCurrentUser());
      } else {
        router.push({ pathname: '/login-page' }, '/login');
      }
      if (!projectList.length) {
        dispatch(getProjectList());
      }
    }
  }, [auth.user?.id]);

  return (
    <div>
      <LayoutProfile isCurrentUser={true}>
        <ProfileSellListings></ProfileSellListings>
      </LayoutProfile>
    </div>
  );
};

ProfileSellListingsPage.getInitialProps = async ({ req }: PageContext) => {
  const isServer = !!req;
  if (isServer) {
    const store = initializeStore({}, req.cookies);
    await store.dispatch(getUserInformation());
    await store.dispatch(getSellListingPartOne());
    await store.dispatch(getSellListingsByCurrentUser());
    await store.dispatch(getProjectList());
    return {
      isServer,
      initialReduxState: store.getState(),
    };
  }
  return {
    isServer,
  };
};

export default ProfileSellListingsPage;
