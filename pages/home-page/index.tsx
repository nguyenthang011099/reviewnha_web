import { NextPage } from 'next';
import { initializeStore } from '../../store';
import { PageContext } from '../../src/types/type.appcontext';
import { getUserInformation } from '../../src/reducers/auth/actions';
import { useEffect } from 'react';
import { useRouter } from 'next/router';
import { getSellListingPartOne } from '../../src/reducers/sell-listing/actions';
import { useSelector } from 'react-redux';
import { RootState } from '../../src/reducers';

interface Props {
  isServer?: boolean;
}

const HomePage: NextPage<Props> = () => {
  const router = useRouter();
  const dataTypeTransaction = useSelector((state: RootState) => state.sellListing.stepOneData?.transactionTypes || []);

  useEffect(() => {
    if (dataTypeTransaction?.length) {
      router.push(
        { pathname: '/sell-listings-page', query: { transactionType: dataTypeTransaction[0].value } },
        `/sell-listings/?transactionType=${dataTypeTransaction[0].value}`,
      );
    } else {
      router.push({ pathname: '/sell-listings-page' }, `/sell-listings`);
    }
  }, []);

  return <div className="home_page min_height_80_vh"></div>;
};

HomePage.getInitialProps = async ({ req }: PageContext) => {
  const isServer = !!req;
  if (isServer) {
    const store = initializeStore({}, req.cookies);
    await store.dispatch(getUserInformation());
    await store.dispatch(getSellListingPartOne());
    return {
      isServer,
      initialReduxState: store.getState(),
    };
  }
  return {
    isServer,
  };
};

export default HomePage;
