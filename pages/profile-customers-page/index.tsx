import { NextPage } from 'next';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import customerApi from '../../src/api/customer';
import ProfileCustomers from '../../src/containers/profile-customers/ProfileCustomers';
import LayoutProfile from '../../src/layouts/LayoutProfile';
import { RootState } from '../../src/reducers';
import { getUserInformation } from '../../src/reducers/auth/actions';
import { getSellListingPartOne } from '../../src/reducers/sell-listing/actions';
import { PageContext } from '../../src/types/type.appcontext';
import { Customer } from '../../src/types/type.customer';
import { initializeStore } from '../../store';

const ProfileCustomersPage: NextPage = () => {
  const router = useRouter();
  const auth = useSelector((state: RootState) => state.auth);
  const [customerList, setCustomerList] = useState<Customer[]>([]);

  useEffect(() => {
    if (auth.authenticated) {
      getCustomerList();
    } else {
      router.push({ pathname: '/login-page' }, '/login');
    }
  }, [auth.user?.id]);

  const getCustomerList = () => {
    customerApi.getList().then((res: any) => {
      if (res.success && res.data?.length) {
        setCustomerList(res.data);
      }
    });
  };

  return (
    <div>
      <LayoutProfile isCurrentUser={true}>
        <ProfileCustomers list={customerList}></ProfileCustomers>
      </LayoutProfile>
    </div>
  );
};

ProfileCustomersPage.getInitialProps = async ({ req }: PageContext) => {
  const isServer = !!req;
  if (isServer) {
    const store = initializeStore({}, req.cookies);
    await store.dispatch(getUserInformation());
    await store.dispatch(getSellListingPartOne());
    return {
      isServer,
      initialReduxState: store.getState(),
    };
  }
  return {
    isServer,
  };
};

export default ProfileCustomersPage;
