import { NextPage } from 'next';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Login from '../../src/containers/login/Login';
import { RootState } from '../../src/reducers';
import { getRedirectLinkOauth, getUserInformation } from '../../src/reducers/auth/actions';
import { PageContext } from '../../src/types/type.appcontext';
import { CallbackURLType } from '../../src/types/type.auth';
import { initializeStore } from '../../store';

interface Props {
  isServer?: boolean;
}

const LoginPage: NextPage<Props> = (props) => {
  const dispatch = useDispatch();
  const callbackUrl: CallbackURLType = useSelector((state: RootState) => state.auth.callbackUrl);

  useEffect(() => {
    if (!props.isServer) {
      dispatch(getRedirectLinkOauth('google', callbackUrl.url || window.location.origin, 'seller'));
      dispatch(getRedirectLinkOauth('facebook', callbackUrl.url || window.location.origin, 'seller'));
    }
  }, []);

  return (
    <div className="home_page height_100_vh">
      <Login></Login>
    </div>
  );
};

LoginPage.getInitialProps = async ({ req }: PageContext) => {
  const isServer = !!req;
  if (isServer) {
    const store = initializeStore({}, req.cookies);
    await store.dispatch(getUserInformation());
    let callbackUrl: string = req.protocol + '://' + req.headers.host;
    await store.dispatch(getRedirectLinkOauth('google', callbackUrl, 'seller'));
    await store.dispatch(getRedirectLinkOauth('facebook', callbackUrl, 'seller'));
    return {
      isServer,
      initialReduxState: store.getState(),
    };
  }
  return {
    isServer,
  };
};

export default LoginPage;
