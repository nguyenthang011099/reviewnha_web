import { NextPage } from 'next';
import { useRouter } from 'next/router';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import BlogDetail from '../../src/containers/blog-detail/BlogDetail';
import { RootState } from '../../src/reducers';
import { getUserInformation } from '../../src/reducers/auth/actions';
import {
  getBlogBySlug,
  getBlogsByCategory,
  getBlogsByType,
  resetLoadingBlogDetail,
} from '../../src/reducers/blog/actions';
import { getSellListingPartOne } from '../../src/reducers/sell-listing/actions';
import { PageContext } from '../../src/types/type.appcontext';
import { Blog, BlogsAllData } from '../../src/types/type.blog';
import { initializeStore } from '../../store';

interface Props {
  isServer?: boolean;
}

const BlogDetailPage: NextPage<Props> = (props) => {
  const dispatch = useDispatch();
  const router = useRouter();
  const blogDetail: Blog = useSelector((state: RootState) => state.blog.blogDetail);
  const cacheCategoryBlogs: string[] = useSelector((state: RootState) => state.blog.cacheCategoryBlogs);
  const blogsAll: BlogsAllData = useSelector((state: RootState) => state.blog.blogsAll);

  useEffect(() => {
    return () => {
      dispatch(resetLoadingBlogDetail());
    };
  }, []);

  useEffect(() => {
    if (!props.isServer) {
      const slug = router.query.slug as string;
      if (slug) {
        dispatch(getBlogBySlug(slug));
      }
      if (!blogsAll?.featureBlogs?.blogs?.length) {
        dispatch(getBlogsByType('featured', 0));
      }
    }
  }, [router.query]);

  useEffect(() => {
    if (blogDetail.id) {
      const categorySlug = blogDetail?.category?.slug;
      if (categorySlug && !cacheCategoryBlogs.includes(categorySlug)) {
        dispatch(getBlogsByCategory(categorySlug, 0));
      }
    }
  }, [blogDetail.category]);

  return (
    <div className="blog_detail_page">
      <BlogDetail></BlogDetail>
    </div>
  );
};

BlogDetailPage.getInitialProps = async ({ req }: PageContext) => {
  const isServer = !!req;
  if (isServer) {
    const store = initializeStore({}, req.cookies);
    await store.dispatch(getUserInformation());
    await store.dispatch(getSellListingPartOne());
    const slug = req.params.slug;
    await store.dispatch(getBlogBySlug(slug));
    const categorySlug = store.getState()?.blog?.blogDetail?.category?.slug;
    if (categorySlug) {
      await store.dispatch(getBlogsByCategory(categorySlug, 0));
    }
    await store.dispatch(getBlogsByType('featured', 0));
    return {
      isServer,
      initialReduxState: store.getState(),
    };
  }
  return {
    isServer,
  };
};

export default BlogDetailPage;
