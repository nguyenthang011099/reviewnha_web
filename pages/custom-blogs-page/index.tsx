import { NextPage } from 'next';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import CustomBlogs from '../../src/containers/blogs/CustomBlogs';
import { RootState } from '../../src/reducers';
import { getUserInformation } from '../../src/reducers/auth/actions';
import { getBlogsByTag, getBlogsByType } from '../../src/reducers/blog/actions';
import { getSellListingPartOne } from '../../src/reducers/sell-listing/actions';
import { PageContext } from '../../src/types/type.appcontext';
import { initializeStore } from '../../store';

interface Props {
  isServer?: boolean;
}

const CustomBlogPage: NextPage<Props> = (props) => {
  const dispatch = useDispatch();
  const featureBlogs = useSelector((state: RootState) => state.blog.blogsAll.featureBlogs);
  const router = useRouter();

  useEffect(() => {
    if (!props.isServer) {
      const type = getCustomType();
      if (type === 'feature') {
        if (!featureBlogs?.blogs?.length) {
          dispatch(getBlogsByType('featured', 0));
        }
      } else {
        dispatch(getBlogsByTag(router.query.tagSlug as string, 0));
      }
    }
  }, []);

  const getCustomType = () => {
    if (router.query?.tagSlug) {
      return 'tag';
    }
    return 'feature';
  };

  return (
    <div className="feature_blogs_page">
      <CustomBlogs type={getCustomType()}></CustomBlogs>
    </div>
  );
};

CustomBlogPage.getInitialProps = async ({ req }: PageContext) => {
  const isServer = !!req;
  if (isServer) {
    const store = initializeStore({}, req.cookies);
    await store.dispatch(getUserInformation());
    await store.dispatch(getSellListingPartOne());
    const tagSlug = req.params.tagSlug;
    if (tagSlug) {
      await store.dispatch(getBlogsByTag(tagSlug, 0));
    } else {
      await store.dispatch(getBlogsByType('featured', 0));
    }
    return {
      isServer,
      initialReduxState: store.getState(),
    };
  }
  return {
    isServer,
  };
};

export default CustomBlogPage;
