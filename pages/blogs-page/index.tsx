import { NextPage } from 'next';
import { useRouter } from 'next/router';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Blogs from '../../src/containers/blogs/Blogs';
import { RootState } from '../../src/reducers';
import { getUserInformation } from '../../src/reducers/auth/actions';
import {
  getBlogCategories,
  getBlogsByCategory,
  getBlogsByType,
  getVideos,
  setActiveCategory,
} from '../../src/reducers/blog/actions';
import { getSellListingPartOne } from '../../src/reducers/sell-listing/actions';
import { PageContext } from '../../src/types/type.appcontext';
import { BlogCategory, BlogsAllData, VideoList } from '../../src/types/type.blog';
import { initializeStore } from '../../store';

interface Props {
  isServer?: boolean;
}

const BlogsPage: NextPage<Props> = (props) => {
  const dispatch = useDispatch();
  const router = useRouter();
  const cacheCategoryBlogs: string[] = useSelector((state: RootState) => state.blog.cacheCategoryBlogs);
  const categories: BlogCategory[] = useSelector((state: RootState) => state.blog.categories);
  const blogsAll: BlogsAllData = useSelector((state: RootState) => state.blog.blogsAll);
  const videoList: VideoList = useSelector((state: RootState) => state.blog.videoList);

  useEffect(() => {
    if (!props.isServer) {
      const categorySlug = router.query.categorySlug as string;
      if (!categories.length) {
        dispatch(getBlogCategories());
      }
      if (categorySlug && categorySlug !== 'all' && categorySlug !== 'video') {
        dispatch(setActiveCategory(categorySlug));
        if (!cacheCategoryBlogs.includes(categorySlug)) {
          dispatch(getBlogsByCategory(categorySlug, 0));
        }
      } else if (categorySlug === 'video') {
        dispatch(setActiveCategory('video'));
        !videoList?.videos?.length && dispatch(getVideos(0));
      } else {
        dispatch(setActiveCategory('all'));
        !blogsAll?.highlightBlogs?.blogs?.length && dispatch(getBlogsByType('highlight', 0));
        !blogsAll?.normalBlogs?.blogs?.length && dispatch(getBlogsByType('normal', 0));
        !videoList?.videos?.length && dispatch(getVideos(0));
      }
    }
  }, [router.query]);

  return (
    <div className="posts_page">
      <Blogs></Blogs>
    </div>
  );
};

BlogsPage.getInitialProps = async ({ req }: PageContext) => {
  const isServer = !!req;
  if (isServer) {
    const store = initializeStore({}, req.cookies);
    const categorySlug = req.params.categorySlug;
    await store.dispatch(getUserInformation());
    await store.dispatch(getSellListingPartOne());
    await store.dispatch(getBlogCategories());
    if (categorySlug && categorySlug !== 'all' && categorySlug !== 'video') {
      await store.dispatch(setActiveCategory(categorySlug));
      await store.dispatch(getBlogsByCategory(categorySlug, 0));
    } else if (categorySlug === 'video') {
      await store.dispatch(setActiveCategory('video'));
      await store.dispatch(getVideos(0));
    } else {
      await store.dispatch(setActiveCategory('all'));
      await store.dispatch(getBlogsByType('highlight', 0));
      await store.dispatch(getBlogsByType('normal', 0));
      await store.dispatch(getVideos(0));
    }
    return {
      isServer,
      initialReduxState: store.getState(),
    };
  }
  return {
    isServer,
  };
};

export default BlogsPage;
