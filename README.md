## Getting Started By Npm

- First, install packages:

```bash
npm i
# or
yarn
```

- After that, run the development server by:

```bash
npm run dev
# or
yarn dev
```

## Getting Started By Docker

- First, build docker image:

```bash
docker-compose buid
```

- Secondary, install packages:

```bash
docker-compose run web yarn
```

- After that, run the development server by:

```bash
docker-compose up
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.
