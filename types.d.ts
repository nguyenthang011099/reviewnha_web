declare module '*.png' {
  const _: string;
  export default _;
}

declare module '*.jpg' {
  const _: string;
  export default _;
}

declare module '*.jpeg' {
  const _: string;
  export default _;
}

declare module '*.woff2' {
  const _: string;
  export default _;
}

declare module 'react-location-picker' {
  const _: any;
  export default _;
}
